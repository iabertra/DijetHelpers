#===============================================================
#
# Job options file
#
#===============================================================
evgenConfig.description = "CI dijet"
evgenConfig.keywords = ["QCD","jets"]
#evgenConfig.contact = ["Marco Vanadia"]

include ( "MC14JobOptions/Pythia8_AU2_CT10_Common.py" )

genSeq.Pythia8.Commands += [
    #get the gluons through QCD
    #and the qq/qqbar from CI
    "HardQCD:gg2gg = on",
    "HardQCD:gg2qqbar = on",
    "HardQCD:qg2qg = on",
    "HardQCD:qqbar2gg = on",
    'ContactInteractions:QCqq2qq = on',
    'ContactInteractions:QCqqbar2qqbar  = on',
    'ContactInteractions:Lambda = 7000.',
    'ContactInteractions:etaLL = -1',
    'PhaseSpace:mHatMin = 1000.',
    'PhaseSpace:pTHatMin = 250.',
    ]

#include("MC14JobOptions/JetFilter_JZ4.py")
evgenConfig.minevents = 1000
#==============================================================
#
# End of job options file
#
###############################################################

