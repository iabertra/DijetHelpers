import shutil
import fileinput
import re

masses = [ 500, 600, 800, 1000, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000 ]
IDs    = [ 892, 893, 894,  895,  896,  897,  898,  899,  900,  901,  902,  903,  904,  905,  906 ]

for i in range( len(masses) ):
  print( str(masses[i]) + " " + str(IDs[i]) )
  mass = masses[i]
  id   = IDs[i]
# build file name
  filename = "MC15.303" + str(id) + ".Pythia8EvtGen_A14_NNPDF23LO_ExcitedQ" + str(mass) + "Lambda" + str(mass) + "f1.py"
# copy template to file name
  shutil.copyfile( "template.py", filename )
# replace XXXX with the appropiate mass
  #for line in fileinput.input(filename, inplace=1, backup='.bak'):
  for line in fileinput.input(filename, inplace=1):
    line = re.sub("XXXX",str(mass), line.rstrip())
    print(line)
  fileinput.close()


