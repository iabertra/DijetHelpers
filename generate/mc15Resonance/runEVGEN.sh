#directory with the job options
JOBOPTDIR=$PWD/jobOpt/
#MCPROD flag (be careful because the job otions MUST begin with this)
MCPROD=MC15
#center-of-mass-energy
ECM=13000
#number of events per job
NEVENTSPERJOB=1000

#run number for the joboptions

IDS="303892 303893 303894 303895 303896 303897 303898 303899 303900 303901 303902 303903 303904 303905 303906"

for t in `echo ${IDS}`; do
  echo ${t}
  RUNNUMBER=${t}
  JOBOPTNAME=`"ls" MC15.${t}.Pythia8EvtGen_A14_NNPDF23LO_ExcitedQ*Lambda*f1.py`
  echo "Running ${RUNNUMBER} with ${JOBOPTNAME}"
  OUTDIR="evgen_${RUNNUMBER}"
  if [ ! -d $OUTDIR ]; then
    mkdir $OUTDIR
  else
    rm -rf $OUTDIR/*
  fi

  Generate_tf.py --ecmEnergy $ECM --runNumber $RUNNUMBER --firstEvent 0 --maxEvents $NEVENTSPERJOB --randomSeed 1234567 --jobConfig $JOBOPTNAME --outputEVNTFile ${OUTDIR}/OUT.EVNT.pool.root --evgenJobOpts=/afs/cern.ch/atlas/software/kits/EvgenJobOpts/MC15JobOpts-00-00-15_v0.tar.gz

done


