if [ ! -e input_grid_13TeV.tgz ]; then
  tar -czvf input_grid_13TeV.tgz pwgborngrid.top pwgbtlgrid.top pwggrid.dat pwghistnorms.top pwgstat.dat pwgubound.dat pwgxgrid.dat pwhg_checklimits
fi

Generate_trf.py ecmEnergy=13000 runNumber=300003 randomSeed=1234 firstEvent=1 maxEvents=1000 jobConfig=./powheg_input.py outputEVNTFile=output_1.evgen.root inputGenConfFile=./input_grid_13TeV.tgz

