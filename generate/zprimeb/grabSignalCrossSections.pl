#!/usr/bin/perl -w

use strict;
use warnings;

print "crossSection = \{";
my $first = 1;
for my $file (`ls EvgenZprimeMadGraph/MG5_aMC_v2_2_3/Dijet_signal/Events/*/*.txt`) {
    my $fileText = `cat $file`;
    die "mismatch2" unless( $fileText =~ m/^  10030 +([0-9]+) +# MZph/m );
    my $massGeV = $1;
    # die "mismatch1" unless( $fileText =~ m/# +Integrated weight (pb) +: +(.+)/m );
    die "mismatch1" unless( $fileText =~ m/# +Integrated weight +\(pb\) +\: *(.+)/m );
    my $xsecPb = $1;
    if( $first eq 1 ) {
        $first = 0;
    } else {
        print ", ";
    }
    print "$massGeV: $xsecPb";
}
print "\}\n";

