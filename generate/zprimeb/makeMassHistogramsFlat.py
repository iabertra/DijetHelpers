from ROOT import *
import array
from math import *

file = TFile.Open("MCoutput.root")

final_binning = [ 946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055 ]

VectorOf_M_Zprime = [ "350","400","450","500","550","600","650","700","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1550","1600","1650","1700","1750","1800","1850","1900","1950","2000","2100","2200","2300","2400","2500","2600","2700","2800","2900","3000","3200","3400","3600","3800","4000","4200","4400","4600","4800","5000","5200","5400","5600","6000"]

RunNumberDict = {}
hist_bare = {}
hist_cuts = {}
outputFiles = {}
debugFile = TFile.Open("debug_out.root","RECREATE")
hist_debug = TH1D("hist_debug","",len(final_binning)-1, array.array('d',final_binning))

RunNumberOffset = 0
for mass in VectorOf_M_Zprime:
    f = TFile.Open("output_"+mass+".root","RECREATE")
    outputFiles[ mass ] = f
    hist_bare[ mass ] = TH1D("hist_bare_"+mass,"",len(final_binning)-1, array.array('d',final_binning))
    hist_cuts[ mass ] = TH1D("hist_cuts_"+mass,"",len(final_binning)-1, array.array('d',final_binning))
    hist_bare[ mass ].SetDirectory(f)
    hist_cuts[ mass ].SetDirectory(f)
    RunNumberDict[ 180900+RunNumberOffset ] = mass
    RunNumberOffset = RunNumberOffset + 1
    print "opened output file for Z' mass: " + mass + " and run: " + str(RunNumberOffset-1+180900)

t = file.Get("outTree");

print "files open."

entryNumber = 0
for entry in t:

    tv1 = TLorentzVector()
    tv2 = TLorentzVector()

    have_tv1 = False
    have_tv2 = False
    
    njets = len(entry.jet_pt)

    entryNumber = entryNumber + 1
    if entryNumber%1000 == 0 or entryNumber<100:
        print entryNumber
        print "run " + str(entry.runNumber) + " event " + str(entryNumber) + " njets " + str(njets)
    # if entryNumber>1000:
    #     break

    for ijet in range(njets):
        p = TLorentzVector()
        p.SetPtEtaPhiE(entry.jet_pt[ijet],entry.jet_eta[ijet],entry.jet_phi[ijet],entry.jet_E[ijet])
        if fabs(p.Eta())>2.7:
            continue
        if p.Pt()>tv1.Pt():
            tv1=p
            have_tv1 = True
        elif p.Pt()>tv2.Pt():
            tv2=p
            have_tv2 = True
        
    if not (have_tv1 and have_tv2):
        continue

    # have the two leading jets. compute mjj search cuts
    q = tv1 + tv2
    yStar = 0.5*(tv1.Rapidity() - tv2.Rapidity())

    zprimemass = RunNumberDict[ entry.runNumber ]
    if zprimemass<300:
        print "unknown Z' mass! exiting"
        exit

    # print "z' mass: " + zprimemass + " dijet mass: " + str(q.M())
    hist_debug.Fill(q.M())
    
    hist_bare[ zprimemass ].Fill(q.M())

    if tv1.Pt()<410:
        continue
    if tv2.Pt()<50:
        continue
    if q.M()<1100:
        continue
    if fabs(yStar)>0.6:
        continue
    
    hist_cuts[ zprimemass ].Fill(q.M())

for mass in VectorOf_M_Zprime:    
    ofile = outputFiles[ mass ]
    ofile.cd()
    # print " mass " + mass + " " + str(hist_bare[mass].GetEntries())
    #    print hist_bare[ mass ].GetName()
    hist_bare[ mass ].SetName( "h_bare" )
    hist_cuts[ mass ].SetName( "h_cuts" )
    # hist_bare[ k ].SetDirectory( outputFiles[ mass ] )
    # hist_cuts[ k ].SetDirectory( outputFiles[ mass ] )
    hist_bare[ mass ].Write()
    hist_cuts[ mass ].Write()
    ofile.Write()
    ofile.Close()

debugFile.cd()
hist_debug.Write()
debugFile.Close()

file.Close()
