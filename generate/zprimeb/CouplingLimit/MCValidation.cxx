#define MCValidation_cxx
// The class definition in MCValidation.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// Root > T->Process("MCValidation.C")
// Root > T->Process("MCValidation.C","some options")
// Root > T->Process("MCValidation.C+")
//

#include "MCValidation.h"
#include <TH2.h>
#include <TStyle.h>
#include <iostream>
#include <TStopwatch.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TF1.h>


void MCValidation::Begin(TTree * /*tree*/)
{
	// The Begin() function is called at the start of the query.
	// When running with PROOF Begin() is only called on the client.
	// The tree argument is deprecated (on PROOF 0 is passed).
	
        
	TString option = GetOption();
        double intCross = 0.;


        
        TFile * fout = TFile::Open(m_outputDir+"/output"+m_name+".root","RECREATE");
        
//        h_mass = new TH1D("h_mass","h_mass",200 ,m_mass-1000,m_mass+1000.); 

/*	Float_t bins[] = {0,20,40,60,80,100,120,140,160,180,200,
                    216,234,253,272,294,316,339,364,390,
                    417,445,474,504,535,566,599,633,668,
                    705,743,782,822,864,907,952,999,
                    1048,1098,1150,1203,1259,1316,1376,
                    1437,1501,1567,1635,1706,1779,1854,
                    1932,2012,2095,2181,2269,2360,2454,
                    2551,2650,2753,2860,2970,3084,3202,
                    3324,3450,3581,3716,3855,3999,
                    4149,4303,4463,4629,4800,4977,
                    5160,5350,5546,5748,5958,6174,
                    6397, 6628, 6866, 7112, 7366, 7628, 7898, 8177,
                    8464, 8760, 9065, 9379, 9702, 10035 };
        Int_t  binnum = sizeof(bins)/sizeof(Float_t) -1;
        h_mass = new TH1D("h_mass","h_mass",binnum,bins);
        h_mass_noCut = new TH1D("h_mass_noCut","h_mass_noCut",binnum,bins);
        h_mass_detector = new TH1D("h_mass_detector","h_mass_detector",binnum,bins);
*/
        h_mass = new TH1D("h_mass","h_mass",200,250,m_mass*1.2);
        h_mass_noCut = new TH1D("h_mass_noCut","h_mass_noCut",200,250,m_mass*1.2);
        h_mass_detector = new TH1D("h_mass_detector","h_mass_detector",200,250,m_mass*1.2);
        

        h_mass->Sumw2();
        h_mass_noCut->Sumw2();
        h_mass_detector->Sumw2();
        h_mass->SetDefaultSumw2();
        h_pT = new TH1D("h_pT","h_pT",100 ,0, m_mass/2+1000.); 
        h_eta = new TH1D("h_eta","h_eta",100 ,-5.,5.); 
        h_phi = new TH1D("h_phi","h_phi",64 ,-3.2,3.2); 
        h_ystar = new TH1D("h_ystar","h_ystar",100 ,-5,6);
        h_nJets = new TH1D("h_nJets","h_nJets",14 ,0.5,14.5); 
        h_nJets_cut = new TH1D("h_nJets_cut","h_nJets_cut",14 ,0.5,14.5); 
        
}

void MCValidation::SlaveBegin(TTree * /*tree*/)
{
	// The SlaveBegin() function is called after the Begin() function.
	// When running with PROOF SlaveBegin() is called on each slave server.
	// The tree argument is deprecated (on PROOF 0 is passed).

	TString option = GetOption();

}

Bool_t MCValidation::Process(Long64_t entry)
{
	// The Process() function is called for each entry in the tree (or possibly
	// keyed object in the case of PROOF) to be processed. The entry argument
	// specifies which entry in the currently loaded tree is to be processed.
	// It can be passed to either MCValidation::GetEntry() or TBranch::GetEntry()
	// to read either all or the required parts of the data. When processing
	// keyed objects with PROOF, the object is already loaded and is available
	// via the fObject pointer.
	//
	// This function should contain the "body" of the analysis. It can contain
	// simple or elaborate selection criteria, run algorithms on the data
	// of the event and typically fill histograms.
	//
	// The processing can be stopped by calling Abort().
	//
	// Use fStatus to set the return value of TTree::Process().
	//
	// The return value is currently not used.
	GetEntry(entry);
        
	// For Leading and Subleading jets:
	double LJetPt = -1;
	double SJetPt = -1;
	int indexLJet = -1;
	int indexSJet = -1;


        TF1 f_resolution("f1","[0]/sqrt(x)+[1]+[2]/x");
        f_resolution.SetParameters(0.315,0.03046,7.61);

	// Performance evaluation
	TStopwatch m_timer;
	m_timer.Reset();
	m_timer.Start();
	double m_totaltime=0;
	int counter = 0; 
	UInt_t currentRun = 0;
// 
// 	std::cout << "Running over " << nentries << " events \n";
// 	for (Long64_t jentry=0; jentry<nentries;jentry++) {
// 		Long64_t ientry = LoadTree(jentry);
// 		if (ientry < 0) break;
// 		//nb = fChain->GetEntry(jentry);   nbytes += nb;
// 		counter++;
// 		b_RunNumber->GetEntry(ientry);
// 		b_EventNumber->GetEntry(ientry);
// 		if (currentRun!=RunNumber) {
// 			currentRun = RunNumber;
// 			std::cout << "Start with run " << currentRun<< "\n";
// 		}
// 		if(counter % 500 == 0)
// 		{
// 			m_timer.Stop();
// 			m_totaltime += m_timer.CpuTime();
// 			std::cout << "Run: " <<RunNumber << ", events Processed: " << counter << "\tCPU Time: " << m_timer.CpuTime() << endl;
// 			m_timer.Start();
// 		} 
        double weight = 1.0;//ReturnWeight(RunNumber);
//         intCross += weight;
        // weight =0.000111507  * 1000. / 10000.;
        //  weight =  4.2e-6 * 1000. / 10788.;
// 		if (jentry ==0) std::cout << "Weight: " << weight << endl;
// 
// 		b_jet_AntiKt6TruthJets_WZ_n->GetEntry(ientry);
// 		b_jet_AntiKt6TruthJets_WZ_E->GetEntry(ientry);
// 		b_jet_AntiKt6TruthJets_WZ_pt->GetEntry(ientry); 
// 		b_jet_AntiKt6TruthJets_WZ_eta->GetEntry(ientry);
// 		b_jet_AntiKt6TruthJets_WZ_phi->GetEntry(ientry);
// 
// 
        // Initialize : indexes and pT of 2 leading jets
        LJetPt = -1;
        SJetPt = -1;
        indexLJet = -1;
        indexSJet = -1;
	int nJets_above25 = 0;
        for(int i=0; i<jet_AntiKt6TruthJets_WZ_n; i++)
        {
                double thisJetPt =(*jet_AntiKt6TruthJets_WZ_pt)[i];
		if (thisJetPt>25000) nJets_above25++;
                double thisJetEta =(*jet_AntiKt6TruthJets_WZ_eta)[i];
                double thisJetPhi =(*jet_AntiKt6TruthJets_WZ_phi)[i];
                double thisJetE =(*jet_AntiKt6TruthJets_WZ_E)[i];
                // h_pT->Fill(thisJetPt/1000.,weight);
                // h_eta->Fill(thisJetEta,weight);
                // h_phi->Fill(thisJetPhi,weight);
                TLorentzVector thisJet;
                thisJet.SetPtEtaPhiE(thisJetPt,thisJetEta,thisJetPhi,thisJetE);
                double thisJetY = thisJet.Rapidity();
                //      if (fabs(thisJetY) >= y_max) continue;
                if(thisJetPt > LJetPt)
                {
                        indexSJet = indexLJet;
                        SJetPt = LJetPt;
                        indexLJet = i;
                        LJetPt = thisJetPt;
                } else if(thisJetPt > SJetPt )
                {
                        indexSJet = i;
                        SJetPt = thisJetPt;
                }
        }
        if (indexLJet == -1 || indexSJet == -1) {
                return kTRUE;
        }  

        TLorentzVector LJet;  // Leading Jet
        LJet.SetPtEtaPhiE( (*jet_AntiKt6TruthJets_WZ_pt)[indexLJet], (*jet_AntiKt6TruthJets_WZ_eta)[indexLJet], (*jet_AntiKt6TruthJets_WZ_phi)[indexLJet],(*jet_AntiKt6TruthJets_WZ_E)[indexLJet] );
        TLorentzVector SJet; // Subleading Jet
        SJet.SetPtEtaPhiE( (*jet_AntiKt6TruthJets_WZ_pt)[indexSJet], (*jet_AntiKt6TruthJets_WZ_eta)[indexSJet], (*jet_AntiKt6TruthJets_WZ_phi)[indexSJet],(*jet_AntiKt6TruthJets_WZ_E)[indexSJet] );
        h_pT->Fill( LJet.Pt()/1000.,weight);
        h_eta->Fill(LJet.Eta(),weight);
        h_phi->Fill(LJet.Phi(),weight);
        h_ystar->Fill((LJet.Eta()-SJet.Eta())/2.,weight);

        TLorentzVector DiJet = LJet+SJet;
        double Mjj = DiJet.M()/1000.; // GeV!

        double smearingFactor=1;
        if(m_useSmearing){
          double smearingWidth=f_resolution.Eval(Mjj);
          // TODO REMOVE, ONLY FOR TESTING
          smearingWidth*=0.5;
          // END

          smearingFactor=r_randomGenerator.Gaus(1,smearingWidth);
//          std::cout << "SMEARING: " << smearingFactor << std::endl;
 
        }

        h_mass_noCut->Fill(Mjj*smearingFactor,weight);
        if(Mjj<250) return kTRUE;
        if(fabs((LJet.Eta()-SJet.Eta())/2.)>0.6) return kTRUE; // For y* cutted samples
        if(fabs(SJet.Eta())>2.8) return kTRUE; // For eta cut
        if(fabs(LJet.Eta())>2.8) return kTRUE; // For eta cut
        h_mass->Fill(Mjj,weight);
        h_mass_detector->Fill(Mjj*smearingFactor,weight);
        h_nJets->Fill(jet_AntiKt6TruthJets_WZ_n);
        h_nJets_cut->Fill(nJets_above25);


	return kTRUE;
}

void MCValidation::SlaveTerminate()
{
	// The SlaveTerminate() function is called after all entries or objects
	// have been processed. When running with PROOF SlaveTerminate() is called
	// on each slave server.

}

void MCValidation::Terminate()
{
	// The Terminate() function is the last function to be called during
	// a query. It always runs on the client, it can be used to present
	// the results graphically or save the results to file.

//        std::cout << "Acceptance: " <<h_mass_detector->Integral()/h_mass_noCut->Integral() << std::endl;
        
	h_mass->Write();
	h_mass_detector->Write();
        h_mass_noCut->Write();
	h_ystar->Write();
        h_pT->Write(); 
        h_eta->Write();
        h_phi->Write();
        h_nJets->Write();
        h_nJets_cut->Write();
}
