#!/bin/python
import sys
import os
import commands

import math

from ROOT import *
import atlasStyleMacro

def ensure_dir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)

## Inputs needed:
# 1) Limits per width from Gaussians -> detectorDict,sevenDict,tenDict,fifteenDict (detector resolution, 7%, 10%, 15%)
# 2) Parametrisation of dijet mass resolution
# 3) Cross section from Madgraph for 20% coupled Zprime
# 4) Ntuple per Mass, with dijet mass with and without cuts (named h_mass_detector and h_mass_noCut, see below), file should be at TFile(OutputDirMain+Out+"output"+str(mass)+".root") or change location below
#   i) By running code on MC truth NTuple (change doHistoProduction to True, see below)
#   or ii) Run your own NTuple/xAod code



## 1) LHCP 2015 (Gauss Limits)
detectorDict= {3400: 0.120338889157, 1800: 0.670683643332, 5000: 0.0606283365328, 3100: 0.226796072808, 1750: 0.940584885954, 1450: 0.941929232871, 2700: 0.209594659929, 1400: 1.5152898812, 1250: 1.62977309639, 2300: 0.32441697524, 4000: 0.0870387370163, 1300: 1.22665445153, 2800: 0.228177441256, 1850: 0.462790726844, 1900: 0.474967150702, 2500: 0.228519836916, 1600: 1.3806722133, 1650: 1.20958684728, 3700: 0.093989107177, 2000: 0.399123859635, 2900: 0.270897118362, 2200: 0.272055865966, 1700: 1.12430631363, 3200: 0.146320759492, 4400: 0.0484745765133, 1350: 1.56879975496, 4800: 0.0506909017415, 4600: 0.0492549611619, 3000: 0.270902865069, 3600: 0.128425682458, 4200: 0.0497468451127, 1200: 1.60799975657, 3500: 0.136486899801, 1500: 1.18013896559, 1950: 0.462880626909, 2400: 0.243841017507, 3900: 0.0701896065959, 2600: 0.21413449895, 3800: 0.0810365824699, 2100: 0.365447251398, 1550: 1.53590296652, 3300: 0.0943317040443}
sevenDict= {1300: 2.11427996758, 2800: 0.486221930181, 1900: 0.759753841106, 1850: 0.907289202671, 2500: 0.398694170444, 1600: 2.90153898632, 3700: 0.108583722536, 1650: 2.49627901492, 2000: 0.609961603062, 2200: 0.472958910626, 2900: 0.448238844182, 1700: 1.95536353669, 3200: 0.254437262748, 4400: 0.0497214109345, 1350: 1.98368797266, 3400: 0.186369173137, 1800: 1.08158832702, 3100: 0.323944030049, 1750: 1.44457288028, 1450: 2.59778279023, 1400: 1.94874126204, 2700: 0.472645613976, 2300: 0.434927261872, 4000: 0.0932489565876, 1500: 2.96207048229, 1950: 0.680470191228, 2400: 0.397953628086, 3900: 0.0855286049039, 2600: 0.421515241373, 3800: 0.0940267840725, 1550: 3.10703363914, 2100: 0.52427131105, 3300: 0.212482490639, 4600: 0.0567826649636, 3600: 0.130306345535, 3000: 0.391755182896, 4200: 0.0709378800383, 3500: 0.15855241947}
tenDict= {3500: 0.149878340614, 3600: 0.133177804447, 3000: 0.463092340942, 4200: 0.0772549690622, 3300: 0.256319742343, 2600: 0.590034079188, 2100: 0.619394829133, 3800: 0.0942561635875, 1550: 4.31683468924, 3900: 0.0868764230975, 2400: 0.512843146357, 1500: 4.77063275005, 1950: 0.823065637924, 4000: 0.0927591989609, 2700: 0.605173505146, 1400: 4.07849220312, 2300: 0.522853686805, 1450: 4.58992530274, 1750: 1.59823686962, 3100: 0.389799443831, 1800: 1.28177533386, 3400: 0.198129458296, 1700: 2.11327253428, 3200: 0.31429317337, 2200: 0.553427950686, 2900: 0.526779080909, 2000: 0.743301063399, 3700: 0.107519235176, 1650: 2.88848026965, 2500: 0.531992986919, 1600: 3.63829388622, 2800: 0.568831322827, 1850: 1.08533046588, 1900: 0.91515938592}
fifteenDict= {3500: 0.146396692856, 3000: 0.508860797612, 3600: 0.129959376132, 2600: 0.853031710736, 3800: 0.0985341275175, 2100: 0.812250321598, 3900: 0.0946502770312, 3300: 0.212525524704, 2400: 0.796380305624, 1950: 1.00972529571, 4000: 0.0955144664784, 2700: 0.801550240466, 2300: 0.725167881331, 1800: 1.45103390483, 3400: 0.177996095472, 1750: 1.71210065376, 3100: 0.384100648764, 2200: 0.793896666039, 2900: 0.615604390023, 1700: 2.05708086137, 3200: 0.29242505464, 2500: 0.872837053124, 1600: 3.70722068156, 2800: 0.748787638463, 1900: 1.11743201295, 1850: 1.24857575273, 2000: 0.926493713833, 1650: 2.8427345561, 3700: 0.114793754769}


## 1) EPS 2015 (Gauss Limits)
# detectorDict= {1500: 0.76238569232, 3200: 0.141351190937, 3100: 0.11067953613, 1950: 0.434600771876, 3000: 0.10528326398, 2600: 0.155432591832, 1750: 0.843405521413, 1600: 1.54126539071, 1700: 1.17613553514, 1550: 1.0456423301, 1400: 2.21913632755, 1200: 1.64883342895, 2000: 0.531466344506, 2800: 0.152939685437, 1850: 0.441563786008, 1650: 1.50768865276, 1450: 1.52977788626, 2700: 0.163201394846, 1800: 0.578891629912, 2900: 0.114197388927, 1350: 1.79993148338, 2300: 0.282110520452, 1900: 0.408802925005, 1300: 1.3421714902, 1250: 1.29220398593, 2500: 0.13965353194, 3400: 0.162787197492, 2400: 0.210651804552, 2100: 0.601890593522, 3300: 0.161910006935, 2200: 0.437944336726}
# sevenDict= {1850: 0.896082584388, 2800: 0.193472364672, 2000: 0.921801088423, 1450: 2.19974402048, 1650: 2.2752135199, 2600: 0.210008126042, 3000: 0.220413030178, 1950: 0.908213967792, 3100: 0.238482445418, 1500: 2.28207900807, 1400: 2.44093989874, 1750: 1.30849868154, 1550: 2.51821444106, 1700: 1.7597428802, 1600: 2.54936489607, 2500: 0.2308962448, 2200: 0.61722737819, 2100: 0.871894209412, 2400: 0.278548893119, 1800: 0.988999327018, 2700: 0.198631608485, 1300: 2.53335731415, 2900: 0.177176956346, 1350: 2.68295793628, 2300: 0.400157446937, 1900: 0.86872439976}
# tenDict= {1700: 2.03738835648, 1750: 1.6625203252, 1550: 3.68072868981, 1600: 3.11388888889, 1400: 3.48490836197, 1950: 1.19194074074, 2600: 0.252665802696, 1500: 3.87809303486, 1650: 2.54663361357, 1450: 3.75385840611, 2800: 0.272505282462, 1850: 1.34881293698, 2000: 1.08791444033, 2900: 0.306329482575, 2300: 0.428443101205, 1900: 1.28241914676, 1800: 1.48386719116, 2700: 0.238486267166, 2100: 0.841221107284, 2200: 0.574643347843, 2400: 0.333324708926, 2500: 0.275149114742}
# fifteenDict= {1850: 2.6594176634, 2000: 1.04269085156, 1650: 4.58371217676, 1950: 1.27954196963, 2600: 0.36501531283, 1750: 3.50118654123, 1600: 5.10515057562, 1700: 4.0231788922, 2500: 0.37847922267, 2100: 0.754733296923, 2200: 0.591446860445, 2400: 0.42524968789, 1800: 3.02408969805, 2700: 0.385258508613, 2300: 0.466858980735, 1900: 1.49344098047}

## 2) Paraxeters of fit to dijet xass resolution with f1 =TF1("f1","[0] + [1]*x + [2]*x*x + [3]*x*x*x + [4]*x*x*x*x + [5]*x*x*x*x*x + [6]*x*x*x*x*x*x;")
fitParameters=[0.0488777, -2.92624e-05, 1.39116e-08, -3.69195e-12,  5.48313e-16, -4.24416e-20,  1.33082e-24]

## 3) Cross sections extracted from MadGraph log for 20% coupling of Zprime
crossSection = {1000: 0.26063066, 1050: 0.21278096, 1100: 0.17517271, 1150: 0.14517477, 1200: 0.12084679, 1250: 0.1012236, 1300: 0.08517111, 1350: 0.07197304, 1400: 0.061018757, 1450: 0.051994544, 1500: 0.044409141, 1550: 0.038077982, 1600: 0.032718266, 1650: 0.028212815, 1700: 0.024390385, 1750: 0.021134309, 1800: 0.018344066, 1850: 0.015970266, 1900: 0.013912653, 1950: 0.0121492209, 2000: 0.0106319214, 2100: 0.0081725309, 2200: 0.006324972, 2300: 0.0049158678, 2400: 0.0038381575, 2500: 0.0030086294, 2600: 0.00236997109, 2700: 0.00186941543, 2800: 0.00148056175, 2900: 0.00117565481, 3000: 0.00093599007, 3200: 0.00059595677, 3400: 0.000382170413, 350: 14.28918, 3600: 0.000246171866, 3800: 0.000159263026, 4000: 0.000103271892, 400: 8.512141, 4200: 6.71144171e-05, 4400: 4.36066593e-05, 450: 5.515483, 4600: 2.83477002e-05, 4800: 1.83982394e-05, 5000: 1.192550236e-05, 500: 3.748349, 5200: 7.70747786e-06, 5400: 4.96959916e-06, 550: 2.63564, 5600: 3.19034751e-06, 6000: 1.295457739e-06, 600: 1.9070961, 650: 1.4115916, 700: 1.0654463, 750: 0.8176539, 800: 0.6359635, 850: 0.501206, 900: 0.3992004, 950: 0.32099274}

## 4) 


### END INPUTS

### Variables to set


#Fill Up these Vectors with the names, input and output dir

doHistoProduction = False  ## Run once with True to extract dijet mass spectrum from input nTuple

#These will be the name of the samples in the TLegend
SampleName = 'Zprime variable coupling MG'

#InputNtuples coming from the event generator and Reco_trf.py MC12.Pythia8_qStarQGOnly1000_AU2_CT10.py.root
InputNtuple = '/afs/cern.ch/user/b/boveia/work/MCoutput/DAOD_TRUTH1.MC15.18*.MadGraphPythia8EvtGen_A14MSTW2008LO_Zprime_%df1.py.root'  ## (20% coupling)

#Target Directory for Histograms and Plots
OutputDirMain='/afs/cern.ch/user/b/boveia/work/zprimeb/CouplingLimit/outputDir/'

# ICHEP 2015
Vector_Masses = [ "350","400","450","500","550","600","650","700","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1550","1600","1650","1700","1750","1800","1850","1900","1950","2000","2100","2200","2300","2400","2500","2600","2700","2800","2900","3000","3200","3400","3600","3800","4000","4200","4400","4600","4800","5000","5200","5400","5600","6000"]
## above 2.7 TeV couplings too large? (>2.5)
#,"2800","2900","3000","3200","3400","3600","3800","4000","4200"]

# Name of output txt file with coupling limits
nameOutput='limit'


### END User defined


def coupling(mass):
  return 0.2

def xsectionLimit(mass,width):
  f1 =TF1("f1","[0] + [1]*x + [2]*x*x + [3]*x*x*x + [4]*x*x*x*x + [5]*x*x*x*x*x + [6]*x*x*x*x*x*x");
  f1.SetParameters(*fitParameters)

  detectorResolution=f1.Eval(mass)
  signalResolution=width/mass
  print "width(%)|detector resolution(%): " + str(signalResolution) + "|" + str(detectorResolution)
  if signalResolution<detectorResolution:
    print "Resolution<Detector"
    if mass in detectorDict:
        return detectorDict[mass]
    else:
        return "-"
  elif signalResolution<0.07: 
    print "Resolution<0.07"
    if mass in sevenDict:
        return sevenDict[mass]
    else:
        return "-"
  elif signalResolution<0.10:
    print "Resolution<0.10"
    if mass in tenDict:
        return tenDict[mass]
    else:
        return "-"    
  elif signalResolution<0.15:
    if mass in fifteenDict:
        return fifteenDict[mass]
    else:
        return "-"
  else:
    return "-"

def crystalball(x,par):
    """
    n:par[0]
    alpha:par[1]
    centroid:par[2]
    sigma:par[3]
    peak amplitude:par[4]
    """
    if (x[0]-par[2])/par[3] < -par[1]:
        term1 = ((par[0]/abs(par[1]))**par[0])
        term2 = math.exp(-(par[1]**2)/2)
        term3 = par[0]/abs(par[1])-abs(par[1])-(x[0]-par[2])/par[3]
        term4 = pow(float(term3),float(par[0]))
        y = (par[4]*term1*term2/term4)
    else:
        y = (par[4]*math.exp(-(((x[0]-par[2])/par[3])**2)/2))
    return y


#Settings Area BEGIN

histoName = 'h_cuts'
HistoAxisTitle = 'smeared m_{jj}'

#Settings Area END

 
#Here starts the Histo production (if needed) and the Plotting procedure ---> Change it only if it strictly necessary

#Ensure the OutputDir
ensure_dir(OutputDirMain)

couplingResult=[]



#doHistoProduction "Zone" BEGIN

if doHistoProduction == True :

  #compile code
  compile_Clean="make clean"
  os.system(compile_Clean)
  compile_Make="make"
  os.system(compile_Make)
     
  configFile="ConfigFile.config"
  
  for imass,mass in enumerate(Vector_Masses) :
    if imass==0 :
      StringMasses=str(mass)
    else :
      StringMasses=StringMasses+":"+str(mass)
        
  os.system("rm "+configFile)
        
  os.system("touch "+configFile)
        
  config = open(configFile,'r+')

        
  config.write("""
  ######
  #	Config file test
  ######
  
  #ZPrime Masses
  Masses		"""+StringMasses+"""
  #Number of Files
  NumberMCSamples  1
  #Directories
  #UseSmearing?
  UseSmearing True
  """)


  WorkInProgressConfig=config.read()
  string="NtupleFile0 "+InputNtuple+"\n"
  config.write(WorkInProgressConfig+string)

  WorkInProgressConfig=config.read()
  string="OutputHistoFile0 "+OutputDirMain+"\n"
  config.write(WorkInProgressConfig+string)

  config.close()  

  #Execute the code to produce validation histograms
  Executable="/bin/MC12ValidationFramework"
  executionString="."+Executable+" "+configFile
  print executionString
  os.system("pwd")
  os.system(executionString)

#doHistoProduction "Zone" END


#Plotting "Zone" BEGIN
c=TCanvas()
pdf=OutputDirMain+"Zprime_basicHistos_MCValidation.pdf"
c.Print(pdf+"[");

  
for iMass,mass in enumerate(Vector_Masses) :

  iFile=TFile.Open(OutputDirMain+"output_"+str(mass)+".root")
  if not iFile:
      continue
  massInt=int(mass)
  if massInt<1200:
      continue

  l = TLegend(0.7,0.65,0.93,0.93)
      
  l.SetFillColor(1)
  l.SetFillStyle(0)
  l.SetLineColor(0)
  l.SetLineStyle(0)
  l.SetLineWidth(0)
  l.SetShadowColor(0)
  l.SetTextFont(42)
  l.SetTextSize(0.02)


  l.SetHeader("m(Zprime)"+mass+" GeV")
  
  stack=THStack()
  
  histo = iFile.Get(histoName)

  h_mass_noCut=iFile.Get("h_bare")

  integral = histo.Integral()
  print iFile.GetName() + " entries: " + str(integral)
  if integral==0:
    continue
    integral=1
  histo.Scale(1/integral)

  f1 = TF1("crystal",crystalball,0.5*massInt,1.15*massInt,5)
  f1.SetParameters(1.8,0.5,1.04*massInt,0.007*massInt,1000)
  f1.SetParLimits(0,0.5,10.0)
  f1.SetParLimits(1,0.05,3.5)
  f1.SetParLimits(2,0.7*massInt,1.05*massInt)
  f1.SetParLimits(3,0.005*massInt,0.3*massInt)
  histo.Scale(1/histo.Integral());
  histo.Fit(f1,"IMR");
  # Calculate "gaussian efficiency" 3sigma/all events
  sigma=histo.GetFunction("crystal").GetParameter(3)
  mean=histo.GetFunction("crystal").GetParameter(2)
  print mean,sigma,mean-3*sigma,mean+3*sigma
  eff=histo.Integral(histo.FindBin(mean-3*sigma),histo.FindBin(mean+3*sigma))
  print "Mass|Efficiency:",mass,eff
  acceptance = integral/h_mass_noCut.Integral()
  xL = xsectionLimit(massInt,sigma)
  if xL!="-":
    couplingLimit=coupling(mass)*pow(xL/(eff*acceptance*crossSection[massInt]),0.5)
    couplingResult.append([massInt,couplingLimit])
  else:
    couplingLimit='None'
  print "Mass|couplingLimit: "+mass+"|"+str(couplingLimit)
  print "Signal mass | N_events total | N_events passing hard cuts | N_events within Gaussian mean +/- 3 sigma | Gaussian mean | Gaussian width | alpha | power"
  print mass,h_mass_noCut.Integral(),integral,eff*integral,mean,sigma,histo.GetFunction("crystal").GetParameter(1),histo.GetFunction("crystal").GetParameter(0)
  histo.SetMarkerColor(2)
  histo.SetMarkerStyle(22)
  histo.SetLineColor(2)
  l.AddEntry(histo,SampleName,"APL")
  stack.Add(histo)
  stack.Draw("A")
  stack.GetXaxis().SetTitle(HistoAxisTitle)
  stack.GetYaxis().SetTitleOffset(1.6)
  stack.GetYaxis().SetTitle("Normalized Entries")
    
  stack.Draw("nostack")
  l.Draw("same")
  c.SaveAs(OutputDirMain+histoName+mass+".png")
    
  c.Print(pdf)

  print "writing coupling results to " + nameOutput
  thefile = open(nameOutput, 'w')
  for item in couplingResult:
    thefile.write("{0} {1}\n".format(item[0],item[1]))
    print "COUPLINGRESULT: " + "{0} {1}\n".format(item[0],item[1])

  iFile.Close()
    
  
c.Print(pdf+"]")
#Plotting "Zone" END
