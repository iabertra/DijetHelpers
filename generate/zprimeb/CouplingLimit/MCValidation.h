//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu May 17 18:38:05 2012 by ROOT version 5.32/01
// from TTree truth/truth
// found on file: output.root
//////////////////////////////////////////////////////////

#ifndef MCValidation_h
#define MCValidation_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TH1D.h>
#include <TRandom3.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
using std::vector;

// Fixed size dimensions of array or collections stored in the TTree if any.

class MCValidation : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   // Declaration of leaf types
   UInt_t          RunNumber;
   UInt_t          EventNumber;
   UInt_t          timestamp;
   UInt_t          timestamp_ns;
   UInt_t          lbn;
   UInt_t          bcid;
   UInt_t          detmask0;
   UInt_t          detmask1;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   UInt_t          mc_channel_number;
   UInt_t          mc_event_number;
   Float_t         mc_event_weight;
   UInt_t          pixelFlags;
   UInt_t          sctFlags;
   UInt_t          trtFlags;
   UInt_t          larFlags;
   UInt_t          tileFlags;
   UInt_t          muonFlags;
   UInt_t          fwdFlags;
   UInt_t          coreFlags;
   UInt_t          pixelError;
   UInt_t          sctError;
   UInt_t          trtError;
   UInt_t          larError;
   UInt_t          tileError;
   UInt_t          muonError;
   UInt_t          fwdError;
   UInt_t          coreError;
   Bool_t          isSimulation;
   Bool_t          isCalibration;
   Bool_t          isTestBeam;
   Int_t           mcevt_n;
   vector<int>     *mcevt_signal_process_id;
   vector<int>     *mcevt_event_number;
   vector<double>  *mcevt_event_scale;
   vector<double>  *mcevt_alphaQCD;
   vector<double>  *mcevt_alphaQED;
   vector<int>     *mcevt_pdf_id1;
   vector<int>     *mcevt_pdf_id2;
   vector<double>  *mcevt_pdf_x1;
   vector<double>  *mcevt_pdf_x2;
   vector<double>  *mcevt_pdf_scale;
   vector<double>  *mcevt_pdf1;
   vector<double>  *mcevt_pdf2;
   vector<vector<double> > *mcevt_weight;
   vector<int>     *mcevt_nparticle;
   vector<short>   *mcevt_pileUpType;
   Int_t           mcVxn;
   vector<float>   *mcVxx;
   vector<float>   *mcVxy;
   vector<float>   *mcVxz;
   Int_t           vxn;
   vector<float>   *vxx;
   vector<float>   *vxy;
   vector<float>   *vxz;
   Int_t           jet_AntiKt6TruthJets_WZ_n;
   vector<float>   *jet_AntiKt6TruthJets_WZ_E;
   vector<float>   *jet_AntiKt6TruthJets_WZ_pt;
   vector<float>   *jet_AntiKt6TruthJets_WZ_m;
   vector<float>   *jet_AntiKt6TruthJets_WZ_eta;
   vector<float>   *jet_AntiKt6TruthJets_WZ_phi;
   vector<float>   *jet_AntiKt6TruthJets_WZ_EtaOrigin;
   vector<float>   *jet_AntiKt6TruthJets_WZ_PhiOrigin;
   vector<float>   *jet_AntiKt6TruthJets_WZ_MOrigin;
   vector<float>   *jet_AntiKt6TruthJets_WZ_EtaOriginEM;
   vector<float>   *jet_AntiKt6TruthJets_WZ_PhiOriginEM;
   vector<float>   *jet_AntiKt6TruthJets_WZ_MOriginEM;
   vector<float>   *jet_AntiKt6TruthJets_WZ_WIDTH;
   vector<float>   *jet_AntiKt6TruthJets_WZ_n90;
   vector<float>   *jet_AntiKt6TruthJets_WZ_Timing;
   vector<float>   *jet_AntiKt6TruthJets_WZ_LArQuality;
   vector<float>   *jet_AntiKt6TruthJets_WZ_nTrk;
   vector<float>   *jet_AntiKt6TruthJets_WZ_sumPtTrk;
   vector<float>   *jet_AntiKt6TruthJets_WZ_OriginIndex;
   vector<float>   *jet_AntiKt6TruthJets_WZ_HECQuality;
   vector<float>   *jet_AntiKt6TruthJets_WZ_NegativeE;
   vector<float>   *jet_AntiKt6TruthJets_WZ_AverageLArQF;
   vector<float>   *jet_AntiKt6TruthJets_WZ_YFlip12;
   vector<float>   *jet_AntiKt6TruthJets_WZ_YFlip23;
   vector<float>   *jet_AntiKt6TruthJets_WZ_BCH_CORR_CELL;
   vector<float>   *jet_AntiKt6TruthJets_WZ_BCH_CORR_DOTX;
   vector<float>   *jet_AntiKt6TruthJets_WZ_BCH_CORR_JET;
   vector<float>   *jet_AntiKt6TruthJets_WZ_BCH_CORR_JET_FORCELL;
   vector<float>   *jet_AntiKt6TruthJets_WZ_ENG_BAD_CELLS;
   vector<float>   *jet_AntiKt6TruthJets_WZ_N_BAD_CELLS;
   vector<float>   *jet_AntiKt6TruthJets_WZ_N_BAD_CELLS_CORR;
   vector<float>   *jet_AntiKt6TruthJets_WZ_BAD_CELLS_CORR_E;
   vector<float>   *jet_AntiKt6TruthJets_WZ_NumTowers;
   vector<int>     *jet_AntiKt6TruthJets_WZ_SamplingMax;
   vector<float>   *jet_AntiKt6TruthJets_WZ_fracSamplingMax;
   vector<float>   *jet_AntiKt6TruthJets_WZ_hecf;
   vector<float>   *jet_AntiKt6TruthJets_WZ_tgap3f;
   vector<int>     *jet_AntiKt6TruthJets_WZ_isUgly;
//   vector<int>     *jet_AntiKt6TruthJets_WZ_isBadLooseMinus;
   vector<int>     *jet_AntiKt6TruthJets_WZ_isBadLoose;
   vector<int>     *jet_AntiKt6TruthJets_WZ_isBadMedium;
   vector<int>     *jet_AntiKt6TruthJets_WZ_isBadTight;
   vector<float>   *jet_AntiKt6TruthJets_WZ_emfrac;
   vector<float>   *jet_AntiKt6TruthJets_WZ_Offset;
   vector<float>   *jet_AntiKt6TruthJets_WZ_EMJES;
   vector<float>   *jet_AntiKt6TruthJets_WZ_EMJES_EtaCorr;
   vector<float>   *jet_AntiKt6TruthJets_WZ_EMJESnooffset;
   vector<float>   *jet_AntiKt6TruthJets_WZ_GCWJES;
   vector<float>   *jet_AntiKt6TruthJets_WZ_GCWJES_EtaCorr;
   vector<float>   *jet_AntiKt6TruthJets_WZ_CB;
   vector<float>   *jet_AntiKt6TruthJets_WZ_LCJES;
   vector<float>   *jet_AntiKt6TruthJets_WZ_emscale_E;
   vector<float>   *jet_AntiKt6TruthJets_WZ_emscale_pt;
   vector<float>   *jet_AntiKt6TruthJets_WZ_emscale_m;
   vector<float>   *jet_AntiKt6TruthJets_WZ_emscale_eta;
   vector<float>   *jet_AntiKt6TruthJets_WZ_emscale_phi;
   Int_t           jet_AntiKt6TopoNewEM_n;
   vector<float>   *jet_AntiKt6TopoNewEM_E;
   vector<float>   *jet_AntiKt6TopoNewEM_pt;
   vector<float>   *jet_AntiKt6TopoNewEM_m;
   vector<float>   *jet_AntiKt6TopoNewEM_eta;
   vector<float>   *jet_AntiKt6TopoNewEM_phi;
   vector<float>   *jet_AntiKt6TopoNewEM_EtaOrigin;
   vector<float>   *jet_AntiKt6TopoNewEM_PhiOrigin;
   vector<float>   *jet_AntiKt6TopoNewEM_MOrigin;
   vector<float>   *jet_AntiKt6TopoNewEM_EtaOriginEM;
   vector<float>   *jet_AntiKt6TopoNewEM_PhiOriginEM;
   vector<float>   *jet_AntiKt6TopoNewEM_MOriginEM;
   vector<float>   *jet_AntiKt6TopoNewEM_WIDTH;
   vector<float>   *jet_AntiKt6TopoNewEM_n90;
   vector<float>   *jet_AntiKt6TopoNewEM_Timing;
   vector<float>   *jet_AntiKt6TopoNewEM_LArQuality;
   vector<float>   *jet_AntiKt6TopoNewEM_nTrk;
   vector<float>   *jet_AntiKt6TopoNewEM_sumPtTrk;
   vector<float>   *jet_AntiKt6TopoNewEM_OriginIndex;
   vector<float>   *jet_AntiKt6TopoNewEM_HECQuality;
   vector<float>   *jet_AntiKt6TopoNewEM_NegativeE;
   vector<float>   *jet_AntiKt6TopoNewEM_AverageLArQF;
   vector<float>   *jet_AntiKt6TopoNewEM_YFlip12;
   vector<float>   *jet_AntiKt6TopoNewEM_YFlip23;
   vector<float>   *jet_AntiKt6TopoNewEM_BCH_CORR_CELL;
   vector<float>   *jet_AntiKt6TopoNewEM_BCH_CORR_DOTX;
   vector<float>   *jet_AntiKt6TopoNewEM_BCH_CORR_JET;
   vector<float>   *jet_AntiKt6TopoNewEM_BCH_CORR_JET_FORCELL;
   vector<float>   *jet_AntiKt6TopoNewEM_ENG_BAD_CELLS;
   vector<float>   *jet_AntiKt6TopoNewEM_N_BAD_CELLS;
   vector<float>   *jet_AntiKt6TopoNewEM_N_BAD_CELLS_CORR;
   vector<float>   *jet_AntiKt6TopoNewEM_BAD_CELLS_CORR_E;
   vector<float>   *jet_AntiKt6TopoNewEM_NumTowers;
   vector<int>     *jet_AntiKt6TopoNewEM_SamplingMax;
   vector<float>   *jet_AntiKt6TopoNewEM_fracSamplingMax;
   vector<float>   *jet_AntiKt6TopoNewEM_hecf;
   vector<float>   *jet_AntiKt6TopoNewEM_tgap3f;
   vector<int>     *jet_AntiKt6TopoNewEM_isUgly;
//   vector<int>     *jet_AntiKt6TopoNewEM_isBadLooseMinus;
   vector<int>     *jet_AntiKt6TopoNewEM_isBadLoose;
   vector<int>     *jet_AntiKt6TopoNewEM_isBadMedium;
   vector<int>     *jet_AntiKt6TopoNewEM_isBadTight;
   vector<float>   *jet_AntiKt6TopoNewEM_emfrac;
   vector<float>   *jet_AntiKt6TopoNewEM_Offset;
   vector<float>   *jet_AntiKt6TopoNewEM_EMJES;
   vector<float>   *jet_AntiKt6TopoNewEM_EMJES_EtaCorr;
   vector<float>   *jet_AntiKt6TopoNewEM_EMJESnooffset;
   vector<float>   *jet_AntiKt6TopoNewEM_GCWJES;
   vector<float>   *jet_AntiKt6TopoNewEM_GCWJES_EtaCorr;
   vector<float>   *jet_AntiKt6TopoNewEM_CB;
   vector<float>   *jet_AntiKt6TopoNewEM_LCJES;
   vector<float>   *jet_AntiKt6TopoNewEM_emscale_E;
   vector<float>   *jet_AntiKt6TopoNewEM_emscale_pt;
   vector<float>   *jet_AntiKt6TopoNewEM_emscale_m;
   vector<float>   *jet_AntiKt6TopoNewEM_emscale_eta;
   vector<float>   *jet_AntiKt6TopoNewEM_emscale_phi;
   Int_t           mc_n;
   vector<float>   *mc_E;
   vector<float>   *mc_pt;
   vector<float>   *mc_m;
   vector<float>   *mc_eta;
   vector<float>   *mc_phi;
   vector<float>   *mc_px;
   vector<float>   *mc_py;
//   vector<float>   *mc_pz;
   vector<int>     *mc_status;
   vector<int>     *mc_barcode;
   vector<int>     *mc_pdgId;
   vector<float>   *mc_charge;
   vector<float>   *mc_vx_x;
   vector<float>   *mc_vx_y;
   vector<float>   *mc_vx_z;
   vector<int>     *mc_vx_barcode;
   vector<vector<int> > *mc_child_index;
   vector<vector<int> > *mc_parent_index;
   Float_t         MET_Truth_NonInt_etx;
   Float_t         MET_Truth_NonInt_ety;
   Float_t         MET_Truth_Int_etx;
   Float_t         MET_Truth_Int_ety;
   Float_t         MET_Truth_IntCentral_etx;
   Float_t         MET_Truth_IntCentral_ety;
   Float_t         MET_Truth_IntFwd_etx;
   Float_t         MET_Truth_IntFwd_ety;
   Float_t         MET_Truth_IntOutCover_etx;
   Float_t         MET_Truth_IntOutCover_ety;
   Float_t         MET_Truth_IntMuons_etx;
   Float_t         MET_Truth_IntMuons_ety;
   Int_t           el_n;
   vector<float>   *el_E;
   vector<float>   *el_pt;
   vector<float>   *el_m;
   vector<float>   *el_px;
   vector<float>   *el_py;
   vector<float>   *el_pz;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<int>     *el_status;
   vector<int>     *el_barcode;
   vector<int>     *el_charge;
   Int_t           mu_muid_n;
   vector<float>   *mu_muid_E;
   vector<float>   *mu_muid_pt;
   vector<float>   *mu_muid_m;
   vector<float>   *mu_muid_px;
   vector<float>   *mu_muid_py;
   vector<float>   *mu_muid_pz;
   vector<float>   *mu_muid_eta;
   vector<float>   *mu_muid_phi;
   vector<int>     *mu_muid_status;
   vector<int>     *mu_muid_barcode;
   vector<int>     *mu_muid_charge;
   Int_t           mu_staco_n;
   vector<float>   *mu_staco_E;
   vector<float>   *mu_staco_pt;
   vector<float>   *mu_staco_m;
   vector<float>   *mu_staco_px;
   vector<float>   *mu_staco_py;
   vector<float>   *mu_staco_pz;
   vector<float>   *mu_staco_eta;
   vector<float>   *mu_staco_phi;
   vector<int>     *mu_staco_status;
   vector<int>     *mu_staco_barcode;
   vector<int>     *mu_staco_charge;
   Int_t           tau_n;
   vector<float>   *tau_pt;
   vector<float>   *tau_m;
   vector<float>   *tau_eta;
   vector<float>   *tau_phi;
   vector<int>     *tau_status;
   vector<int>     *tau_barcode;
   vector<int>     *tau_charge;
   Int_t           ph_n;
   vector<float>   *ph_E;
   vector<float>   *ph_pt;
   vector<float>   *ph_m;
   vector<float>   *ph_px;
   vector<float>   *ph_py;
   vector<float>   *ph_pz;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<int>     *ph_status;
   vector<int>     *ph_barcode;
   Int_t           el_truth_n;
   vector<float>   *el_truth_E;
   vector<float>   *el_truth_pt;
   vector<float>   *el_truth_m;
   vector<float>   *el_truth_px;
   vector<float>   *el_truth_py;
   vector<float>   *el_truth_pz;
   vector<float>   *el_truth_eta;
   vector<float>   *el_truth_phi;
   vector<int>     *el_truth_status;
   vector<int>     *el_truth_barcode;
   vector<int>     *el_truth_charge;
   Int_t           mu_muid_truth_n;
   vector<float>   *mu_muid_truth_E;
   vector<float>   *mu_muid_truth_pt;
   vector<float>   *mu_muid_truth_m;
   vector<float>   *mu_muid_truth_px;
   vector<float>   *mu_muid_truth_py;
   vector<float>   *mu_muid_truth_pz;
   vector<float>   *mu_muid_truth_eta;
   vector<float>   *mu_muid_truth_phi;
   vector<int>     *mu_muid_truth_status;
   vector<int>     *mu_muid_truth_barcode;
   vector<int>     *mu_muid_truth_charge;
   Int_t           mu_staco_truth_n;
   vector<float>   *mu_staco_truth_E;
   vector<float>   *mu_staco_truth_pt;
   vector<float>   *mu_staco_truth_m;
   vector<float>   *mu_staco_truth_px;
   vector<float>   *mu_staco_truth_py;
   vector<float>   *mu_staco_truth_pz;
   vector<float>   *mu_staco_truth_eta;
   vector<float>   *mu_staco_truth_phi;
   vector<int>     *mu_staco_truth_status;
   vector<int>     *mu_staco_truth_barcode;
   vector<int>     *mu_staco_truth_charge;
   Int_t           trueTau_n;
   vector<float>   *trueTau_pt;
   vector<float>   *trueTau_m;
   vector<float>   *trueTau_eta;
   vector<float>   *trueTau_phi;
   vector<int>     *trueTau_status;
   vector<int>     *trueTau_barcode;
   vector<int>     *trueTau_charge;
   Int_t           ph_truth_n;
   vector<float>   *ph_truth_E;
   vector<float>   *ph_truth_pt;
   vector<float>   *ph_truth_m;
   vector<float>   *ph_truth_px;
   vector<float>   *ph_truth_py;
   vector<float>   *ph_truth_pz;
   vector<float>   *ph_truth_eta;
   vector<float>   *ph_truth_phi;
   vector<int>     *ph_truth_status;
   vector<int>     *ph_truth_barcode;

   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_timestamp_ns;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_detmask0;   //!
   TBranch        *b_detmask1;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_mc_channel_number;   //!
   TBranch        *b_mc_event_number;   //!
   TBranch        *b_mc_event_weight;   //!
   TBranch        *b_pixelFlags;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_trtFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_muonFlags;   //!
   TBranch        *b_fwdFlags;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_pixelError;   //!
   TBranch        *b_sctError;   //!
   TBranch        *b_trtError;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_muonError;   //!
   TBranch        *b_fwdError;   //!
   TBranch        *b_coreError;   //!
   TBranch        *b_isSimulation;   //!
   TBranch        *b_isCalibration;   //!
   TBranch        *b_isTestBeam;   //!
   TBranch        *b_mcevt_n;   //!
   TBranch        *b_mcevt_signal_process_id;   //!
   TBranch        *b_mcevt_event_number;   //!
   TBranch        *b_mcevt_event_scale;   //!
   TBranch        *b_mcevt_alphaQCD;   //!
   TBranch        *b_mcevt_alphaQED;   //!
   TBranch        *b_mcevt_pdf_id1;   //!
   TBranch        *b_mcevt_pdf_id2;   //!
   TBranch        *b_mcevt_pdf_x1;   //!
   TBranch        *b_mcevt_pdf_x2;   //!
   TBranch        *b_mcevt_pdf_scale;   //!
   TBranch        *b_mcevt_pdf1;   //!
   TBranch        *b_mcevt_pdf2;   //!
   TBranch        *b_mcevt_weight;   //!
   TBranch        *b_mcevt_nparticle;   //!
   TBranch        *b_mcevt_pileUpType;   //!
   TBranch        *b_mcVxn;   //!
   TBranch        *b_mcVxx;   //!
   TBranch        *b_mcVxy;   //!
   TBranch        *b_mcVxz;   //!
   TBranch        *b_vxn;   //!
   TBranch        *b_vxx;   //!
   TBranch        *b_vxy;   //!
   TBranch        *b_vxz;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_n;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_E;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_pt;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_m;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_eta;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_phi;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_EtaOrigin;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_PhiOrigin;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_MOrigin;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_EtaOriginEM;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_PhiOriginEM;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_MOriginEM;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_WIDTH;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_n90;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_Timing;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_LArQuality;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_nTrk;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_sumPtTrk;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_OriginIndex;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_HECQuality;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_NegativeE;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_AverageLArQF;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_YFlip12;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_YFlip23;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_BCH_CORR_CELL;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_BCH_CORR_DOTX;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_BCH_CORR_JET;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_BCH_CORR_JET_FORCELL;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_ENG_BAD_CELLS;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_N_BAD_CELLS;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_N_BAD_CELLS_CORR;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_BAD_CELLS_CORR_E;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_NumTowers;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_SamplingMax;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_fracSamplingMax;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_hecf;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_tgap3f;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_isUgly;   //!
//   TBranch        *b_jet_AntiKt6TruthJets_WZ_isBadLooseMinus;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_isBadLoose;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_isBadMedium;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_isBadTight;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_emfrac;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_Offset;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_EMJES;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_EMJES_EtaCorr;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_EMJESnooffset;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_GCWJES;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_GCWJES_EtaCorr;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_CB;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_LCJES;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_emscale_E;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_emscale_pt;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_emscale_m;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_emscale_eta;   //!
   TBranch        *b_jet_AntiKt6TruthJets_WZ_emscale_phi;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_n;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_E;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_pt;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_m;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_eta;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_phi;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_EtaOrigin;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_PhiOrigin;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_MOrigin;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_EtaOriginEM;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_PhiOriginEM;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_MOriginEM;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_WIDTH;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_n90;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_Timing;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_LArQuality;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_nTrk;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_sumPtTrk;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_OriginIndex;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_HECQuality;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_NegativeE;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_AverageLArQF;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_YFlip12;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_YFlip23;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_BCH_CORR_CELL;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_BCH_CORR_DOTX;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_BCH_CORR_JET;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_BCH_CORR_JET_FORCELL;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_ENG_BAD_CELLS;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_N_BAD_CELLS;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_N_BAD_CELLS_CORR;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_BAD_CELLS_CORR_E;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_NumTowers;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_SamplingMax;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_fracSamplingMax;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_hecf;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_tgap3f;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_isUgly;   //!
//   TBranch        *b_jet_AntiKt6TopoNewEM_isBadLooseMinus;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_isBadLoose;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_isBadMedium;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_isBadTight;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_emfrac;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_Offset;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_EMJES;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_EMJES_EtaCorr;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_EMJESnooffset;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_GCWJES;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_GCWJES_EtaCorr;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_CB;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_LCJES;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_emscale_E;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_emscale_pt;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_emscale_m;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_emscale_eta;   //!
   TBranch        *b_jet_AntiKt6TopoNewEM_emscale_phi;   //!
   TBranch        *b_mc_n;   //!
   TBranch        *b_mc_E;   //!
   TBranch        *b_mc_pt;   //!
   TBranch        *b_mc_m;   //!
   TBranch        *b_mc_eta;   //!
   TBranch        *b_mc_phi;   //!
   TBranch        *b_mc_px;   //!
   TBranch        *b_mc_py;   //!
//   TBranch        *b_mc_pz;   //!
   TBranch        *b_mc_status;   //!
   TBranch        *b_mc_barcode;   //!
   TBranch        *b_mc_pdgId;   //!
   TBranch        *b_mc_charge;   //!
   TBranch        *b_mc_vx_x;   //!
   TBranch        *b_mc_vx_y;   //!
   TBranch        *b_mc_vx_z;   //!
   TBranch        *b_mc_vx_barcode;   //!
   TBranch        *b_mc_child_index;   //!
   TBranch        *b_mc_parent_index;   //!
   TBranch        *b_MET_Truth_NonInt_etx;   //!
   TBranch        *b_MET_Truth_NonInt_ety;   //!
   TBranch        *b_MET_Truth_Int_etx;   //!
   TBranch        *b_MET_Truth_Int_ety;   //!
   TBranch        *b_MET_Truth_IntCentral_etx;   //!
   TBranch        *b_MET_Truth_IntCentral_ety;   //!
   TBranch        *b_MET_Truth_IntFwd_etx;   //!
   TBranch        *b_MET_Truth_IntFwd_ety;   //!
   TBranch        *b_MET_Truth_IntOutCover_etx;   //!
   TBranch        *b_MET_Truth_IntOutCover_ety;   //!
   TBranch        *b_MET_Truth_IntMuons_etx;   //!
   TBranch        *b_MET_Truth_IntMuons_ety;   //!
   TBranch        *b_el_n;   //!
   TBranch        *b_el_E;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_m;   //!
   TBranch        *b_el_px;   //!
   TBranch        *b_el_py;   //!
   TBranch        *b_el_pz;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_status;   //!
   TBranch        *b_el_barcode;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_mu_muid_n;   //!
   TBranch        *b_mu_muid_E;   //!
   TBranch        *b_mu_muid_pt;   //!
   TBranch        *b_mu_muid_m;   //!
   TBranch        *b_mu_muid_px;   //!
   TBranch        *b_mu_muid_py;   //!
   TBranch        *b_mu_muid_pz;   //!
   TBranch        *b_mu_muid_eta;   //!
   TBranch        *b_mu_muid_phi;   //!
   TBranch        *b_mu_muid_status;   //!
   TBranch        *b_mu_muid_barcode;   //!
   TBranch        *b_mu_muid_charge;   //!
   TBranch        *b_mu_staco_n;   //!
   TBranch        *b_mu_staco_E;   //!
   TBranch        *b_mu_staco_pt;   //!
   TBranch        *b_mu_staco_m;   //!
   TBranch        *b_mu_staco_px;   //!
   TBranch        *b_mu_staco_py;   //!
   TBranch        *b_mu_staco_pz;   //!
   TBranch        *b_mu_staco_eta;   //!
   TBranch        *b_mu_staco_phi;   //!
   TBranch        *b_mu_staco_status;   //!
   TBranch        *b_mu_staco_barcode;   //!
   TBranch        *b_mu_staco_charge;   //!
   TBranch        *b_tau_n;   //!
   TBranch        *b_tau_pt;   //!
   TBranch        *b_tau_m;   //!
   TBranch        *b_tau_eta;   //!
   TBranch        *b_tau_phi;   //!
   TBranch        *b_tau_status;   //!
   TBranch        *b_tau_barcode;   //!
   TBranch        *b_tau_charge;   //!
   TBranch        *b_ph_n;   //!
   TBranch        *b_ph_E;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_m;   //!
   TBranch        *b_ph_px;   //!
   TBranch        *b_ph_py;   //!
   TBranch        *b_ph_pz;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_status;   //!
   TBranch        *b_ph_barcode;   //!
   TBranch        *b_el_truth_n;   //!
   TBranch        *b_el_truth_E;   //!
   TBranch        *b_el_truth_pt;   //!
   TBranch        *b_el_truth_m;   //!
   TBranch        *b_el_truth_px;   //!
   TBranch        *b_el_truth_py;   //!
   TBranch        *b_el_truth_pz;   //!
   TBranch        *b_el_truth_eta;   //!
   TBranch        *b_el_truth_phi;   //!
   TBranch        *b_el_truth_status;   //!
   TBranch        *b_el_truth_barcode;   //!
   TBranch        *b_el_truth_charge;   //!
   TBranch        *b_mu_muid_truth_n;   //!
   TBranch        *b_mu_muid_truth_E;   //!
   TBranch        *b_mu_muid_truth_pt;   //!
   TBranch        *b_mu_muid_truth_m;   //!
   TBranch        *b_mu_muid_truth_px;   //!
   TBranch        *b_mu_muid_truth_py;   //!
   TBranch        *b_mu_muid_truth_pz;   //!
   TBranch        *b_mu_muid_truth_eta;   //!
   TBranch        *b_mu_muid_truth_phi;   //!
   TBranch        *b_mu_muid_truth_status;   //!
   TBranch        *b_mu_muid_truth_barcode;   //!
   TBranch        *b_mu_muid_truth_charge;   //!
   TBranch        *b_mu_staco_truth_n;   //!
   TBranch        *b_mu_staco_truth_E;   //!
   TBranch        *b_mu_staco_truth_pt;   //!
   TBranch        *b_mu_staco_truth_m;   //!
   TBranch        *b_mu_staco_truth_px;   //!
   TBranch        *b_mu_staco_truth_py;   //!
   TBranch        *b_mu_staco_truth_pz;   //!
   TBranch        *b_mu_staco_truth_eta;   //!
   TBranch        *b_mu_staco_truth_phi;   //!
   TBranch        *b_mu_staco_truth_status;   //!
   TBranch        *b_mu_staco_truth_barcode;   //!
   TBranch        *b_mu_staco_truth_charge;   //!
   TBranch        *b_trueTau_n;   //!
   TBranch        *b_trueTau_pt;   //!
   TBranch        *b_trueTau_m;   //!
   TBranch        *b_trueTau_eta;   //!
   TBranch        *b_trueTau_phi;   //!
   TBranch        *b_trueTau_status;   //!
   TBranch        *b_trueTau_barcode;   //!
   TBranch        *b_trueTau_charge;   //!
   TBranch        *b_ph_truth_n;   //!
   TBranch        *b_ph_truth_E;   //!
   TBranch        *b_ph_truth_pt;   //!
   TBranch        *b_ph_truth_m;   //!
   TBranch        *b_ph_truth_px;   //!
   TBranch        *b_ph_truth_py;   //!
   TBranch        *b_ph_truth_pz;   //!
   TBranch        *b_ph_truth_eta;   //!
   TBranch        *b_ph_truth_phi;   //!
   TBranch        *b_ph_truth_status;   //!
   TBranch        *b_ph_truth_barcode;   //!

   MCValidation(TTree * /*tree*/ =0, TString name="3000", int mass=3000, TString outputDir="", bool useSmearing=false) : fChain(0) { m_name = name; m_mass = mass; m_outputDir=outputDir; m_useSmearing=useSmearing; }
   virtual ~MCValidation() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();
   TString m_name;
   TString m_outputDir;
   bool m_useSmearing;
   int m_mass;
   
   ClassDef(MCValidation,0);
   
   //histograms
   TH1D *h_mass;
   TH1D *h_mass_detector;
   TH1D *h_mass_noCut;
   TH1D *h_pT; 
   TH1D *h_eta; 
   TH1D *h_phi; 
   TH1D *h_ystar; 
   TH1D *h_nJets; 
   TH1D *h_nJets_cut;

   TRandom3 r_randomGenerator;
   
};

#endif

#ifdef MCValidation_cxx
void MCValidation::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mcevt_signal_process_id = 0;
   mcevt_event_number = 0;
   mcevt_event_scale = 0;
   mcevt_alphaQCD = 0;
   mcevt_alphaQED = 0;
   mcevt_pdf_id1 = 0;
   mcevt_pdf_id2 = 0;
   mcevt_pdf_x1 = 0;
   mcevt_pdf_x2 = 0;
   mcevt_pdf_scale = 0;
   mcevt_pdf1 = 0;
   mcevt_pdf2 = 0;
   mcevt_weight = 0;
   mcevt_nparticle = 0;
   mcevt_pileUpType = 0;
   mcVxx = 0;
   mcVxy = 0;
   mcVxz = 0;
   vxx = 0;
   vxy = 0;
   vxz = 0;
   jet_AntiKt6TruthJets_WZ_E = 0;
   jet_AntiKt6TruthJets_WZ_pt = 0;
   jet_AntiKt6TruthJets_WZ_m = 0;
   jet_AntiKt6TruthJets_WZ_eta = 0;
   jet_AntiKt6TruthJets_WZ_phi = 0;
   jet_AntiKt6TruthJets_WZ_EtaOrigin = 0;
   jet_AntiKt6TruthJets_WZ_PhiOrigin = 0;
   jet_AntiKt6TruthJets_WZ_MOrigin = 0;
   jet_AntiKt6TruthJets_WZ_EtaOriginEM = 0;
   jet_AntiKt6TruthJets_WZ_PhiOriginEM = 0;
   jet_AntiKt6TruthJets_WZ_MOriginEM = 0;
   jet_AntiKt6TruthJets_WZ_WIDTH = 0;
   jet_AntiKt6TruthJets_WZ_n90 = 0;
   jet_AntiKt6TruthJets_WZ_Timing = 0;
   jet_AntiKt6TruthJets_WZ_LArQuality = 0;
   jet_AntiKt6TruthJets_WZ_nTrk = 0;
   jet_AntiKt6TruthJets_WZ_sumPtTrk = 0;
   jet_AntiKt6TruthJets_WZ_OriginIndex = 0;
   jet_AntiKt6TruthJets_WZ_HECQuality = 0;
   jet_AntiKt6TruthJets_WZ_NegativeE = 0;
   jet_AntiKt6TruthJets_WZ_AverageLArQF = 0;
   jet_AntiKt6TruthJets_WZ_YFlip12 = 0;
   jet_AntiKt6TruthJets_WZ_YFlip23 = 0;
   jet_AntiKt6TruthJets_WZ_BCH_CORR_CELL = 0;
   jet_AntiKt6TruthJets_WZ_BCH_CORR_DOTX = 0;
   jet_AntiKt6TruthJets_WZ_BCH_CORR_JET = 0;
   jet_AntiKt6TruthJets_WZ_BCH_CORR_JET_FORCELL = 0;
   jet_AntiKt6TruthJets_WZ_ENG_BAD_CELLS = 0;
   jet_AntiKt6TruthJets_WZ_N_BAD_CELLS = 0;
   jet_AntiKt6TruthJets_WZ_N_BAD_CELLS_CORR = 0;
   jet_AntiKt6TruthJets_WZ_BAD_CELLS_CORR_E = 0;
   jet_AntiKt6TruthJets_WZ_NumTowers = 0;
   jet_AntiKt6TruthJets_WZ_SamplingMax = 0;
   jet_AntiKt6TruthJets_WZ_fracSamplingMax = 0;
   jet_AntiKt6TruthJets_WZ_hecf = 0;
   jet_AntiKt6TruthJets_WZ_tgap3f = 0;
   jet_AntiKt6TruthJets_WZ_isUgly = 0;
//   jet_AntiKt6TruthJets_WZ_isBadLooseMinus = 0;
   jet_AntiKt6TruthJets_WZ_isBadLoose = 0;
   jet_AntiKt6TruthJets_WZ_isBadMedium = 0;
   jet_AntiKt6TruthJets_WZ_isBadTight = 0;
   jet_AntiKt6TruthJets_WZ_emfrac = 0;
   jet_AntiKt6TruthJets_WZ_Offset = 0;
   jet_AntiKt6TruthJets_WZ_EMJES = 0;
   jet_AntiKt6TruthJets_WZ_EMJES_EtaCorr = 0;
   jet_AntiKt6TruthJets_WZ_EMJESnooffset = 0;
   jet_AntiKt6TruthJets_WZ_GCWJES = 0;
   jet_AntiKt6TruthJets_WZ_GCWJES_EtaCorr = 0;
   jet_AntiKt6TruthJets_WZ_CB = 0;
   jet_AntiKt6TruthJets_WZ_LCJES = 0;
   jet_AntiKt6TruthJets_WZ_emscale_E = 0;
   jet_AntiKt6TruthJets_WZ_emscale_pt = 0;
   jet_AntiKt6TruthJets_WZ_emscale_m = 0;
   jet_AntiKt6TruthJets_WZ_emscale_eta = 0;
   jet_AntiKt6TruthJets_WZ_emscale_phi = 0;
   jet_AntiKt6TopoNewEM_E = 0;
   jet_AntiKt6TopoNewEM_pt = 0;
   jet_AntiKt6TopoNewEM_m = 0;
   jet_AntiKt6TopoNewEM_eta = 0;
   jet_AntiKt6TopoNewEM_phi = 0;
   jet_AntiKt6TopoNewEM_EtaOrigin = 0;
   jet_AntiKt6TopoNewEM_PhiOrigin = 0;
   jet_AntiKt6TopoNewEM_MOrigin = 0;
   jet_AntiKt6TopoNewEM_EtaOriginEM = 0;
   jet_AntiKt6TopoNewEM_PhiOriginEM = 0;
   jet_AntiKt6TopoNewEM_MOriginEM = 0;
   jet_AntiKt6TopoNewEM_WIDTH = 0;
   jet_AntiKt6TopoNewEM_n90 = 0;
   jet_AntiKt6TopoNewEM_Timing = 0;
   jet_AntiKt6TopoNewEM_LArQuality = 0;
   jet_AntiKt6TopoNewEM_nTrk = 0;
   jet_AntiKt6TopoNewEM_sumPtTrk = 0;
   jet_AntiKt6TopoNewEM_OriginIndex = 0;
   jet_AntiKt6TopoNewEM_HECQuality = 0;
   jet_AntiKt6TopoNewEM_NegativeE = 0;
   jet_AntiKt6TopoNewEM_AverageLArQF = 0;
   jet_AntiKt6TopoNewEM_YFlip12 = 0;
   jet_AntiKt6TopoNewEM_YFlip23 = 0;
   jet_AntiKt6TopoNewEM_BCH_CORR_CELL = 0;
   jet_AntiKt6TopoNewEM_BCH_CORR_DOTX = 0;
   jet_AntiKt6TopoNewEM_BCH_CORR_JET = 0;
   jet_AntiKt6TopoNewEM_BCH_CORR_JET_FORCELL = 0;
   jet_AntiKt6TopoNewEM_ENG_BAD_CELLS = 0;
   jet_AntiKt6TopoNewEM_N_BAD_CELLS = 0;
   jet_AntiKt6TopoNewEM_N_BAD_CELLS_CORR = 0;
   jet_AntiKt6TopoNewEM_BAD_CELLS_CORR_E = 0;
   jet_AntiKt6TopoNewEM_NumTowers = 0;
   jet_AntiKt6TopoNewEM_SamplingMax = 0;
   jet_AntiKt6TopoNewEM_fracSamplingMax = 0;
   jet_AntiKt6TopoNewEM_hecf = 0;
   jet_AntiKt6TopoNewEM_tgap3f = 0;
   jet_AntiKt6TopoNewEM_isUgly = 0;
//   jet_AntiKt6TopoNewEM_isBadLooseMinus = 0;
   jet_AntiKt6TopoNewEM_isBadLoose = 0;
   jet_AntiKt6TopoNewEM_isBadMedium = 0;
   jet_AntiKt6TopoNewEM_isBadTight = 0;
   jet_AntiKt6TopoNewEM_emfrac = 0;
   jet_AntiKt6TopoNewEM_Offset = 0;
   jet_AntiKt6TopoNewEM_EMJES = 0;
   jet_AntiKt6TopoNewEM_EMJES_EtaCorr = 0;
   jet_AntiKt6TopoNewEM_EMJESnooffset = 0;
   jet_AntiKt6TopoNewEM_GCWJES = 0;
   jet_AntiKt6TopoNewEM_GCWJES_EtaCorr = 0;
   jet_AntiKt6TopoNewEM_CB = 0;
   jet_AntiKt6TopoNewEM_LCJES = 0;
   jet_AntiKt6TopoNewEM_emscale_E = 0;
   jet_AntiKt6TopoNewEM_emscale_pt = 0;
   jet_AntiKt6TopoNewEM_emscale_m = 0;
   jet_AntiKt6TopoNewEM_emscale_eta = 0;
   jet_AntiKt6TopoNewEM_emscale_phi = 0;
   mc_E = 0;
   mc_pt = 0;
   mc_m = 0;
   mc_eta = 0;
   mc_phi = 0;
   mc_px = 0;
   mc_py = 0;
//   mc_pz = 0;
   mc_status = 0;
   mc_barcode = 0;
   mc_pdgId = 0;
   mc_charge = 0;
   mc_vx_x = 0;
   mc_vx_y = 0;
   mc_vx_z = 0;
   mc_vx_barcode = 0;
   mc_child_index = 0;
   mc_parent_index = 0;
   el_E = 0;
   el_pt = 0;
   el_m = 0;
   el_px = 0;
   el_py = 0;
   el_pz = 0;
   el_eta = 0;
   el_phi = 0;
   el_status = 0;
   el_barcode = 0;
   el_charge = 0;
   mu_muid_E = 0;
   mu_muid_pt = 0;
   mu_muid_m = 0;
   mu_muid_px = 0;
   mu_muid_py = 0;
   mu_muid_pz = 0;
   mu_muid_eta = 0;
   mu_muid_phi = 0;
   mu_muid_status = 0;
   mu_muid_barcode = 0;
   mu_muid_charge = 0;
   mu_staco_E = 0;
   mu_staco_pt = 0;
   mu_staco_m = 0;
   mu_staco_px = 0;
   mu_staco_py = 0;
   mu_staco_pz = 0;
   mu_staco_eta = 0;
   mu_staco_phi = 0;
   mu_staco_status = 0;
   mu_staco_barcode = 0;
   mu_staco_charge = 0;
   tau_pt = 0;
   tau_m = 0;
   tau_eta = 0;
   tau_phi = 0;
   tau_status = 0;
   tau_barcode = 0;
   tau_charge = 0;
   ph_E = 0;
   ph_pt = 0;
   ph_m = 0;
   ph_px = 0;
   ph_py = 0;
   ph_pz = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_status = 0;
   ph_barcode = 0;
   el_truth_E = 0;
   el_truth_pt = 0;
   el_truth_m = 0;
   el_truth_px = 0;
   el_truth_py = 0;
   el_truth_pz = 0;
   el_truth_eta = 0;
   el_truth_phi = 0;
   el_truth_status = 0;
   el_truth_barcode = 0;
   el_truth_charge = 0;
   mu_muid_truth_E = 0;
   mu_muid_truth_pt = 0;
   mu_muid_truth_m = 0;
   mu_muid_truth_px = 0;
   mu_muid_truth_py = 0;
   mu_muid_truth_pz = 0;
   mu_muid_truth_eta = 0;
   mu_muid_truth_phi = 0;
   mu_muid_truth_status = 0;
   mu_muid_truth_barcode = 0;
   mu_muid_truth_charge = 0;
   mu_staco_truth_E = 0;
   mu_staco_truth_pt = 0;
   mu_staco_truth_m = 0;
   mu_staco_truth_px = 0;
   mu_staco_truth_py = 0;
   mu_staco_truth_pz = 0;
   mu_staco_truth_eta = 0;
   mu_staco_truth_phi = 0;
   mu_staco_truth_status = 0;
   mu_staco_truth_barcode = 0;
   mu_staco_truth_charge = 0;
   trueTau_pt = 0;
   trueTau_m = 0;
   trueTau_eta = 0;
   trueTau_phi = 0;
   trueTau_status = 0;
   trueTau_barcode = 0;
   trueTau_charge = 0;
   ph_truth_E = 0;
   ph_truth_pt = 0;
   ph_truth_m = 0;
   ph_truth_px = 0;
   ph_truth_py = 0;
   ph_truth_pz = 0;
   ph_truth_eta = 0;
   ph_truth_phi = 0;
   ph_truth_status = 0;
   ph_truth_barcode = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
/*   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("timestamp_ns", &timestamp_ns, &b_timestamp_ns);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
   fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);*/
   fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
   fChain->SetBranchAddress("mc_event_number", &mc_event_number, &b_mc_event_number);
   fChain->SetBranchAddress("mc_event_weight", &mc_event_weight, &b_mc_event_weight);
/*   fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
   fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
   fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
   fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
   fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
   fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
   fChain->SetBranchAddress("isSimulation", &isSimulation, &b_isSimulation);
   fChain->SetBranchAddress("isCalibration", &isCalibration, &b_isCalibration);
   fChain->SetBranchAddress("isTestBeam", &isTestBeam, &b_isTestBeam);*/
   fChain->SetBranchAddress("mcevt_n", &mcevt_n, &b_mcevt_n);
   fChain->SetBranchAddress("mcevt_signal_process_id", &mcevt_signal_process_id, &b_mcevt_signal_process_id);
   fChain->SetBranchAddress("mcevt_event_number", &mcevt_event_number, &b_mcevt_event_number);
   fChain->SetBranchAddress("mcevt_event_scale", &mcevt_event_scale, &b_mcevt_event_scale);
   fChain->SetBranchAddress("mcevt_alphaQCD", &mcevt_alphaQCD, &b_mcevt_alphaQCD);
   fChain->SetBranchAddress("mcevt_alphaQED", &mcevt_alphaQED, &b_mcevt_alphaQED);
   fChain->SetBranchAddress("mcevt_pdf_id1", &mcevt_pdf_id1, &b_mcevt_pdf_id1);
   fChain->SetBranchAddress("mcevt_pdf_id2", &mcevt_pdf_id2, &b_mcevt_pdf_id2);
   fChain->SetBranchAddress("mcevt_pdf_x1", &mcevt_pdf_x1, &b_mcevt_pdf_x1);
   fChain->SetBranchAddress("mcevt_pdf_x2", &mcevt_pdf_x2, &b_mcevt_pdf_x2);
   fChain->SetBranchAddress("mcevt_pdf_scale", &mcevt_pdf_scale, &b_mcevt_pdf_scale);
   fChain->SetBranchAddress("mcevt_pdf1", &mcevt_pdf1, &b_mcevt_pdf1);
   fChain->SetBranchAddress("mcevt_pdf2", &mcevt_pdf2, &b_mcevt_pdf2);
   fChain->SetBranchAddress("mcevt_weight", &mcevt_weight, &b_mcevt_weight);
/*   fChain->SetBranchAddress("mcevt_nparticle", &mcevt_nparticle, &b_mcevt_nparticle);
   fChain->SetBranchAddress("mcevt_pileUpType", &mcevt_pileUpType, &b_mcevt_pileUpType);
   fChain->SetBranchAddress("mcVxn", &mcVxn, &b_mcVxn);
   fChain->SetBranchAddress("mcVxx", &mcVxx, &b_mcVxx);
   fChain->SetBranchAddress("mcVxy", &mcVxy, &b_mcVxy);
   fChain->SetBranchAddress("mcVxz", &mcVxz, &b_mcVxz);
   fChain->SetBranchAddress("vxn", &vxn, &b_vxn);
   fChain->SetBranchAddress("vxx", &vxx, &b_vxx);
   fChain->SetBranchAddress("vxy", &vxy, &b_vxy);
   fChain->SetBranchAddress("vxz", &vxz, &b_vxz);*/
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_n", &jet_AntiKt6TruthJets_WZ_n, &b_jet_AntiKt6TruthJets_WZ_n);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_E", &jet_AntiKt6TruthJets_WZ_E, &b_jet_AntiKt6TruthJets_WZ_E);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_pt", &jet_AntiKt6TruthJets_WZ_pt, &b_jet_AntiKt6TruthJets_WZ_pt);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_m", &jet_AntiKt6TruthJets_WZ_m, &b_jet_AntiKt6TruthJets_WZ_m);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_eta", &jet_AntiKt6TruthJets_WZ_eta, &b_jet_AntiKt6TruthJets_WZ_eta);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_phi", &jet_AntiKt6TruthJets_WZ_phi, &b_jet_AntiKt6TruthJets_WZ_phi);
/*   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_EtaOrigin", &jet_AntiKt6TruthJets_WZ_EtaOrigin, &b_jet_AntiKt6TruthJets_WZ_EtaOrigin);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_PhiOrigin", &jet_AntiKt6TruthJets_WZ_PhiOrigin, &b_jet_AntiKt6TruthJets_WZ_PhiOrigin);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_MOrigin", &jet_AntiKt6TruthJets_WZ_MOrigin, &b_jet_AntiKt6TruthJets_WZ_MOrigin);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_EtaOriginEM", &jet_AntiKt6TruthJets_WZ_EtaOriginEM, &b_jet_AntiKt6TruthJets_WZ_EtaOriginEM);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_PhiOriginEM", &jet_AntiKt6TruthJets_WZ_PhiOriginEM, &b_jet_AntiKt6TruthJets_WZ_PhiOriginEM);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_MOriginEM", &jet_AntiKt6TruthJets_WZ_MOriginEM, &b_jet_AntiKt6TruthJets_WZ_MOriginEM);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_WIDTH", &jet_AntiKt6TruthJets_WZ_WIDTH, &b_jet_AntiKt6TruthJets_WZ_WIDTH);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_n90", &jet_AntiKt6TruthJets_WZ_n90, &b_jet_AntiKt6TruthJets_WZ_n90);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_Timing", &jet_AntiKt6TruthJets_WZ_Timing, &b_jet_AntiKt6TruthJets_WZ_Timing);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_LArQuality", &jet_AntiKt6TruthJets_WZ_LArQuality, &b_jet_AntiKt6TruthJets_WZ_LArQuality);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_nTrk", &jet_AntiKt6TruthJets_WZ_nTrk, &b_jet_AntiKt6TruthJets_WZ_nTrk);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_sumPtTrk", &jet_AntiKt6TruthJets_WZ_sumPtTrk, &b_jet_AntiKt6TruthJets_WZ_sumPtTrk);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_OriginIndex", &jet_AntiKt6TruthJets_WZ_OriginIndex, &b_jet_AntiKt6TruthJets_WZ_OriginIndex);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_HECQuality", &jet_AntiKt6TruthJets_WZ_HECQuality, &b_jet_AntiKt6TruthJets_WZ_HECQuality);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_NegativeE", &jet_AntiKt6TruthJets_WZ_NegativeE, &b_jet_AntiKt6TruthJets_WZ_NegativeE);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_AverageLArQF", &jet_AntiKt6TruthJets_WZ_AverageLArQF, &b_jet_AntiKt6TruthJets_WZ_AverageLArQF);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_YFlip12", &jet_AntiKt6TruthJets_WZ_YFlip12, &b_jet_AntiKt6TruthJets_WZ_YFlip12);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_YFlip23", &jet_AntiKt6TruthJets_WZ_YFlip23, &b_jet_AntiKt6TruthJets_WZ_YFlip23);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_BCH_CORR_CELL", &jet_AntiKt6TruthJets_WZ_BCH_CORR_CELL, &b_jet_AntiKt6TruthJets_WZ_BCH_CORR_CELL);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_BCH_CORR_DOTX", &jet_AntiKt6TruthJets_WZ_BCH_CORR_DOTX, &b_jet_AntiKt6TruthJets_WZ_BCH_CORR_DOTX);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_BCH_CORR_JET", &jet_AntiKt6TruthJets_WZ_BCH_CORR_JET, &b_jet_AntiKt6TruthJets_WZ_BCH_CORR_JET);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_BCH_CORR_JET_FORCELL", &jet_AntiKt6TruthJets_WZ_BCH_CORR_JET_FORCELL, &b_jet_AntiKt6TruthJets_WZ_BCH_CORR_JET_FORCELL);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_ENG_BAD_CELLS", &jet_AntiKt6TruthJets_WZ_ENG_BAD_CELLS, &b_jet_AntiKt6TruthJets_WZ_ENG_BAD_CELLS);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_N_BAD_CELLS", &jet_AntiKt6TruthJets_WZ_N_BAD_CELLS, &b_jet_AntiKt6TruthJets_WZ_N_BAD_CELLS);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_N_BAD_CELLS_CORR", &jet_AntiKt6TruthJets_WZ_N_BAD_CELLS_CORR, &b_jet_AntiKt6TruthJets_WZ_N_BAD_CELLS_CORR);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_BAD_CELLS_CORR_E", &jet_AntiKt6TruthJets_WZ_BAD_CELLS_CORR_E, &b_jet_AntiKt6TruthJets_WZ_BAD_CELLS_CORR_E);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_NumTowers", &jet_AntiKt6TruthJets_WZ_NumTowers, &b_jet_AntiKt6TruthJets_WZ_NumTowers);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_SamplingMax", &jet_AntiKt6TruthJets_WZ_SamplingMax, &b_jet_AntiKt6TruthJets_WZ_SamplingMax);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_fracSamplingMax", &jet_AntiKt6TruthJets_WZ_fracSamplingMax, &b_jet_AntiKt6TruthJets_WZ_fracSamplingMax);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_hecf", &jet_AntiKt6TruthJets_WZ_hecf, &b_jet_AntiKt6TruthJets_WZ_hecf);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_tgap3f", &jet_AntiKt6TruthJets_WZ_tgap3f, &b_jet_AntiKt6TruthJets_WZ_tgap3f);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_isUgly", &jet_AntiKt6TruthJets_WZ_isUgly, &b_jet_AntiKt6TruthJets_WZ_isUgly);
//   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_isBadLooseMinus", &jet_AntiKt6TruthJets_WZ_isBadLooseMinus, &b_jet_AntiKt6TruthJets_WZ_isBadLooseMinus);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_isBadLoose", &jet_AntiKt6TruthJets_WZ_isBadLoose, &b_jet_AntiKt6TruthJets_WZ_isBadLoose);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_isBadMedium", &jet_AntiKt6TruthJets_WZ_isBadMedium, &b_jet_AntiKt6TruthJets_WZ_isBadMedium);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_isBadTight", &jet_AntiKt6TruthJets_WZ_isBadTight, &b_jet_AntiKt6TruthJets_WZ_isBadTight);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_emfrac", &jet_AntiKt6TruthJets_WZ_emfrac, &b_jet_AntiKt6TruthJets_WZ_emfrac);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_Offset", &jet_AntiKt6TruthJets_WZ_Offset, &b_jet_AntiKt6TruthJets_WZ_Offset);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_EMJES", &jet_AntiKt6TruthJets_WZ_EMJES, &b_jet_AntiKt6TruthJets_WZ_EMJES);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_EMJES_EtaCorr", &jet_AntiKt6TruthJets_WZ_EMJES_EtaCorr, &b_jet_AntiKt6TruthJets_WZ_EMJES_EtaCorr);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_EMJESnooffset", &jet_AntiKt6TruthJets_WZ_EMJESnooffset, &b_jet_AntiKt6TruthJets_WZ_EMJESnooffset);
//   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_GCWJES", &jet_AntiKt6TruthJets_WZ_GCWJES, &b_jet_AntiKt6TruthJets_WZ_GCWJES);
//   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_GCWJES_EtaCorr", &jet_AntiKt6TruthJets_WZ_GCWJES_EtaCorr, &b_jet_AntiKt6TruthJets_WZ_GCWJES_EtaCorr);
//   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_CB", &jet_AntiKt6TruthJets_WZ_CB, &b_jet_AntiKt6TruthJets_WZ_CB);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_LCJES", &jet_AntiKt6TruthJets_WZ_LCJES, &b_jet_AntiKt6TruthJets_WZ_LCJES);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_emscale_E", &jet_AntiKt6TruthJets_WZ_emscale_E, &b_jet_AntiKt6TruthJets_WZ_emscale_E);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_emscale_pt", &jet_AntiKt6TruthJets_WZ_emscale_pt, &b_jet_AntiKt6TruthJets_WZ_emscale_pt);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_emscale_m", &jet_AntiKt6TruthJets_WZ_emscale_m, &b_jet_AntiKt6TruthJets_WZ_emscale_m);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_emscale_eta", &jet_AntiKt6TruthJets_WZ_emscale_eta, &b_jet_AntiKt6TruthJets_WZ_emscale_eta);
   fChain->SetBranchAddress("jet_AntiKt6TruthJets_WZ_emscale_phi", &jet_AntiKt6TruthJets_WZ_emscale_phi, &b_jet_AntiKt6TruthJets_WZ_emscale_phi);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_n", &jet_AntiKt6TopoNewEM_n, &b_jet_AntiKt6TopoNewEM_n);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_E", &jet_AntiKt6TopoNewEM_E, &b_jet_AntiKt6TopoNewEM_E);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_pt", &jet_AntiKt6TopoNewEM_pt, &b_jet_AntiKt6TopoNewEM_pt);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_m", &jet_AntiKt6TopoNewEM_m, &b_jet_AntiKt6TopoNewEM_m);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_eta", &jet_AntiKt6TopoNewEM_eta, &b_jet_AntiKt6TopoNewEM_eta);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_phi", &jet_AntiKt6TopoNewEM_phi, &b_jet_AntiKt6TopoNewEM_phi);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_EtaOrigin", &jet_AntiKt6TopoNewEM_EtaOrigin, &b_jet_AntiKt6TopoNewEM_EtaOrigin);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_PhiOrigin", &jet_AntiKt6TopoNewEM_PhiOrigin, &b_jet_AntiKt6TopoNewEM_PhiOrigin);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_MOrigin", &jet_AntiKt6TopoNewEM_MOrigin, &b_jet_AntiKt6TopoNewEM_MOrigin);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_EtaOriginEM", &jet_AntiKt6TopoNewEM_EtaOriginEM, &b_jet_AntiKt6TopoNewEM_EtaOriginEM);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_PhiOriginEM", &jet_AntiKt6TopoNewEM_PhiOriginEM, &b_jet_AntiKt6TopoNewEM_PhiOriginEM);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_MOriginEM", &jet_AntiKt6TopoNewEM_MOriginEM, &b_jet_AntiKt6TopoNewEM_MOriginEM);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_WIDTH", &jet_AntiKt6TopoNewEM_WIDTH, &b_jet_AntiKt6TopoNewEM_WIDTH);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_n90", &jet_AntiKt6TopoNewEM_n90, &b_jet_AntiKt6TopoNewEM_n90);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_Timing", &jet_AntiKt6TopoNewEM_Timing, &b_jet_AntiKt6TopoNewEM_Timing);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_LArQuality", &jet_AntiKt6TopoNewEM_LArQuality, &b_jet_AntiKt6TopoNewEM_LArQuality);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_nTrk", &jet_AntiKt6TopoNewEM_nTrk, &b_jet_AntiKt6TopoNewEM_nTrk);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_sumPtTrk", &jet_AntiKt6TopoNewEM_sumPtTrk, &b_jet_AntiKt6TopoNewEM_sumPtTrk);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_OriginIndex", &jet_AntiKt6TopoNewEM_OriginIndex, &b_jet_AntiKt6TopoNewEM_OriginIndex);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_HECQuality", &jet_AntiKt6TopoNewEM_HECQuality, &b_jet_AntiKt6TopoNewEM_HECQuality);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_NegativeE", &jet_AntiKt6TopoNewEM_NegativeE, &b_jet_AntiKt6TopoNewEM_NegativeE);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_AverageLArQF", &jet_AntiKt6TopoNewEM_AverageLArQF, &b_jet_AntiKt6TopoNewEM_AverageLArQF);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_YFlip12", &jet_AntiKt6TopoNewEM_YFlip12, &b_jet_AntiKt6TopoNewEM_YFlip12);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_YFlip23", &jet_AntiKt6TopoNewEM_YFlip23, &b_jet_AntiKt6TopoNewEM_YFlip23);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_BCH_CORR_CELL", &jet_AntiKt6TopoNewEM_BCH_CORR_CELL, &b_jet_AntiKt6TopoNewEM_BCH_CORR_CELL);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_BCH_CORR_DOTX", &jet_AntiKt6TopoNewEM_BCH_CORR_DOTX, &b_jet_AntiKt6TopoNewEM_BCH_CORR_DOTX);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_BCH_CORR_JET", &jet_AntiKt6TopoNewEM_BCH_CORR_JET, &b_jet_AntiKt6TopoNewEM_BCH_CORR_JET);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_BCH_CORR_JET_FORCELL", &jet_AntiKt6TopoNewEM_BCH_CORR_JET_FORCELL, &b_jet_AntiKt6TopoNewEM_BCH_CORR_JET_FORCELL);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_ENG_BAD_CELLS", &jet_AntiKt6TopoNewEM_ENG_BAD_CELLS, &b_jet_AntiKt6TopoNewEM_ENG_BAD_CELLS);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_N_BAD_CELLS", &jet_AntiKt6TopoNewEM_N_BAD_CELLS, &b_jet_AntiKt6TopoNewEM_N_BAD_CELLS);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_N_BAD_CELLS_CORR", &jet_AntiKt6TopoNewEM_N_BAD_CELLS_CORR, &b_jet_AntiKt6TopoNewEM_N_BAD_CELLS_CORR);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_BAD_CELLS_CORR_E", &jet_AntiKt6TopoNewEM_BAD_CELLS_CORR_E, &b_jet_AntiKt6TopoNewEM_BAD_CELLS_CORR_E);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_NumTowers", &jet_AntiKt6TopoNewEM_NumTowers, &b_jet_AntiKt6TopoNewEM_NumTowers);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_SamplingMax", &jet_AntiKt6TopoNewEM_SamplingMax, &b_jet_AntiKt6TopoNewEM_SamplingMax);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_fracSamplingMax", &jet_AntiKt6TopoNewEM_fracSamplingMax, &b_jet_AntiKt6TopoNewEM_fracSamplingMax);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_hecf", &jet_AntiKt6TopoNewEM_hecf, &b_jet_AntiKt6TopoNewEM_hecf);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_tgap3f", &jet_AntiKt6TopoNewEM_tgap3f, &b_jet_AntiKt6TopoNewEM_tgap3f);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_isUgly", &jet_AntiKt6TopoNewEM_isUgly, &b_jet_AntiKt6TopoNewEM_isUgly);
//   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_isBadLooseMinus", &jet_AntiKt6TopoNewEM_isBadLooseMinus, &b_jet_AntiKt6TopoNewEM_isBadLooseMinus);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_isBadLoose", &jet_AntiKt6TopoNewEM_isBadLoose, &b_jet_AntiKt6TopoNewEM_isBadLoose);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_isBadMedium", &jet_AntiKt6TopoNewEM_isBadMedium, &b_jet_AntiKt6TopoNewEM_isBadMedium);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_isBadTight", &jet_AntiKt6TopoNewEM_isBadTight, &b_jet_AntiKt6TopoNewEM_isBadTight);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_emfrac", &jet_AntiKt6TopoNewEM_emfrac, &b_jet_AntiKt6TopoNewEM_emfrac);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_Offset", &jet_AntiKt6TopoNewEM_Offset, &b_jet_AntiKt6TopoNewEM_Offset);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_EMJES", &jet_AntiKt6TopoNewEM_EMJES, &b_jet_AntiKt6TopoNewEM_EMJES);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_EMJES_EtaCorr", &jet_AntiKt6TopoNewEM_EMJES_EtaCorr, &b_jet_AntiKt6TopoNewEM_EMJES_EtaCorr);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_EMJESnooffset", &jet_AntiKt6TopoNewEM_EMJESnooffset, &b_jet_AntiKt6TopoNewEM_EMJESnooffset);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_GCWJES", &jet_AntiKt6TopoNewEM_GCWJES, &b_jet_AntiKt6TopoNewEM_GCWJES);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_GCWJES_EtaCorr", &jet_AntiKt6TopoNewEM_GCWJES_EtaCorr, &b_jet_AntiKt6TopoNewEM_GCWJES_EtaCorr);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_CB", &jet_AntiKt6TopoNewEM_CB, &b_jet_AntiKt6TopoNewEM_CB);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_LCJES", &jet_AntiKt6TopoNewEM_LCJES, &b_jet_AntiKt6TopoNewEM_LCJES);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_emscale_E", &jet_AntiKt6TopoNewEM_emscale_E, &b_jet_AntiKt6TopoNewEM_emscale_E);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_emscale_pt", &jet_AntiKt6TopoNewEM_emscale_pt, &b_jet_AntiKt6TopoNewEM_emscale_pt);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_emscale_m", &jet_AntiKt6TopoNewEM_emscale_m, &b_jet_AntiKt6TopoNewEM_emscale_m);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_emscale_eta", &jet_AntiKt6TopoNewEM_emscale_eta, &b_jet_AntiKt6TopoNewEM_emscale_eta);
   fChain->SetBranchAddress("jet_AntiKt6TopoNewEM_emscale_phi", &jet_AntiKt6TopoNewEM_emscale_phi, &b_jet_AntiKt6TopoNewEM_emscale_phi);*/
   fChain->SetBranchAddress("mc_n", &mc_n, &b_mc_n);
//   fChain->SetBranchAddress("mc_E", &mc_E, &b_mc_E);
   fChain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
   fChain->SetBranchAddress("mc_m", &mc_m, &b_mc_m);
   fChain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
   fChain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
//   fChain->SetBranchAddress("mc_px", &mc_px, &b_mc_px);
//   fChain->SetBranchAddress("mc_py", &mc_py, &b_mc_py);
//   fChain->SetBranchAddress("mc_pz", &mc_pz, &b_mc_pz);
   fChain->SetBranchAddress("mc_status", &mc_status, &b_mc_status);
   fChain->SetBranchAddress("mc_barcode", &mc_barcode, &b_mc_barcode);
   fChain->SetBranchAddress("mc_pdgId", &mc_pdgId, &b_mc_pdgId);
   fChain->SetBranchAddress("mc_charge", &mc_charge, &b_mc_charge);
   fChain->SetBranchAddress("mc_vx_x", &mc_vx_x, &b_mc_vx_x);
   fChain->SetBranchAddress("mc_vx_y", &mc_vx_y, &b_mc_vx_y);
   fChain->SetBranchAddress("mc_vx_z", &mc_vx_z, &b_mc_vx_z);
   fChain->SetBranchAddress("mc_vx_barcode", &mc_vx_barcode, &b_mc_vx_barcode);
   fChain->SetBranchAddress("mc_child_index", &mc_child_index, &b_mc_child_index);
   fChain->SetBranchAddress("mc_parent_index", &mc_parent_index, &b_mc_parent_index);
/*   fChain->SetBranchAddress("MET_Truth_NonInt_etx", &MET_Truth_NonInt_etx, &b_MET_Truth_NonInt_etx);
   fChain->SetBranchAddress("MET_Truth_NonInt_ety", &MET_Truth_NonInt_ety, &b_MET_Truth_NonInt_ety);
   fChain->SetBranchAddress("MET_Truth_Int_etx", &MET_Truth_Int_etx, &b_MET_Truth_Int_etx);
   fChain->SetBranchAddress("MET_Truth_Int_ety", &MET_Truth_Int_ety, &b_MET_Truth_Int_ety);
   fChain->SetBranchAddress("MET_Truth_IntCentral_etx", &MET_Truth_IntCentral_etx, &b_MET_Truth_IntCentral_etx);
   fChain->SetBranchAddress("MET_Truth_IntCentral_ety", &MET_Truth_IntCentral_ety, &b_MET_Truth_IntCentral_ety);
   fChain->SetBranchAddress("MET_Truth_IntFwd_etx", &MET_Truth_IntFwd_etx, &b_MET_Truth_IntFwd_etx);
   fChain->SetBranchAddress("MET_Truth_IntFwd_ety", &MET_Truth_IntFwd_ety, &b_MET_Truth_IntFwd_ety);
   fChain->SetBranchAddress("MET_Truth_IntOutCover_etx", &MET_Truth_IntOutCover_etx, &b_MET_Truth_IntOutCover_etx);
   fChain->SetBranchAddress("MET_Truth_IntOutCover_ety", &MET_Truth_IntOutCover_ety, &b_MET_Truth_IntOutCover_ety);
   fChain->SetBranchAddress("MET_Truth_IntMuons_etx", &MET_Truth_IntMuons_etx, &b_MET_Truth_IntMuons_etx);
   fChain->SetBranchAddress("MET_Truth_IntMuons_ety", &MET_Truth_IntMuons_ety, &b_MET_Truth_IntMuons_ety);
   fChain->SetBranchAddress("el_n", &el_n, &b_el_n);
   fChain->SetBranchAddress("el_E", &el_E, &b_el_E);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_m", &el_m, &b_el_m);
   fChain->SetBranchAddress("el_px", &el_px, &b_el_px);
   fChain->SetBranchAddress("el_py", &el_py, &b_el_py);
   fChain->SetBranchAddress("el_pz", &el_pz, &b_el_pz);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_status", &el_status, &b_el_status);
   fChain->SetBranchAddress("el_barcode", &el_barcode, &b_el_barcode);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("mu_muid_n", &mu_muid_n, &b_mu_muid_n);
   fChain->SetBranchAddress("mu_muid_E", &mu_muid_E, &b_mu_muid_E);
   fChain->SetBranchAddress("mu_muid_pt", &mu_muid_pt, &b_mu_muid_pt);
   fChain->SetBranchAddress("mu_muid_m", &mu_muid_m, &b_mu_muid_m);
   fChain->SetBranchAddress("mu_muid_px", &mu_muid_px, &b_mu_muid_px);
   fChain->SetBranchAddress("mu_muid_py", &mu_muid_py, &b_mu_muid_py);
   fChain->SetBranchAddress("mu_muid_pz", &mu_muid_pz, &b_mu_muid_pz);
   fChain->SetBranchAddress("mu_muid_eta", &mu_muid_eta, &b_mu_muid_eta);
   fChain->SetBranchAddress("mu_muid_phi", &mu_muid_phi, &b_mu_muid_phi);
   fChain->SetBranchAddress("mu_muid_status", &mu_muid_status, &b_mu_muid_status);
   fChain->SetBranchAddress("mu_muid_barcode", &mu_muid_barcode, &b_mu_muid_barcode);
   fChain->SetBranchAddress("mu_muid_charge", &mu_muid_charge, &b_mu_muid_charge);
   fChain->SetBranchAddress("mu_staco_n", &mu_staco_n, &b_mu_staco_n);
   fChain->SetBranchAddress("mu_staco_E", &mu_staco_E, &b_mu_staco_E);
   fChain->SetBranchAddress("mu_staco_pt", &mu_staco_pt, &b_mu_staco_pt);
   fChain->SetBranchAddress("mu_staco_m", &mu_staco_m, &b_mu_staco_m);
   fChain->SetBranchAddress("mu_staco_px", &mu_staco_px, &b_mu_staco_px);
   fChain->SetBranchAddress("mu_staco_py", &mu_staco_py, &b_mu_staco_py);
   fChain->SetBranchAddress("mu_staco_pz", &mu_staco_pz, &b_mu_staco_pz);
   fChain->SetBranchAddress("mu_staco_eta", &mu_staco_eta, &b_mu_staco_eta);
   fChain->SetBranchAddress("mu_staco_phi", &mu_staco_phi, &b_mu_staco_phi);
   fChain->SetBranchAddress("mu_staco_status", &mu_staco_status, &b_mu_staco_status);
   fChain->SetBranchAddress("mu_staco_barcode", &mu_staco_barcode, &b_mu_staco_barcode);
   fChain->SetBranchAddress("mu_staco_charge", &mu_staco_charge, &b_mu_staco_charge);
   fChain->SetBranchAddress("tau_n", &tau_n, &b_tau_n);
   fChain->SetBranchAddress("tau_pt", &tau_pt, &b_tau_pt);
   fChain->SetBranchAddress("tau_m", &tau_m, &b_tau_m);
   fChain->SetBranchAddress("tau_eta", &tau_eta, &b_tau_eta);
   fChain->SetBranchAddress("tau_phi", &tau_phi, &b_tau_phi);
   fChain->SetBranchAddress("tau_status", &tau_status, &b_tau_status);
   fChain->SetBranchAddress("tau_barcode", &tau_barcode, &b_tau_barcode);
   fChain->SetBranchAddress("tau_charge", &tau_charge, &b_tau_charge);
   fChain->SetBranchAddress("ph_n", &ph_n, &b_ph_n);
   fChain->SetBranchAddress("ph_E", &ph_E, &b_ph_E);
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_m", &ph_m, &b_ph_m);
   fChain->SetBranchAddress("ph_px", &ph_px, &b_ph_px);
   fChain->SetBranchAddress("ph_py", &ph_py, &b_ph_py);
   fChain->SetBranchAddress("ph_pz", &ph_pz, &b_ph_pz);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_status", &ph_status, &b_ph_status);
   fChain->SetBranchAddress("ph_barcode", &ph_barcode, &b_ph_barcode);
   fChain->SetBranchAddress("el_truth_n", &el_truth_n, &b_el_truth_n);
   fChain->SetBranchAddress("el_truth_E", &el_truth_E, &b_el_truth_E);
   fChain->SetBranchAddress("el_truth_pt", &el_truth_pt, &b_el_truth_pt);
   fChain->SetBranchAddress("el_truth_m", &el_truth_m, &b_el_truth_m);
   fChain->SetBranchAddress("el_truth_px", &el_truth_px, &b_el_truth_px);
   fChain->SetBranchAddress("el_truth_py", &el_truth_py, &b_el_truth_py);
   fChain->SetBranchAddress("el_truth_pz", &el_truth_pz, &b_el_truth_pz);
   fChain->SetBranchAddress("el_truth_eta", &el_truth_eta, &b_el_truth_eta);
   fChain->SetBranchAddress("el_truth_phi", &el_truth_phi, &b_el_truth_phi);
   fChain->SetBranchAddress("el_truth_status", &el_truth_status, &b_el_truth_status);
   fChain->SetBranchAddress("el_truth_barcode", &el_truth_barcode, &b_el_truth_barcode);
   fChain->SetBranchAddress("el_truth_charge", &el_truth_charge, &b_el_truth_charge);
   fChain->SetBranchAddress("mu_muid_truth_n", &mu_muid_truth_n, &b_mu_muid_truth_n);
   fChain->SetBranchAddress("mu_muid_truth_E", &mu_muid_truth_E, &b_mu_muid_truth_E);
   fChain->SetBranchAddress("mu_muid_truth_pt", &mu_muid_truth_pt, &b_mu_muid_truth_pt);
   fChain->SetBranchAddress("mu_muid_truth_m", &mu_muid_truth_m, &b_mu_muid_truth_m);
   fChain->SetBranchAddress("mu_muid_truth_px", &mu_muid_truth_px, &b_mu_muid_truth_px);
   fChain->SetBranchAddress("mu_muid_truth_py", &mu_muid_truth_py, &b_mu_muid_truth_py);
   fChain->SetBranchAddress("mu_muid_truth_pz", &mu_muid_truth_pz, &b_mu_muid_truth_pz);
   fChain->SetBranchAddress("mu_muid_truth_eta", &mu_muid_truth_eta, &b_mu_muid_truth_eta);
   fChain->SetBranchAddress("mu_muid_truth_phi", &mu_muid_truth_phi, &b_mu_muid_truth_phi);
   fChain->SetBranchAddress("mu_muid_truth_status", &mu_muid_truth_status, &b_mu_muid_truth_status);
   fChain->SetBranchAddress("mu_muid_truth_barcode", &mu_muid_truth_barcode, &b_mu_muid_truth_barcode);
   fChain->SetBranchAddress("mu_muid_truth_charge", &mu_muid_truth_charge, &b_mu_muid_truth_charge);
   fChain->SetBranchAddress("mu_staco_truth_n", &mu_staco_truth_n, &b_mu_staco_truth_n);
   fChain->SetBranchAddress("mu_staco_truth_E", &mu_staco_truth_E, &b_mu_staco_truth_E);
   fChain->SetBranchAddress("mu_staco_truth_pt", &mu_staco_truth_pt, &b_mu_staco_truth_pt);
   fChain->SetBranchAddress("mu_staco_truth_m", &mu_staco_truth_m, &b_mu_staco_truth_m);
   fChain->SetBranchAddress("mu_staco_truth_px", &mu_staco_truth_px, &b_mu_staco_truth_px);
   fChain->SetBranchAddress("mu_staco_truth_py", &mu_staco_truth_py, &b_mu_staco_truth_py);
   fChain->SetBranchAddress("mu_staco_truth_pz", &mu_staco_truth_pz, &b_mu_staco_truth_pz);
   fChain->SetBranchAddress("mu_staco_truth_eta", &mu_staco_truth_eta, &b_mu_staco_truth_eta);
   fChain->SetBranchAddress("mu_staco_truth_phi", &mu_staco_truth_phi, &b_mu_staco_truth_phi);
   fChain->SetBranchAddress("mu_staco_truth_status", &mu_staco_truth_status, &b_mu_staco_truth_status);
   fChain->SetBranchAddress("mu_staco_truth_barcode", &mu_staco_truth_barcode, &b_mu_staco_truth_barcode);
   fChain->SetBranchAddress("mu_staco_truth_charge", &mu_staco_truth_charge, &b_mu_staco_truth_charge);
   fChain->SetBranchAddress("trueTau_n", &trueTau_n, &b_trueTau_n);
   fChain->SetBranchAddress("trueTau_pt", &trueTau_pt, &b_trueTau_pt);
   fChain->SetBranchAddress("trueTau_m", &trueTau_m, &b_trueTau_m);
   fChain->SetBranchAddress("trueTau_eta", &trueTau_eta, &b_trueTau_eta);
   fChain->SetBranchAddress("trueTau_phi", &trueTau_phi, &b_trueTau_phi);
   fChain->SetBranchAddress("trueTau_status", &trueTau_status, &b_trueTau_status);
   fChain->SetBranchAddress("trueTau_barcode", &trueTau_barcode, &b_trueTau_barcode);
   fChain->SetBranchAddress("trueTau_charge", &trueTau_charge, &b_trueTau_charge);
   fChain->SetBranchAddress("ph_truth_n", &ph_truth_n, &b_ph_truth_n);
   fChain->SetBranchAddress("ph_truth_E", &ph_truth_E, &b_ph_truth_E);
   fChain->SetBranchAddress("ph_truth_pt", &ph_truth_pt, &b_ph_truth_pt);
   fChain->SetBranchAddress("ph_truth_m", &ph_truth_m, &b_ph_truth_m);
   fChain->SetBranchAddress("ph_truth_px", &ph_truth_px, &b_ph_truth_px);
   fChain->SetBranchAddress("ph_truth_py", &ph_truth_py, &b_ph_truth_py);
   fChain->SetBranchAddress("ph_truth_pz", &ph_truth_pz, &b_ph_truth_pz);
   fChain->SetBranchAddress("ph_truth_eta", &ph_truth_eta, &b_ph_truth_eta);
   fChain->SetBranchAddress("ph_truth_phi", &ph_truth_phi, &b_ph_truth_phi);
   fChain->SetBranchAddress("ph_truth_status", &ph_truth_status, &b_ph_truth_status);
   fChain->SetBranchAddress("ph_truth_barcode", &ph_truth_barcode, &b_ph_truth_barcode);*/
}

Bool_t MCValidation::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef MCValidation_cxx
