#!/usr/bin/perl -w

use strict;
use warnings;


my @lines = `cat limit`;


my %limitHash;
my @limits;

for my $line (@lines) {
    chomp $line;
    # next unless $line=~m/COUPLINGRESULT\: ([0-9]+) ([0-9]+\.[0-9]+)/;
    next unless $line=~m/([0-9]+) ([0-9]+\.[0-9]+)/;
    $limitHash{ $1 } = $2;
    push @limits, "$1 $2";
}

# my @massArray;
# my @couplingArray;
# for my $mass ( keys %limitHash ) {
#     my $coupling = $limitHash{$mass};
#     push @massArray, $mass;
#     push @couplingArray, $coupling;
# }

# print "lhcp2015LimitX=[";
# my $firstLine = 1;
# for my $mass (@massArray) {
#     if( $firstLine eq 1 ) {
#         $firstLine = 0;
#         print "$mass";
#     } else {
#         print ",$mass";
#     }
# }
# print "]\n";
# $firstLine = 1;
# print "lhcp2015LimitY=[";
# for my $coupling (@couplingArray) {
#     if( $firstLine eq 1 ) {
#         $firstLine = 0;
#         print "$coupling";
#     } else {
#         print ",$coupling";
#     }
# }
# print "]\n";

my @sortedLimits = sort {
    $a =~ m/([0-9]+ )/;
    my $nA = $1;
    $b =~ m/([0-9]+ )/;
    my $nB = $1;
    $nA cmp $nB;
} @limits;

# print "\n\n";
# print join(",",@limits);
# print "\n\n";
# print join(",", @sortedLimits);

print "lhcp2015LimitX=[";
my $firstLine = 1;
for my $limit (@sortedLimits) {
    $limit =~ m/([0-9]+) ([0-9]+\.[0-9]+)/;
    my $mass = $1;
    if( $firstLine eq 1 ) {
        $firstLine = 0;
        print "$mass";
    } else {
        print ",$mass";
    }
}
print "]\n";
$firstLine = 1;
print "lhcp2015LimitY=[";
for my $limit (@sortedLimits) {
    $limit =~ m/([0-9]+) ([0-9]+\.[0-9]+)/;
    my $coupling = $2;
    if( $firstLine eq 1 ) {
        $firstLine = 0;
        print "$coupling";
    } else {
        print ",$coupling";
    }
}
print "]\n";
