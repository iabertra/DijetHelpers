#ifndef UTILITIES
#define UTILITIES

#include <sstream>
#include <cstdlib>
#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"

class Utilities{
  
 public:
  
 // Convert a double into a string
 static std::string doubleToString(const double & doubleValue){
   
     std::stringstream doubleString;
     doubleString << doubleValue;     

     return (doubleString.str());    
 };
 
 // Make a string of low bin edges separated by :
 static std::string makeBinString(int nBins ,double lowBinValue, double highBinValue){
   
   double deltaBin = (highBinValue - lowBinValue)/nBins;
   
   std::stringstream binString;
   binString << lowBinValue;    
   
   for(int iBin = 1; iBin <= nBins; iBin++){
     binString << ":";
     double currentBinValue = lowBinValue + (iBin*deltaBin);
     binString << currentBinValue;
  }

   return (binString.str());
     
 }; 
 
 
 // Retrieves a vector of doubles starting from a colon(:)-delimited list of numbers - see JetPerformanceHistograms.cxx
 static std::vector<int> tokenizeBins(const std::string & binString) {

  //TODO: some checks on string validity go here 

  //convert a string into tokens
  //first of all make it into a c string, and remove the const qualifier -> this gets us in trouble!
  //copy the string 
 // TString binStringTmp = binString.Copy();

//  char * binCString = const_cast<char *>(binStringTmp.c_str());

  
  char * binCString = new char [binString.size()+1];
  strcpy (binCString, binString.c_str());  
  //tokenize!
  char * token;
  int Dtoken; 
  std::vector<int> binVector;
  //printf ("Splitting string \"%s\" into tokens:\n", binCString);
  token = strtok(binCString,":");
  while (token != NULL)  {
//    printf ("%s\n",token);
    //convert to double
    Dtoken = strtod(token, NULL);
    //push back in vector of bins
    binVector.push_back(Dtoken);
    //go to next token
    token = strtok (NULL, ":");
  }

  return binVector;

  };
  
  
  // Sanity checks on the vector of low bin edges (e.g. they are in increasing order) - see JetPerformanceHistograms.cxx
  static bool checkBinVector(std::vector<double> binVector) {


  //temp variable for previous 
  double previousBin = -9999999;

  //check that all bins are in increasing order
  for (unsigned int i = 0; i<binVector.size(); i++) {
  //TODO: try and understand why these exceptions don't work
//     try {
//       if (previousBin > binVector[i]) throw binOrderingEx;
//     }
//     catch(exception& e) {
//       cout << e.what() << endl;
//       log->Message(Logger::ERROR, "in checkBinVector()");
//       log->Message(Logger::ERROR, e.what());
//     }

//     std::cout << binVector[i] << std::endl;
    if (previousBin > binVector[i]) {
      std::cout << "ERROR: Utilities::checkBinVector() - low edges of bins not in increasing order." << std::endl;
      return false;
    }

    else previousBin = binVector[i];
  }

  return true;
  };  

 static TString FormCalibName(TString reco_jetPrefix, TString truth_jetPrefix){
 
 TString calibname;//CD: initialisation? if it works I shouldn't complain :)
 if(reco_jetPrefix.Contains("EM")||truth_jetPrefix.Contains("EM"))calibname="EM";
 else if(reco_jetPrefix.Contains("LC")||truth_jetPrefix.Contains("LC"))calibname="LC";
 else calibname="";
 if(reco_jetPrefix.Contains("Topo")||truth_jetPrefix.Contains("Topo"))calibname+="Topo";
 else if(reco_jetPrefix.Contains("Tower")||truth_jetPrefix.Contains("Tower"))calibname+="Tower";
 
 return calibname;
 };
 
 static std::vector<TString> Vectorize(TString str) {
  std::vector<TString> result;
  TObjArray *strings = str.Tokenize(" ");
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
  Utilities::add(result,os->GetString());

  return result;
};
 static void add(std::vector<TString> &vec, TString a) { vec.push_back(a); };
};


#endif
