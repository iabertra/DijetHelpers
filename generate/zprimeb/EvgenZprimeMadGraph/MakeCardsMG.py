#!/usr/bin/python

import commands
import os

def coupling(mass):
  return 0.2

def ZprimeWidth(mass,couplingPar):
  massTop=173.34
  pi=3.14159265
  result=couplingPar*couplingPar*mass*(5+pow(1-(4*pow(massTop,2))/(pow(mass,2)),0.5))/(144*pi)
  return result

VectorOf_M_Zprime = [ "350","400","450","500","550","600","650","700","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1550","1600","1650","1700","1750","1800","1850","1900","1950","2000","2100","2200","2300","2400","2500","2600","2700","2800","2900","3000","3200","3400","3600","3800","4000","4200","4400","4600","4800","5000","5200","5400","5600","6000"]
## above 2.7 TeV couplings to large (>2.5)

ChannelNumbers = xrange(180900,180970) ## coupling 20%



for M_Zprime_value, channelNumber in zip(VectorOf_M_Zprime, ChannelNumbers) :
  couplingStrength=coupling(M_Zprime_value)
  #let's write the run_card.dat
  width = str(ZprimeWidth(int(M_Zprime_value),couplingStrength))
  runcardFileName = "run_card.dat"
  outputRunCardFile = open(runcardFileName,"w")

  outputRunCardFile.write("""
#*********************************************************************
#                       MadGraph/MadEvent                            *
#                  http://madgraph.hep.uiuc.edu                      *
#                                                                    *
#                        run_card.dat                                *
#                                                                    *
#  This file is used to set the parameters of the run.               *
#                                                                    *
#  Some notation/conventions:                                        *
#                                                                    *
#   Lines starting with a '# ' are info or comments                  *
#                                                                    *
#   mind the format:   value    = variable     ! comment             *
#*********************************************************************
#
#*******************                                                 
# Running parameters
#*******************                                                 
#                                                                    
#*********************************************************************
# Tag name for the run (one word)                                    *
#*********************************************************************
  tag_1     = run_tag ! name of the run 
#*********************************************************************
# Run to generate the grid pack                                      *
#*********************************************************************
  .false.     = gridpack  !True = setting up the grid pack
#*********************************************************************
# Number of events and rnd seed                                      *
# Warning: Do not generate more than 1M events in a single run       *
# If you want to run Pythia, avoid more than 50k events in a run.    *
#*********************************************************************
  30000 = nevents ! Number of unweighted events requested 
      0       = iseed   ! rnd seed (0=assigned automatically=default))
#*********************************************************************
# Collider type and energy                                           *
# lpp: 0=No PDF, 1=proton, -1=antiproton, 2=photon from proton,      *
#                                         3=photon from electron     *
#*********************************************************************
        1     = lpp1    ! beam 1 type 
        1     = lpp2    ! beam 2 type
     6500     = ebeam1  ! beam 1 total energy in GeV
     6500     = ebeam2  ! beam 2 total energy in GeV
#*********************************************************************
# Beam polarization from -100 (left-handed) to 100 (right-handed)    *
#*********************************************************************
        0     = polbeam1 ! beam polarization for beam 1
        0     = polbeam2 ! beam polarization for beam 2
#*********************************************************************
# PDF CHOICE: this automatically fixes also alpha_s and its evol.    *
#*********************************************************************
 'cteq6l1'    = pdlabel     ! PDF set                                     
#*********************************************************************
# Renormalization and factorization scales                           *
#*********************************************************************
 F        = fixed_ren_scale  ! if .true. use fixed ren scale
 F        = fixed_fac_scale  ! if .true. use fixed fac scale
 91.1880  = scale            ! fixed ren scale
 91.1880  = dsqrt_q2fact1    ! fixed fact scale for pdf1
 91.1880  = dsqrt_q2fact2    ! fixed fact scale for pdf2
 1        = scalefact        ! scale factor for event-by-event scales
#*********************************************************************
# Matching - Warning! ickkw > 1 is still beta
#*********************************************************************
 0        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching
 1        = highestmult      ! for ickkw=2, highest mult group
 1        = ktscheme         ! for ickkw=1, 1 Durham kT, 2 Pythia pTE
 1        = alpsfact         ! scale factor for QCD emission vx
 F        = chcluster        ! cluster only according to channel diag
 T        = pdfwgt           ! for ickkw=1, perform pdf reweighting
#*********************************************************************
# Automatic ptj and mjj cuts if xqcut > 0
# (turn off for VBF and single top processes)
#**********************************************************
   T  = auto_ptj_mjj  ! Automatic setting of ptj and mjj
#**********************************************************
#                                                                    
#**********************************
# BW cutoff (M+/-bwcutoff*Gamma)
#**********************************
  15  = bwcutoff      ! (M+/-bwcutoff*Gamma)
#**********************************************************
# Apply pt/E/eta/dr/mij cuts on decay products or not
# (note that etmiss/ptll/ptheavy/ht/sorted cuts always apply)
#**********************************************************
   T  = cut_decays    ! Cut decay products 
#*************************************************************
# Number of helicities to sum per event (0 = all helicities)
# 0 gives more stable result, but longer run time (needed for
# long decay chains e.g.).
# Use >=2 if most helicities contribute, e.g. pure QCD.
#*************************************************************
   0  = nhel          ! Number of helicities used per event
#*******************                                                 
# Standard Cuts
#*******************                                                 
#                                                                    
#*********************************************************************
# Minimum and maximum pt's (for max, -1 means no cut)                *
#*********************************************************************
 20  = ptj       ! minimum pt for the jets 
  0  = ptb       ! minimum pt for the b 
 10  = pta       ! minimum pt for the photons 
 10  = ptl       ! minimum pt for the charged leptons 
  0  = misset    ! minimum missing Et (sum of neutrino's momenta)
  0  = ptheavy   ! minimum pt for one heavy final state
 1.0 = ptonium   ! minimum pt for the quarkonium states
 -1  = ptjmax    ! maximum pt for the jets
 -1  = ptbmax    ! maximum pt for the b
 -1  = ptamax    ! maximum pt for the photons
 -1  = ptlmax    ! maximum pt for the charged leptons
 -1  = missetmax ! maximum missing Et (sum of neutrino's momenta)
#*********************************************************************
# Minimum and maximum E's (in the lab frame)                         *
#*********************************************************************
  0  = ej     ! minimum E for the jets 
  0  = eb     ! minimum E for the b 
  0  = ea     ! minimum E for the photons 
  0  = el     ! minimum E for the charged leptons 
 -1   = ejmax ! maximum E for the jets
 -1   = ebmax ! maximum E for the b
 -1   = eamax ! maximum E for the photons
 -1   = elmax ! maximum E for the charged leptons
#*********************************************************************
# Maximum and minimum absolute rapidity (for max, -1 means no cut)   *
#*********************************************************************
   5  = etaj    ! max rap for the jets 
  -1  = etab    ! max rap for the b
 2.5  = etaa    ! max rap for the photons 
 2.5  = etal    ! max rap for the charged leptons 
 0.6  = etaonium ! max rap for the quarkonium states
   0  = etajmin ! min rap for the jets
   0  = etabmin ! min rap for the b
   0  = etaamin ! min rap for the photons
   0  = etalmin ! main rap for the charged leptons
#*********************************************************************
# Minimum and maximum DeltaR distance                                *
#*********************************************************************
 0.4 = drjj    ! min distance between jets 
 0   = drbb    ! min distance between b's 
 0.4 = drll    ! min distance between leptons 
 0.4 = draa    ! min distance between gammas 
 0   = drbj    ! min distance between b and jet 
 0.4 = draj    ! min distance between gamma and jet 
 0.4 = drjl    ! min distance between jet and lepton 
 0   = drab    ! min distance between gamma and b 
 0   = drbl    ! min distance between b and lepton 
 0.4 = dral    ! min distance between gamma and lepton 
 -1  = drjjmax ! max distance between jets
 -1  = drbbmax ! max distance between b's
 -1  = drllmax ! max distance between leptons
 -1  = draamax ! max distance between gammas
 -1  = drbjmax ! max distance between b and jet
 -1  = drajmax ! max distance between gamma and jet
 -1  = drjlmax ! max distance between jet and lepton
 -1  = drabmax ! max distance between gamma and b
 -1  = drblmax ! max distance between b and lepton
 -1  = dralmax ! maxdistance between gamma and lepton
#*********************************************************************
# Minimum and maximum invariant mass for pairs                       *
#*********************************************************************
 0   = mmjj    ! min invariant mass of a jet pair 
 0   = mmbb    ! min invariant mass of a b pair 
 0   = mmaa    ! min invariant mass of gamma gamma pair
 0   = mmll    ! min invariant mass of l+l- (same flavour) lepton pair
 -1  = mmjjmax ! max invariant mass of a jet pair
 -1  = mmbbmax ! max invariant mass of a b pair
 -1  = mmaamax ! max invariant mass of gamma gamma pair
 -1  = mmllmax ! max invariant mass of l+l- (same flavour) lepton pair
#*********************************************************************
# Minimum and maximum invariant mass for all letpons                 *
#*********************************************************************
 0   = mmnl    ! min invariant mass for all letpons (l+- and vl) 
 -1  = mmnlmax ! max invariant mass for all letpons (l+- and vl) 
#*********************************************************************
# Minimum and maximum pt for 4-momenta sum of leptons                *
#*********************************************************************
 0   = ptllmin  ! Minimum pt for 4-momenta sum of leptons(l and vl)
 -1  = ptllmax  ! Maximum pt for 4-momenta sum of leptons(l and vl)
#*********************************************************************
# Inclusive cuts                                                     *
#*********************************************************************
 0  = xptj ! minimum pt for at least one jet  
 0  = xptb ! minimum pt for at least one b 
 0  = xpta ! minimum pt for at least one photon 
 0  = xptl ! minimum pt for at least one charged lepton 
#*********************************************************************
# Control the pt's of the jets sorted by pt                          *
#*********************************************************************
 0   = ptj1min ! minimum pt for the leading jet in pt
 0   = ptj2min ! minimum pt for the second jet in pt
 0   = ptj3min ! minimum pt for the third jet in pt
 0   = ptj4min ! minimum pt for the fourth jet in pt
 -1  = ptj1max ! maximum pt for the leading jet in pt 
 -1  = ptj2max ! maximum pt for the second jet in pt
 -1  = ptj3max ! maximum pt for the third jet in pt
 -1  = ptj4max ! maximum pt for the fourth jet in pt
 0   = cutuse  ! reject event if fails any (0) / all (1) jet pt cuts
#*********************************************************************
# Control the pt's of leptons sorted by pt                           *
#*********************************************************************
 0   = ptl1min ! minimum pt for the leading lepton in pt
 0   = ptl2min ! minimum pt for the second lepton in pt
 0   = ptl3min ! minimum pt for the third lepton in pt
 0   = ptl4min ! minimum pt for the fourth lepton in pt
 -1  = ptl1max ! maximum pt for the leading lepton in pt 
 -1  = ptl2max ! maximum pt for the second lepton in pt
 -1  = ptl3max ! maximum pt for the third lepton in pt
 -1  = ptl4max ! maximum pt for the fourth lepton in pt
#*********************************************************************
# Control the Ht(k)=Sum of k leading jets                            *
#*********************************************************************
 0   = htjmin ! minimum jet HT=Sum(jet pt)
 -1  = htjmax ! maximum jet HT=Sum(jet pt)
 0   = ihtmin  !inclusive Ht for all partons (including b)
 -1  = ihtmax  !inclusive Ht for all partons (including b)
 0   = ht2min ! minimum Ht for the two leading jets
 0   = ht3min ! minimum Ht for the three leading jets
 0   = ht4min ! minimum Ht for the four leading jets
 -1  = ht2max ! maximum Ht for the two leading jets
 -1  = ht3max ! maximum Ht for the three leading jets
 -1  = ht4max ! maximum Ht for the four leading jets
#*********************************************************************
# WBF cuts                                                           *
#*********************************************************************
 0   = xetamin ! minimum rapidity for two jets in the WBF case  
 0   = deltaeta ! minimum rapidity for two jets in the WBF case 
#*********************************************************************
# maximal pdg code for quark to be considered as a light jet         *
# (otherwise b cuts are applied)                                     *
#*********************************************************************
 4 = maxjetflavor    ! Maximum jet pdg code
#*********************************************************************
# Jet measure cuts                                                   *
#*********************************************************************
 0   = xqcut   ! minimum kt jet measure between partons
#*********************************************************************
""")


  paramcardFileName = "param_card.dat"
  outputParamCardFile = open(paramcardFileName,"w")

  outputParamCardFile.write("""
######################################################################
## PARAM_CARD AUTOMATICALY GENERATED BY MG5 FOLLOWING UFO MODEL   ####
######################################################################
##                                                                  ##
##  Width set on Auto will be computed following the information    ##
##        present in the decay.py files of the model. By default,   ##
##        this is only 1->2 decay modes.                            ##
##                                                                  ##
######################################################################

###################################
## INFORMATION FOR CKMBLOCK
###################################
Block ckmblock 
    1 2.277360e-01 # cabi 

###################################
## INFORMATION FOR FRBLOCK
###################################
Block frblock 
    1 """+str(couplingStrength)+""" # gz 

###################################
## INFORMATION FOR MASS
###################################
Block mass 
    1 5.040000e-03 # MD 
    2 2.550000e-03 # MU 
    3 1.010000e-01 # MS 
    4 1.270000e+00 # MC 
    5 4.700000e+00 # MB 
    6 1.720000e+02 # MT 
   11 5.110000e-04 # Me 
   13 1.056600e-01 # MM 
   15 1.777000e+00 # MTA 
   23 9.118760e+01 # MZ 
   25 1.200000e+02 # MH 
  10030 """+M_Zprime_value+""" # MZph 
  10031 3.000000e+02 # MZpl 
##  Not dependent paramater.
## Those values should be edited following the 
## analytical expression. MG5 ignore those values 
## but they are important for interfacing the output of MG5
## to external program such as Pythia.
  12 0.000000 # ve : 0.0 
  14 0.000000 # vm : 0.0 
  16 0.000000 # vt : 0.0 
  21 0.000000 # g : 0.0 
  22 0.000000 # a : 0.0 
  24 79.824360 # w+ : cmath.sqrt(MZ__exp__2/2. + cmath.sqrt(MZ__exp__4/4. - (aEW*cmath.pi*MZ__exp__2)/(Gf*sqrt__2))) 

###################################
## INFORMATION FOR SMINPUTS
###################################
Block sminputs 
    1 1.279000e+02 # aEWM1 
    2 1.166370e-05 # Gf 
    3 1.184000e-01 # aS 

###################################
## INFORMATION FOR YUKAWA
###################################
Block yukawa 
    1 5.040000e-03 # ymdo 
    2 2.550000e-03 # ymup 
    3 1.010000e-01 # yms 
    4 1.270000e+00 # ymc 
    5 4.700000e+00 # ymb 
    6 1.720000e+02 # ymt 
   11 5.110000e-04 # yme 
   13 1.056600e-01 # ymm 
   15 1.777000e+00 # ymtau 

###################################
## INFORMATION FOR DECAY
###################################
DECAY   6 1.508336e+00 # WT 
DECAY  23 2.495200e+00 # WZ 
DECAY  24 2.085000e+00 # WW 
DECAY  25 5.753088e-03 # WH 
##  Not dependent paramater.
## Those values should be edited following the 
## analytical expression. MG5 ignore those values 
## but they are important for interfacing the output of MG5
## to external program such as Pythia.
DECAY  1 0.000000 # d : 0.0 
DECAY  2 0.000000 # u : 0.0 
DECAY  3 0.000000 # s : 0.0 
DECAY  4 0.000000 # c : 0.0 
DECAY  5 0.000000 # b : 0.0 
DECAY  11 0.000000 # e- : 0.0 
DECAY  12 0.000000 # ve : 0.0 
DECAY  13 0.000000 # mu- : 0.0 
DECAY  14 0.000000 # vm : 0.0 
DECAY  15 0.000000 # ta- : 0.0 
DECAY  16 0.000000 # vt : 0.0 
DECAY  21 0.000000 # g : 0.0 
DECAY  22 0.000000 # a : 0.0 
DECAY  10030 """+width+""" # zph : (gz__exp__2*MZph*(5 + cmath.sqrt(1 - (4*MT__exp__2)/MZph__exp__2)))/(144.*cmath.pi) 0.308725
DECAY  10031 0.132629 # zpl : (5*gz__exp__2*MZpl)/(144.*cmath.pi) 
#===========================================================
# QUANTUM NUMBERS OF NEW STATE(S) (NON SM PDG CODE)
#===========================================================

Block QNUMBERS 10030  # zph 
        1 0  # 3 times electric charge
        2 3  # number of spin states (2S+1)
        3 1  # colour rep (1: singlet, 3: triplet, 8: octet)
        4 0  # Particle/Antiparticle distinction (0=own anti)
Block QNUMBERS 10031  # zpl 
        1 0  # 3 times electric charge
        2 3  # number of spin states (2S+1)
        3 1  # colour rep (1: singlet, 3: triplet, 8: octet)
        4 0  # Particle/Antiparticle distinction (0=own anti)
""")

  outputParamCardFile.close()
  outputRunCardFile.close()

  dijetDir = "./MG5_aMC_v2_2_3/Dijet_signal"
  os.system("mkdir -p " + dijetDir)
  os.system("cp *.dat "+dijetDir+"/Cards/.")
  os.system("ls")
  os.system("./"+dijetDir+"/bin/generate_events -f "+M_Zprime_value+"_MC12")
  os.system("gunzip "+dijetDir+"/Events/"+M_Zprime_value+"_MC12/unweighted_events.lhe.gz")
  os.system("mv "+dijetDir+"/Events/"+M_Zprime_value+"_MC12/unweighted_events.lhe JO/group.phys-gener.madgraph."+str(channelNumber)+".Zprime_"+M_Zprime_value+".TXT.mc12_v1._00001.events")
  os.system("tar cfvz JO/group.phys-gener.madgraph."+str(channelNumber)+".Zprime_"+M_Zprime_value+".TXT.mc12_v1._00001.tar.gz -C JO/ group.phys-gener.madgraph."+str(channelNumber)+".Zprime_"+M_Zprime_value+".TXT.mc12_v1._00001.events")
