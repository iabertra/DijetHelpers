###############################################
#Configuration file for sideband fitter
#
###############################################

SqrtS_inGeV            13000.0

mJJStartPointGeV       1800.0
mJJEndPointGeV         10000.0

leftSideBandNBins      4
middleNBins            3
rightSideBandNBins     4

nParamFunction         3

fitType                L0NSQ

#fitType                M0NSQ

nFitAttempts           10000
minChiSquare/NDF       1000
variabilityFactor      0.1

doSmoothing            true
#2nd derivative computed over 1,3,5,... bins 
smooth_step            1
secDerivativeAccurary  3.0
doNRefits              1000

#Scaled histogram plotting
doScaledHistPlot       false

doWindowPval           false
#####################
#pseudo-experiments
estGlobalFitQuality    true	
nPseudoExperiments     10000

################################################################################################
#The list of fit options is given in parameter option.
#         option = "W"  Set all weights to 1 for non empty bins; ignore error bars
#                = "WW" Set all weights to 1 including empty bins; ignore error bars
#                = "I"  Use integral of function in bin, normalized by the bin volume,
#                       instead of value at bin center
#                = "L"  Use Loglikelihood method (default is chisquare method)
#                = "WL" Use Loglikelihood method and bin contents are not integer,
#                       i.e. histogram is weighted (must have Sumw2() set)
#                = "U"  Use a User specified fitting algorithm (via SetFCN)
#                = "Q"  Quiet mode (minimum printing)
#                = "V"  Verbose mode (default is between Q and V)
#                = "E"  Perform better Errors estimation using Minos technique
#                = "B"  User defined parameter settings are used for predefined functions
#                       like "gaus", "expo", "poln", "landau".
#                       Use this option when you want to fix one or more parameters for these functions.
#                = "M"  More. Improve fit results.
#                       It uses the IMPROVE command of TMinuit (see TMinuit::mnimpr).
#                       This algorithm attempts to improve the found local minimum by searching for a
#                       better one.
#                = "R"  Use the Range specified in the function range
#                = "N"  Do not store the graphics function, do not draw
#                = "0"  Do not plot the result of the fit. By default the fitted function
#                       is drawn unless the option"N" above is specified.
#                = "+"  Add this new fitted function to the list of fitted functions
#                       (by default, any previous function is deleted)
#                = "C"  In case of linear fitting, don't calculate the chisquare
#                       (saves time)
#                = "F"  If fitting a polN, switch to minuit fitter
#                = "S"  The result of the fit is returned in the TFitResultPtr
#                       (see below Access to the Fit Resultv)
################################################################################################
