#define makeclass_tla_validation_cxx
#include "TriggerToOfflineComparisonCode/makeclass_tla_validation.h"
#include "TriggerToOfflineComparisonCode/monitoring_class.h"

//#include "tlv_pt_histograms.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TString.h>
#include <TProfile.h>
#include <TLatex.h>
#include <iostream>
#include <TLorentzVector.h>
#include <math.h>
#include <TMath.h>
#include <cmath>
#include <vector>
//#include <array>
using std::vector;

void makeclass_tla_validation::Loop(TString ofn, int DataEnergyTeV = 0)
{
  //   In a ROOT session, you can do:
  //      Root > .L makeclass_tla_validation.C
  //      Root > makeclass_tla_validation t
  //      Root > t.GetEntry(12); // Fill t data members with entry number 12
  //      Root > t.Show();       // Show values of entry 12
  //      Root > t.Show(16);     // Read and show values of entry 16
  //      Root > t.Loop();       // Loop on all entries
  //
  
  //    This is the loop skeleton where:
  //    jentry is the global entry number in the chain
  //    ientry is the entry number in the current Tree
  //    Note that the argument to GetEntry must be:
  //    jentry for TChain::GetEntry
  //    ientry for TTree::GetEntry and TBranch::GetEntry
  
  if (fChain == 0) return;
  
  monitoring_class Monitor(ofn);
  
  //Calibration tool for offline jets
  bool use_calib_tool = true; //*******************************************************************************
  //Monte Carlo weights (true for MC, false for data)
  bool use_weights = true;  //*********************************************************************************
  
  //-------------------------------------------
  // Turn off all but the branches we want to use:
  fChain->SetBranchStatus("*",0);
  //Branches for offline jets:
  fChain->SetBranchStatus("jet_AntiKt4TopoEM_n",1);
  fChain->SetBranchStatus("jet_AntiKt4TopoEM_E",1);
  fChain->SetBranchStatus("jet_AntiKt4TopoEM_pt",1);
  fChain->SetBranchStatus("jet_AntiKt4TopoEM_m",1);
  fChain->SetBranchStatus("jet_AntiKt4TopoEM_eta",1);
  fChain->SetBranchStatus("jet_AntiKt4TopoEM_phi",1);
  //Branches for jet calibration:
  if(use_calib_tool){
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_emscale_E",1);
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_emscale_m",1);
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_emscale_eta",1);
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_emscale_phi",1);
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_ActiveAreaPx",1);
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_ActiveAreaPy",1);
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_ActiveAreaPz",1);
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_ActiveAreaE",1);
    fChain->SetBranchStatus("Eventshape_rhoKt4EM",1);
    fChain->SetBranchStatus("averageIntPerXing",1);
    fChain->SetBranchStatus("vxp_nTracks",1);
  }
  //Branches for trigger jets:
  fChain->SetBranchStatus("trig_EF_jet_calibtags",1);
  fChain->SetBranchStatus("trig_EF_jet_n",1);
  fChain->SetBranchStatus("trig_EF_jet_E",1);
  fChain->SetBranchStatus("trig_EF_jet_pt",1);
  fChain->SetBranchStatus("trig_EF_jet_m",1);
  fChain->SetBranchStatus("trig_EF_jet_eta",1);
  fChain->SetBranchStatus("trig_EF_jet_phi",1);
  fChain->SetBranchStatus("mcevt_weight",1);
  fChain->SetBranchStatus("AntiKt4Truth_pt",1);
  
  //-------------------------------------------
  
  //Offline jet calibration setup:
  //---------------------------------
  // create the calibration object:
  JetAnalysisCalib::JetCalibrationTool *m_JESTool;
  
  TString jetAlgo="AntiKt4TopoEM";  //Should be set to one of "AntiKt4TopoEM", "AntiKt4LCTopo", "AntiKt6TopoEM", "AntiKt6LCTopo"
  TString JES_config_file = "";
  if(DataEnergyTeV == 14){
    JES_config_file="RootCore/data/ApplyJetCalibration/CalibrationConfigs/muScan2013/Rel17.2_JES_muScan_25ns_80_80.config";
  }
  else if(DataEnergyTeV == 8){
    JES_config_file="RootCore/data/ApplyJetCalibration/CalibrationConfigs/JES_Full2012dataset_Preliminary_Jan13.config";
  }
  else{
    printf("Don't know which jet calibration to use (8 or 14 TeV), so now running without calibration!\n");
    use_calib_tool = false;
  }
  
  bool isData = false;
  if(use_calib_tool){
    m_JESTool = new JetAnalysisCalib::JetCalibrationTool(jetAlgo,JES_config_file,isData);
  }
  //---------------------------------
  
  Monitor.MakeTheHistos();
  
  Long64_t nentries = fChain->GetEntries();
  
  std::cout << "Will run over " << nentries << " events" << std::endl;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    
    if (jentry && jentry%10000==0) {
      printf("Processed %.2fM / %.2fM, %6.2f%%\n",
             1e-6*jentry,1e-6*nentries,1e2*jentry/nentries);
      printf("Eventshape rho is %6.2f\n",Eventshape_rhoKt4EM);
      std::cout << std::flush;
    }
    
    //Load all turned on branches into memory
    fChain->GetEntry(jentry);
    
    //Setting weights for Monte Carlo simulation events
    double w = 1.0;
    if (mcevt_weight->size()==0) fatal("Current event has no MC evt weight!");
    if(use_weights) w = mcevt_weight->at(0).at(0);
    
    //Load jets into vectors:
    //-----------------------------------------------------------------------
    vector<TLorentzVector> recoJets, trigJets;
    
    for (unsigned int irjet=0; irjet<jet_AntiKt4TopoEM_pt->size(); ++irjet) {
      if (use_calib_tool) {
        double Eraw    = jet_AntiKt4TopoEM_emscale_E->at(irjet);
        double eta     = jet_AntiKt4TopoEM_emscale_eta->at(irjet);
        double phi     = jet_AntiKt4TopoEM_emscale_phi->at(irjet);
        double m       = jet_AntiKt4TopoEM_emscale_m->at(irjet);
        double Ax      = jet_AntiKt4TopoEM_ActiveAreaPx->at(irjet);
        double Ay      = jet_AntiKt4TopoEM_ActiveAreaPy->at(irjet);
        double Az      = jet_AntiKt4TopoEM_ActiveAreaPz->at(irjet);
        double Ae      = jet_AntiKt4TopoEM_ActiveAreaE->at(irjet);
        double rho     = Eventshape_rhoKt4EM;
        
        // For the pile-up correction, we need mu and NPV(2+ tracks)
        double mu = averageIntPerXing;
        int NPV=0; // count the number of vertices with 2 or more tracks
        
        for (unsigned int ii=0; ii<vxp_nTracks->size(); ii++){
          if (vxp_nTracks->at(ii)>=2) NPV++;
        }
        
        TLorentzVector jet = m_JESTool->ApplyJetAreaOffsetEtaJES(Eraw,eta,phi,m,Ax,Ay,Az,Ae,rho,mu,NPV);
        //convert the jet to GeV:
        jet *= 0.001;
        
        if(jet.Pt()<=monitoring_class::pt_Cut) continue;
        recoJets.push_back(jet);
      } else {
        TLorentzVector jet;
        jet.SetPtEtaPhiM(jet_AntiKt4TopoEM_pt->at(irjet)*1e-3,jet_AntiKt4TopoEM_eta->at(irjet),
                         jet_AntiKt4TopoEM_phi->at(irjet),jet_AntiKt4TopoEM_m->at(irjet)*1e-3);
        if(jet.Pt()<=monitoring_class::pt_Cut) continue;
        //          if(jet.Eta() > etaCutoff || jet.Eta() < -etaCutoff) continue;
        recoJets.push_back(jet);
      }
    }
    
    //Apply cut on events with too much pile up
    //-----------------------------------------
    // get the truth jet pT - in GeV
    double truth_pt = 999.0;
    if (AntiKt4Truth_pt->size()) truth_pt = AntiKt4Truth_pt->at(0)/1000;
    
    // access the average pT of the two leading reco jets
    double pTavg=0;
    if (recoJets.size()>1) pTavg=(recoJets[0].Pt()+recoJets[1].Pt())/2;
    else if (recoJets.size()==1) pTavg=recoJets[0].Pt(); //in rare cases...
    
    // apply "MC quality cut"
    if ( pTavg / truth_pt > 1.4 ) continue;
    //-----------------------------------------
    
    for (unsigned int itjet=0; itjet<trig_EF_jet_pt->size(); ++itjet){
      if (trig_EF_jet_calibtags->at(itjet) != "AntiKt4_topo_calib_EMJES")
        continue;
      TLorentzVector jet;
      jet.SetPtEtaPhiM(trig_EF_jet_pt->at(itjet)*1e-3,trig_EF_jet_eta->at(itjet),
                       trig_EF_jet_phi->at(itjet),trig_EF_jet_m->at(itjet)*1e-3);
      if(jet.Pt()<=monitoring_class::pt_Cut) continue;
      //       if(jet.Eta() > etaCutoff || jet.Eta() < -etaCutoff) continue;
      trigJets.push_back(jet);
    }//Loaded jets into vectors.
    //-----------------------------------------------------------------------
    
    Monitor.FillTheHistos(trigJets,recoJets,w);
  } //Looped over all events.
  
  Monitor.FinalizeAndSaveOutput();
  
  printf("   Finished.\n");
}
