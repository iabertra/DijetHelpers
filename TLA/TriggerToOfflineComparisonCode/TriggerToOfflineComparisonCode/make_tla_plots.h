#include <iostream>
#include <vector>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TColor.h>
#include <TLatex.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TFrame.h>
#include <TChain.h>
#include <THStack.h>
#include <TLine.h>
#include <TMath.h>
#include "Riostream.h"

using namespace std;

void plot_tla_validation(TString ifn, TString ofs);
