#!/usr/bin/env python

#******************************************
#import stuff
import os, sys, time

#******************************************
#profiles
histograms=[]
histograms.append('eta_SumPtTrkPt500PV_binned')
histograms.append('pt_SumPtTrkPt500PV_binned')
histograms.append('eta_NumTrkPt500PV_binned')
histograms.append('pt_NumTrkPt500PV_binned')
histograms.append('eta_TrackWidthPt500PV_binned')
histograms.append('pt_TrackWidthPt500PV_binned')

xlabels=[]
xlabels.append('#eta')
xlabels.append('p_{T} [GeV]')
xlabels.append('#eta')
xlabels.append('p_{T} [GeV]')
xlabels.append('#eta')
xlabels.append('p_{T} [GeV]')

ylabels=[]
ylabels.append('<SumPtTrkPt500PV>')
ylabels.append('<SumPtTrkPt500PV>')
ylabels.append('<NumTrkPt500PV>')
ylabels.append('<NumTrkPt500PV>')
ylabels.append('<TrackWidthPt500PV>')
ylabels.append('<TrackWidthPt500PV>')

if len(histograms) == len(xlabels) and len(histograms) == len(xlabels):
    for ii in range(len(histograms)):    
        command = 'python plotHistograms.py --files results/histograms.mc.pythia8.25ns.root results/histograms.data.data.25ns.root results/histograms.data.data.50ns.root --hist1 '+histograms[ii]+' --xlabel "'+xlabels[ii]+'" --ylabel "'+ylabels[ii]+'" --labels "Pythia8 25 ns" "data 25 ns" "data 50 ns" --notes "#sqrt{s} = 13 TeV" --lumis 0.80 0.55 0.26 --compareProfiles --tag standard --save'
        os.system(command)
else:
    print '***WARNING*** histograms, x-labels and y-labels don\'t have the same number of entries'

    
#******************************************
#distributions
histograms=[]
histograms.append('eta')
histograms.append('phi')
histograms.append('pt')
histograms.append('mjj')

xlabels=[]
xlabels.append('#eta')
xlabels.append('#phi')
xlabels.append('p_{T} [GeV]')
xlabels.append('m_{jj} [GeV]')

ylabels=[]
ylabels.append('arbitrary')
ylabels.append('arbitrary')
ylabels.append('arbitrary')
ylabels.append('arbitrary')

logxs=[]
logxs.append('')
logxs.append('')
logxs.append('')
logxs.append('--logx')

logys=[]
logys.append('--logy')
logys.append('')
logys.append('--logy')
logys.append('--logy')

if len(histograms) == len(xlabels) and len(histograms) == len(xlabels):
    for ii in range(len(histograms)):    
        command = 'python plotHistograms.py --files results/histograms.mc.pythia8.25ns.root results/histograms.data.data.25ns.root results/histograms.data.data.50ns.root --hist1 '+histograms[ii]+' --xlabel "'+xlabels[ii]+'" --ylabel "'+ylabels[ii]+'" --labels "Pythia8 25 ns" "data 25 ns" "data 50 ns" --notes "#sqrt{s} = 13 TeV" --lumis 0.80 0.55 0.26 --compareDistributions --tag standard --save '+logxs[ii]+' '+logys[ii]
        os.system(command)
else:
    print '***WARNING*** histograms, x-labels and y-labels don\'t have the same number of entries'
