#!/bin/bash
#list of commands to plot histograms

if [[ $# -ne 6 ]]
    then {
	echo
	echo "justPushPlay.sh"
	echo "HOW TO: ./justPushPlay.sh filemc tagmc file25 tag25 file50 tag50"
	exit
    }
fi

filemc=$1
tagmc=$2

file25=$3
tag25=$4

file50=$5
tag50=$6

#Pythia 8 25 ns
python plotHistograms.py --file $filemc --hist1 eta_pt_binned --hist2 eta_SumPtTrkPt500PV_binned --xlabel "#eta" --ylabel "p_{T} [GeV]" --notes "Pythia8 25 ns" "#sqrt{s} = 13 TeV" "L = 0.80 fb^{-1}" --label1 calo-jets --label2 tracks --plotBinned --save --tag $tagmc
python plotHistograms.py --file $filemc --hist1 eta_SumPtTrkPt500PV_binned --xlabel "#eta" --ylabel "#sump_{T}^{track} [GeV]" --notes "Pythia8 25 ns" "#sqrt{s} = 13 TeV" "L = 0.80 fb^{-1}" --plotProfile --save --tag $tagmc
python plotHistograms.py --file $filemc --hist1 pt_SumPtTrkPt500PV_binned --xlabel "p_{T}" --ylabel "#sump_{T}^{track} [GeV]" --notes "Pythia8 25 ns" "#sqrt{s} = 13 TeV" "L = 0.80 fb^{-1}" --plotProfile --save --tag $tagmc

python plotHistograms.py --file $filemc --hist1 eta_NumTrkPt500PV_binned --xlabel "#eta" --ylabel "n_{tracks}" --notes "Pythia8 25 ns" "#sqrt{s} = 13 TeV" "L = 0.80 fb^{-1}" --plotProfile --save --tag $tagmc
python plotHistograms.py --file $filemc --hist1 pt_NumTrkPt500PV_binned --xlabel "p_{T}" --ylabel "n_{tracks}" --notes "Pythia8 25 ns" "#sqrt{s} = 13 TeV" "L = 0.80 fb^{-1}" --plotProfile --save --tag $tagmc

python plotHistograms.py --file $filemc --hist1 eta_TrackWidthPt500PV_binned --xlabel "#eta" --ylabel "track width" --notes "Pythia8 25 ns" "#sqrt{s} = 13 TeV" "L = 0.80 fb^{-1}" --plotProfile --save --tag $tagmc
python plotHistograms.py --file $filemc --hist1 pt_TrackWidthPt500PV_binned --xlabel "p_{T}" --ylabel "track width" --notes "Pythia8 25 ns" "#sqrt{s} = 13 TeV" "L = 0.80 fb^{-1}" --plotProfile --save --tag $tagmc

#data 25 ns
python plotHistograms.py --file $file25 --hist1 eta_pt_binned --hist2 eta_SumPtTrkPt500PV_binned --xlabel "#eta" --ylabel "p_{T} [GeV]" --notes "data 25 ns" "#sqrt{s} = 13 TeV" "L = 0.55 fb^{-1}" --label1 calo-jets --label2 tracks --plotBinned --save --tag $tag25
python plotHistograms.py --file $file25 --hist1 eta_SumPtTrkPt500PV_binned --xlabel "#eta" --ylabel "#sump_{T}^{track} [GeV]" --notes "data 25 ns" "#sqrt{s} = 13 TeV" "L = 0.55 fb^{-1}" --plotProfile --save --tag $tag25
python plotHistograms.py --file $file25 --hist1 pt_SumPtTrkPt500PV_binned --xlabel "p_{T}" --ylabel "#sump_{T}^{track} [GeV]" --notes "data 25 ns" "#sqrt{s} = 13 TeV" "L = 0.55 fb^{-1}" --plotProfile --save --tag $tag25

python plotHistograms.py --file $file25 --hist1 eta_NumTrkPt500PV_binned --xlabel "#eta" --ylabel "n_{tracks}" --notes "data 25 ns" "#sqrt{s} = 13 TeV" "L = 0.55 fb^{-1}" --plotProfile --save --tag $tag25
python plotHistograms.py --file $file25 --hist1 pt_NumTrkPt500PV_binned --xlabel "p_{T}" --ylabel "n_{tracks}" --notes "data 25 ns" "#sqrt{s} = 13 TeV" "L = 0.55 fb^{-1}" --plotProfile --save --tag $tag25

python plotHistograms.py --file $file25 --hist1 eta_TrackWidthPt500PV_binned --xlabel "#eta" --ylabel "track width" --notes "data 25 ns" "#sqrt{s} = 13 TeV" "L = 0.55 fb^{-1}" --plotProfile --save --tag $tag25
python plotHistograms.py --file $file25 --hist1 pt_TrackWidthPt500PV_binned --xlabel "p_{T}" --ylabel "track width" --notes "data 25 ns" "#sqrt{s} = 13 TeV" "L = 0.55 fb^{-1}" --plotProfile --save --tag $tag25

#data 50 ns
python plotHistograms.py --file $file50 --hist1 eta_pt_binned --hist2 eta_SumPtTrkPt500PV_binned --xlabel "#eta" --ylabel "p_{T} [GeV]" --notes "data 50 ns" "#sqrt{s} = 13 TeV" "L = 0.26 fb^{-1}" --label1 calo-jets --label2 tracks --plotBinned --save --tag $tag50
python plotHistograms.py --file $file50 --hist1 eta_SumPtTrkPt500PV_binned --xlabel "#eta" --ylabel "#sump_{T}^{track} [GeV]" --notes "data 50 ns" "#sqrt{s} = 13 TeV" "L = 0.26 fb^{-1}" --plotProfile --save --tag $tag50
python plotHistograms.py --file $file50 --hist1 pt_SumPtTrkPt500PV_binned --xlabel "p_{T}" --ylabel "#sump_{T}^{track} [GeV]" --notes "data 50 ns" "#sqrt{s} = 13 TeV" "L = 0.26 fb^{-1}" --plotProfile --save --tag $tag50

python plotHistograms.py --file $file50 --hist1 eta_NumTrkPt500PV_binned --xlabel "#eta" --ylabel "n_{tracks}" --notes "data 50 ns" "#sqrt{s} = 13 TeV" "L = 0.26 fb^{-1}" --plotProfile --save --tag $tag50
python plotHistograms.py --file $file50 --hist1 pt_NumTrkPt500PV_binned --xlabel "p_{T}" --ylabel "n_{tracks}" --notes "data 50 ns" "#sqrt{s} = 13 TeV" "L = 0.26 fb^{-1}" --plotProfile --save --tag $tag50

python plotHistograms.py --file $file50 --hist1 eta_TrackWidthPt500PV_binned --xlabel "#eta" --ylabel "track width" --notes "data 50 ns" "#sqrt{s} = 13 TeV" "L = 0.26 fb^{-1}" --plotProfile --save --tag $tag50
python plotHistograms.py --file $file50 --hist1 pt_TrackWidthPt500PV_binned --xlabel "p_{T}" --ylabel "track width" --notes "data 50 ns" "#sqrt{s} = 13 TeV" "L = 0.26 fb^{-1}" --plotProfile --save --tag $tag50
