#!/bin/sh


N=9

#yStar_cut=0.6
#NLJet_cut=50

for i in `seq 5 5`
do

    mkdir -p cut_${i}
    cp ./Ref/makeTree.C cut_${i}/.
    cp ./Ref/Analyze_* cut_${i}/.

    if [ ${i} = 1 ]; then
        yStarMin_cut=0
        LJet_cut=300
        yStar_cut=0.6
        NLJet_cut=50
    elif [ ${i} = 2 ]; then
        yStarMin_cut=0
        LJet_cut=350
        yStar_cut=0.6
        NLJet_cut=50
    elif [ ${i} = 3 ]; then
        yStarMin_cut=0
        LJet_cut=400
        yStar_cut=0.6
        NLJet_cut=50
    elif [ ${i} = 4 ]; then
        yStarMin_cut=0.175
        yStar_cut=1.18
        NLJet_cut=50
        LJet_cut=350
    elif [ ${i} = 5 ]; then
        yStar_cut=0.6
        NLJet_cut=50
        LJet_cut=350
    fi

    for j in `seq 1 $N`
    do


        runScriptName="cut_${i}/runscript_${i}_${j}.sh"
        echo "#!/bin/bash" > ${runScriptName}
        echo "#SBATCH -n 2 # Number of cores" >> ${runScriptName}
        echo "#SBATCH -N 1 # Ensure that all cores are on one machine" >> ${runScriptName}
        echo "#SBATCH -t 0-04:00 # Runtime in D-HH:MM" >> ${runScriptName}
        echo "#SBATCH -p serial_requeue # Partition to submit to" >> ${runScriptName}
        echo "#SBATCH --mem=10000 # Memory pool for all cores (see also --mem-per-cpu)" >> ${runScriptName}
        echo "#SBATCH -o cut_${i}/hostname_${j}.out # File to which STDOUT will be written" >> ${runScriptName}
        echo "#SBATCH -e cut_${i}/hostname_${j}.err # File to which STDERR will be written" >> ${runScriptName}
        echo "#SBATCH --mail-type=ALL # Type of email notification- BEGIN,END,FAIL,ALL" >> ${runScriptName}
        echo "#SBATCH --mail-user=bclark@physics.harvard.edu # Email to which notifications will be sent" >> ${runScriptName}

        echo "hostname" >> ${runScriptName}

        echo "root -l -b -q cut_${i}/runRes_${j}.c" >> ${runScriptName}



        runRes="cut_${i}/runRes_${j}.c"
        echo "{" > ${runRes}
        echo "#include <TLorentzVector.h>" >> ${runRes}
        echo "gROOT->ProcessLine(\".L /n/atlasfs/atlasdata/atlasdata1/clark/AnalysisBinning/cut_${i}/Analyze_${j}.C\");" >> ${runRes}
        echo "gROOT->ProcessLine(\"Analyze_${j} t2\");" >> ${runRes}
        echo "fileout = \"/n/atlasfs/atlasdata/atlasdata1/clark/AnalysisBinning/cut_${i}/J${j}.root\";" >> ${runRes}
        echo "float yStarMinCut = ${yStarMin_cut};" >> ${runRes}
        echo "float yStarCut = ${yStar_cut};" >> ${runRes}
        echo "float LJet = ${LJet_cut};" >> ${runRes}
        echo "float NLJet = ${NLJet_cut};" >> ${runRes}
        echo "gROOT->ProcessLine(\".x /n/atlasfs/atlasdata/atlasdata1/clark/AnalysisBinning/cut_${i}/makeTree.C\");" >>${runRes}
        echo "}" >>${runRes}


        if [ $1 = "submit" ]; then
            sbatch ${runScriptName}
        fi
    done
done
