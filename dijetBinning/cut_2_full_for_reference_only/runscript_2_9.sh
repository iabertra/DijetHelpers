#!/bin/bash
#SBATCH -n 2 # Number of cores
#SBATCH -N 1 # Ensure that all cores are on one machine
#SBATCH -t 0-04:00 # Runtime in D-HH:MM
#SBATCH -p serial_requeue # Partition to submit to
#SBATCH --mem=10000 # Memory pool for all cores (see also --mem-per-cpu)
#SBATCH -o cut_2/hostname_9.out # File to which STDOUT will be written
#SBATCH -e cut_2/hostname_9.err # File to which STDERR will be written
#SBATCH --mail-type=ALL # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bclark@physics.harvard.edu # Email to which notifications will be sent
hostname
root -l -b -q cut_2/runRes_9.c
