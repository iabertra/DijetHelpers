
{
    
    
    
    double max_num_events = 10**10;  // make large to get full data set
    double num_events_quick_stats = 10**10; // make large to get full data set (set small if quick debugging)
    double Luminosity = 1;  //fb-1  #not properly done, so just leave at 1 !!!!!!!
    int data = 13;
    //#########################################################################################################

    
    
    
    
    
    
    //Check for maximum of distribution
    double mjj_min = 99999999;
    double mjj_max = -99999999;
    TH1F* h_mjj_initial = new TH1F("h_mjj_initial","Recon mjj",1000,0,12000);
    Long64_t nentries = tree->GetEntriesFast();
    for(int ientry = 0; ientry < nentries; ientry++){
        if(ientry < 0) break;
        if(ientry>max_num_events) break;
        t.GetEntry(ientry);

        double mjj = t.varRecon;
        double mjjTruth = t.varTruth;
        double weight = t.weight * Luminosity;
        
        h_mjj_initial->Fill(mjj,weight);
        
        if(mjj<mjj_min) mjj_min = mjj;
        if(mjj>mjj_max) mjj_max = mjj;
    }
    double mjj_min = h_mjj_initial->GetBinCenter(h_mjj_initial->GetMaximumBin()) + 100;

    cout << mjj_min << "....." << mjj_max << endl;
    
    cout << "mjj max for distribution is " << h_mjj_initial->GetBinCenter(h_mjj_initial->GetMaximumBin()) << endl;
    
//    if (mjj_min < 800) mjj_min = 800;
    
    
//    double mjj_min = 846;
//    double mjj_max = 9897;
//    
    
    
    //Begin finding resolution as a function of mjj
    
    
    std::vector<double> res_mean;
    std::vector<double> res_sig;
    std::vector<double> res_mean_error;
    std::vector<double> res_sig_error;
    std::vector<double> res_bin_center;
    std::vector<double> res_numEvents;
    std::vector<double> res_bin_size;   // This is anctually the bin error or half a bin size
    std::vector<double> resolution_adj;
    int res_nbins;


    if(data==13){
        //        //-----13TeV-------------------------------------------------------------------
        
        
//        std::vector<double> res_bin_edges;
////        res_bin_edges.push_back(mjj_min-70-40);
        
        
//        res_bin_edges.push_back(mjj_min-70);  //600
//        res_bin_edges.push_back(mjj_min+100);  //800
//        res_bin_edges.push_back(mjj_min+300);  //1100
//        res_bin_edges.push_back(mjj_min+300+350);
//        res_bin_edges.push_back(mjj_min+300+350+400);
//        res_bin_edges.push_back(mjj_min+300+350+400+450);
//        res_bin_edges.push_back(mjj_min+300+350+400+450+600);
//        res_bin_edges.push_back(mjj_min+300+350+400+450+600+760);
//        res_bin_edges.push_back(mjj_min+300+350+400+450+600+800+1000);
//        res_bin_edges.push_back(mjj_min+300+350+400+450+600+800+1000+1400);
//        res_bin_edges.push_back(mjj_min+300+350+400+450+600+800+1000+1400+2000);
//        res_bin_edges.push_back(mjj_min+300+350+400+450+600+800+1000+1400+2000+2200);  //4700);
        
//        res_nbins = res_bin_edges.size()-1;

        int res_bin_edges[30]={600,620,660,700,750,800,860,920,990,1060,1160,1260,1370,1480,1600,1750,1900,2200,2450,2750,3100,3500,4000,4500,5200,5800,6500,7400,8600,10000}; //,10000};

        res_nbins = 30 - 2 + 1;

}
    
    
    
    //----Make resolution histograms and define resolution bin centers-----
    TH1D* hResArray[res_nbins];
    for(int i=0;i<res_nbins;i++){
        res_bin_center.push_back((res_bin_edges[i] + res_bin_edges[i+1])/2);
        res_bin_size.push_back((res_bin_edges[i+1] - res_bin_edges[i])/2);
        int a = res_bin_edges[i];
        int b = res_bin_edges[i+1];
        hResArray[i] = new TH1D(Form("hResArray%i",i),Form("Res for mjj = %i - %i GeV",a,b),200,0.7,1.3);
    }

    //----Loop over events and fill resolution histograms-----
    Long64_t nentries = tree->GetEntriesFast();
    for(int ientry = 0; ientry < nentries; ientry++){
        if(ientry < 0) break;
        if(ientry>max_num_events) break;
        
        if (ientry%1000000 == 0) cout << ientry << endl;
        
        t.GetEntry(ientry);
        
        double mjj = t.varRecon;
        double mjjTruth = t.varTruth;
        double weight = t.weight * Luminosity;
        

        
        for(int i=0;i<res_nbins;i++){
            if(res_bin_edges[i+1]>=mjjTruth){
                double prop = mjj/mjjTruth;
                hResArray[i]->Fill(prop,weight);
                break;
            }
        }
    }
    
    
    //-----Calc statistics for resolution histograms-------
    //I should be calculating these statistics for each res bin real-time.  If the fit quality is lacking, I will then make the bin larger.
    for(int i=0;i<res_nbins;i++){
        hResArray[i]->Fit("gaus");
        double mean=gaus->GetParameter(1);
        double sig=gaus->GetParameter(2);
        double r1 = mean - 1.5*sig;
        double r2 = mean + 1.5*sig;
        hResArray[i]->Fit("gaus","","",r1,r2);
        
        res_mean.push_back(gaus->GetParameter(1));
        res_sig.push_back(gaus->GetParameter(2));
        res_numEvents.push_back(gaus->GetParameter(0));
        res_mean_error.push_back(gaus->GetParError(1));
        res_sig_error.push_back(gaus->GetParError(2));
        resolution_adj.push_back(res_sig[i]/res_mean[i]);
        
        //        res_mean_error.push_back(gaus->GetParameter(1) / sqrt(gaus->GetParameter(0)));
//        res_sig_error.push_back(gaus->GetParameter(2) / sqrt(2*gaus->GetParameter(0)-1));
    }

    
    
    
    
//    Minimizer is Linear / Robust (h=0.95)
//    Chi2                      =  4.12835e-07
//    NDf                       =           17
//    p0                        =    0.0488777
//    p1                        = -2.92624e-05
//    p2                        =  1.39116e-08
//    p3                        = -3.69195e-12
//    p4                        =  5.48313e-16
//    p5                        = -4.24416e-20
//    p6                        =  1.33082e-24
    
    
    
    
}
