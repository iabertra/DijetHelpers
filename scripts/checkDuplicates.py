#!/usr/bin/env python

###########################################################
# checkDuplicates.py                                      #
# A short script to check for duplicate events.           #
# Option to create a new TTree with duplicates removed.   #
# For more information contact Jeff.Dandoy@cern.ch        #
###########################################################

import os, glob, array, argparse, subprocess, shutil

#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--removeDuplicates", dest='removeDuplicates', action='store_true', default=False, help="Remove any duplicates from the tree")
parser.add_argument("--pathToTrees", dest='pathToTrees', default="./fileStorage/Data_fGRL_20150826/tree/", help="Path to the input trees")
parser.add_argument("--fileName", dest='fileName', default="", help="Filename to run over")
parser.add_argument("--nominal", dest='nominal', action='store_true', default=False, help="Nominal only")

args = parser.parse_args()

from ROOT import *

def checkDuplicates():

  ## Collect files
  if len(args.fileName) > 0:
    files = [args.fileName]
  else:
    files = glob.glob(args.pathToTrees+"/*.root")
#    files = glob.glob(args.pathToTrees+"/Combined_*.root")
  treeName = "outTree"

  for file in files:

    print "Checking file ", file.split('/')[-1]
    inFile = TFile.Open(file, "READ")

    ### Create list of all event values ###
    tree = inFile.Get("outTree")
    treePlayer = tree.GetPlayer()
    treePlayer.SetScanRedirect(True)
    treePlayer.SetScanFileName("tmpOut.txt")
    tree.Scan("eventNumber","","precision=15 colsize=15")
    #tree.Scan("eventNumber:runNumber")
    outFile = open("tmpOut2.txt", "w")
    devnull = open(os.devnull)
    subprocess.call(["sed", "s/^.\{10\}//g", "tmpOut.txt"], stdout=outFile, stderr=devnull)

    ### Check for duplicate lines ###
    DuplicateList = set()
    with open('tmpOut2.txt') as f:
      for i in range(3):
        next(f)
      seen = set()
      for line in f:
        if line in seen:
          val = int(line.split('*')[1].replace(' ',''))
          DuplicateList.add( val )
        else:
          seen.add(line)
    print len(DuplicateList), "duplicates of", tree.GetEntries(), "events"
    if( len(DuplicateList) > 0):
      print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      print DuplicateList

    ### Cleanup Temporary Files ###
    os.remove("tmpOut.txt")
    os.remove("tmpOut2.txt")
    del tree
    inFile.Close()

    inFile = TFile.Open(file, "READ")
    ## Create output with duplicates removed if requested ##
    if args.removeDuplicates:
      if len( os.path.dirname(file)) > 0:
        outName = os.path.dirname(file)+'/Duplicateless_'+os.path.basename(file)
      else:
        outName = 'Duplicateless_'+os.path.basename(file)
      if len(DuplicateList) == 0:
        print "No duplicates, copying to", outName
        shutil.copyfile(file, outName)
      else:
        print "Duplicates exist, saving to ", outName
        outFile = TFile.Open(outName, "UPDATE")
        for key in inFile.GetListOfKeys():
          print key.GetName()
          if not key.GetName().startswith("outTree") or (args.nominal and key.GetName() != "outTree"):
            object = inFile.Get( key.GetName() )
            object.Write("", TObject.kOverwrite)
          elif not args.nominal or key.GetName() == "outTree":
            treeName = key.GetName()

            tree = inFile.Get( treeName )

            print "Removing duplicates from ", file
            newTree = tree.CloneTree(0)
            newTree.SetName(treeName)

            eventNumber = array.array('i', [0])
            tree.SetBranchAddress("eventNumber", eventNumber)

            EventNumberDecision = set()
            i=0
            while tree.GetEntry(i):
              i+=1
              if (i%1000) == 0:
                print "Event ", i
              if not eventNumber[0] in EventNumberDecision:
                EventNumberDecision.add(eventNumber[0])
                newTree.Fill()

            newTree.Write("", TObject.kOverwrite)
        outFile.Close()
    inFile.Close()


if __name__ == "__main__":
  checkDuplicates()
  print "Finished checkDuplicates()"
