#!/usr/bin/env python

#******************************************
#makeDataLikeFromTree.py
#******************************************

#******************************************
## argparse import, must be done before ROOT to preserve pyroot options ##
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("-v", dest='verbose', action='store_true', default=False, help="Verbose mode for debugging")
parser.add_argument("--massCombo", dest='massCombo', default="12", type=int, help="Jets to use for combination")
parser.add_argument("--default", dest='default', action='store_true', default=False, help="Run the default configuration, expecting all QCD and QStar files")
parser.add_argument("--nominal", dest='nominal', action='store_true', default=False, help="Run on only the nominal tree")
parser.add_argument("--powheg", dest='powheg', action='store_true', default=False, help="Treat weights like powheg")
parser.add_argument("--scaledOnly", dest='scaledOnly', action='store_true', default=False, help="Do not make data like")
parser.add_argument("--data", dest='data', action='store_true', default=False, help="Make data histograms")
parser.add_argument("--noClean", dest='noClean', action='store_true', default=False, help="Turn off cleaning")
parser.add_argument("--mbj", dest='mbj', action='store_true', default=False, help="Run on mbj distribution")
parser.add_argument("--path", dest='path', default="./gridOutput/tree",
     help="Path to the input trees")
parser.add_argument("--dirTag", dest='dirTag', default="",
     help="Extra tag to append to final directory name")
parser.add_argument("--tags", dest='tags', default="",
     help="Comma seperated list of tags. Input files if they contain all the tags")
parser.add_argument("--lumi", dest='lumi', default=1.0,
     type=float, help="Luminosity of the output sample")
parser.add_argument("--lumies", dest='lumies', default="",
     help="Comma separated string of luminosities")
parser.add_argument("--name", dest='outputName', default="QCDDiJet",
     help="Name of output file")
parser.add_argument("--ystar", dest='ystar', default=0.6,
     type=float, help="yStar cut to apply")
parser.add_argument("--yboost", dest='yboost', default=100.0,
     type=float, help="yBoost cut to apply")
parser.add_argument("--eta", dest='eta', default=100.0,
     type=float, help="jet eta cut to apply")
parser.add_argument("--njets", dest='njets', default=2,
     type=int, help="Minimum number of jets")
parser.add_argument("--leadPt", dest='leadPt', default=440,
     type=float, help="Lead Jet Pt cut to apply")
parser.add_argument("--trigger", dest='trigger', default="",
     help="Trigger to apply")
parser.add_argument("--bTagWP", dest='bTagWP', default="",
     help="B-Tagging working points to apply. Either 85, 77, 70, 60, or None. Seperate WPs to use by a comma, ordered from leading jet down.")
parser.add_argument("--simpleBTag", dest='simpleBTag', default="",
     help="B-Tagging working point to apply. Either 85, 77, 70, or 60. Also include flt or fix, i.e. 77fix. Only 1 WP allowed.")
parser.add_argument("--seed", dest='seed', default=10,
     type=int, help="Seed for getDataLikeHist")
parser.add_argument("--signal", dest='signalType', default="QStar",
     help="Signal type")
parser.add_argument("--signalTreatment", dest='signalTreatment', action='store_true', default=False, help="Only print data-like if integral of effective is > integral of scaled")
parser.add_argument("--bkg", dest='bkgType', default="QCDDiJet",
     help="Background type")
args = parser.parse_args()

## other imports ##
import ROOT, plotUtils, math, os, sys, time, array

#******************************************
## set ATLAS style ##
import AtlasStyle
AtlasStyle.SetAtlasStyle()

#******************************************
def makeDataLikeFromTree():

  print '\nmaking data-like histograms'
  binningName = '13TeV'
  #binningName = 'Coarse13TeV'
  if not args.trigger or len(args.trigger) == 0:
    args.trigger = None

  #------------------------------------------
  ## input parameters ##
  print '\nparameters:'
  print ' path:             %s'%args.path
  print ' tags:             %s'%args.tags
  print ' lumi [fb]:        %s'%args.lumi
  print ' lumies [fb]:      %s'%args.lumies
  print ' output name:      %s'%args.outputName
  print ' y* cut:           %s'%args.ystar
  print ' njets cut:        %s'%args.njets
  print ' lead jet pt cut:  %s'%args.leadPt
  print ' trigger:          %s'%args.trigger
  print ' bTagWP:           %s'%args.bTagWP
  print ' random seed:      %s'%args.seed

  #### Configure jet mass ####
  massJets = sorted(list(str(args.massCombo)))
  for iJet, jet in enumerate(massJets):
    massJets[iJet] = int(jet)-1  #Jets must go from counters to indicies

  if args.mbj:
    massName = "mbj"
  elif args.massCombo == 12:
    massName = "mjj"
  else:
    massName = "m"+str(args.massCombo)

  if massJets[-1] > args.njets:
    args.njets = massJets[-1]

  ## choose ystar cut ##
  if len(massJets) == 2:
    doYstar = True
  else:
    doYstar = False


  #### Configure BTag ####
  allowedBTagWP = ["60", "70", "77", "85", "None"]
  if len(args.bTagWP) == 0:
    bTagWPs = None
  else:
    bTagWPs = args.bTagWP.split(',')
  if bTagWPs and not all( WP in allowedBTagWP for WP in bTagWPs):
    print "Error, BTag Working Points", bTagWPs, "not all in the allowed list ", allowedBTagWP
    exit(1)


  args.tags = args.tags.split(',')
  #------------------------------------------
  ## get the files in args.path which match args.tags ##
  ## get list of merged output files ##
  inputFiles = plotUtils.getMergedTreeFileList(args.path, args.tags)

  print '\nmatching input files:'
  for file in inputFiles:
    print ' %s'%file

## make sure we have found some files ##
  if len(inputFiles) == 0:
    raise SystemExit('\n***EXIT*** no files found for args.tags: %s'%(args.tags))

  #------------------------------------------

  #------------------------------------------
  ## set error sum and overflow ##
  ROOT.TH1.SetDefaultSumw2()
  ROOT.TH1.StatOverflows()

  #------------------------------------------
  ## before creating histograms get all the different NP variations ##
  #open the first input file
  f = ROOT.TFile.Open(inputFiles[0],'READ')

  ## get list of trees with different NP variations ##
  sysNames = []
  sysOutNames = []
  print '\nSystematic Trees:'
  for key in f.GetListOfKeys():
    #if 'outTree' in key.GetName() and not 'JET' in key.GetName(): #for testing on Nominal only
    if 'outTree' in key.GetName() and (not args.nominal or 'outTree' == key.GetName()):
      print " ", key.GetName(),
      sysNames.append(key.GetName())
      outputName = sysNames[-1].lstrip('outTree')
      if outputName == '':
        outputName = 'Nominal'
      sysOutNames.append(outputName)
      if (args.verbose):  print ' %s \t%s'%(sysOutNames[-1], sysNames[-1])
  print ""
  del f


  f_emptyTree = False
  if len(sysNames) == 0:
    print "Error, no trees found.  Are you running on an empty file? (i.e. JZ0)"
    f_emptyTree = True

  #------------------------------------------
  ## output file ##
  outputDir = args.path+'/'+args.dirTag+'dataLikeHists_v'+str(args.seed)+'/'
  #outputDir = args.path+'/../dataLikeHists_v'+str(args.seed)+'/'
  if not os.path.exists(outputDir):
    os.makedirs(outputDir)

  outputFileName = outputDir+'/dataLikeHistograms.'+args.outputName
  if bTagWPs:
    outputFileName += '_'+'o'.join(bTagWPs)+'WP'
  outputFile = ROOT.TFile.Open(outputFileName+'.root','UPDATE')

  ## create directory structure ##
  outputFile.cd()
  for sysOutName in sysOutNames:
    if not outputFile.GetDirectory(sysOutName):
      outputFile.mkdir(sysOutName)


  #------------------------------------------
  ## Format luminosities ##
  if len(args.lumies) == 0:
    lumies = [args.lumi]
  else:
    lumies = [ float(lumi) for lumi in args.lumies.split(',') ]

  # Remove unecessary decimal points
  for iL, lumi in enumerate(lumies):
    if int(lumi) == lumi:
      lumies[iL] = int(lumi)

  #------------------------------------------
  ## histograms ##
  scaledHistograms = [ [] for i in sysNames] # [File][Lumi]

  for itree, tree in enumerate(sysNames):
    histName = massName+'_Scaled_'+args.outputName

    for iL, lumi in enumerate(lumies):
      thisHistName = histName
      thisHistName += '_'+str(lumi).replace('.','p')+'fb'
      if bTagWPs:
        thisHistName += '_'+'+'.join(bTagWPs)+'WP'
      scaledHistograms[itree].append( plotUtils.getMassHist(thisHistName, binningName) )
      scaledHistograms[itree][-1].SetTitle(thisHistName)

#  xaxisLabel = 'm_{12} [GeV]'

  #------------------------------------------
  ## loop over input files ##

  acceptanceFile = open(outputDir+"/../Acceptances.txt", "a")

  for inputFile in inputFiles:
    acceptanceFile.write( "File: "+os.path.basename(inputFile)+'\n' )

    #------------------------------------------
    ## open input file ##
    if (args.verbose): print '\nAdding file: %s'%os.path.basename(inputFile)
    f = ROOT.TFile.Open(inputFile,'READ')

    #------------------------------------------
    ## get number of events from cutflow ##
    sampleEvents = 0
    keys = f.GetListOfKeys()
    for key in keys:

      if 'cutflow' in key.GetName() and ( (args.powheg and 'weighted' in key.GetName()) or (not args.powheg and not 'weighted' in key.GetName()) ):
        sampleEvents = key.ReadObj().GetBinContent(1)
        if args.powheg and "JZ5" in inputFile:
          sampleEvents = sampleEvents - 352220000.
        continue

    if sampleEvents == 0:
      raise SystemExit('\n***WARNING*** no cutflow entries found')

    #------------------------------------------
    ## loop over sysNames ##
    for itree, treeName in enumerate(sysNames):

      #------------------------------------------
      ## open tree ##
      if(args.verbose): print '  adding tree: %s'%treeName
      tree = f.Get(treeName)

      if not tree:
        print ' ***WARNING*** tree', treeName, 'not found in file', inputFile, '. Skipping this tree\n'
        continue

      if(args.verbose): print "Acceptance of Tree ", treeName, tree.GetEntries(), "/", sampleEvents
      acceptanceFile.write( "   Tree: "+treeName+" "+str(tree.GetEntries()) +' / '+str(sampleEvents)+'\n' )

      if tree.GetEntries() == 0:
        print ' ***WARNING*** tree', treeName, ' is empty for file', inputFile, '.  Skipping this tree\n'
        continue

      tree.SetBranchStatus('*',0)
      b_yStar = array.array('f',[0])
      b_jet_eta = ROOT.std.vector('float')()
      b_jet_phi = ROOT.std.vector('float')()
      b_jet_E =   ROOT.std.vector('float')()
      b_mjj = array.array('f',[0])
      b_jet_pt = ROOT.std.vector('float')()
      b_weight = array.array('f', [0])
#      b_weight_btag = array.array('f', [0])
      b_weight_btag = ROOT.std.vector('float')()
      b_mcEventWeight = array.array('f', [0])
      if not args.noClean:
        b_jet_clean_passLooseBad = ROOT.std.vector('int')()
      if args.trigger:
        b_passedTriggers = ROOT.std.vector('string')()
      if bTagWPs:
        b_jet_MV2c20_pass = []
        b_jet_MV2c20_SF = []
        for iWP, thisWP in enumerate(bTagWPs):
          if thisWP == "None":
            b_jet_MV2c20_pass.append(None)
            b_jet_MV2c20_SF.append(None)
          elif iWP > 0 and thisWP in bTagWPs[:iWP]:
            previousWP = bTagWPs.index(thisWP)
            b_jet_MV2c20_pass.append(b_jet_MV2c20_pass[previousWP])
            b_jet_MV2c20_SF.append(b_jet_MV2c20_SF[previousWP])
          else:
            b_jet_MV2c20_pass.append(ROOT.std.vector('int')())
            b_jet_MV2c20_SF.append(ROOT.std.vector(ROOT.std.vector('float'))())


      tree.SetBranchStatus( "weight", 1)
      tree.SetBranchStatus( "yStar", 1)
      tree.SetBranchStatus( "jet_eta", 1)
      tree.SetBranchStatus( "jet_phi", 1)
      tree.SetBranchStatus( "jet_E", 1)
      tree.SetBranchStatus( "jet_pt", 1)
      if args.powheg:
        tree.SetBranchStatus( "mcEventWeight", 1)
      if not args.noClean:
        tree.SetBranchStatus( "jet_clean_passLooseBad", 1)
      if args.trigger:
        tree.SetBranchStatus( "passedTriggers", 1)
      if bTagWPs:
        for thisWP in set(bTagWPs):
          if not thisWP == "None":
            tree.SetBranchStatus( ("jet_MV2c20_is"+thisWP), 1)
            tree.SetBranchStatus( ("jet_MV2c20_SF"+thisWP), 1)
      if args.simpleBTag:
        massType = "mbb"
        if args.mbj:
          massType = "mbj"
        if 'flt' in args.simpleBTag:
          if '77' in args.simpleBTag:
            tree.SetBranchStatus( massType+"_flt_7777", 1)
            tree.SetBranchAddress( massType+"_flt_7777", b_mjj)
            tree.SetBranchStatus( "weight_btag_flt_77", 1)
            tree.SetBranchAddress( "weight_btag_flt_77", b_weight_btag)
          elif '85' in args.simpleBTag:
            tree.SetBranchStatus( massType+"_flt_8585", 1)
            tree.SetBranchAddress( massType+"_flt_8585", b_mjj)
            tree.SetBranchStatus( "weight_btag_flt_85", 1)
            tree.SetBranchAddress( "weight_btag_flt_85", b_weight_btag)
        elif 'fix' in args.simpleBTag:
          if "77" in args.simpleBTag:
            tree.SetBranchStatus( massType+"_fix_7777", 1)
            tree.SetBranchAddress( massType+"_fix_7777", b_mjj)
            tree.SetBranchStatus( "weight_btag_fix_77", 1)
            tree.SetBranchAddress( "weight_btag_fix_77", b_weight_btag)
          elif "85" in args.simpleBTag:
            tree.SetBranchStatus( massType+"_fix_8585", 1)
            tree.SetBranchAddress( massType+"_fix_8585", b_mjj)
            tree.SetBranchStatus( "weight_btag_fix_85", 1)
            tree.SetBranchAddress( "weight_btag_fix_85", b_weight_btag)
        #tree.SetBranchStatus( "mjj", 1)
        #tree.SetBranchAddress( "mjj", b_mjj)


      tree.SetBranchAddress( "weight", b_weight)
      tree.SetBranchAddress( "yStar", b_yStar)
      tree.SetBranchAddress("jet_eta", b_jet_eta)
      tree.SetBranchAddress("jet_phi", b_jet_phi)
      tree.SetBranchAddress("jet_E", b_jet_E)
      tree.SetBranchAddress("jet_pt", b_jet_pt)
      if args.powheg:
        tree.SetBranchAddress( "mcEventWeight", b_mcEventWeight)
      if not args.noClean:
        tree.SetBranchAddress( "jet_clean_passLooseBad", b_jet_clean_passLooseBad )
      if args.trigger:
        tree.SetBranchAddress( "passedTriggers", b_passedTriggers)
      if bTagWPs:
        for iWP, thisWP in enumerate(bTagWPs):
          if not thisWP == "None":
            tree.SetBranchAddress( ("jet_MV2c20_is"+thisWP), b_jet_MV2c20_pass[iWP])
            tree.SetBranchAddress( ("jet_MV2c20_SF"+thisWP), b_jet_MV2c20_SF[iWP])


      #------------------------------------------
      ## Add this file to scaledHistograms ##
      tmpHist = []
      for iL, lumi in enumerate(lumies):
        tmpHist.append( plotUtils.getMassHist('tmpHist_'+str(iL), binningName) )
      count = 0;
      maxEvents = tree.GetEntries()+1
#      maxEvents = 100000
      while tree.GetEntry(count) and count < maxEvents:
        if count % 10000 == 0:
          print count
        count += 1
        fillWeight = 1.

        if b_jet_pt.size() < args.njets:
          continue


        ## All njets leading jets must be clean

        f_failClean = False
        if not args.noClean:
          for iJet in range(args.njets):
            if b_jet_clean_passLooseBad[iJet] == 0:
              f_failClean = True
              continue
        if f_failClean:
          continue

        if doYstar and (abs(b_jet_eta[massJets[0]]-b_jet_eta[massJets[1]])/2) > args.ystar:
          continue
        if doYstar and (abs(b_jet_eta[massJets[0]]+b_jet_eta[massJets[1]])/2) > args.yboost:
          continue
        if ( abs(b_jet_eta[massJets[0]]) > args.eta or abs(b_jet_eta[massJets[1]]) > args.eta ):
          continue
        #if abs(b_yStar[0]) > args.ystar:
        #  continue
        if b_jet_pt[0]  < args.leadPt:
          continue
        if args.powheg and b_mcEventWeight[0] >= 350000000:
          continue
        if args.trigger and not args.trigger in b_passedTriggers:
          continue

        fillWeight *= b_weight[0]

        if bTagWPs:
          if len(bTagWPs) == 1:
            ## If there is only 1 btag specified, require either leading or subleading jet to pass
            ## Add scale factors for both of them??
            if not ( b_jet_MV2c20_pass[0][0] or b_jet_MV2c20_pass[0][1] ):
              continue #next event
            if not args.data:
              if b_jet_MV2c20_pass[0][0]:
                fillWeight *= b_jet_MV2c20_SF[0][0][0]
              elif b_jet_MV2c20_pass[0][1]:
                fillWeight *= b_jet_MV2c20_SF[0][1][0]
          else:
            hasFailedBTag = False
            for iWP, thisWP in enumerate(bTagWPs):
              iJet = iWP  # they are ordered!
              if thisWP == "None":
                continue #next WP
              if iJet >= b_jet_MV2c20_pass[iWP].size():
                hasFailedBTag = True
              elif not( b_jet_MV2c20_pass[iWP][iJet] ):
                hasFailedBTag = True
              else:
                if not args.data:
                  fillWeight *= b_jet_MV2c20_SF[iWP][iJet][0]


            if hasFailedBTag:
              continue #next event


        if args.simpleBTag:
          mass = b_mjj[0]
          fillWeight *= b_weight_btag.at(0)
          #fillWeight *= b_weight_btag[0]
        else:
          mass = getMassCombo(massJets, b_jet_pt, b_jet_eta, b_jet_phi, b_jet_E)
        #mass = b_mjj[0]

        for iL, lumi in enumerate(lumies):
          thisFillWeight = fillWeight
          if not args.data:
            thisFillWeight *= float(lumi)/sampleEvents
          tmpHist[iL].Fill( mass, thisFillWeight )

      for iL, lumi in enumerate(lumies):
        tmpHist[iL].Draw()
        scaledHistograms[itree][iL].Add(tmpHist[iL])
        tmpHist[iL].Clear()
        tmpHist[iL].Delete()

      del tree

    f.Close()
  acceptanceFile.close()

  #------------------------------------------
  ## get data-like histograms ##

  ## loop over trees ##
  for itree, sysDir in enumerate(sysOutNames):

    if not args.data:
      ## get effective entries mjj histogram ##
      effectiveEntryHists = []
      for iL, lumi in enumerate(lumies):
        effectiveEntryHists.append( plotUtils.getEffectiveEntriesHistogram(scaledHistograms[itree][iL], scaledHistograms[itree][iL].GetName().replace('Scaled','EffectiveEntries')) )

      ## get data-like mjj histogram ##
      if not args.scaledOnly:
        names = [ hist.GetName().replace('Scaled','DataLike') for hist in scaledHistograms[itree] ]
        dataLikeHists = plotUtils.getDataLikeHists(effectiveEntryHists[0], scaledHistograms[itree], names, args.seed)
        for dataLikeHist in dataLikeHists:
          if not dataLikeHist:
            print ' ***WARNING*** data-like histogram does not have enough effective entries: ', inputFile, ', sysDir ', sysDir, ', lumi', lumi, ', fabs(ystar) < ', args.ystar, ', and output args.luminosity', args.lumi, '.\n'


    #------------------------------------------
    ## write output file ##
    outputFile.cd(sysDir)
    for iL, lumi in enumerate(lumies):
      if args.data:
        scaledHistograms[itree][iL].SetName( scaledHistograms[itree][iL].GetName().replace('Scaled', 'Data' ) )
        scaledHistograms[itree][iL].SetTitle( scaledHistograms[itree][iL].GetTitle().replace('Scaled', 'Data' ) )
        scaledHistograms[itree][iL].Write(scaledHistograms[itree][iL].GetName(), ROOT.TObject.kOverwrite)
      else:
        scaledHistograms[itree][iL].Write(scaledHistograms[itree][iL].GetName(), ROOT.TObject.kOverwrite)
        effectiveEntryHists[iL].Write(effectiveEntryHists[iL].GetName(), ROOT.TObject.kOverwrite)
        if not args.scaledOnly and dataLikeHists[iL]:
          #if doing signal, need to check integral because of pileup issues
          if not args.signalTreatment or effectiveEntryHists[iL].Integral() > scaledHistograms[itree][iL].Integral():
            dataLikeHists[iL].Write(dataLikeHists[iL].GetName(), ROOT.TObject.kOverwrite)

 #------------------------------------------
 #END tree loop

  outputFile.Close()

def getMassCombo(massJets, jet_pt, jet_eta, jet_phi, jet_E):

  X, Y, Z, E = 0., 0., 0., 0.
  for iJet in massJets:
    E += jet_E[iJet]
    apt = abs( jet_pt[iJet] )
    X += apt*ROOT.TMath.Cos( jet_phi[iJet] )
    Y += apt*ROOT.TMath.Sin( jet_phi[iJet] )
    Z += apt/ROOT.TMath.Tan(2.0*ROOT.TMath.ATan(ROOT.TMath.Exp(-jet_eta[iJet])))

  mass_mag2 = E*E-(X*X+Y*Y+Z*Z)
  if mass_mag2 < 0:
    print "Error, negative mass!"
    exit(1)
  else:
    mass = ROOT.TMath.Sqrt( mass_mag2 )
  return mass



#******************************************
if __name__ == '__main__':

  #------------------------------------------
  ## Do the default configuration ##

  if args.default:
    if len(args.lumies) == 0:
      args.lumies = "0.1,0.3,0.5,0.7,1,1.1,1.2,1.3,1.4,1.5,2,3,4,10"

    signalTypes = []

    if "WPrime" in args.signalType:
      signalOutName = "WPrime"
      signalInName = "Wprime_qq_"
      signalTypes = [1000, 1200, 1500, 1700, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500]

    if "ZPrime" in args.signalType:
      signalOutName = "ZPrime"
      signalInName = "dijet_"
      signalTypes = [
#      "mR1000_mDM10000_gSM0p10", "mR1000_mDM10000_gSM0p20", "mR1000_mDM10000_gSM0p30",
#      "mR1500_mDM10000_gSM0p10", "mR1500_mDM10000_gSM0p20", "mR1500_mDM10000_gSM0p30",
#      "mR2000_mDM10000_gSM0p10", "mR2000_mDM10000_gSM0p20", "mR2000_mDM10000_gSM0p30", "mR2000_mDM10000_gSM0p40",
#      "mR2500_mDM10000_gSM0p20", "mR2500_mDM10000_gSM0p30", "mR2500_mDM10000_gSM0p40", "mR2500_mDM10000_gSM0p50", "mR2500_mDM10000_gSM0p60",
#      "mR3000_mDM10000_gSM0p40", "mR3000_mDM10000_gSM0p50", "mR3000_mDM10000_gSM0p60", "mR3000_mDM10000_gSM0p70", "mR3000_mDM10000_gSM0p80",
#      "mR3500_mDM10000_gSM0p70", "mR3500_mDM10000_gSM0p80", "mR3500_mDM10000_gSM0p90", "mR3500_mDM10000_gSM1p00"]
      "mR2000_mDM10000_gSM0p10"
      ]
#      signalTypes = [
#      "mR2p5_mDM10_gSM0p10",
#      "mR3p0_mDM10_gSM0p10",
#      "mR3p0_mDM10_gSM0p20",
#      "mR3p0_mDM10_gSM0p30",
#      "mR3p5_mDM10_gSM0p10",
#      "mR3p5_mDM10_gSM0p20",
#      "mR3p5_mDM10_gSM0p30",
#      "mR3p5_mDM10_gSM0p40",
#      "mR3p5_mDM10_gSM0p50",
#      "mR3p5_mDM10_gSM0p60"
#      ]

    elif "QBHRSFullSim" in args.signalType:
      signalOutName = "QBHRS"
      signalInName = "Mth"
      signalTypes = ["04500", "08500"]

    elif "QBHRS" in args.signalType:
      signalOutName = "QBHRS"
      signalInName = "Mth"
      signalTypes = ["04500", "05000", "05500", "06000", "06500", "07000", "07500", "08000", "08500"]

    elif "QStar" in args.signalType:
     signalOutName = "QStar"
     signalInName = "ExcitedQ"
     signalTypes = [500, 600, 800, 1000, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500]

    ###### For QBH ######
    elif "QBHADDFullSim" in args.signalType:
      signalOutName = "QBHADD"
      signalInName = "Mth"
      signalTypes = ["04500", "10000"]

    elif "QBHADD" in args.signalType:
      signalOutName = "QBHADD"
      signalInName = "Mth"
      signalTypes = ["04500", "05000", "05500", "06000", "06500", "07000", "07500", "08000", "08500", "09000", "09500", "10000"]

    ##### For BlackMax ######
    elif "BlackMax50" in args.signalType:
      signalOutName = "BlackMax"
      signalInName = "MthMD"
      signalTypes = [6000]

    elif "BlackMax" in args.signalType:
      signalOutName = "BlackMax"
      signalInName = "MthMD"
      signalTypes = [4000, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000]

    elif not "None" in args.signalType:
      print "Requested signal ", args.signalType, "not found, exiting"
      exit(1)

    for iSignal in range(len(signalTypes)):
      signalTypes[iSignal] = signalInName+str(signalTypes[iSignal])


    if not "None" in args.bkgType:
      args.signalTreatment = False
      args.tags = "Pythia8,jetjet" #,A14NNPDF23LO"
      args.outputName = args.bkgType
      makeDataLikeFromTree()


    if not "None" in args.signalType:
      args.signalTreatment = True
      for signalTag in signalTypes:
        args.tags = signalTag
        args.outputName = signalTag.replace(signalInName, signalOutName)
        if "ZPrime" in args.signalType:
          args.outputName = args.outputName.replace("_mDM10000_", "").replace("_mDM10_","")
        makeDataLikeFromTree()

  #------------------------------------------
  ## Do a single configuration ##
  else:
   makeDataLikeFromTree()

  print '\ndone'

