###############################################
#  plotUtils.py                               #
#                                             #
#  purpose: collect common plotting functions #
#                                             #
###############################################

import ROOT
from array import array
import re, sys, os, math, numpy
from decimal import Decimal

##############################################################################################
##############################################################################################
# LIST OF FUNCTIONS BELOW AND DESCRIPTION
#  getMassBins : get an array with the standard binning for the mJJ histogram
#  getMassHist : get histogram using the standard binning for the mJJ histogram
#  printHist   : dump hist content
#  checkHistPoissonianErrors : compare errors to sqrt(N) - print out only
#  setHistPoissonianErrors : set bin errors to sqrt(N)
#  roundHistogram : round histogram bin content to int - fill bin i N_i times with weight 1
#  getSignalMass : get signal mass tag from a string - NEEDS UPDATING
#  getQStarXSec  : get QStar XSEC - currently 8 TeV - NEEDS UPDATING
#  getQBHXSec    : get QBH XSEC - currently 8 TeV - NEEDS UPDATING
#  getScaleFactor: get xsec * lumi / nEvents - lumi and nEvents are inputs
#  getHistogramContainingPercentage : NEED TO READ
#  getEffectiveEntriesHistogram : returns hist w/ N event (weight=1) with equal error to input hist
#  createPseudoDataHist : fill hist from PDF crated from input hist
#  getDataLikeHist : given effective entries and input hist at desired lumi, give hist with weight=1 with shape and lumi of input hist
##############################################################################################
##############################################################################################

###############################################
# Get an array which defines the bin boundaries for the standard dijet mass plot
#def getMassBins( opt = "13TeV" ):

#  binLowE = []

#  if "13TeV" in opt:
    # original binning - 8 TeV + some guesses afterwards
    #binLowE = [96.0, 105, 116, 127, 138, 151, 164, 177, 192, 207, 222, 239, 256, 274, 293, 312, 333, 354, 376, 400, 424, 449, 475, 502, 530, 558, 587, 618, 650, 682, 716, 750, 787, 825, 865, 906, 949, 994, 1040, 1088, 1138, 1189, 1242, 1297, 1353, 1410, 1469, 1531, 1595, 1663, 1733, 1807, 1884, 1964, 2047, 2132, 2221, 2313, 2407, 2503, 2602, 2703, 2807, 2916, 3028, 3146, 3267, 3394, 3525, 3660, 3801, 3946, 4096, 4250, 4409, 4571, 4738, 4909, 5085, 5267, 5453, 5644, 5843, 6066, 6311, 6566, 6831, 7107, 7394, 7692, 8003, 8326, 8662, 9012, 9376, 9755, 10149, 10559, 10985, 11429, 11890, 12370, 12870, 13390, 13930, 14493]
    # binning 1st pass in collaboration with SM jet group
#    binLowE = [0, 30, 70, 110, 160, 210, 260, 310,  370, 440, 510, 590, 670, 760, 850, 950, 1060, 1180, 1310, 1450, 1600, 1760, 1940, 2120, 2330, 2550, 2780, 3040, 3310, 3610, 3930, 4270, 4640, 5040, 5470, 5940, 6440, 7000, 7495, 8104, 8735, 9353, 10000]
    #Ning Binning
    #binLowE = [0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,3300,3400,3500,3600,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,4700,4800,4900,5000]

#  if "8TeV" in opt:
#    binLowE = [0.0,20.0,40.0,60.0,80.0,100.0,120.0,140.0,160.0,180.0,200.0,216.0,234.0,253.0,272.0,294.0,316.0,339.0,364.0,390.0,417.0,445.0,474.0,504.0,535.0,566.0,599.0,633.0,668.0,705.0,743.0,782.0,822.0,864.0,907.0,952.0,999.0,1048.0,1098.0,1150.0,1203.0,1259.0,1316.0,1376.0,1437.0,1501.0,1567.0,1635.0,1706.0,1779.0,1854.0,1932.0,2012.0,2095.0,2181.0,2269.0,2360.0,2454.0,2551.0,2650.0,2753.0,2860.0,2970.0,3084.0,3202.0,3324.0,3450.0,3581.0,3716.0,3855.0,3999.0,4149.0,4303.0,4463.0,4629.0,4800.0,4977.0,5160.0,5350.0,5546.0,5748.0,5958.0,6174.0,6397.0,6628.0,6866.0,7112.0,7366.0,7628.0,7898.0,8177.0]
    #Ning Binning
#  return binLowE

###############################################
# Get an empty histogram with the binning for the standard dijet mass plot
#def getMassHist( name, opt = "13TeV" ):
#  bins = getMassBins( opt )
#  hist = ROOT.TH1F( name, name, len(bins)-1, array('f', bins) )
#  hist.Sumw2()
#  return hist

###############################################
# Get an array which defines the bin boundaries for user histograms
def getBins( opt = "Simple" ):
  #binLowE = numpy.array()
  binLowE = [] #numpy.array([])

  if "Simple" in opt:
    binLowE = [0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,3300,3400,3500,3600,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,4700,4800,4900,5000]
  
  if "finepTBins" in opt:
    #binLowE = [15. ,20.,25. ,35. ,45. ,55. ,70. ,85. ,100. ,116. ,134. ,152. ,172. ,194. ,216. ,240. ,264. ,290. ,318. ,346.,376.,408.,442.,478.,516.,556.,598.,642.,688.,736.,786.,838.,894.,952.,1012.,1076.,1162.,1310.,1530.,1992.,2500.,4500.,7000.]
    binLowE = [.015 ,.02,.025 ,.035 ,.045 ,.055 ,.07 ,.085 ,.1 ,.116 ,.134 ,.152 ,.172 ,.194 ,.216 ,.240 ,.264 ,.290 ,.318 ,.346,.376,.408,.442,.478,.516,.556,.598,.642,.688,.736,.786,.838,.894,.952,1.012,1.076,1.162,1.310,1.530,1.992,2.500,4.500,7.000]

  if "modpTBins" in opt: # same as fine pT bins but bin boundary at 150 GeV to match cut
    #binLowE = [15. ,20.,25. ,35. ,45. ,55. ,70. ,85. ,100. ,116. ,134. ,150. ,172. ,194. ,216. ,240. ,264. ,290. ,318. ,346.,376.,408.,425.,442.,478.,516.,556.,598.,642.,688.,736.,786.,838.,894.,952.,1012.,1076.,1162.,1310.,1530.,1992.,2500.,4500.,7000.]
    binLowE = [.015 ,.02,.025 ,.035 ,.045 ,.055 ,.07 ,.085 ,.1 ,.116 ,.134 ,.15 ,.172 ,.194 ,.216 ,.240 ,.264 ,.290 ,.318 ,.346,.376,.408,.442,.478,.516,.556,.598,.642,.688,.736,.786,.838,.894,.952,1.012,1.076,1.162,1.310,1.530,1.992,2.500,4.500,7.000]

  if "coarsepTBins" in opt:
    #binLowE = [50., 100., 152., 240., 408., 598., 1012., 2500.,4500.,7000.]
    binLowE = [.050, .100, .152, .240, .408, .598, 1.012, 2.500,4.500,7.000]
  
  if "fineetaBins" in opt: # From Fabrice
    binLowE = [0,0.3,0.8,1.2,2.1,2.8,3.6,4.4]

  if "coarseetaBins" in opt:
    binLowE = [0,1.2,2.0,2.8,3.2,4.5]
 
  if "finenSegBins" in opt: # Remove once fixed C2 and C3 FIXME
    binLowE = [0,1]
    for x in xrange(5,1005,5):
      binLowE.append(x)

  if "Shaunpt" in opt:
    npTBinsForVector=150
    pTRange=5000.;
    logBins = numpy.array([])
    dx = (math.log(pTRange) - math.log(10))/npTBinsForVector
    for i in range (npTBinsForVector+1): # add on 1 due to python range function definition
      logBins = numpy.append(logBins, math.exp(math.log(10)+i*dx))
    binLowE = logBins.tolist()
    for iBin, val in enumerate(binLowE):
      binLowE[iBin] = binLowE[iBin]/1000.	

  if "Shauneta" in opt:
    nEtaBins=60
    etaRange=3.
    etaBins = numpy.array([]) 
    for i in range (nEtaBins+1): 
      etaBins = numpy.append(etaBins, etaRange*(2.*i/nEtaBins-1.))
    binLowE = etaBins.tolist()
  
  if "Shaunphi" in opt:
    nPhiBins=100
    phiRange= math.pi
    phiBins = numpy.array([])
    for i in range (nPhiBins+1): 
      phiBins = numpy.append(phiBins, phiRange*(2.*i/nPhiBins-1.))
    binLowE = phiBins.tolist()

  if "coarsephiBins" in opt:
    nPhiBins=60
    phiRange= math.pi
    phiBins = numpy.array([])
    for i in range (nPhiBins+1): 
      phiBins = numpy.append(phiBins, phiRange*(2.*i/nPhiBins-1.))
    binLowE = phiBins.tolist()

  if "Meghanseg" in opt: 
    binLowE = [0,1,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,90,100,300]
  
  if "Shaunseg" in opt: 
    binLowE = [0,1,5,10,15,20,25,50,75,100,150,200,300,1000] 
 
  if "Shaunlargeeta" in opt:
    binLowE = [0, 1.3, 1.9,2.7]
 
  if "Shaunlargept" in opt:
    binLowE = [50, 60, 80, 100,  200,  600,  1000, 1600, 3000, 4500] # large pt bins  

  if "respBins" in opt:
     binLowE =[0,0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,0.20,0.21,0.22,0.23,0.24,0.25,0.26,0.27,0.28,0.29,0.30,0.31,0.32,0.33,0.34,0.35,0.36,0.37,0.38,0.39,0.40,0.41,0.42,0.43,0.44,0.45,0.46,0.47,0.48,0.49,0.50,0.51,0.52,0.53,0.54,0.55,0.56,0.57,0.58,0.59,0.60,0.61,0.62,0.63,0.64,0.65,0.66,0.67,0.68,0.69,0.70,0.71,0.72,0.73,0.74,0.75,0.76,0.77,0.78,0.79,0.80,0.81,0.82,0.83,0.84,0.85,0.86,0.87,0.88,0.89,0.90,0.91,0.92,0.93,0.94,0.95,0.96,0.97,0.98,0.99,1.0,1.01,1.02,1.03,1.04,1.05,1.06,1.07,1.08,1.09,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20,1.21,1.22,1.23,1.24,1.25,1.26,1.27,1.28,1.29,1.30,1.31,1.32,1.33,1.34,1.35,1.36,1.37,1.38,1.39,1.40,1.41,1.42,1.43,1.44,1.45,1.46,1.47,1.48,1.49,1.50,1.51,1.52,1.53,1.54,1.55,1.56,1.57,1.58,1.59,1.60,1.61,1.62,1.63,1.64,1.65,1.66,1.67,1.68,1.69,1.70,1.71,1.72,1.73,1.74,1.75,1.76,1.77,1.78,1.79,1.80,1.81,1.82,1.83,1.84,1.85,1.86,1.87,1.88,1.89,1.90,1.91,1.92,1.93,1.94,1.95,1.96,1.97,1.98,1.99,2.0]
  
  if "asymBins" in opt:
     binLowE =[0,0.1,.20,0.30,0.40,0.50,0.60,0.70,0.80,0.90,1.0]

  if "coarserespBins" in opt:
     binLowE =[0,0.08,0.16,0.24,0.32,0.40,0.48,0.56,0.64,0.72,0.80,0.88,0.96,1.04,1.12,1.20,1.28,1.36,1.44,1.52,1.60,1.68,1.76,1.84,1.92,2.0]
     #binLowE =[0,0.02,0.04,0.06,0.08,0.1,0.12,0.14,0.16,0.18,0.20,0.22,0.24,0.26,0.28,0.30,0.32,0.34,0.36,0.38,0.40,0.42,0.44,0.46,0.48,0.50,0.52,0.54,0.56,0.58,0.60,0.62,0.64,0.66,0.68,0.70,0.72,0.74,0.76,0.78,0.80,0.82,0.84,0.86,0.88,0.90,0.92,0.94,0.96,0.98,1.0,1.02,1.04,1.06,1.08,1.10,1.12,1.14,1.16,1.18,1.20,1.22,1.24,1.26,1.28,1.30,1.32,1.34,1.36,1.38,1.40,1.42,1.44,1.46,1.48,1.50,1.52,1.54,1.56,1.58,1.60,1.62,1.64,1.66,1.68,1.70,1.72,1.74,1.76,1.78,1.80,1.82,1.84,1.86,1.88,1.90,1.92,1.94,1.96,1.98,2.0]
     #binLowE =[0,0.04,0.08,0.12,0.16,0.20,0.24,0.28,0.32,0.36,0.40,0.44,0.48,0.52,0.56,0.60,0.64,0.68,0.72,0.76,0.80,0.84,0.88,0.92,0.96,1.0,1.04,1.08,1.12,1.16,1.20,1.24,1.28,1.32,1.36,1.40,1.44,1.48,1.52,1.56,1.60,1.64,1.68,1.72,1.76,1.80,1.84,1.88,1.92,1.96,2.0]

  if "ChiBins" in opt:
    binLowE = [1., 1.34986, 1.82212, 2.4596, 3.32012, 4.48169, 6.04965, 8.16617, 11.0232, 14.8797, 20.0855, 30.]

  if "MjjBinsForChi" in opt:
    binLowE = [600, 800, 1200, 1600, 2000, 2600, 3200, 8000, 10000]
  
  if "13TeV" in opt:
    #binLowE = [0, 30, 70, 110, 160, 210, 260, 310,  370, 440, 510, 590, 670, 760, 850, 950, 1060, 1180, 1310, 1450, 1600, 1760, 1940, 2120, 2330, 2550, 2780, 3040, 3310, 3610, 3930, 4270, 4640, 5040, 5470, 5940, 6440, 7000, 7495, 8104, 8735, 9353, 10000]
    binLowE = [0., .030, .070, .110, .160, .210, .260, .310, .370, .440, .510, .590, .670, .760, .850, .950, 1.060, 1.180, 1.310, 1.450, 1.600, 1.760, 1.940, 2.120, 2.330, 2.550, 2.780, 3.040, 3.310, 3.610, 3.930, 4.270, 4.640, 5.040, 5.470, 5.940, 6.440, 7.000, 7.495, 8.104, 8.735, 9.353, 10.000]
     ## Fine Dijet binning used in analysis To use: Brian's June 18th 96 bins Option 9 ##
    #binLowE = [946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055]
  return binLowE

###############################################
# Get an empty TH1F histogram with  either regular or variable binning specified by user
# For regular binning use e.g. '100,0,10' as opt to obtain 100 bins between 0 and 10
def get1DHist( name, opt = "Simple" ):
  if ',' in opt:
    nbins = int(opt.split(",")[0])
    firstbin = float(opt.split(",")[1])
    lastbin = float(opt.split(",")[2])
    hist = ROOT.TH1F( name, name, nbins, firstbin,lastbin )
    hist.Sumw2()
  else:
    bins = getBins( opt )
    hist = ROOT.TH1F( name, name, len(bins)-1, array('f', bins) )
    hist.Sumw2()
  return hist

###############################################
# Get an empty TH2F histogram with  either regular or variable binning specified by user
# For regular binning use e.g. '100,0,10' as opt1 or opt2 to obtain 100 bins between 0 and 10
def get2DHist( name, opt1 = "Simple",opt2 = "Simple" ):
  if ',' in opt1 and ',' in opt2:
    nbins1 = int(opt1.split(",")[0])
    nbins2 = int(opt2.split(",")[0])
    firstbin1 = float(opt1.split(",")[1])
    firstbin2 = float(opt2.split(",")[1])
    lastbin1 = float(opt1.split(",")[2])
    lastbin2 = float(opt2.split(",")[2])
    hist = ROOT.TH2F( name, name, nbins1, firstbin1,lastbin1,nbins2, firstbin2,lastbin2)
    hist.Sumw2()
  if ',' in opt1 and ',' not in opt2:
    print "C2 FIX"
    print opt1 
    print opt2
    nbins1 = int(opt1.split(",")[0])
    print nbins1
    print type(nbins1)
    firstbin1 = float(opt1.split(",")[1])
    print type(firstbin1)
    print firstbin1
    lastbin1 = float(opt1.split(",")[2])
    print type(lastbin1)
    print lastbin1
    bins2 = getBins( opt2 )
    print bins2
    #hist = ROOT.TH2F( name, name, 2, array('f',[90,1,2]) ,2,3,4)
    hist = ROOT.TH2F( name, name, nbins1, firstbin1,lastbin1,len(bins2)-1, array('f', bins2))
    hist.Sumw2()
  if ',' not in opt1 and ',' in opt2:
    print "C3 FIX"
    nbins2 = int(opt2.split(",")[0])
    firstbin2 = float(opt2.split(",")[1])
    lastbin2 = float(opt2.split(",")[2])
    bins1 = getBins( opt1 )
    hist = ROOT.TH2F( name, name, len(bins1)-1, array('f', bins1), nbins2, firstbin2, lastbin2 )
    hist.Sumw2()
  if ',' not in opt1 and ',' not in opt2:  
    bins1 = getBins( opt1 )
    bins2 = getBins( opt2 )
    hist = ROOT.TH2F( name, name, len(bins1)-1, array('f', bins1),len(bins2)-1, array('f', bins2) )
    hist.Sumw2()
  return hist

###############################################
# Get an empty TH3F histogram with variable binning specified by user # FIXME so work for uniform and mixed too
def get3DHist( name, opt1 = "Simple",opt2 = "Simple",opt3 = "Simple" ):
  if ',' not in opt1 and ',' not in opt2 and ',' not in opt3:  
    bins1 = getBins( opt1 )
    bins2 = getBins( opt2 )
    bins3 = getBins( opt3 )
    hist = ROOT.TH3F( name, name, len(bins1)-1, array('f', bins1),len(bins2)-1, array('f', bins2),len(bins3)-1, array('f', bins3) )
    hist.Sumw2()
  return hist

#******************************************
def printHist (hist):
    """print histogram content
    """
    for bin in xrange(1,hist.GetNbinsX()+1):
        print "bin %5s = %10.4f +/- %10.4f"%(bin, hist.GetBinContent(bin), hist.GetBinError(bin))

#******************************************
def checkHistPoissonianErrors (hist):
    """check if the errors of the histogram are Poissonian
    """
    for bin in xrange(1,hist.GetNbinsX()+1):
        if hist.GetBinContent(bin) != 0:
            if hist.GetBinError(bin) != math.sqrt(hist.GetBinContent(bin)):
                print "bin %5s = %10.4f +/- %10.4f\t->\t%10.4f"%(bin, hist.GetBinContent(bin), hist.GetBinError(bin), math.sqrt(hist.GetBinContent(bin)))
            else:
                print "bin %5s is ok"%bin
        else:
            print "bin %5s is empty"%bin

#******************************************
def setHistPoissonianErrors (hist, debug=False):
    """set Poissonian errors
    """
    for bin in xrange(1,hist.GetNbinsX()+1):
        hist.SetBinError(bin, math.sqrt(hist.GetBinContent(bin)))
        if debug:
            print "bin %5s = %10.4f +/- %10.4f"%(ii, hist.GetBinContent(bin), hist.GetBinError(bin))
    return hist

#******************************************
def roundHistogram (hist):
    """round the histogram bin content to integer numbers; NOTE fill histogram one entry at a time
    """
    newHist = ROOT.TH1D("newHist","newHist",hist.GetXaxis().GetNbins(),hist.GetXaxis().GetXbins().GetArray())
    newHist.SetDefaultSumw2(ROOT.kTRUE)
    for bin in xrange(1,hist.GetNbinsX()+1):
        bincontent = int(round(hist.GetBinContent(bin)))
        for iBinCont in xrange(bincontent):
            newHist.Fill(newHist.GetBinCenter(bin))
    return newHist

#******************************************
def getSignalMass(name):
    if "ExcitedQ" in name:
        regEx = re.compile("ExcitedQ(.*)Lambda")
    elif "BlackMax" in name:
        regEx = re.compile("MthMD(.*)\.e2303")
    else:
        regEx = re.compile("_14TeV(.*)_AU2")

    match = regEx.search(name)
    sample = match.group(1)
    return sample

#******************************************
def getQStarXSec(mass):
    #cross sections are in nb
    sampleDict = {}
    sampleDict["2000"]  = 3.680e-02
    sampleDict["3000"]  = 2.985e-03
    sampleDict["4000"]  = 3.534e-04
    sampleDict["5000"]  = 4.833e-05
    sampleDict["6000"]  = 7.047e-06
    sampleDict["7000"]  = 1.039e-06
    sampleDict["8000"]  = 1.626e-07
    sampleDict["9000"]  = 3.186e-08
    sampleDict["10000"] = 9.205e-09
    sampleDict["11000"] = 3.653e-09
    sampleDict["12000"] = 1.684e-09
    sampleDict["13000"] = 8.456e-10

    #return cross section in nb
    #print 'QStar [%s GeV] cross section: %s nb'%(mass, str(sampleDict[mass]/1e3))
    return sampleDict[mass]

#******************************************
#function taken from CommonFunctions.py
def getQBHXSec(mass):
	#cross sections are in pb
    sampleDict = {}
    sampleDict["2000"] = 2.570e+03
    sampleDict["3000"] = 2.194e+02
    sampleDict["4000"] = 2.742e+01
    sampleDict["5000"] = 3.850e+00
    sampleDict["6000"] = 5.507e-01
    sampleDict["7000"] = 7.374e-02
    sampleDict["8000"] = 8.615e-03
    sampleDict["9000"] = 8.318e-04
    sampleDict["10000"] = 5.754e-05
    sampleDict["11000"] = 2.431e-06
    sampleDict["12000"] = 4.606e-08

    #return cross section in nb
    #print 'QBH [%s GeV] cross section: %s nb'%(mass, str(sampleDict[mass]/1e3))
    return sampleDict[mass]/1e3

#******************************************
def getScaleFactor(model, mass, lumi, nEvents):
    #print "lumi = %s nb^-1"%lumi
    if model == "QStar":
        return (getQStarXSec(mass) * lumi) / nEvents
    elif model == "QBH":
        return (getQBHXSec(mass) * lumi) / nEvents
    else:
        raise SystemExit("\n***ERROR*** unknown signal sample: %s"%name)

#******************************************
def getHistogramContainingPercentage(hist, percentage, lowBin=-999, highBin=-999):

    if lowBin == -999 : lowBin = 1
    if highBin == -999 : highBin = hist.GetNbinsX()+1

    nbins = hist.GetNbinsX()
    if (nbins==0) : return None

    integral = hist.Integral()
    if (integral==0) : return None

    firstBin = 1
    while (hist.GetBinContent(firstBin)==0) : firstBin = firstBin+1

    lastBin = nbins
    while (hist.GetBinContent(lastBin)==0) : lastBin = lastBin-1

    binsToScan = lastBin - firstBin
    if (binsToScan==0) : return None

    scanStep = binsToScan/100
    if (scanStep<1) : scanStep=1

    thisPercentage=0
    thisInterval=0

    remember1=1
    remember2=1

    #a really big interval to start with.
    smallestInterval= hist.GetBinLowEdge(lastBin) + hist.GetBinWidth(lastBin) - hist.GetBinLowEdge(firstBin) + 1e12

    rememberThisPercentage=0

    for bin1 in xrange(firstBin, lastBin+1, scanStep) :
        for bin2 in xrange(bin1, lastBin+1, scanStep) :

            #print "bins ", bin1, " to ", bin2
            thisPercentage = (hist.Integral(bin1,bin2))/integral
            thisInterval = hist.GetBinLowEdge(bin2) +  hist.GetBinWidth(bin2) - hist.GetBinLowEdge(bin1)
            #print " -. contain ", thisPercentage

            if thisPercentage >= percentage :
                if thisInterval < smallestInterval :
                    remember1=bin1
                    remember2=bin2
                    smallestInterval=thisInterval
                    rememberThisPercentage=thisPercentage
                break

    #print "best interval are bins : ", remember1, " , ", remember2
    #print " which span ", smallestInterval, "GeV"
    lowBin=remember1
    highBin=remember2

    #now make the histogram
    choppedHist = hist.Clone()
    choppedHist.Reset("ICE")
    for iBin in xrange(1, hist.GetNbinsX()+1) :
        if iBin < lowBin : continue
        if iBin > highBin : continue
        choppedHist.SetBinContent(iBin, hist.GetBinContent(iBin))
        choppedHist.SetBinError(iBin, hist.GetBinError(iBin))

    #print "bins [" , lowBin, "," , highBin , "] contain " , rememberThisPercentage*100, "percent of the signal"
    return choppedHist

#******************************************
def getEffectiveEntriesHistogram(hist, name = "hee"):
    hee = ROOT.TH1D(name,name,hist.GetXaxis().GetNbins(),hist.GetXaxis().GetXbins().GetArray())
    for bin in xrange(1,hist.GetNbinsX()+1):
        if hist.GetBinError(bin) != 0.:
            nee = pow(hist.GetBinContent(bin), 2) /  pow(hist.GetBinError(bin), 2)
        else:
            nee = 0.
        hee.SetBinContent(bin, nee)
        hee.SetBinError(bin, math.sqrt(nee))
    return hee

#******************************************
def createPseudoDataHist(inHist):

    #nEvents = inHist.GetSumOfWeights()
    nEvents = inHist.Integral()
    #nEvents = inHist.GetEntries() * lumi

    print "number of events: %s"%nEvents
    random = ROOT.TRandom3(1986)
    gRandom = random

    normalizedMassDist = inHist.Clone("normalized_mjj")
    normalizedMassDist.Scale(1./normalizedMassDist.Integral())
    nEventsFluctuated = random.Poisson(floor(nEvents))
    pseudoData = inHist.Clone("pseudo_data")
    pseudoData.Reset()
    pseudoData.FillRandom(normalizedMassDist, int(nEventsFluctuated))

    return pseudoData

#******************************************
def getDataLikeHist( eff, scaled, name, jobSeed = 10 ):
  dataLike = ROOT.TH1D(name, name, eff.GetXaxis().GetNbins(),eff.GetXaxis().GetXbins().GetArray())
  dataLike.SetDirectory(0)
  #random number generator
  rand3 = ROOT.TRandom3(1986) #1986 #ORIGINAL

  #loop over bins
  for bin in xrange(1,eff.GetNbinsX()+1):
    #enough effective entries?
    nee = eff.GetBinContent(bin)
    if nee >= scaled.GetBinContent(bin):

      #set seed
      #NOTE the seed for each bin must be always the same
      binSeed = int( round( eff.GetBinCenter(bin) + jobSeed*1e5 ))
      #print '  bin %s\t seed = %i'%(bin, binSeed)
      rand3.SetSeed(binSeed)

      #get data-like bin content by drawing entries
      #NOTE weights are poissonian by construction
      for jj in xrange( int( round( nee ) ) ):
        if rand3.Uniform() < scaled.GetBinContent(bin) / nee :
          dataLike.Fill(dataLike.GetBinCenter(bin))

  return dataLike

#******************************************
def getLumiFromFileName(fileName):
    #get luminosity value from file name
    keys = fileName.split('.')
    for ii in xrange(len(keys)):
        if keys[ii] == 'ifb':
            return keys[ii-1]
    raise SystemExit('\n***ERROR*** couldn\'t find luminosity for file: %s'%fileName)

#******************************************
def getSignalMassFromFileName(fileName):
    #get luminosity value from file name
    keys = fileName.split('.')
    for ii in xrange(len(keys)):
        if keys[ii] == 'GeV':
            return keys[ii-1]
    raise SystemExit('\n***ERROR*** couldn\'t find signal mass for file: %s'%fileName)

#******************************************
def getSignalModelFromFileName(fileName):
    #get luminosity value from file name
    keys = fileName.split('.')
    for ii in xrange(len(keys)):
        if keys[ii] == 'GeV':
            return keys[ii-2]
    raise SystemExit('\n***ERROR*** couldn\'t find signal model for file: %s'%fileName)
