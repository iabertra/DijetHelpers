#!/usr/bin/env python
import os
import ROOT, sys
from ROOT import *

# Lydia Beresford

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('../atlasstyle-00-03-05/AtlasStyle.C') #~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
# Set batch mode for root
ROOT.gROOT.SetBatch(True)
#******************************************

#------------------------------------------
#check input parameters
if len(sys.argv) != 6:
  raise SystemExit(
          '\n***ERROR*** wrong input parameters (%s/%s) \
          \nHOW TO: python XChecksplots.py inname1 inname2 legend1 legend2 outname\n\
	  \'inname1\' first input file name (usually MC), not including .root or ScaledHistograms ... \n\
          \'inname2\' second input file name (usually Data), not including .root or ScaledHistograms ... \n\
          Note first input file is scaled to integral of second input file for many of the plots !! \n\
	  \'legend1\' first input file legend label e.g. MC15a \n\
          \'legend2\' second input file legend label e.g. Data Period C ... \n\
          \'outname\' output directory name to put plots in\n\
EXAMPLE: python XChecksplots.py BJets RunI myplots'\
            %(len(sys.argv),6))

#------------------------------------------
# Setup, including getting some inputs from command line
# define input path, extension

SinglePlots = True

# MC File
InputPath='../results/'
InputExtension='ScaledHistograms.10.ifb.%s.root'%sys.argv[1].strip()
InputFile=InputPath+InputExtension
InputHistsFile=ROOT.TFile(InputFile) 
# Data File
InputExtension2='ScaledHistograms.10.ifb.%s.root'%sys.argv[2].strip()
InputFile2=InputPath+InputExtension2
InputHistsFile2=ROOT.TFile(InputFile2) 

mylegendlabel = [sys.argv[3].strip(),sys.argv[4].strip()]
OutDirectory = sys.argv[5].strip()
mypdf = OutDirectory+".pdf"

# make plots folder i.e. make OutDirectory
if not os.path.exists("../plots/%s"%OutDirectory):
    os.makedirs("../plots/%s"%OutDirectory)

#------------------------------------------
# Overlay plot function

def OverlayMultiHists(HistList,ColourList,LegendList,OutName,XName,YName,SinglePlots = True, logx = 0, logy = 0, OutDir = OutDirectory, SetYMinMax = 0, YMin = 0, YMax = 1, SetXMinMax = 0, XMin = 0, XMax = 1, FileList =[InputHistsFile,InputHistsFile2],LegendLabel = mylegendlabel):

  if len(HistList) is not len(ColourList):
    raise SystemExit('\n**** %s: HistList length (%d) is not equal to ColourList length (%d)'%(OutName,len(HistList),len(ColourList)))

  # Setup canvas and markers
  canvas = ROOT.TCanvas("Overlay_%s"%OutName,"Overlay_%s"%OutName,0,0,800,600)
  markersize=0.8
  hollowstyle=24 # default
  markerstyle=20 # default

  # Setup legend 
  lXmin = 0.2
  lXmax = 0.40
  lYmin = 0.62
  lYmax = 0.76
  if "Week1" in OutName:
    lYmin = lYmin-0.1 
    lXmin = lXmin-0.2
    lXmax = lXmax-0.2
  if "mjjPTDecomposition" in OutName:
    lYmin = lYmin-0.1 
    lXmin = lXmin+0.45
    lXmax = lXmax+0.45
  if OutName == "NSegments":
    lXmin = lXmin+0.42
    lXmax = lXmin+0.42
  if "Eta" in OutName:
    lXmin = lXmin+0.22
    lXmax = lXmax+0.22
  if "pTAsym" in OutName:
    lXmin = lXmin+0.2
    lXmax = lXmax+0.2
  lYmax = 0.76

  l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
  l.SetFillStyle(0)
  l.SetFillColor(0)
  l.SetLineColor(0)
  l.SetTextFont(42)
  l.SetTextSize(0.04)
  l.SetBorderSize(0)

  # Draw Data histos 
  for i,(Hist, Colour, Legend) in enumerate(zip(HistList, ColourList, LegendList)):

    # Different markers if overlay more than 1 MC and 1 Data Histogram
    if i == 0:
      markerstyle=20
    if i == 1:
      markerstyle=21
    if i == 2:
      markerstyle=22

    DataFile= FileList[1]
    Hist=DataFile.Get(Hist)
    Hist.SetMarkerStyle(markerstyle)
    Hist.SetMarkerSize(markersize)
    Hist.SetMarkerColorAlpha(Colour,1)

    #legend
    l.AddEntry(Hist,"%s %s"%(LegendLabel[1],Legend),"pl") 
    l.Draw()

    # Profile histo if it is TH2F
    histtype = type(Hist)
    if histtype is TH2F:
      shs = Hist.ProfileX()#"_pfx",1,-1,"g")
      Hist = shs

    # Axis names and range
    Hist.GetXaxis().SetTitle(XName)
    Hist.GetYaxis().SetTitle(YName)
    if SetYMinMax:
      Hist.GetYaxis().SetRangeUser(YMin,YMax)
    if SetXMinMax:
      Hist.GetXaxis().SetRangeUser(XMin,XMax)
    #print Hist.ClassName()
    #print Hist.GetName()

    if i == 0: # For first hist draw, for ones after draw "same"
      Hist.Draw()
    else:
      Hist.Draw("same")

  # Draw MC histos
  for i,(Hist, Colour, Legend) in enumerate(zip(HistList,ColourList,LegendList)):
    
    # Different markers if overlay more than 1 MC and 1 Data Histogram

    if i == 0:
      hollowstyle=24
    if i == 1:
      hollowstyle=25
    if i == 2:
      hollowstyle=26
  
    MCFile= FileList[0]
    Hist=MCFile.Get(Hist)
    Hist.SetName("%s_%s"%(Hist.GetName(),"MC"))#File.GetName()))
    Hist.SetMarkerStyle(hollowstyle)
    Hist.SetMarkerSize(markersize)
    if Colour is kBlack:
     Colour = kBlue # Don't allow MC to be shown in black
    Hist.SetMarkerColorAlpha(Colour,1)

    # Scale MC histo to integral of the data if not TH2F
    histtype = type(Hist)
    if histtype is not TH2F:
      if Hist.Integral() != 0:
        Hist.Scale( 1./Hist.Integral())
      Hist.Scale( FileList[1].Get(HistList[i]).Integral())
      print "Scaling by %s"%str(FileList[1].Get(HistList[i]).Integral())

    #legend
    l.AddEntry(Hist,"%s %s"%(LegendLabel[0],Legend),"pl") 
    l.Draw()
 
    # Profile histo if it is TH2F
    if histtype is TH2F:
      fhs = Hist.ProfileX()#"_pfx",1,-1,"g")
      Hist = fhs

    # Axis names and range
    Hist.GetXaxis().SetTitle(XName)
    Hist.GetYaxis().SetTitle(YName)
    if SetYMinMax:
      Hist.GetYaxis().SetRangeUser(YMin,YMax)
    if SetXMinMax:
      Hist.GetXaxis().SetRangeUser(XMin,XMax)
    #print Hist.ClassName()
    #print Hist.GetName()

    Hist.Draw("same")

  canvas.SetLogx(logx)
  canvas.SetLogy(logy)
  
  #ATLAS
  a = ROOT.TLatex()
  a.SetNDC()
  a.SetTextFont(73)
  a.SetTextColor(1)
  a.SetTextSize(26)
  a.DrawLatex(lXmin+0.01,lYmax+0.085,'ATLAS #it{Internal}')

  # CME + Lumi
  lu = ROOT.TLatex()
  lu.SetNDC()
  lu.SetTextFont(73)
  lu.SetTextColor(1)
  lu.SetTextSize(22)
  lu.DrawLatex(lXmin,lYmax+0.01,'#sqrt{s} = 13 TeV, 3.6 fb^{-1}')

  if SinglePlots is True:
    SaveName = "../plots/%s/%s.pdf"%(OutDir,OutName)
    print "Saving: "+SaveName
    canvas.SaveAs(SaveName)

  SaveName = "../plots/%s/%s"%(OutDirectory,mypdf)
  canvas.Print(SaveName)

def OverlayHistsWithRatio(HistList,ColourList,LegendList,OutName,XName,YName,SinglePlots = True, logx = 0, logy = 0, OutDir = OutDirectory, SetYMinMax = 0, YMin = 0, YMax = 1, SetXMinMax = 0, XMin = 0, XMax = 1, FileList =[InputHistsFile,InputHistsFile2],LegendLabel = mylegendlabel):

  # Ratio hist
  plotRatio = True # FIXME have as input
  if plotRatio:
    if len(HistList) ==1:

      # Setup canvas and markers 
      canvas = ROOT.TCanvas("Ratio_%s"%OutName)#,"Ratio_%s"%OutName,0,0,800,600)
      markersize=0.8
      hollowstyle=24 # default
      markerstyle=20 # default

      # Setup legend 
      lXmin = 0.2
      lXmax = 0.40
      lYmin = 0.62
      lYmax = 0.76
      if "mjjPTDecomposition" in OutName:
        lYmax = lYmax+0.25 #Xmin = lXmin+0.45
        #lXmax = lXmin+0.6
      if OutName == "NSegments":
        lXmin = lXmin+0.42
        lXmax = lXmin+0.42
      if "Eta" in OutName:
        lXmin = lXmin+0.22
        lXmax = lXmax+0.22
      if "pTAsym" in OutName:
        lXmin = lXmin+0.2
        lXmax = lXmax+0.2

      l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
      l.SetFillStyle(0)
      l.SetFillColor(0)
      l.SetLineColor(0)
      l.SetTextFont(42)
      l.SetTextSize(0.065)
      l.SetBorderSize(0)

      # Setup pads 
      pad1 = ROOT.TPad("pad1","pad1",0,0.3,1,1)
      pad2 = ROOT.TPad("pad2","pad2",0,0.01,1,0.3)
      pad1.Draw()
      pad2.Draw()
      pad1.cd()
      pad1.SetBottomMargin(0)
      pad2.SetTopMargin(0)
      pad2.SetBottomMargin(.5)
      zeroLine = ROOT.TF1("zl0", "0", -50000, 50000 )
      zeroLine.SetTitle("")
      zeroLine.SetLineWidth(1)
      zeroLine.SetLineStyle(7)
      zeroLine.SetLineColor(ROOT.kBlack)

      pad1.SetLogy(logy)
      pad1.SetLogx(logx)

      # Get histos
      MCFile= FileList[0]
      MCHist = MCFile.Get(HistList[0])
      MCHist.SetName("%s_%s"%(MCHist.GetName(),"MC"))
      DataFile= FileList[1]
      DataHist = DataFile.Get(HistList[0])
      DataHist.SetName("%s_%s"%(DataHist.GetName(),"Data"))

      # Scale MC histo to integral of the data if not TH2F
      histtype = type(MCHist)
      if histtype is not TH2F:
        if MCHist.Integral() != 0:
          MCHist.Scale( 1./MCHist.Integral())
        MCHist.Scale( DataHist.Integral())
        print "Scaling by %s"%str(DataHist.Integral())

      DataHist.SetMarkerSize(markersize)
      DataColour = ColourList[0]
      DataHist.SetMarkerColorAlpha(DataColour,1)

      MCHist.SetMarkerSize(markersize)
      MCHist.SetMarkerStyle(hollowstyle)
      MCColour = ColourList[0]
      if MCColour is kBlack:
       MCColour = kBlue # Don't allow MC to be shown in black
      MCHist.SetMarkerColorAlpha(MCColour,1)

      # Top pad
      pad1.cd()
     
      # Profile histos if type TH2F
      histtype = type(MCHist)
      if histtype is TH2F:
        mch = MCHist.ProfileX()#"_pfx",1,-1,"g")
        MCHist = mch
      histtype = type(DataHist)
      if histtype is TH2F:
        dh = DataHist.ProfileX()#"_pfx",1,-1,"g")
        DataHist = dh

      # Set axis ranges
      if SetYMinMax:
        DataHist.GetYaxis().SetRangeUser(YMin,YMax)
        MCHist.GetYaxis().SetRangeUser(YMin,YMax)
      if SetXMinMax:
        DataHist.GetXaxis().SetRangeUser(XMin,XMax)
        MCHist.GetXaxis().SetRangeUser(XMin,XMax)

      DataHist.GetYaxis().SetTitle(YName)
      DataHist.GetYaxis().SetTitleSize( 0.06 )
      DataHist.GetYaxis().SetTitleOffset( 0.7 )
      DataHist.GetXaxis().SetTitle(XName)
      DataHist.Draw()

      MCHist.GetYaxis().SetTitle(YName)
      MCHist.GetYaxis().SetTitleSize( 0.06 )
      DataHist.GetYaxis().SetTitleOffset( 0.7 )
      MCHist.GetXaxis().SetTitle(XName)
      MCHist.Draw("same")

      #legend
      l.AddEntry(MCHist,"%s %s"%(LegendLabel[0],LegendList[0]),"pl") 
      l.Draw()
      l.AddEntry(DataHist,"%s %s"%(LegendLabel[1],LegendList[1]),"pl") 
      l.Draw()

      #ATLAS
      a = ROOT.TLatex()
      a.SetNDC()
      a.SetTextFont(73)
      a.SetTextColor(1)
      a.SetTextSize(26)
      a.DrawLatex(lXmin+0.01,lYmax+0.085,'ATLAS #it{Internal}')

      # CME + Lumi
      lu = ROOT.TLatex()
      lu.SetNDC()
      lu.SetTextFont(73)
      lu.SetTextColor(1)
      lu.SetTextSize(22)
      lu.DrawLatex(lXmin,lYmax+0.01,'#sqrt{s} = 13 TeV, 3.6 fb^{-1}')

      # Bottom pad                   
      pad2.cd()
      pad2.SetLogx(logx)

      ## Get ratio ##
      if type(DataHist) is TProfile: # ProjectionX is for ROOT magic see https://root.cern.ch/phpBB3/viewtopic.php?t=2101
        tmpRatioHist =  DataHist.Clone().ProjectionX()
        tmpMCHist = MCHist.Clone().ProjectionX()
      else: 
        tmpRatioHist =  DataHist.Clone()
        tmpMCHist = MCHist.Clone()
      if SetXMinMax:
        tmpRatioHist.GetXaxis().SetRangeUser(XMin,XMax)
        tmpMCHist.GetXaxis().SetRangeUser(XMin,XMax)
      tmpRatioHist.Add( tmpMCHist, -1. )
      tmpRatioHist.Divide( tmpMCHist )
       
      #for iBin in range(1, DataHist.GetNbinsX()+1):
      #  print "Bin"
      #  print DataHist.GetBinLowEdge(iBin)
      #  print "Data"
      #  print DataHist.GetBinContent(iBin)
      #  print "MC"
      #  print MCHist.GetBinContent(iBin)
      #  print "By Hand"
      #  if MCHist.GetBinContent(iBin) != 0: 
      #    # print DataHist.GetBinContent(iBin)/MCHist.GetBinContent(iBin)
      #    print (DataHist.GetBinContent(iBin)-MCHist.GetBinContent(iBin))/MCHist.GetBinContent(iBin)
      #  print "Ratio"
      #  print tmpRatioHist.GetBinContent(iBin)
      tmpRatioHist.SetMarkerColor( kBlack )
      tmpRatioHist.SetLineColor( kBlack )
      tmpRatioHist.SetMarkerSize( markersize )
      tmpRatioHist.SetMarkerStyle( markerstyle )

      ## If data is 0 then there should be no ratio drawn
      for iBin in range(1, tmpRatioHist.GetNbinsX()+1):
        if DataHist.GetBinContent(iBin) == 0:
          tmpRatioHist.SetBinContent(iBin, 0)
          tmpRatioHist.SetBinError(iBin, 0)

      tmpRatioHist.GetYaxis().SetTitleSize( 0.1 )
      tmpRatioHist.GetYaxis().SetLabelSize( 0.1 )
      tmpRatioHist.GetYaxis().SetTitleOffset( 0.5 )
      tmpRatioHist.GetXaxis().SetLabelSize( 0.15 )
      tmpRatioHist.GetXaxis().SetTitleSize( 0.15 )
      tmpRatioHist.GetXaxis().SetTitleOffset( 1.0 )

      lowPoint = tmpRatioHist.GetMaximum()
      highPoint = tmpRatioHist.GetMinimum()
      ylow = 0.0
      yhigh = 0.0
      for iBin in range(1, tmpRatioHist.GetNbinsX()+1):
        val = tmpRatioHist.GetBinContent(iBin)
        if val < lowPoint :
          lowPoint = val
        if val > highPoint :
          highPoint = val
      if lowPoint < 0 :
        ylow = lowPoint*1.2
        yhigh = max(highPoint*(1.2),0.15)
        if yhigh > 5: # Don't show outliers above 5!
          yhigh = 5
      else :
        ylow = lowPoint - 0.9*(highPoint - lowPoint)
        yhigh = highPoint + 0.9*(highPoint - lowPoint)
      tmpRatioHist.GetYaxis().SetRangeUser(ylow,yhigh)

      tmpRatioHist.GetYaxis().SetTitle("(Data-MC)/MC")
      tmpRatioHist.GetYaxis().SetNdivisions(605)
      tmpRatioHist.Draw( "same" )
      zeroLine.Draw("same")

      if SinglePlots is True:
        SaveName = "../plots/%s/Ratio_%s.pdf"%(OutDir,OutName)
        print "Saving: "+SaveName
        canvas.SaveAs(SaveName)

      SaveName = "../plots/%s/%s"%(OutDirectory,mypdf)
      canvas.Print(SaveName)
  
#------------------------------------------
# Divide plot function

def DivideMultiHists(HistN,HistD,ColourList,OutName,XName,YName,SinglePlots = True, logx = 0, logy = 0, OutDir = OutDirectory, SetYMinMax = 0, YMin = 0, YMax = 1, FileList =[InputHistsFile,InputHistsFile2],LegendLabel = mylegendlabel):

  if len(ColourList) is not 2:
    raise SystemExit('\n**** %s: ColourList length (%d) is not equal to 2'%(OutName,len(ColourList)))

  # Setup canvas and markers
  canvas = ROOT.TCanvas("Overlay_%s"%OutName,"Overlay_%s"%OutName,0,0,800,600)
  markersize=0.8
  hollowstyle=24 # default
  markerstyle=20 # default

  # Setup legend 
  lXmin = 0.2
  lXmax = 0.40
  lYmin = 0.62
  if "mjjPTDecomposition" in OutName:
    lYmax = lYmax+0.25 #Xmin = lXmin+0.45
    #lXmax = lXmin+0.6
  if OutName == "NSegments":
    lXmin = lXmin+0.42
    lXmax = lXmin+0.42
  if "Eta" in OutName:
    lXmin = lXmin+0.22
    lXmax = lXmax+0.22
  if "pTAsym" in OutName:
    lXmin = lXmin+0.2
    lXmax = lXmax+0.2
  lYmax = 0.76
  
  l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
  l.SetFillStyle(0)
  l.SetFillColor(0)
  l.SetLineColor(0)
  l.SetTextFont(42)
  l.SetTextSize(0.04)
  l.SetBorderSize(0)

  # Get histos
  MCFile= FileList[0]
  MCColour= ColourList[0]
  DataFile= FileList[1]
  DataColour= ColourList[1]

  # Draw MC histo
  MCHistN=MCFile.Get(HistN)
  MCHistCloneN = MCHistN.Clone()
  MCHistCloneN.SetName("%s_%s"%(MCHistN.GetName(),"MC"))
  MCHistD=MCFile.Get(HistD)
  MCHistCloneD = MCHistD.Clone()
  MCHistCloneD.SetName("%s_%s"%(MCHistD.GetName(),"MC"))

  MCHistCloneN.Divide(MCHistCloneD)
  MCHistCloneN.GetXaxis().SetTitle(XName)
  MCHistCloneN.GetYaxis().SetTitle(YName)

  # Set axis ranges
  if SetYMinMax:
    MCHistCloneN.GetYaxis.SetRangeUser(YMin,YMax)
  MCHistCloneN.SetMarkerStyle(hollowstyle)
  MCHistCloneN.SetMarkerSize(markersize)
  if MCColour is kBlack:
   MCColour = kBlue # Don't allow MC to be shown in black
  MCHistCloneN.SetMarkerColorAlpha(MCColour,1)

  # NOTE! DON'T need to Scale MC histo to integral of data as plotting fractions 

  MCHistCloneN.Draw()

  # Draw Data histo
  DataHistN=DataFile.Get(HistN)
  DataHistCloneN = DataHistN.Clone()
  DataHistCloneN.SetName("%s_%s"%(DataHistN.GetName(),"Data"))
  DataHistD=DataFile.Get(HistD)
  DataHistCloneD = DataHistD.Clone()
  DataHistCloneD.SetName("%s_%s"%(DataHistD.GetName(),"Data")) 

  DataHistCloneN.Divide(DataHistCloneD)
  DataHistCloneN.GetXaxis().SetTitle(XName)
  DataHistCloneN.GetYaxis().SetTitle(YName)
  # Set axis ranges
  if SetYMinMax:
    DataHistCloneN.GetYaxis.SetRangeUser(YMin,YMax)
  DataHistCloneN.SetMarkerSize(markersize)
  DataHistCloneN.SetMarkerColorAlpha(DataColour,1)
  DataHistCloneN.Draw("same")

  canvas.SetLogx(logx)
  canvas.SetLogy(logy)

  #legend
  l.AddEntry(MCHistCloneN,"%s"%(LegendLabel[0]),"pl") 
  l.Draw()
  l.AddEntry(DataHistCloneN,"%s"%(LegendLabel[1]),"pl") 
  l.Draw()

  #ATLAS
  a = ROOT.TLatex()
  a.SetNDC()
  a.SetTextFont(73)
  a.SetTextColor(1)
  a.SetTextSize(26)
  a.DrawLatex(lXmin+0.01,lYmax+0.085,'ATLAS #it{Internal}')

  # CME + Lumi
  lu = ROOT.TLatex()
  lu.SetNDC()
  lu.SetTextFont(73)
  lu.SetTextColor(1)
  lu.SetTextSize(22)
  lu.DrawLatex(lXmin,lYmax+0.01,'#sqrt{s} = 13 TeV, 3.6 fb^{-1}')

  if SinglePlots is True:
    SaveName = "../plots/%s/%s.pdf"%(OutDir,OutName)
    print "Saving: "+SaveName
    canvas.SaveAs(SaveName)

  SaveName = "../plots/%s/%s"%(OutDirectory,mypdf)
  canvas.Print(SaveName)

def DivideHistsWithRatio(HistN,HistD,ColourList,OutName,XName,YName,SinglePlots = True, logx = 0, logy = 0, OutDir = OutDirectory, SetYMinMax = 0, YMin = 0, YMax = 1, FileList =[InputHistsFile,InputHistsFile2],LegendLabel = mylegendlabel):

  # Ratio hist
  plotRatio = True # FIXME have as input
  if plotRatio:

    # Setup canvas and markers
    canvas = ROOT.TCanvas("Ratio_%s"%OutName)#,"Ratio_%s"%OutName,0,0,800,600)
    markersize=0.8
    hollowstyle=24 # default
    markerstyle=20 # default

    # Setup legend 
    lXmin = 0.2
    lXmax = 0.40
    lYmin = 0.62
    lYmax = 0.76
    #if "mjjPTDecomposition" in OutName:
    #  lXmin = lXmin+0.45
    #  lXmax = lXmin+0.6
    #if OutName == "NSegments":
    #  lXmin = lXmin+0.45
    #  lXmax = lXmin+0.4
    #  lYmin = lYmin-0.1
    #  lYmax = lYmax-0.1
    #if "Eta" in OutName:
    #  lXmin = lXmin+0.22
    #  lXmax = lXmax-0.25
    #if "pTAsym" in OutName:
    #  lYmin = lYmin+0.12
    #  lYmax = lYmax+0.1

    l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(42)
    l.SetTextSize(0.065)
    l.SetBorderSize(0)

    # Setup pads
    pad1 = ROOT.TPad("pad1","pad1",0,0.3,1,1)
    pad2 = ROOT.TPad("pad2","pad2",0,0.01,1,0.3)
    pad1.Draw()
    pad2.Draw()
    pad1.cd()
    pad1.SetBottomMargin(0)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(.5)
    zeroLine = ROOT.TF1("zl0", "0", -50000, 50000 )
    zeroLine.SetTitle("")
    zeroLine.SetLineWidth(1)
    zeroLine.SetLineStyle(7)
    zeroLine.SetLineColor(ROOT.kBlack)

    # Get files and colours 
    MCFile= FileList[0]
    MCColour= ColourList[0]
    DataFile= FileList[1]
    DataColour= ColourList[1]

    # Top pad
    pad1.cd()
    pad1.SetLogy(logy)
    pad1.SetLogx(logx)
    
    # Draw Data histo
    DataHistN=DataFile.Get(HistN)
    DataHistCloneN = DataHistN.Clone()
    DataHistCloneN.SetName("%s_%s"%(DataHistN.GetName(),"Data"))
    DataHistD=DataFile.Get(HistD)
    DataHistCloneD = DataHistD.Clone()
    DataHistCloneD.SetName("%s_%s"%(DataHistD.GetName(),"Data")) 

    DataHistCloneN.Divide(DataHistCloneD)
    DataHistCloneN.GetXaxis().SetTitle(XName)
    DataHistCloneN.GetYaxis().SetTitle(YName)
    # Set axis ranges
    if SetYMinMax:
      DataHistCloneN.GetYaxis.SetRangeUser(YMin,YMax)
    DataHistCloneN.SetMarkerSize(markersize)
    DataHistCloneN.SetMarkerColorAlpha(DataColour,1)
    DataHistCloneN.GetXaxis().SetTitle(XName)
    DataHistCloneN.GetYaxis().SetTitle(YName)
    DataHistCloneN.Draw()

    # Draw MC histo
    MCHistN=MCFile.Get(HistN)
    MCHistCloneN = MCHistN.Clone()
    MCHistCloneN.SetName("%s_%s"%(MCHistN.GetName(),"MC"))
    MCHistD=MCFile.Get(HistD)
    MCHistCloneD = MCHistD.Clone()
    MCHistCloneD.SetName("%s_%s"%(MCHistD.GetName(),"MC"))

    MCHistCloneN.Divide(MCHistCloneD)
    MCHistCloneN.GetXaxis().SetTitle(XName)
    MCHistCloneN.GetYaxis().SetTitle(YName)
    # Set axis ranges
    if SetYMinMax:
      MCHistCloneN.GetYaxis.SetRangeUser(YMin,YMax)
    MCHistCloneN.SetMarkerStyle(hollowstyle)
    MCHistCloneN.SetMarkerSize(markersize)
    if MCColour is kBlack:
     MCColour = kBlue # Don't allow MC to be shown in black
    MCHistCloneN.SetMarkerColorAlpha(MCColour,1)
    MCHistCloneN.GetXaxis().SetTitle(XName)
    MCHistCloneN.GetYaxis().SetTitle(YName)
    MCHistCloneN.Draw("same")
      
    #legend
    l.AddEntry(MCHistCloneN,"%s"%(LegendLabel[0]),"pl") 
    l.Draw()
    l.AddEntry(DataHistCloneN,"%s"%(LegendLabel[1]),"pl") 
    l.Draw()

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(26)
    a.DrawLatex(lXmin+0.01,lYmax+0.085,'ATLAS #it{Internal}')

    # CME + Lumi
    lu = ROOT.TLatex()
    lu.SetNDC()
    lu.SetTextFont(73)
    lu.SetTextColor(1)
    lu.SetTextSize(22)
    lu.DrawLatex(lXmin,lYmax+0.01,'#sqrt{s} = 13 TeV, 3.6 fb^{-1}')

    # Bottom pad                   
    pad2.cd()
    pad2.SetLogx(logx)

    ## Get ratio ##
    if type(DataHistCloneN) is TProfile: # ProjectionX is for ROOT magic see https://root.cern.ch/phpBB3/viewtopic.php?t=2101
      tmpRatioHist =  DataHistCloneN.Clone().ProjectionX()
      tmpMCHist = MCHistCloneN.Clone().ProjectionX()
    else: 
      tmpRatioHist =  DataHistCloneN.Clone()
      tmpMCHist = MCHistCloneN.Clone()
    tmpRatioHist.Add( tmpMCHist, -1. )
    tmpRatioHist.Divide( tmpMCHist )
    tmpRatioHist.SetMarkerColor( kBlack )
    tmpRatioHist.SetLineColor( kBlack )
    tmpRatioHist.SetMarkerSize( markersize )
    tmpRatioHist.SetMarkerStyle( markerstyle )

    ## If data is 0 then there should be no ratio drawn
    for iBin in range(1, tmpRatioHist.GetNbinsX()+1):
      if DataHistCloneN.GetBinContent(iBin) == 0:
        tmpRatioHist.SetBinContent(iBin, 0)
        tmpRatioHist.SetBinError(iBin, 0)

    tmpRatioHist.GetYaxis().SetTitleSize( 0.1 )
    tmpRatioHist.GetYaxis().SetLabelSize( 0.1 )
    tmpRatioHist.GetYaxis().SetTitleOffset( 0.5 )
    tmpRatioHist.GetXaxis().SetLabelSize( 0.15 )
    tmpRatioHist.GetXaxis().SetTitleSize( 0.15 )
    tmpRatioHist.GetXaxis().SetTitleOffset( 1.0 )

    lowPoint = tmpRatioHist.GetMaximum()
    highPoint = tmpRatioHist.GetMinimum()
    ylow = 0.0
    yhigh = 0.0
    for iBin in range(1, tmpRatioHist.GetNbinsX()+1):
      val = tmpRatioHist.GetBinContent(iBin)
      if val < lowPoint :
        lowPoint = val
      if val > highPoint :
        highPoint = val
    if lowPoint < 0 :
      ylow = lowPoint*1.2
      yhigh = max(highPoint*(1.2),0.15)
      if yhigh > 5: # Don't show outliers above 5!
        yhigh = 5
    else :
      ylow = lowPoint - 0.9*(highPoint - lowPoint)
      yhigh = highPoint + 0.9*(highPoint - lowPoint)
    tmpRatioHist.GetYaxis().SetRangeUser(ylow,yhigh)
    tmpRatioHist.GetYaxis().SetTitle("(Data-MC)/MC")
    tmpRatioHist.GetYaxis().SetNdivisions(605)
    tmpRatioHist.Draw( "psame" )
    zeroLine.Draw("same")

    if SinglePlots is True:
      SaveName = "../plots/%s/Ratio_%s.pdf"%(OutDir,OutName)
      print "Saving: "+SaveName
      canvas.SaveAs(SaveName)

    SaveName = "../plots/%s/%s"%(OutDirectory,mypdf)
    canvas.Print(SaveName)

def OverlayMCHists(Hist1,Hist2,OutName,XName,YName,SinglePlots = True, logx = 0, logy = 0, InHistsFile = "", OutDir = OutDirectory, SetYMinMax = 0, YMin = 0, YMax = 1, SetXMinMax = 0, XMin = 0, XMax = 0):

  # Setup canvas and markers
  canvas = ROOT.TCanvas("MCOverlay_%s"%OutName)#,"Ratio_%s"%OutName,0,0,800,600)

  # Setup legend
  lXmin = 0.2
  lXmax = 0.40
  lYmin = 0.6
  lYmax = 0.7

  l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
  l.SetFillStyle(0)
  l.SetFillColor(0)
  l.SetLineColor(0)
  l.SetTextFont(42)
  l.SetTextSize(0.065)
  l.SetBorderSize(0)

  # Draw 1st histo
  Hist1=InputHistsFile.Get(Hist1)
  Hist2=InputHistsFile.Get(Hist2)
  Hist2.SetMarkerColorAlpha(kRed,1)

  Hist1.GetXaxis().SetTitle(XName)
  Hist1.GetYaxis().SetTitle(YName)
  Hist2.GetXaxis().SetTitle(XName)
  Hist2.GetYaxis().SetTitle(YName)
  if SetYMinMax:
    Hist1.GetYaxis().SetRangeUser(YMin,YMax)
    Hist2.GetYaxis().SetRangeUser(YMin,YMax)
  if SetXMinMax:
    Hist1.GetXaxis().SetRangeUser(XMin,XMax)
    Hist2.GetXaxis().SetRangeUser(XMin,XMax)

  Hist1.Draw()
  Hist2.Draw("same")
  canvas.SetLogx(logx)
  canvas.SetLogy(logy)

  # Setting stuff

  #ATLAS
  a = ROOT.TLatex()
  a.SetNDC()
  a.SetTextFont(73)
  a.SetTextColor(1)
  a.SetTextSize(26)
  a.DrawLatex(lXmin+0.01,lYmax+0.085,'ATLAS #it{Internal}')

  # CME + Lumi
  lu = ROOT.TLatex()
  lu.SetNDC()
  lu.SetTextFont(73)
  lu.SetTextColor(1)
  lu.SetTextSize(22)
  lu.DrawLatex(lXmin,lYmax+0.01,'#sqrt{s} = 13 TeV, 3.6 fb^{-1}')

  canvas.Update()

  if SinglePlots is True:
    SaveName = "../plots/%s/%s.pdf"%(OutDir,OutName)
    print "Saving: "+SaveName
    canvas.SaveAs(SaveName)

  SaveName = "../plots/%s/%s"%(OutDirectory,mypdf)
  canvas.Print(SaveName)

def DivideMCHists(HistNIn,HistDIn,OutName,XName,YName,SinglePlots = True, logx = 0, logy = 0, InHistsFile = "", OutDir = OutDirectory, SetYMinMax = 0, YMin = 0, YMax = 1, SetXMinMax = 0, XMin = 0, XMax = 0):
  # Setup canvas and markers
  canvas = ROOT.TCanvas("Ratio_%s"%OutName)#,"Ratio_%s"%OutName,0,0,800,600)
  markersize=0.8
  hollowstyle=24 # default
  markerstyle=20 # default

  # Setup legend
  lXmin = 0.2
  lXmax = 0.40
  lYmin = 0.6
  lYmax = 0.7

  l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
  l.SetFillStyle(0)
  l.SetFillColor(0)
  l.SetLineColor(0)
  l.SetTextFont(42)
  l.SetTextSize(0.065)
  l.SetBorderSize(0)

  # Draw 1st histo
  HistNIn=InputHistsFile.Get("%s"%HistNIn)
  HistDIn=InputHistsFile.Get("%s"%HistDIn)

  HistN = HistNIn.Clone()
  HistN.Divide(HistDIn)
  HistN.GetXaxis().SetTitle(XName)
  HistN.GetYaxis().SetTitle(YName)
  if SetYMinMax:
    HistN.GetYaxis().SetRangeUser(YMin,YMax)
  if SetXMinMax:
    HistN.GetXaxis().SetRangeUser(XMin,XMax)
  HistN.Draw()
  canvas.SetLogx(logx)
  canvas.SetLogy(logy)

  #ATLAS
  a = ROOT.TLatex()
  a.SetNDC()
  a.SetTextFont(73)
  a.SetTextColor(1)
  a.SetTextSize(26)
  a.DrawLatex(lXmin+0.01,lYmax+0.085,'ATLAS #it{Internal}')

  # CME + Lumi
  lu = ROOT.TLatex()
  lu.SetNDC()
  lu.SetTextFont(73)
  lu.SetTextColor(1)
  lu.SetTextSize(22)
  lu.DrawLatex(lXmin,lYmax+0.01,'#sqrt{s} = 13 TeV, 3.6 fb^{-1}')

  canvas.Update()

  # Save as histogram in root file
  SaveName = "../plots/%s/%s.root"%(OutDir,OutName)
  MyFile = ROOT.TFile(SaveName,"RECREATE")
  HistN.Write(OutName)#, ROOT.TObject.kOverwrite)
  MyFile.Close()

  if SinglePlots is True:
    SaveName = "../plots/%s/%s.pdf"%(OutDir,OutName)
    print "Saving: "+SaveName
    canvas.SaveAs(SaveName)

  SaveName = "../plots/%s/%s"%(OutDirectory,mypdf)
  canvas.Print(SaveName)


#******************************************
if __name__ == '__main__':


  canvas = ROOT.TCanvas("MyCanvas","MyCanvas",0,0,800,600)
  SaveName = "../plots/%s/%s("%(OutDirectory,mypdf)
  canvas.Print(SaveName)

  # Lydia For HCW
  #OverlayMultiHists(["jet_pt_vs_NSegments"],[kBlack],["",""],"HCWJetpTvsNSegments","jet p_{T} [GeV]","N_{Segments}",SinglePlots,1) 
  #OverlayMultiHists(["jet_pt_vs_NSegmentsmodpTBins"],[kBlack],["",""],"HCWJetpTvsNSegmentsmodpTBins","jet p_{T} [GeV]","N_{Segments}",SinglePlots,1) 
  #OverlayMultiHists(["jet_E_vs_NSegments"],[kBlack],["",""],"HCWJetEvsNSegments","jet E [GeV]","N_{Segments}",SinglePlots,1,0,OutDirectory,0,0,1,4)
  #OverlayMultiHists(["jet_E_vs_NSegmentsmodpTBins"],[kBlack],["",""],"HCWJetEvsNSegmentsmodpTBins","jet E [GeV]","N_{Segments}",SinglePlots,1,0,OutDirectory,0,0,1,4) 
  #OverlayMultiHists(["jet_phi_vs_NSegments"],[kBlack],["",""],"HCWJetphivsNSegments","jet \phi","N_{Segments}",SinglePlots,0,0,OutDirectory,1,0,1,6) 
  #OverlayMultiHists(["emscaleeta_vs_NSegments"],[kBlack],["",""],"HCWJetEMScaleEtavsNSegments","\eta _{detector}","N_{Segments}",SinglePlots,0,0,OutDirectory,0,0,1,7.5)

  # Standard plots
  OverlayMultiHists(["mjj_vs_SumNSegments"],[kBlack],["",""],"mjjvsSumNSegments","m_{jj} [TeV]","Sum of N_{Segments} behind leading jets") 
  OverlayHistsWithRatio(["mjj_vs_SumNSegments"],[kBlack],["",""],"mjjvsSumNSegments","m_{jj} [TeV]","Sum of N_{Segments} behind leading jets") 
  OverlayMultiHists(["LeadJetpT_vs_NSegments"],[kBlack],["",""],"LeadJetpTvsNSegments","Lead jet p_{T} [TeV]","N_{Segments}",SinglePlots,0,0,OutDirectory,0,0,0,1,0,4.5) 
  OverlayHistsWithRatio(["LeadJetpT_vs_NSegments"],[kBlack],["",""],"LeadJetpTvsNSegments","Lead jet p_{T} [TeV]","N_{Segments}",SinglePlots,0,0,OutDirectory,0,0,0,1,0,4.5)
  OverlayMultiHists(["SubLeadJetpT_vs_NSegments"],[kBlack],["",""],"SubLeadJetpTvsNSegments","Sublead jet p_{T} [TeV]","N_{Segments}",SinglePlots,0,0,OutDirectory,0,0,0,1,0,4.5) 
  OverlayHistsWithRatio(["SubLeadJetpT_vs_NSegments"],[kBlack],["",""],"SubLeadJetpTvsNSegments","Sublead jet p_{T} [TeV]","N_{Segments}",SinglePlots,0,0,OutDirectory,0,0,0,1,0,4.5) 
  OverlayMultiHists(["LeadJetEta_vs_NSegments"],[kBlack],["",""],"LeadJetEtavsNSegments","Lead jet eta","N_{Segments}")
  OverlayHistsWithRatio(["LeadJetEta_vs_NSegments"],[kBlack],["",""],"LeadJetEtavsNSegments","Lead jet eta","N_{Segments}")
  OverlayMultiHists(["SubLeadJetEta_vs_NSegments"],[kBlack],["",""],"SubLeadJetEtavsNSegments","Sublead jet eta","N_{Segments}") 
  OverlayHistsWithRatio(["SubLeadJetEta_vs_NSegments"],[kBlack],["",""],"SubLeadJetEtavsNSegments","Sublead jet eta","N_{Segments}") 
  OverlayMultiHists(["pTAsym_vs_SumNSegments"],[kBlack],["",""],"pTAsymvsSumNSegments","pT Asymmetry","Sum of N_{Segments} behind leading jets",SinglePlots,0,0,OutDirectory,1,0,6)
  OverlayHistsWithRatio(["pTAsym_vs_SumNSegments"],[kBlack],["",""],"pTAsymvsSumNSegments","pT Asymmetry","Sum of N_{Segments} behind leading jets",SinglePlots,0,0,OutDirectory,1,0,6)
  OverlayMultiHists(["NSegments"],[kBlack],["",""],"NSegments","N_{Segments}", "jets",SinglePlots,1,1) 
  OverlayHistsWithRatio(["NSegments"],[kBlack],["",""],"NSegments","N_{Segments}", "jets",SinglePlots,1,1) 

  DivideMultiHists("mjj_1PT","mjj",[kBlue,kBlack],"Overlaymjj1PTFraction","m_{jj} [TeV]","Fraction of events with 1 PT jet")
  DivideHistsWithRatio("mjj_1PT","mjj",[kBlue,kBlack],"Overlaymjj1PTFraction","m_{jj} [TeV]","Fraction of events with 1 PT jet")
  DivideMultiHists("mjj_2PT","mjj",[kBlue,kBlack],"Overlaymjj2PTFraction","m_{jj} [TeV]","Fraction of events with 2 PT jets")
  DivideHistsWithRatio("mjj_2PT","mjj",[kBlue,kBlack],"Overlaymjj2PTFraction","m_{jj} [TeV]","Fraction of events with 2 PT jets")
  OverlayMultiHists(["mjj","mjj_1PT","mjj_2PT"],[kBlue,kRed,kGreen-2],["Inclusive", "1 PT jet", "2 PT jets"],"mjjPTDecomposition","m_{jj} [TeV]", "Entries",SinglePlots,0,1) 

  # Eta binned plots
  coarseetaBins = [0.8,1.2,1.8,2.8] # one line on plot per eta region 

  hists = [] # Kept here as list for all eta bins
  Ehists = []
  legends = []
  for i in range (0,len(coarseetaBins)-1): # loop over eta bins -1 as want number of eta bins not array length 
    name = "jet_pt_vs_NSegments_EtaBinned"+"_eta_"+str(i)
    Ename = "jet_E_vs_NSegments_EtaBinned"+"_eta_"+str(i)
        
    hists.append(name)
    Ehists.append(Ename)
    legends.append(""+str(coarseetaBins[i])+"<= eta _{detector} <"+str(coarseetaBins[i+1]))

  OverlayMultiHists(hists,[kBlue,kRed,kGreen-2],legends,"Week1pTvsNSegmentsEtaBinOverlay", "jet p_{T} [TeV]", "N_{Segments}",SinglePlots,1,0,OutDirectory,0,0,0,1,0.06,4.5)  
  OverlayMultiHists(Ehists,[kBlue,kRed,kGreen-2],legends,"Week1EvsNSegmentsEtaBinOverlay", "jet E [TeV]", "N_{Segments}",SinglePlots,1,0)#,OutDirectory,0,0,0,1,99,100001)  
      
  # Angular plots (for highest mass bin) 
  DivideMultiHists("chi_1PT","chi",[kBlue,kBlack],"Overlaychi1PTFraction","chi ","Fraction of events with 1 PT jet")
  DivideHistsWithRatio("chi_1PT","chi",[kBlue,kBlack],"Overlaychi1PTFraction","chi ","Fraction of events with 1 PT jet")
  DivideMultiHists("chi_2PT","chi",[kBlue,kBlack],"Overlaychi2PTFraction","chi ","Fraction of events with 2 PT jets")
  DivideHistsWithRatio("chi_2PT","chi",[kBlue,kBlack],"Overlaychi2PTFraction","chi ","Fraction of events with 2 PT jets")
  OverlayMultiHists(["chi","chi_1PT","chi_2PT"],[kBlue,kRed,kGreen-2],["Inclusive", "1 PT jet", "2 PT jets"],"chiPTDecomposition","chi ", "Entries") 

  # Fraction plots
  OverlayMCHists("jet_ptfinepTBins","jet_ptfinepTBins_g20seg","OverlayFraction","jet pT [GeV]", "Lead jets",SinglePlots,0,1,InputHistsFile) 
  DivideMCHists("jet_ptfinepTBins_g20seg","jet_ptfinepTBins","DivideFraction","jet pT [GeV]","Fraction of lead jets with > 20 segments",SinglePlots,0,0,InputHistsFile)

  canvas = ROOT.TCanvas("MyCanvas","MyCanvas",0,0,800,600)
  SaveName = "../plots/%s/%s)"%(OutDirectory,mypdf)
  canvas.Print(SaveName)

  print '\ndone'
