#!/bin/python

from ROOT import *
from math import *

import prettyPalette
import atlasStyleMacro

#fin = TFile.Open("user.cyoung.6035908.EXT0._000018.DAOD_SUSY4.test.pool.root")

tree = TChain("CollectionTree")
tree.Add("Inputs/*.root")
fout = TFile.Open("JetROIMatch.root","RECREATE")

# deltaRJetROI_vs_pTJetAll = TH2D( "deltaRJetROI_vs_pTJetAll" , "" , 100 , 410 , 1500, 100, 0, 1 );
# deltaRJetROI_vs_pTJet1 = TH2D( "deltaRJetROI_vs_pTJet1" , "" , 100 , 410 , 1500, 100, 0, 1 );
# deltaRJetROI_vs_pTJet2 = TH2D( "deltaRJetROI_vs_pTJet2" , "" , 100 , 410 , 1500, 100, 0, 1 );
# ROIPt_vs_pTJet1 = TH2D( "ROIPt_vs_pTJet1" , "" , 100 , 0 , 1500, 100 , 410 , 1500 );
# ROIPt_vs_pTJet2 = TH2D( "ROIPt_vs_pTJet2" , "" , 100 , 0 , 1500, 100 , 410 , 1500 );
CutFlowJetPt = TH1D( "CutFlowJetPt" , "" , 100 , 0 , 3000);
LeadingJetPtBegel3 = TH1D( "LeadingJetPtBegel3" , "" , 100 , 0 , 2000);
LeadingJetPtBegel3_Eta = TH2D( "LeadingJetPtBegel3_Eta" , "" , 10 , -2.8 , 2.8 , 100 , 0 , 2000);
LeadingJetPtBegel4 = TH1D( "LeadingJetPtBegel4" , "" , 100 , 0 , 2000);
LeadingJetPtBegel4_Eta = TH2D( "LeadingJetPtBegel4_Eta" , "" , 10 , -2.8 , 2.8 , 100 , 0 , 2000);

LeadingJetPtBegel5 = TH1D( "LeadingJetPtBegel5" , "" , 100 , 0 , 2000);
LeadingJetPtBegel5_Eta = TH2D( "LeadingJetPtBegel5_Eta" , "" , 10 , -2.8 , 2.8 , 100 , 0 , 2000);
LeadingJetPtBegel6 = TH1D( "LeadingJetPtBegel6" , "" , 100 , 0 , 2000);
LeadingJetPtBegel6_Eta = TH2D( "LeadingJetPtBegel6_Eta" , "" , 10 , -2.8 , 2.8 , 100 , 0 , 2000);

variableList = {}
branchTypes = {}
branchList = []
for thisBranch in tree.GetListOfBranches():
    branchName = thisBranch.GetName()
    branchType = thisBranch.GetListOfLeaves().At(0).GetTypeName()
    #print branchName
    #print branchType
    if "vector<float>" in branchType:
      variableList[branchName] = std.vector('float')()
      branchList.append(branchName)
      branchTypes[branchList[-1]] = branchType
      tree.SetBranchStatus(branchList[-1], 1)
      tree.SetBranchAddress(branchList[-1], variableList[branchList[-1]])

count = 0
nEvents = tree.GetEntries()
filled = 0

while tree.GetEntry(count) and count < nEvents:

    count = count + 1
    if count%500==0:
        print count
    jet_tlv = TLorentzVector()

    #select three jet events triggered by j360
    if variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"].size()<3: continue #make sure there are three jets
    if variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][0] < 410000  : continue
    #basic sanity cut because there is junk
    if variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][0] > 7000000 : continue
    CutFlowJetPt.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][0]/1000.)
    firstJetMatch = False
    firstJet_tlv = None
    secondJetMatch = False
    secondJet_tlv = None
    thirdJetMatch = False
    thirdJet_tlv = None


    #loop on the jets
    firstJet_tlv = TLorentzVector()
    secondJet_tlv = TLorentzVector()
    thirdJet_tlv = TLorentzVector()
    fourthJet_tlv = TLorentzVector()
    fifthJet_tlv = TLorentzVector()
    njets = variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"].size()
    for ijet in xrange(0,njets) :
      jet_tlv.SetPtEtaPhiE(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000.,
                             variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_eta"][ijet],
                             variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_phi"][ijet], 0)
      if fabs(jet_tlv.Eta())>2.8:
          continue
      if jet_tlv.Pt()>firstJet_tlv.Pt():
          firstJet_tlv = jet_tlv.Clone()
      elif jet_tlv.Pt()>secondJet_tlv.Pt():
          secondJet_tlv = jet_tlv.Clone()
      elif jet_tlv.Pt()>thirdJet_tlv.Pt():
          thirdJet_tlv = jet_tlv.Clone()
      elif jet_tlv.Pt()>fourthJet_tlv.Pt():
          fourthJet_tlv = jet_tlv.Clone()
      elif jet_tlv.Pt()>fifthJet_tlv.Pt():
          fifthJet_tlv = jet_tlv.Clone()

    # print "jet pts: " + str(firstJet_tlv.Pt()) + "|" + str(secondJet_tlv.Pt())+"|" + str(thirdJet_tlv.Pt())+"|" + str(fourthJet_tlv.Pt()) + "|" + str(fifthJet_tlv.Pt())
          
    #loop on the ROI
    deltaRMin=999
    bestRoi_tlv = TLorentzVector()
    iBestRoi = None
    iBestRoiMatch1 = None
    iBestRoiMatch2 = None
    iBestRoiMatch3 = None
    iBestRoiMatch4 = None
    iBestRoiMatch5 = None
    for iRoi in xrange(0,variableList["LVL1JetRoIsAuxDyn.et8x8"].size()):
      ROI_tlv = TLorentzVector()
      ROI_tlv.SetPtEtaPhiE(variableList["LVL1JetRoIsAuxDyn.et8x8"][iRoi]/1000.,
                           variableList["LVL1JetRoIsAuxDyn.eta"][iRoi],
                           variableList["LVL1JetRoIsAuxDyn.phi"][iRoi], 0)
      if ROI_tlv.Pt()<75:
          continue
        
      thisDeltaR = thirdJet_tlv.DeltaR(ROI_tlv)
      if thirdJet_tlv.Pt()>0 and thisDeltaR < deltaRMin:
        deltaRMin = thisDeltaR
        iBestRoiMatch3 = iRoi
        iBestRoi = iRoi
        bestRoi_tlv = ROI_tlv.Clone()
      thisDeltaR = fourthJet_tlv.DeltaR(ROI_tlv)
      if fourthJet_tlv.Pt()>0 and thisDeltaR < deltaRMin:
        deltaRMin = thisDeltaR
        iBestRoiMatch4 = iRoi
        iBestRoi = iRoi
        bestRoi_tlv = ROI_tlv.Clone()
      thisDeltaR = fifthJet_tlv.DeltaR(ROI_tlv)
      if fifthJet_tlv.Pt()>0 and thisDeltaR < deltaRMin:
        deltaRMin = thisDeltaR
        iBestRoiMatch5 = iRoi
        iBestRoi = iRoi
        bestRoi_tlv = ROI_tlv.Clone()
    #end loop on the ROI
    # print " best ROI match: " + str(iBestRoiMatch3) + "|" + str(iBestRoiMatch4) + "|" + str(iBestRoiMatch5)
    # print " best ROI pt: " + str(bestRoi_tlv.Pt()) + " drmin: " + str(deltaRMin) + " dr1: " + str(firstJet_tlv.DeltaR(bestRoi_tlv))

    if deltaRMin<0.3 and firstJet_tlv.DeltaR(bestRoi_tlv)>0.8:
        LeadingJetPtBegel3.Fill( firstJet_tlv.Pt() )
        LeadingJetPtBegel3_Eta.Fill( firstJet_tlv.Eta() , firstJet_tlv.Pt() )
        for iRoi in xrange(0,variableList["LVL1JetRoIsAuxDyn.et8x8"].size()):
            ROI_tlv = TLorentzVector()
            ROI_tlv.SetPtEtaPhiE(variableList["LVL1JetRoIsAuxDyn.et8x8"][iRoi]/1000.,
                                 variableList["LVL1JetRoIsAuxDyn.eta"][iRoi],
                                 variableList["LVL1JetRoIsAuxDyn.phi"][iRoi], 0)
            if ROI_tlv.Pt()<75:
                continue
            deltaR = firstJet_tlv.DeltaR(ROI_tlv)
            if deltaR<0.3:
                LeadingJetPtBegel4.Fill( firstJet_tlv.Pt() )
                LeadingJetPtBegel4_Eta.Fill( firstJet_tlv.Eta() , firstJet_tlv.Pt() )
                
    if deltaRMin<0.3 and firstJet_tlv.DeltaR(bestRoi_tlv)<0.6:
        LeadingJetPtBegel5.Fill( firstJet_tlv.Pt() )
        LeadingJetPtBegel5_Eta.Fill( firstJet_tlv.Eta() , firstJet_tlv.Pt() )
        for iRoi in xrange(0,variableList["LVL1JetRoIsAuxDyn.et8x8"].size()):
            ROI_tlv = TLorentzVector()
            ROI_tlv.SetPtEtaPhiE(variableList["LVL1JetRoIsAuxDyn.et8x8"][iRoi]/1000.,
                                 variableList["LVL1JetRoIsAuxDyn.eta"][iRoi],
                                 variableList["LVL1JetRoIsAuxDyn.phi"][iRoi], 0)
            if ROI_tlv.Pt()<75:
                continue
            deltaR = firstJet_tlv.DeltaR(ROI_tlv)
            if deltaR<0.3:
                LeadingJetPtBegel6.Fill( firstJet_tlv.Pt() )
                LeadingJetPtBegel6_Eta.Fill( firstJet_tlv.Eta() , firstJet_tlv.Pt() )
            
        
    
    # for ijet in xrange(0,njets) :
    #     deltaRJetROI_vs_pTJetAll.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000, deltaRMin)

    # if (deltaRMin<0.3) and ijet == 0 :
    #     #print "leading jet found a ROI"
    #     firstJetMatch = True
    #     deltaRJetROI_vs_pTJet1.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000, deltaRMin)
    #     ROIPt_vs_pTJet1.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000, variableList["LVL1JetRoIsAuxDyn.et6x6"][iBestRoiMatch1]/1000.)
    #   if (deltaRMin<0.3) and ijet == 1 :
    #     #print "subleading jet found a ROI"
    #     secondJetMatch = True
    #     deltaRJetROI_vs_pTJet2.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000,  deltaRMin)
    #     ROIPt_vs_pTJet2.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000, variableList["LVL1JetRoIsAuxDyn.et6x6"][iBestRoiMatch2]/1000.)
    #
    # if ( firstJetMatch == False or secondJetMatch == False ) :
    #         print "Leading or subleading has no matching ROI."
    #         print firstJet_tlv.Pt(), firstJetMatch
    #         print secondJet_tlv.Pt(), secondJetMatch
    #         if firstJetMatch == False : pTUnmatchedROIs.Fill(firstJet_tlv.Pt());
    #         if secondJetMatch == False : pTUnmatchedROIs.Fill(secondJet_tlv.Pt());
    #
    # if ( firstJetMatch == False and secondJetMatch == False ) :
    #         print "Neither leading nor subleading have any matching ROI."
    #         print firstJet_tlv.Pt(), firstJetMatch
    #         print secondJet_tlv.Pt(), secondJetMatch



fout.cd()
c=TCanvas()

# deltaRJetROI_vs_pTJet1.Draw("COLZ")
# deltaRJetROI_vs_pTJet1.Write()
# deltaRJetROI_vs_pTJet1.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
# deltaRJetROI_vs_pTJet1.GetXaxis().SetTitle("Offline leading jet pT [GeV]")
# c.SaveAs("deltaRJetROI_vs_pTJet1.png")
#
# deltaRJetROI_vs_pTJet2.Draw("COLZ")
# deltaRJetROI_vs_pTJet2.Write()
# deltaRJetROI_vs_pTJet2.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
# deltaRJetROI_vs_pTJet2.GetXaxis().SetTitle("Offline subleading jet pT [GeV]")
# c.SaveAs("deltaRJetROI_vs_pTJet2.png")
#
# deltaRJetROI_vs_pTJetAll.Draw("COLZ")
# deltaRJetROI_vs_pTJetAll.Write()
# deltaRJetROI_vs_pTJetAll.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
# deltaRJetROI_vs_pTJetAll.GetXaxis().SetTitle("Offline jet pT [GeV]")
# c.SaveAs("deltaRJetROI_vs_pTJetAll.png")
#
# ROIPt_vs_pTJet1.Draw("COLZ")
# ROIPt_vs_pTJet1.Write()
# ROIPt_vs_pTJet1.GetYaxis().SetTitle("6x6 ROI et [GeV]")
# ROIPt_vs_pTJet1.GetXaxis().SetTitle("Offline jet pT [GeV]")
# c.SaveAs("ROIPt_vs_pTJet1.png")
#
# ROIPt_vs_pTJet2.Draw("COLZ")
# ROIPt_vs_pTJet2.Write()
# ROIPt_vs_pTJet2.GetYaxis().SetTitle("6x6 ROI et [GeV]")
# ROIPt_vs_pTJet2.GetXaxis().SetTitle("Offline jet pT [GeV]")
# c.SaveAs("ROIPt_vs_pTJet2.png")

CutFlowJetPt.Draw("COLZ")
CutFlowJetPt.Write()
CutFlowJetPt.GetXaxis().SetTitle("Offline jet pT [GeV]")
CutFlowJetPt.GetYaxis().SetTitle("Number of offline jets")
c.SaveAs("CutFlowJetPt.png")

LeadingJetPtBegel3.Draw()
LeadingJetPtBegel3.Write()
LeadingJetPtBegel3_Eta.Draw("COLZ")
LeadingJetPtBegel3_Eta.Write()
LeadingJetPtBegel4.Draw()
LeadingJetPtBegel4.Write()
LeadingJetPtBegel4_Eta.Draw("COLZ")
LeadingJetPtBegel4_Eta.Write()

LeadingJetPtBegel5.Draw()
LeadingJetPtBegel5.Write()
LeadingJetPtBegel5_Eta.Draw("COLZ")
LeadingJetPtBegel5_Eta.Write()
LeadingJetPtBegel6.Draw()
LeadingJetPtBegel6.Write()
LeadingJetPtBegel6_Eta.Draw("COLZ")
LeadingJetPtBegel6_Eta.Write()

fout.Close()

        #LVL1JetRoIsAuxDyn.et6x6
#     nTriggerJets = 0
#     nOfflineJets = 0
#     for ijet in range(variableList["jet_calib_pt"].size()):
#         if variableList["jet_calib_pt"][ijet]/1000. > offline_jet.Pt():
#             nOfflineJets = nOfflineJets + 1
#             offline_jet.SetPtEtaPhiM( variableList["jet_calib_pt"][ijet]/1000. ,
#                                       variableList["jet_calib_eta"][ijet],
#                                       variableList["jet_calib_phi"][ijet],
#                                       variableList["jet_calib_m"][ijet]/1000. )
#
#     for ijet in range(variableList["trigger_jet_calib_pt"].size()):
#         if variableList["trigger_jet_calib_pt"][ijet]/1000. > trigger_jet.Pt() and fabs(offline_jet.DeltaPhi(trigger_jet))>2:
#             nTriggerJets = nTriggerJets + 1
#             trigger_jet.SetPtEtaPhiM( variableList["trigger_jet_calib_pt"][ijet]/1000. ,
#                                       variableList["trigger_jet_calib_eta"][ijet],
#                                       variableList["trigger_jet_calib_phi"][ijet],
#                                       variableList["trigger_jet_calib_m"][ijet]/1000. )
#     if offline_jet.Pt()<410:
#         continue
#     if trigger_jet.Pt()<410:
#         continue
#
#     filled = filled + 1
#     asymmetry = (offline_jet.Pt()-trigger_jet.Pt())/(offline_jet.Pt()+trigger_jet.Pt())
#     print asymmetry
#     print offline_jet.Pt()
#     # if filled>10:
#     #     break
#
#     hoff.Fill(offline_jet.Pt(),asymmetry)
#     htrig.Fill(trigger_jet.Pt(),asymmetry)
#
# c = TCanvas()
# hoff.Draw("colz")
# c.Print("output_off.pdf")
# c.Print("output_off.C")
# htrig.Draw("colz")
# c.Print("output_trig.pdf")
# c.Print("output_trig.C")
