#!/bin/python

from ROOT import *
from math import *

import prettyPalette
import atlasStyleMacro

#fin = TFile.Open("user.cyoung.6035908.EXT0._000018.DAOD_SUSY4.test.pool.root")

tree = TChain("CollectionTree")
tree.Add("Inputs/*.root")
fout = TFile.Open("JetROIMatch.root","RECREATE")

deltaRJetROI_vs_pTJetAll = TH2D( "deltaRJetROI_vs_pTJetAll" , "" , 100 , 410 , 1500, 100, 0, 1 );
deltaRJetROI_vs_pTJet1 = TH2D( "deltaRJetROI_vs_pTJet1" , "" , 100 , 410 , 1500, 100, 0, 1 );
deltaRJetROI_vs_pTJet2 = TH2D( "deltaRJetROI_vs_pTJet2" , "" , 100 , 410 , 1500, 100, 0, 1 );
ROIPt_vs_pTJet1 = TH2D( "ROIPt_vs_pTJet1" , "" , 100 , 0 , 1500, 100 , 410 , 1500 );
ROIPt_vs_pTJet2 = TH2D( "ROIPt_vs_pTJet2" , "" , 100 , 0 , 1500, 100 , 410 , 1500 );
pTUnmatchedROIs = TH1D( "pTUnmatchedROIs" , "" , 100 , 0 , 1500);

variableList = {}
branchTypes = {}
branchList = []
for thisBranch in tree.GetListOfBranches():
    branchName = thisBranch.GetName()
    branchType = thisBranch.GetListOfLeaves().At(0).GetTypeName()
    #print branchName
    #print branchType
    if "vector<float>" in branchType:
      variableList[branchName] = std.vector('float')()
      branchList.append(branchName)
      branchTypes[branchList[-1]] = branchType
      tree.SetBranchStatus(branchList[-1], 1)
      tree.SetBranchAddress(branchList[-1], variableList[branchList[-1]])

count = 0
nEvents = tree.GetEntries()
filled = 0

while tree.GetEntry(count) and count < nEvents:
    count = count + 1
    if count%500==0:
        print count
    jet_tlv = TLorentzVector()

    if variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"].size()>=2: #make sure there are two jets

        firstJetMatch = False
        firstJet_tlv = None
        secondJetMatch = False
        secondJet_tlv = None

        #loop on the jets
        for ijet in xrange(0,variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"].size()) :
          jet_tlv.SetPtEtaPhiE(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000.,
                                 variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_eta"][ijet],
                                 variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_phi"][ijet], 0)

          deltaRMin=999

          if ijet == 0 : firstJet_tlv=jet_tlv.Clone()
          if ijet == 1 : secondJet_tlv=jet_tlv.Clone()
          iBestRoiMatch1 = None
          iBestRoiMatch2 = None

          #loop on the ROI
          for iRoi in xrange(0,variableList["LVL1JetRoIsAuxDyn.et6x6"].size()):
            ROI_tlv = TLorentzVector()
            ROI_tlv.SetPtEtaPhiE(variableList["LVL1JetRoIsAuxDyn.et6x6"][iRoi]/1000.,
                                   variableList["LVL1JetRoIsAuxDyn.eta"][iRoi],
                                   variableList["LVL1JetRoIsAuxDyn.phi"][iRoi], 0)
            if jet_tlv.DeltaR(ROI_tlv) < deltaRMin :
                deltaRMin = jet_tlv.DeltaR(ROI_tlv)
                if ijet == 0 :
                  iBestRoiMatch1 = iRoi
                if ijet == 1 :
                  iBestRoiMatch2 = iRoi
          #end loop on the ROI

          #fill with the best match
          deltaRJetROI_vs_pTJetAll.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000, deltaRMin)

          if (deltaRMin<0.3) and ijet == 0 :
            #print "leading jet found a ROI"
            firstJetMatch = True
            deltaRJetROI_vs_pTJet1.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000, deltaRMin)
            ROIPt_vs_pTJet1.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000, variableList["LVL1JetRoIsAuxDyn.et6x6"][iBestRoiMatch1]/1000.)
          if (deltaRMin<0.3) and ijet == 1 :
            #print "subleading jet found a ROI"
            secondJetMatch = True
            deltaRJetROI_vs_pTJet2.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000,  deltaRMin)
            ROIPt_vs_pTJet2.Fill(variableList["AntiKt4EMTopoJetsAuxDyn.DFCommonJets_Calib_pt"][ijet]/1000, variableList["LVL1JetRoIsAuxDyn.et6x6"][iBestRoiMatch2]/1000.)

        if ( firstJetMatch == False or secondJetMatch == False ) :
                print "Leading or subleading has no matching ROI."
                print firstJet_tlv.Pt(), firstJetMatch
                print secondJet_tlv.Pt(), secondJetMatch
                if firstJetMatch == False : pTUnmatchedROIs.Fill(firstJet_tlv.Pt());
                if secondJetMatch == False : pTUnmatchedROIs.Fill(secondJet_tlv.Pt());

        if ( firstJetMatch == False and secondJetMatch == False ) :
                print "Neither leading nor subleading have any matching ROI."
                print firstJet_tlv.Pt(), firstJetMatch
                print secondJet_tlv.Pt(), secondJetMatch



fout.cd()
c=TCanvas()

deltaRJetROI_vs_pTJet1.Draw("COLZ")
deltaRJetROI_vs_pTJet1.Write()
deltaRJetROI_vs_pTJet1.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
deltaRJetROI_vs_pTJet1.GetXaxis().SetTitle("Offline leading jet pT [GeV]")
c.SaveAs("deltaRJetROI_vs_pTJet1.png")

deltaRJetROI_vs_pTJet2.Draw("COLZ")
deltaRJetROI_vs_pTJet2.Write()
deltaRJetROI_vs_pTJet2.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
deltaRJetROI_vs_pTJet2.GetXaxis().SetTitle("Offline subleading jet pT [GeV]")
c.SaveAs("deltaRJetROI_vs_pTJet2.png")

deltaRJetROI_vs_pTJetAll.Draw("COLZ")
deltaRJetROI_vs_pTJetAll.Write()
deltaRJetROI_vs_pTJetAll.GetYaxis().SetTitle("#DeltaR to nearest 6x6 ROI")
deltaRJetROI_vs_pTJetAll.GetXaxis().SetTitle("Offline jet pT [GeV]")
c.SaveAs("deltaRJetROI_vs_pTJetAll.png")

ROIPt_vs_pTJet1.Draw("COLZ")
ROIPt_vs_pTJet1.Write()
ROIPt_vs_pTJet1.GetYaxis().SetTitle("6x6 ROI et [GeV]")
ROIPt_vs_pTJet1.GetXaxis().SetTitle("Offline jet pT [GeV]")
c.SaveAs("ROIPt_vs_pTJet1.png")

ROIPt_vs_pTJet2.Draw("COLZ")
ROIPt_vs_pTJet2.Write()
ROIPt_vs_pTJet2.GetYaxis().SetTitle("6x6 ROI et [GeV]")
ROIPt_vs_pTJet2.GetXaxis().SetTitle("Offline jet pT [GeV]")
c.SaveAs("ROIPt_vs_pTJet2.png")

pTUnmatchedROIs.Draw("COLZ")
pTUnmatchedROIs.Write()
pTUnmatchedROIs.GetXaxis().SetTitle("Offline jet pT [GeV]")
pTUnmatchedROIs.GetYaxis().SetTitle("Number of offline jets")
c.SaveAs("pTUnmatchedROIs.png")


fout.Close()

        #LVL1JetRoIsAuxDyn.et6x6
#     nTriggerJets = 0
#     nOfflineJets = 0
#     for ijet in range(variableList["jet_calib_pt"].size()):
#         if variableList["jet_calib_pt"][ijet]/1000. > offline_jet.Pt():
#             nOfflineJets = nOfflineJets + 1
#             offline_jet.SetPtEtaPhiM( variableList["jet_calib_pt"][ijet]/1000. ,
#                                       variableList["jet_calib_eta"][ijet],
#                                       variableList["jet_calib_phi"][ijet],
#                                       variableList["jet_calib_m"][ijet]/1000. )
#
#     for ijet in range(variableList["trigger_jet_calib_pt"].size()):
#         if variableList["trigger_jet_calib_pt"][ijet]/1000. > trigger_jet.Pt() and fabs(offline_jet.DeltaPhi(trigger_jet))>2:
#             nTriggerJets = nTriggerJets + 1
#             trigger_jet.SetPtEtaPhiM( variableList["trigger_jet_calib_pt"][ijet]/1000. ,
#                                       variableList["trigger_jet_calib_eta"][ijet],
#                                       variableList["trigger_jet_calib_phi"][ijet],
#                                       variableList["trigger_jet_calib_m"][ijet]/1000. )
#     if offline_jet.Pt()<410:
#         continue
#     if trigger_jet.Pt()<410:
#         continue
#
#     filled = filled + 1
#     asymmetry = (offline_jet.Pt()-trigger_jet.Pt())/(offline_jet.Pt()+trigger_jet.Pt())
#     print asymmetry
#     print offline_jet.Pt()
#     # if filled>10:
#     #     break
#
#     hoff.Fill(offline_jet.Pt(),asymmetry)
#     htrig.Fill(trigger_jet.Pt(),asymmetry)
#
# c = TCanvas()
# hoff.Draw("colz")
# c.Print("output_off.pdf")
# c.Print("output_off.C")
# htrig.Draw("colz")
# c.Print("output_trig.pdf")
# c.Print("output_trig.C")
