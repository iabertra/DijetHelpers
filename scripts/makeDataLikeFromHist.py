#!/usr/bin/env python

#******************************************
#makeDataLikeFromHist.py

#given a simple histogram, get the data-like version
#NOTE just run the script without any parameter to get instructions
#EXAMPLE time python -u makeDataLikeFromHist.py path/to/file.root mjj 1 10
#******************************************

import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--file", dest='file', default="", help="Filename that contains the histogram")
parser.add_argument("--histName", dest='histName', default="", help="Name of the histogram")
parser.add_argument("--newName", dest='newName', default="", help="New name of the histogram")
parser.add_argument("--inLumi", dest='inLumi', default=1.0, type=float, help="Luminosity of the input sample")
parser.add_argument("--outLumi", dest='outLumi', default=1.0, type=float, help="Lumoutosity of the output sample")
parser.add_argument("--seed", dest='seed', default=10, type=int, help="Seed for getDataLikeHist")
parser.add_argument("--systematic", dest='systematic', default="", help="Name of TDirectory to put histograms in")
parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
parser.add_argument("--plot", dest='plot', action='store_true', default=False, help="Plot results?")
args = parser.parse_args()

#******************************************
#import stuff
import ROOT, plotUtils, math, os, sys

#******************************************
#set ATLAS style
import AtlasStyle
AtlasStyle.SetAtlasStyle()

#******************************************
#def makeDataLikeFromHist(file, histName, inLumi, outLumi, seed):
def makeDataLikeFromHist(args):

    print('\nmaking data-like histogram')

    #------------------------------------------
    #input parameters
    print('\ninput parameters:')
    argsdict = vars(args)
    for key,value in argsdict.items():
        print('  %s = %s'%(key,value))

    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #open input file
    f = ROOT.TFile.Open(args.file,'READ')
    if not f:
        raise SystemExit('\n***ERROR*** input file not found: %s'%args.file)

    hist = f.Get(args.histName)
    if not hist:
        raise SystemExit('\n***ERROR*** input histogram not found: %s'%args.histName)
    hist.SetDirectory(0) #Detatch
    f.Close()

    #------------------------------------------
    #output directories
    localdir = os.path.dirname(os.path.realpath(__file__))
    print('\nlocal dir: %s'%localdir)

    if len(args.outDir) > 0:
        #NOTE paths are relative to the current directory
        #NOTE unless the path starts with '/'
        outDir = args.outDir
    else:
        outDir = localdir+'/results/'

    print('output dir: %s'%outDir)
    if not os.path.exists(outDir):
        os.makedirs(outDir)

    #------------------------------------------
    #outDir = args.file.split('.')
    #outDir = outDir[:-1] #remove .root
    #outDir = '.'.join(outDir)+'/'
    #print('output dir: %s'%outDir)
    #if not os.path.exists(outDir): os.makedirs(outDir)

    #------------------------------------------
    #configure input histogram
    #hist.Scale(float(args.outLumi)/float(args.inLumi))
    newName = args.newName
    if len(newName) == 0:
        newName = os.path.basename(args.file) #get basename
        newName = os.path.splitext(newName)[0] #remove extension
    #print('new name: %s'%newName)
    
    #To remove unecessary decimal points from integer names
    if int(args.outLumi) == args.outLumi:
        args.outLumi = int(args.outLumi)
          
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    # open output file
    outFileName = 'dataLikeHistograms.'+newName+'.v'+str(args.seed)+'.root'
    print('output file name: %s'%outFileName)
    outF = ROOT.TFile.Open(outDir+'/'+outFileName, 'UPDATE')
    if len(args.systematic) > 0:
        outF.mkdir( args.systematic )
        outF.cd( args.systematic )

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------
      
    #------------------------------------------
    #get effective entries histogram before scaling
    effEntHist = plotUtils.getEffectiveEntriesHistogram(hist, hist.GetName()+'_effent')
    #effEntHist.SetName( 'mjj_EffectiveEntries_'+args.newName+'_'+str(args.outLumi).replace('.','p')+'fb' )
    effEntHist.SetName(hist.GetName()+'_effent')
    effEntHist.Write("", ROOT.TObject.kOverwrite)

    #scale histogram
    hist.Scale(float(args.outLumi)/float(args.inLumi))
    #hist.SetName( 'mjj_Scaled_'+args.newName+'_'+str(args.outLumi).replace('.','p')+'fb' )
    hist.SetName(hist.GetName())
    hist.Write("", ROOT.TObject.kOverwrite)

    #get data-like histogram
    dataLikeHist = plotUtils.getDataLikeHist(effEntHist, hist, hist.GetName()+'_DataLike', args.seed)
    #dataLikeHist.SetName( 'mjj_DataLike_'+args.newName+'_'+str(args.outLumi).replace('.','p')+'fb' )
    dataLikeHist.SetName(hist.GetName()+'_DataLike')
    dataLikeHist.Write("", ROOT.TObject.kOverwrite)

    #get smooth histogram
    smoothHist = plotUtils.getSmoothHistogram(hist, hist.GetName()+'_smooth')
    #amoothHist.SetName( 'mjj_smooth_'+args.newName+'_'+str(outLumi).replace('.','p')+'fb' )
    smoothHist.SetName(hist.GetName()+'_Smooth')
    smoothHist.Write("", ROOT.TObject.kOverwrite)
    
    #------------------------------------------
    #plot

    if args.plot:
        
        outputFigDir = localdir+'/figures/'
        if not os.path.exists(outputFigDir):
            os.makedirs(outputFigDir)

        #canvas
        c1 = ROOT.TCanvas('c1', 'c1', 200, 50, 800, 600)
        c1.Clear()
        c1.SetLogy(1)
        c1.SetLogx(0)

        hist.SetMarkerColor(ROOT.kAzure+1)
        hist.SetMarkerStyle(25)
        hist.SetLineColor(ROOT.kAzure+1)

        effEntHist.SetMarkerColor(ROOT.kGreen+1)
        effEntHist.SetMarkerStyle(21)
        effEntHist.SetLineColor(ROOT.kGreen+1)

        dataLikeHist.SetMarkerColor(ROOT.kRed+1)
        dataLikeHist.SetMarkerStyle(25)
        dataLikeHist.SetLineColor(ROOT.kRed+1)

        hs = ROOT.THStack("hs","hs")
        hs.Add(effEntHist)
        hs.Add(hist)
        hs.Add(dataLikeHist)
        hs.Draw("nostack")
        hs.GetXaxis().SetTitle("m_{jj} [GeV]")
        hs.GetYaxis().SetTitle("entries")

        #legend
        lXmin = 0.65
        lXmax = 0.80
        lYmin = 0.65
        lYmax = 0.75
        l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
        l.SetFillStyle(0)
        l.SetFillColor(0)
        l.SetLineColor(0)
        l.SetTextFont(43)
        l.SetBorderSize(0)
        l.SetTextSize(15)
        l.AddEntry(effEntHist,"eff. ent. histogram","pl")
        l.AddEntry(hist,"input histogram","pl")
        l.AddEntry(dataLikeHist,"data-like histogram","pl")
        l.Draw()

        #ATLAS
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(lXmin,lYmax+0.12,'ATLAS')

        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(lXmin+0.13,lYmax+0.12,'internal')

        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(lXmin,lYmax+0.05,'#intL dt = %s fb^{-1}'%args.outLumi)

        c1.Update()
        #c1.SaveAs(outputFigDir+'/dataLikeHistogram.'+args.histName+'.'+str(args.outLumi)+'.ifb.pdf')
        c1.SaveAs(outputFigDir+'/dataLikeHistogram.'+args.histName+'.pdf')

        #------------------------------------------
        #effective luminosity of the sample

        #canvas
        c2 = ROOT.TCanvas('c2', 'c2', 1020, 50, 800, 600)
        c2.Clear()
        c2.SetLogy(1)
        c2.SetLogx(0)
        c2.SetGridy(1)
        c2.SetGrid(1)

        ratioHist = effEntHist.Clone()
        ratioHist.SetName('effectiveLuminosity')
        ratioHist.SetTitle('effectiveLuminosity')
        ratioHist.Divide(hist)
        ratioHist.Scale(float(args.inLumi)) #TEST #CHECK

        ratioHist.SetMarkerColor(1)
        ratioHist.SetLineColor(1)
        ratioHist.SetMarkerStyle(20)
        ratioHist.SetXTitle("m_{jj} [GeV]")
        ratioHist.SetYTitle("luminosity [fb^{-1}]")
        ratioHist.Draw("pe")

        #legend
        lXmin=0.20
        lXmax=0.35

        #ATLAS
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(lXmin,lYmax+0.12,'ATLAS')

        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(lXmin+0.13,lYmax+0.12,'internal')

        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(lXmin,lYmax+0.07,'MC effective luminosity')

        #stats
        print('\neffective luminosity at 1.0 TeV = %.3f fb^-1'%ratioHist.GetBinContent(ratioHist.FindBin(1e3)))
        print(  'effective luminosity at 1.5 TeV = %.3f fb^-1'%ratioHist.GetBinContent(ratioHist.FindBin(1.5e3)))
        print(  'effective luminosity at 2.0 TeV = %.3f fb^-1'%ratioHist.GetBinContent(ratioHist.FindBin(2e3)))

        c2.Update()
        c2.WaitPrimitive()
        c1.SaveAs(outputFigDir+'/effectiveLuminosity.'+args.histName+'.pdf')

    #close the output file, at last
    outF.Close()


#******************************************
if __name__ == '__main__':

    makeDataLikeFromHist(args)
    print('\ndone')
