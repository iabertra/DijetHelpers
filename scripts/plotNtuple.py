#!/usr/bin/env python

import os, sys, glob, math
import argparse, time, array
import plotUtils
import ROOT
from ROOT import TLorentzVector

sampleLayerList = ["PreSamplerB", "EMB1", "EMB2", "EMB3", "PreSamplerE", "EME1", "EME2", "EME3", "HEC0", "HEC1", "HEC2", "HEC3"]

sampleBins      = [100,            300,    500,    200,    100,           200,    400,    200,    300,    300,    200,    200    ]


sampleLayerEtaRange=[[0,1.52],[0,1.475],[0,1.475],[0,1.475],[1.5,1.8],[1.375,1.5,1.8,2,2.5,3.2],[1.375,2.5,3.2],[1.5,2.5],[1.5,2.5,3.2],[1.5,2.5,3.2],[1.5,2.5,3.2],[1.5,2.5,3.2]]

sampleLayerList +=["TileBar0", "TileBar1", "TileBar2", "TileGap1", "TileGap2", "TileGap3", "TileExt0", "TileExt1", "TileExt2", "FCAL0", "FCAL1", "FCAL2"]
sampleBins      +=[500,         500,        300,        100,        100,        150,        300,        200,        200,       300,      300,     170]
sampleLayerEtaRange+=[[0,1.0],[0,0.9],[0,0.8],[0.9,1.0],[0.8,0.9],[1.0,1.1,1.2,1.4,1.6],[1.1,1.6],[1.0,1.5],[1.0,1.4],[3.1,4.9],[3.1,4.9],[3.1,4.9]]

EemEtaRange = [[0,1.52],[0,1.475],[1.5,1.8],[1.375,1.5],[1.8,2],[2,2.5],[2.5,3.2],[1.375,2.5],[1.5,2.5],[0,1.0],[0,0.9],[0,0.8],[0.9,1.0],[0.8,0.9],[1.0,1.1],[1.1,1.2],[1.2,1.4],[1.4,1.6],[1.0,1.5],[1.0,1.4],[3.1,4.9],[1.375,3.2],[1.5,3.2],[1.0,1.6],[1.1,1.6]]

sampleLayerEtaRange_extra=[[0],[0],[0],[0],[0],[1.375,2.3,2.8],[1.375,2.3,2.8],[1.5,2.2],[1.8,2.6],[1.8,2.6],[1.8,2.6],[1.8,2.6],[0],[0],[0],[0],[0],[0],[0],[1.0,1.25,1.5],[1.0,1.25,1.4],[0],[0],[0]]

phiBins = 56
phiRange = ROOT.TMath.Pi()

sampleMin  = [0]*len(sampleLayerList)
sampleMax = []
etaBinning = [0,0.8,1.2,1.3,1.6,2.1,2.8,3.1,4.9]



for iBin in xrange(0,len(sampleBins)) :
  sampleMax.append(5*sampleBins[iBin])  #max is 5 times original bins to avoid overflow

layerListWithLength = [0,1,2,3,12,13,14]
#layerNames = ["EM Barrel PreSampler", "EM Barrel Layer 1", "EM Barrel Layer 2","EM Barrel Layer 3","Tile Barrel Layer 0","Tile Barrel Layer 1", "Tile Barrel Layer 2"]
layerLengths = {0:11., 1:90.627802691,2:337.219730944,3:42.152466368,12:321.283783784,13:878.175675675,14:385.54054054}
interactionLengths = {0:0.036184211, 1:0.311184211, 2:1.157894737, 3:0.144736842, 12:1.5, 13:4.1, 14:1.8}

localdir = os.path.dirname(os.path.realpath(__file__))
print localdir
LikelihoodFile  = ROOT.TFile.Open(localdir+'/QGLikelihood.root','READ')
#LikelihoodFile.ls()
SumRatioParam_0 = LikelihoodFile.Get("SumRatioParam_0")
SumRatioParam_1 = LikelihoodFile.Get("SumRatioParam_1")
SumRatioParam_2 = LikelihoodFile.Get("SumRatioParam_2")
SumRatioParam_3 = LikelihoodFile.Get("SumRatioParam_3")
SumLikeRatioMaxX = SumRatioParam_3.GetX()[7]
SumLikeRatioMinX = SumRatioParam_3.GetX()[0]

DiffRatioParam_0 = LikelihoodFile.Get("DiffRatioParam_0")
DiffRatioParam_1 = LikelihoodFile.Get("DiffRatioParam_1")
DiffRatioParam_2 = LikelihoodFile.Get("DiffRatioParam_2")
DiffRatioParam_3 = LikelihoodFile.Get("DiffRatioParam_3")


SumLikeRatio = ROOT.TF1("SumLikeRatio","cheb3",10,120)
DiffLikeRatio = ROOT.TF1("DiffLikeRatio","cheb3",0,100)



# Variable names that go bad...

njetVarName = "njet"
jet__GhostMuonSegmentCountVarName="jet__GhostMuonSegmentCount"
jet__EMFracVarName="jet__EMFrac"
jet__HECFracVarName="jet__HECFrac"
jet__FracSamplingMaxVarName="jet__FracSamplingMax"
jet__FracSamplingMaxIndexVarName="jet__FracSamplingMaxIndex"


#
#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='b', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("-v", dest='v', action='store_true', default=False, help="Verbose mode for debugging")
parser.add_argument("--powheg", dest='powheg', action='store_true', default=False, help="If running on powheg")
parser.add_argument("--lumi", dest='lumi', type=float, default=1.0, help="Desired Luminosity")


#------------------------------------------------------------------------------------------------------------------------
#  ARGUMENTS FOR PLOTNTUPLE.PY....  They are active in this script, but generally inputed for use in plotNtuple.py
#------------------------------------------------------------------------------------------------------------------------

#parser.add_argument("--verbose", dest='verbose', action='store_true', default=False, help="VERBOSE!!")
#parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
#------------------------------------------
parser.add_argument("--file", dest='file', default="", help="Input file name which contains a tree")
parser.add_argument("--tree", dest='tree', default="outTree", help="Name of tree")
parser.add_argument("--maxEvents", dest='maxEvents', type=int, default=-1, help="Max number of events in tree to process")
parser.add_argument("--outDir", dest='outDir', default="./histograms", help="Name of output directory for ROOT file containing histograms")
parser.add_argument("--outFileName", dest='outFileName', default="plots.root", help="output file name which contains filled histograms")

parser.add_argument("--histType", dest='histType', default="Scaled", help="Type of output file")

parser.add_argument("--applyRW", dest='RWFile', default="", help="File for applying reweighting")
parser.add_argument("--truthOnlyInput", dest='truthOnlyInput', action='store_true', default=False, help="Run on MC input which is truth level only")
#------------------------------------------
## adding all branches and the assoicated parameters
parser.add_argument("--plotAll", dest='plotAll', action='store_true', default=False, help="Plot all tree entries.  This will not plot vector branches")
parser.add_argument("--plotAllVector", dest='plotAllVector', action='store_true', default=False, help="Plot all tree entries for branches of vectors")
parser.add_argument("--fillJetPlots", dest='fillJetPlots', action='store_true', default=False, help="Fill plots for individual jets (slow!)")
parser.add_argument("--nJetToPlot", dest='nJetToPlot', default=100, type=int, help="Number of jets to put in jet plots (-1 is all)")
parser.add_argument("--nBins", dest='nBins', default=120, type=int, help="Default number of bins to be used for 1D histograms added on the fly")
#------------------------------------------
## truning on and off specific plot sets
parser.add_argument("--basicInfo", dest='basicInfo', action='store_true', default=False, help="runNumber, lumiBlock, etc.")
parser.add_argument("--typicalVariables", dest='typicalVariables', action='store_true', default=False, help="mjj, yStar, yBoost, NPV, njets, etc. (all non-vectors!)")
parser.add_argument("--chiVariables", dest='chiVariables', action='store_true', default=False, help="chi variables")
parser.add_argument("--sensitivity", dest='sensitivity', action='store_true', default=False, help="mjj and chi sensitivity to a user inputed cut specified elsewhere")
parser.add_argument("--cleaning", dest='cleaning', action='store_true', default=False, help="plot cleaning variables")
parser.add_argument("--punchThrough", dest='punchThrough', action='store_true', default=False, help="fill punch through study")
parser.add_argument("--kinematics", dest='kinematics', action='store_true', default=False, help="fill kinematic variables")
parser.add_argument("--energyLayers", dest='energyLayers', action='store_true', default=False, help="energyLayers")
parser.add_argument("--energyLayersLength", dest='energyLayersLength', action='store_true', default=False, help="energyLayersLength")
parser.add_argument("--truth", dest='truth', action='store_true', default=False, help="fill truth jet kinematic variables")
parser.add_argument("--wideJets", dest='wideJets', action='store_true', default=False, help="wide jet variables")



parser.add_argument("--quarkGluon",dest='quarkGluon',action='store_true', default=False,  help="apply quarkGluon samples")

parser.add_argument("--quarkGluonOverlap",dest='quarkGluonOverlap',action='store_true', default=False,  help="apply independent quarkGluon samples - True means that the definitions are independent and can overlap ")

parser.add_argument("--quarkGluonTrackOffset", dest='quarkGluonTrackOffset', type=float,  default=0.,  help="apply quarkGluon samples")
parser.add_argument("--quarkGluonTrackSlope", dest='quarkGluonTrackSlope', type=float,  default=0.02,  help="apply quarkGluon samples")
parser.add_argument("--quarkGluonRootSlope", dest='quarkGluonRootSlope', type=float,  default=0.02,  help="apply quarkGluon samples")
parser.add_argument("--quarkGluonParam", dest='quarkGluonParam', default="pT",  help="pT, lnpT, lnpt2, or mjj")

parser.add_argument("--gluonTrackOffset", dest='gluonTrackOffset', type=float,  default=0.,  help="apply quarkGluon samples")
parser.add_argument("--gluonTrackSlope", dest='gluonTrackSlope', type=float, default=0.02,  help="apply quarkGluon samples")
parser.add_argument("--gluonRootSlope", dest='gluonRootSlope', type=float, default=0.02,  help="apply quarkGluon samples")
parser.add_argument("--gluonParam", dest='gluonParam', default="pT",  help="pT, lnpt, lnpt2, or mjj")

parser.add_argument("--quarkTrackOffset", dest='quarkTrackOffset', type=float,  default=0.,  help="apply quarkGluon samples")
parser.add_argument("--quarkTrackSlope", dest='quarkTrackSlope', type=float, default=0.02,  help="apply quarkGluon samples")
parser.add_argument("--quarkRootSlope", dest='quarkRootSlope', type=float, default=0.0,  help="apply quarkGluon samples")
parser.add_argument("--quarkParam", dest='quarkParam', default="pT",  help="pT, lnpT, lnpT2, or mjj")



parser.add_argument("--morphing", dest='morphing',action='store_true', default=False,  help="use 1 GeV mass binning")
#parser.add_argument("--truthDetail", dest='truthDetail', action='store_true', default=False, help="fill detailed truth jet variables, i.e. flavTag Truth, etc.")
parser.add_argument("--QGselection", dest='QGselection', default="JJ",  help="JJ select all events, QQ select QQ only, QG, GG etc")
parser.add_argument("--truthQGselection", dest='truthQGselection', default="JJ",  help="JJ select all events, QQ select QQ only, QG, GG etc")

parser.add_argument("--letterKinematicPlots", dest='letterKinematicPlots', action='store_true', default=False, help="variables plotted for letter... much quicker")


parser.add_argument("--flavTag", dest='flavTag', action='store_true', default=False, help="flavor tags for jets")
parser.add_argument("--jetConstituents", dest='jetConstituents', action='store_true', default=False, help="Include plots of jet constituents")
parser.add_argument("--jetConstituentsAll", dest='jetConstituentsAll', action='store_true', default=False, help="Include plots of jet constituents")
parser.add_argument("--jetCaloQuant", dest='jetCaloQuant', action='store_true', default=False, help="Include plots of calo based quantities in jets i.e. width")
parser.add_argument("--jetTrackQuant", dest='jetTrackQuant', action='store_true', default=False, help="Include plots of composite track quantities in jets")
parser.add_argument("--jetTrack", dest='jetTrack', action='store_true', default=False, help="Include plots of track quantities in jets")
parser.add_argument("--jetTrackAll", dest='jetTrackAll', action='store_true', default=False, help="Include plots of track quantities in jets")
parser.add_argument("--jetTrackQuantAll", dest='jetTrackQuantAll', action='store_true', default=False, help="Include plots of ALL composite track quantities in jets")  #a few more plots than jetTrackQuant... do we need both?
parser.add_argument("--kinematicDistributions", dest='kinematicDistributions', action='store_true', default=False, help="fill kinematic distributions in Sec 8.1 of supporting note")

parser.add_argument("--quickStudy", dest='quickStudy', action='store_true', default=False, help="quick study")

#parser.add_argument("--", dest='', action='store_true', default=False, help="")


parser.add_argument("--do_massPartonPlots", dest='do_massPartonPlots', action='store_true', default=False, help="Include plots of mjj split by incoming and outgoing parton")
parser.add_argument("--plotDijetSlices", dest='plotDijetSlices', action='store_true', default=False, help="Plot dijet only with each slice a different color")



####### CUT OPTIONS HERE FOR EASY CONFIGURABILITY #####
parser.add_argument("--minimalCuts",        dest='minimalCuts',    action='store_true', default=False,  help="apply minimal analysis cuts")
parser.add_argument("--resonanceCuts",        dest='resonanceCuts',    action='store_true', default=False,  help="apply resonance analysis cuts")
parser.add_argument("--angularCuts",        dest='angularCuts',    action='store_true', default=False,  help="apply angular analysis cuts")
####### EVENT LEVEL CUTS #####
parser.add_argument("--cut_yStarMin",   dest='cut_yStarMin',   type=float, default=-1, help="Minimum yStar cut")
parser.add_argument("--cut_yStarMax",   dest='cut_yStarMax',   type=float, default=-1, help="Maximum yStar cut")
parser.add_argument("--cut_yBoost",  dest='cut_yBoost',  type=float, default=-1,  help="yBoost cut")
parser.add_argument("--cut_NJet",    dest='cut_NJet',    type=float, default=-1,  help="N Jet cut")
parser.add_argument("--cut_LJetPt",  dest='cut_LJetPt',  type=float, default=-1,  help="Leading Jet pT [GeV]")
parser.add_argument("--cut_LJetPtMax",  dest='cut_LJetPtMax',  type=float, default=-1,  help="Max cut on leading Jet pT [GeV]")
parser.add_argument("--cut_NLJetPt", dest='cut_NLJetPt', type=float, default=-1,  help="Next-to-Leading Jet pT [GeV]")
parser.add_argument("--cut_mjjMin",  dest='cut_mjjMin',  type=float, default=-1,  help="Minimum dijet mass [GeV]")
parser.add_argument("--cut_mjjMax",  dest='cut_mjjMax',  type=float, default=-1,  help="Maximum dijet mass [GeV]")
parser.add_argument("--cut_NPVMax",  dest='cut_NPVMax',  type=float, default=-1,  help="Maximum NPV")
#parser.add_argument("--cut_AvgMuMin",  dest='cut_AvgMuMin',  type=float, default=0.1,  help="Minimum NPV - DEFAULT at 0.1 for now")
##############################
####### JET LEVEL CUTS - for plots of individual jet quantities #####
parser.add_argument("--cut_jetPtMin",  dest='cut_jetPtMin',    type=float, default=-1,  help="for jet plots: Minimum jet pT cut [GeV]")
parser.add_argument("--cut_jetPtMax",  dest='cut_jetPtMax',    type=float, default=-1,  help="for jet plots: Maximum jet pT cut [GeV]")
parser.add_argument("--cut_jetEtaMin",  dest='cut_jetEtaMin',  type=float, default=-1,  help="for jet plots: Minimum jet pT cut [GeV]")
parser.add_argument("--cut_jetEtaMax",  dest='cut_jetEtaMax',  type=float, default=-1,  help="for jet plots: Maximum jet pT cut [GeV]")
parser.add_argument("--cut_jetYMin",    dest='cut_jetYMin',    type=float, default=-1,  help="for jet plots: Minimum jet pT cut [GeV]")
parser.add_argument("--cut_jetYMax",    dest='cut_jetYMax',    type=float, default=-1,  help="for jet plots: Maximum jet pT cut [GeV]")
parser.add_argument("--cut_jetDetEtaMin",    dest='cut_jetDetEtaMin',    type=float, default=-1,  help="for jet plots: Minimum jet pT cut [GeV]")
parser.add_argument("--cut_jetDetEtaMax",    dest='cut_jetDetEtaMax',    type=float, default=-1,  help="for jet plots: Maximum jet pT cut [GeV]")
parser.add_argument("--cut_jetNumTrkPt500PVMin",    dest='cut_jetNumTrkPt500PVMin',    type=float, default=-1,  help="for jet plots: Min number of tracks in jet (pT>500MeV)")
parser.add_argument("--cut_jetMV2c20Min",    dest='cut_jetMV2c20Min',    type=float, default=-1,  help="for jet plots: Min MV2c20 cut")
parser.add_argument("--cut_jetEMin", dest='cut_jetEMin', type=float, default=-1, help="for jet plots: Minimum jet E cut [GeV]")
parser.add_argument("--cut_jetEMax", dest='cut_jetEMax', type=float, default=-1, help="for jet plots: Maximum jet E cut [GeV]")

parser.add_argument("--cut_caloLayerName", dest='cut_caloLayerName', default="None", help="energy layer with maximum energy deposit")

parser.add_argument("--noCleanEvent",          dest='noCleanEvent',    action='store_true', default=False,  help="DO NOT apply LooseBad cleaning at the event level")
parser.add_argument("--cleanJet",          dest='cleanJet',    action='store_true', default=False,  help="apply LooseBad cleaning jet by jet")

parser.add_argument("--tileGap",        dest='tileGap',    action='store_true', default=False,  help="apply tileGap clean cut")
parser.add_argument("--lbn",            dest='lbn',    action='store_true', default=False,  help="specific cut for 265545/73 data")
parser.add_argument("--mbts",            dest='mbts',    action='store_true', default=False,  help="require MBTS Trigger")
parser.add_argument("--rd0filled",            dest='rd0filled',    action='store_true', default=False,  help="require RD0_FILLED Trigger")
parser.add_argument("--truthMatch",          dest='truthMatch',    action='store_true', default=False,  help="require truth matched jets")
parser.add_argument("--puMatch",             dest='puMatch',    action='store_true', default=False,  help="require pu matched jets")
parser.add_argument("--truthB",          dest='truthB',    action='store_true', default=False,  help="require truth labeled b-jets")
parser.add_argument("--truthC",          dest='truthC',    action='store_true', default=False,  help="require truth labeled c-jets")
parser.add_argument("--truthL",          dest='truthL',    action='store_true', default=False,  help="require truth labeled l-jets")

parser.add_argument("--triggers",   dest='triggers',  default='', help='Comma seperated list of triggers to cut on')


parser.add_argument("--cut_massDropMin",    dest='cut_massDropMin',    type=float, default=-1,  help="for jet plots: Minimum massDrop cut [GeV]")
parser.add_argument("--cut_massDropMax",    dest='cut_massDropMax',    type=float, default=-1,  help="for jet plots: Maximum massDrop cut [GeV]")
parser.add_argument("--cut_pTjjMin",   dest='cut_pTjjMin',   type=float, default=-1, help="Minimum pTjj cut")
parser.add_argument("--cut_pTjjMax",   dest='cut_pTjjMax',   type=float, default=-1, help="Maximum pTjj cut")
parser.add_argument("--cut_NJetMax",    dest='cut_NJetMax',    type=float, default=-1,  help="N Jet Max cut")
parser.add_argument("--cut_wideJetdR",    dest='cut_wideJetdR',    type=float, default=-1,  help="wide jet dR cut")

##############################
#energy bins: <100, 100 - 150, 150 - 250, 250 - 400, 400 - 600, 600 - 1000 GeV, >1 GeV
#eta bins: 0-1.2, 1.2 - 2.0, 2.0 - 3.2, 3.2 - 4.5
#parser.add_argument("--cut_jetEMin", dest="cut_jetEMin", type=float, default=-1)
#parser.add_argument("--cut_jetEMax", dest="cut_jetEMax", type=float, default=-1)
#parser.add_argument("--caloLayerName", dest="caloLayerName", type=str, default="None")

parser.add_argument('--applyNLOCorrection', dest='applyNLOCorrection', action='store_true', default=False, help='apply k-factors for NLO corrections')
parser.add_argument('--applyEWCorrection', dest='applyEWCorrection', action='store_true', default=False, help='apply k-factors for EW corrections')

parser.add_argument('--correctLargeEtaJets', dest='correctLargeEtaJets', default = -1, help='Value by which to multiply pT of jets outside of eta 1.8')
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------


args = parser.parse_args()
print args

from ROOT import *
import AtlasStyle

TH1.SetDefaultSumw2(True)
TH2.SetDefaultSumw2(True)
AtlasStyle.SetAtlasStyle()







def main():
    if len(args.triggers) > 0:
      args.triggers = list( args.triggers.split(',') )

#    sampleName = plotUtils.getName( args.file, True ) # true gives each dijet slice a unique name
    sampleName = os.path.basename(args.file).split('.')
    if len(sampleName) < 6:    #----MERGE CHANGE   MAKE  < 7
      print ("Error, we expect input files to have more than 6 fields of information, where a field is speerated by '.'")
      print ("For example: user.jdandoy.data15_comm.00265545.physics_MinBias.None.JetInputs_DataMay23_v0_20150527_tree.root")
      exit(1)


    if sampleName[len(sampleName)-2].find("tree")>-1:    #---MERGE CHANGE
      sampleName = '_'.join( sampleName[2:5] )          #---MERGE CHANGE
    else:                                                       #---MERGE CHANGE
      tempSampleName = '_'.join( sampleName[2:5] )              #---MERGE CHANGE
      sampleName = '_'.join( [tempSampleName, str(sampleName[len(sampleName)-2])] )    #---MERGE CHANGE

#sampleName = '_'.join( [ str(sampleName[2:5]), str(sampleName[len(sampleName)-2]) ] )


#------- Set resonance or angular cuts, but use user-inputed values if any offered-------
#Note:  cleaning is done by default, unless the user user --noEventCleaning

    if "JET" in args.tree:  args.noCleanEvent = True

    #SET COMMON CUTS HERE
    if args.resonanceCuts or args.angularCuts or args.minimalCuts:
      if args.cut_LJetPt < 0: args.cut_LJetPt = 440.0
#      if "data" in sampleName: args.lbn = True
#      if args.nJetToPlot == 100:  args.nJetToPlot = 3
      if len(args.triggers) == 0: args.triggers = ["HLT_j360"]
      if args.cut_NLJetPt < 0: args.cut_NLJetPt = 50.0

    #Set minimal cuts here
    if args.minimalCuts:
      if args.cut_jetEtaMax < 0: args.cut_jetEtaMax = 2.8

    #Set resonance specific cuts here
    if args.resonanceCuts and not args.angularCuts:
      if args.cut_mjjMin < 0: args.cut_mjjMin = 1100.0
      if args.cut_yStarMax < 0: args.cut_yStarMax = 0.6

    #Set angular specific cuts here
    if args.angularCuts and not args.resonanceCuts:
      if args.cut_mjjMin < 0: args.cut_mjjMin = 2500.0
      if args.cut_yStarMax < 0: args.cut_yStarMax = 1.7
      if args.cut_yBoost < 0: args.cut_yBoost = 1.1

    # Set SM cuts - dijet analysis
#    if args.smCuts:
#      if args.cut_LJetPt < 410: args.cut_jetPtMin = 410
#      if args.cut_jetPtMin < 0: args.cut_jetPtMin = 100
#      if args.cut_jetYMax  < 0: args.cut_jetYMax  = 3.0
#      #if args.cut_HT2Min   < 0: args.cut_HT2Min   = 880



    if args.wideJets and args.cut_wideJetdR < 0: args.cut_wideJetdR = 1.2
#    if args.wideJets and len(args.triggers) == 0: args.triggers = ["HLT_j360"]



#    if args.quickStudy: args.cut_mjjMin = -1.0
#-----------------------------------------------------------------------------------------


    print( "Making histograms for : " + sampleName )

    #------------------------------------------
    ## create output directory and log files of configuration
    if not os.path.exists(args.outDir):
        os.mkdir(args.outDir)
#    log = open(args.outDir + "/" + args.histType + "." + sampleName + ".log", 'w')
    log = open(args.outFileName.replace(".root", ".log"), 'w')
    log.truncate() # clear the file
    argsDict = vars(args)
    for key in sorted(argsDict.keys()):
      log.write( key + "\t" + str(argsDict[key]) + "\n")
    log.close()



    #------------------------------------------
    ## get the file, tree and sample name
    inFile = TFile.Open(args.file, "READ")
    inTree = inFile.Get(args.tree)

    branches = inTree.GetListOfBranches()
    for branch in branches:
        #print branch.GetName()
        if ("GhostMuonSegmentCount" in branch.GetName()):
            global jet__GhostMuonSegmentCountVarName
            if branch.GetName()!=jet__GhostMuonSegmentCountVarName:
                #print jet__GhostMuonSegmentCountVarName
                jet__GhostMuonSegmentCountVarName = branch.GetName()
                #print jet__GhostMuonSegmentCountVarName
        if ("EMFrac" in branch.GetName()):
            global jet__EMFracVarName
            if branch.GetName()!=jet__EMFracVarName:
                #print jet__EMFracVarName
                jet__EMFracVarName = branch.GetName()
                #print jet__EMFracVarName
        if ("HECFrac" in branch.GetName()):
            global jet__HECFracVarName
            if branch.GetName()!=jet__HECFracVarName:
                #print jet__HECFracVarName
                jet__HECFracVarName = branch.GetName()
                #print jet__HECFracVarName
        if ("njet" in branch.GetName()):
            global njetVarName
            if branch.GetName()!=njetVarName:
                print njetVarName
                njetVarName = branch.GetName()
                print njetVarName

        if ("FracSamplingMax" in branch.GetName() and "Index" not in branch.GetName() ):
            global jet__FracSamplingMaxVarName
            if branch.GetName()!=jet__FracSamplingMaxVarName:
                #print jet__FracSamplingMaxVarName
                jet__FracSamplingMaxVarName = branch.GetName()
                #print jet__FracSamplingMaxVarName
        if ("FracSamplingMaxIndex" in branch.GetName()  ):
            global jet__FracSamplingMaxIndexVarName
            if branch.GetName()!=jet__FracSamplingMaxIndexVarName:
                #print jet__FracSamplingMaxIndexVarName
                jet__FracSamplingMaxIndexVarName = branch.GetName()
                #print jet__FracSamplingMaxIndexVarName



    #------------------------------------------
    ## get number of events from cutflow ##
#    sampleEvents = 0
#    keys = inFile.GetListOfKeys()
#    for key in keys:
#
#      if args.powheg and 'cutflow' in key.GetName() and 'weighted' in key.GetName():
#        sampleEvents = key.ReadObj().GetBinContent(1)
#        print sampleEvents
#        if "JZ5" in args.file:
#          sampleEvents = sampleEvents - 352220000
#        continue
#      elif not args.powheg and 'cutflow' in key.GetName() and not 'weighted' in key.GetName():
#      #if 'cutflow' in key.GetName() and str(dsid) in key.GetName():
#        sampleEvents = key.ReadObj().GetBinContent(1)
#        continue
#
#    if sampleEvents == 0:
#      raise SystemExit('\n***WARNING*** no cutflow entries found')
#
#    print( "\t total events : " + str(sampleEvents) )



#------------------------------------------
## get number of events from cutflow ##
    sampleEvents = 1.
    fracLumi = 1. 
    keys = inFile.GetListOfKeys()
    for key in keys:
      print key.GetName()
      if 'cutflow' in key.GetName() and 'weighted' in key.GetName():
        recordedCutflow = inFile.Get( key.GetName() )

        sampleEvents = key.ReadObj().GetBinContent(1)
        
        Name = inFile.GetName() 
        if "WithSW" in Name: sampleEvents = plotUtils.getSampleEvents(Name)
        elif "Sherpa" in Name: 
          print "in sherpa weighting"
          sampleEvents = 1./plotUtils.getSampleEvents(Name)
        
        '''
        if "JZ1W" in inFile.getName(): sampleEvents = 19963000.0
        elif "JZ2W"  in inFile.GetName(): sampleEvents = 7384.153593387626
        elif "JZ3W"  in inFile.GetName(): sampleEvents = 145.4567747937165
        elif "JZ4W"  in inFile.GetName(): sampleEvents = 4.451498624218423
        elif "JZ5W"  in inFile.GetName(): sampleEvents = 0.16628921478919437    
        elif "JZ6W"  in inFile.GetName(): sampleEvents = 0.027796986941066512   
        elif "JZ7W"  in inFile.GetName(): sampleEvents = 0.0038236895330489903  
        elif "JZ8W"  in inFile.GetName(): sampleEvents = 0.0011087719160303312  
        elif "JZ9W"  in inFile.GetName(): sampleEvents = 0.00021489790011135126 
        elif "JZ10W" in inFile.GetName(): sampleEvents = 2.650346890226054e-05  
        elif "JZ11W" in inFile.GetName(): sampleEvents = 1.4022733093478103e-05 
        elif "JZ12W" in inFile.GetName(): sampleEvents = 8.150041809475757e-06 
        '''        
        print "Number of events from cutflow: " + str(sampleEvents)
        print inFile.GetName(),("JZ1W" in inFile)
        recordedCutflow.SetDirectory(0)

        if not "data" in sampleName:
          recordedCutflow.Scale( 1./ sampleEvents )
          #Get xs from first tree entry
          for event in inTree:
#            recordedCutflow.Scale( inTree.weight_xs )
            break #only once!
        break

    for key in keys:
      if args.powheg and 'cutflow' in key.GetName() and 'weighted' in key.GetName():
        sampleEvents = key.ReadObj().GetBinContent(1)
        print sampleEvents
        if "JZ5" in args.file:
          sampleEvents = sampleEvents - 352220000
          continue
      elif not args.powheg and 'cutflow' in key.GetName() and 'weighted' in key.GetName():
        #if 'cutflow' in key.GetName() and str(dsid) in key.GetName():
        recordedCutflow = inFile.Get( key.GetName() )
        sampleEvents = key.ReadObj().GetBinContent(1)
        Name = inFile.GetName() 
        if "WithSW" in Name: sampleEvents = plotUtils.getSampleEvents(Name)
        elif "Sherpa" in Name: 
          print "in sherpa weighting"
          sampleEvents = 1./plotUtils.getSampleEvents(Name)
        '''
        if "JZ1W" in inFile.GetName(): sampleEvents = 19963000.0
        elif "JZ2W"  in inFile.GetName(): sampleEvents = 7384.153593387626
        elif "JZ3W"  in inFile.GetName(): sampleEvents = 145.4567747937165
        elif "JZ4W"  in inFile.GetName(): sampleEvents = 4.451498624218423
        elif "JZ5W"  in inFile.GetName(): sampleEvents = 0.16628921478919437    
        elif "JZ6W"  in inFile.GetName(): sampleEvents = 0.027796986941066512   
        elif "JZ7W"  in inFile.GetName(): sampleEvents = 0.0038236895330489903  
        elif "JZ8W"  in inFile.GetName(): sampleEvents = 0.0011087719160303312  
        elif "JZ9W"  in inFile.GetName(): sampleEvents = 0.00021489790011135126 
        elif "JZ10W" in inFile.GetName(): sampleEvents = 2.650346890226054e-05  
        elif "JZ11W" in inFile.GetName(): sampleEvents = 1.4022733093478103e-05 
        elif "JZ12W" in inFile.GetName(): sampleEvents = 8.150041809475757e-06    
        '''
        continue

    print "After  Number of events from cutflow: " + str(sampleEvents)

    if sampleEvents == 0:
      raise SystemExit('\n***WARNING*** no cutflow entries found')

    print( "\t total events : " + str(sampleEvents) )

#    if not "data" in sampleName:
#      recordedCutflow.Scale( 1./ sampleEvents )
#    #Get xs from first tree entry
#      for event in inTree:
#        recordedCutflow.Scale( inTree.weight_xs )
#        continue #only once!
#------------------------------------------

    '''
    if args.maxEvents > 0 and "JZ3" in inFile: args.maxEvents = 400000
    if args.maxEvents > 0 and "JZ4" in inFile: args.maxEvents = 400000
    if args.maxEvents > 0 and "JZ5" in inFile: args.maxEvents = 250000
    if args.maxEvents > 0 and "JZ6" in inFile: args.maxEvents = 250000
    if args.maxEvents > 0 and "JZ6" in inFile: args.maxEvents = 100000
    if args.maxEvents > 0 and "JZ8" in inFile: args.maxEvents = 100000
    if args.maxEvents > 0 and "JZ9" in inFile: args.maxEvents = 100000
    if args.maxEvents > 0 and "JZ10" in inFile: args.maxEvents = 50000
    if args.maxEvents > 0 and "JZ11" in inFile: args.maxEvents = 50000
    if args.maxEvents > 0 and "JZ12" in inFile: args.maxEvents = 50000
    '''
    '''
    if args.maxEvents > 0 and inTree.GetEntries() > args.maxEvents:
      scaleFactor = float(inTree.GetEntries()) / float(args.maxEvents)
      print( "\t running over max :" + str(args.maxEvents))
      print( "\t scale factor: " + str(scaleFactor) )
      sampleEvents = sampleEvents / scaleFactor
    '''



#------------------------------------------
    ## get the list of plots and fill them
#    plotList1D = getPlotList1D(inTree, sampleName)
#    plotList2D = getPlotList2D(inTree, sampleName)

#    print plotList1D
#
#    if(args.v):
#      print("\n 1D plots" )
#      for varName in plotList1D:
#        print( varName + "\t" + plotList1D[varName].GetName() + "\t" + plotList1D[varName].GetTitle() )
#      print("\n" )
#      print("\n 2D plots" )
#      for varName in plotList2D: print( varName )
#      print("\n" )

#    fillHists(inTree, sampleName, sampleEvents, plotList1D, plotList2D)

### make the output file and write the histograms to it
#    outFileName = args.outDir + "/" + args.studyInfo + args.histType + "." + sampleName + ".root"
#    outFile = ROOT.TFile(outFileName, 'RECREATE')
#    outFile.cd()
#
#    for varName in plotList1D:
#      plotList1D[varName].Write()
#      print(varName)
#      print type(plotList1D[varName])
#    for varName in plotList2D:
#      plotList2D[varName].Write()
#------------------------------------------


#    if args.do_massPartonPlots:
#        args.do_massPartonPlots = False
#        print("do_massPartonPlots IS NOT CURRENTLY SUPPORTED")#
#    if args.jetConstituents:
#        args.jetConstituents = False
#        print("jetConstituents IS NOT CURRENTLY SUPPORTED") #
#    if args.jetConstituentsAll:
#        args.jetConstituentsAll = False
#        print("jetConstituents IS NOT CURRENTLY SUPPORTED") #
#    if args.jetCaloQuant:
#        args.jetCaloQuant = False
#        print("jetCaloQuant IS NOT CURRENTLY SUPPORTED") #
#    if args.jetTrack:
#        args.jetTrack = False
#        print("jetTrack IS NOT CURRENTLY SUPPORTED") #
#    if args.jetTrackQuant or args.jetTrackQuantAll:
#        args.jetTrackQuant = False
#        args.jetTrackQuantAll = False
#        print("jetTrackQuant IS NOT CURRENTLY SUPPORTED") #

    plotList = {}
#    plotList = getPlotListCutFlow( inTree, sampleName, plotList )
    if args.basicInfo:                      plotList = getPlotListBasicInfo( inTree, sampleName, plotList )
    if args.typicalVariables:               plotList = getPlotListTypicalVariables( inTree, sampleName, plotList )
    if args.chiVariables:                   plotList = getPlotListChiVariables( inTree, sampleName, plotList )
    if args.sensitivity:                    plotList = getPlotListSensitivity( inTree, sampleName, plotList )
    if args.cleaning:                       plotList = getPlotListCleaning( inTree, sampleName, plotList )
    if args.punchThrough:                   plotList = getPlotListPunchThrough( inTree, sampleName, plotList )
    if args.kinematics:                     plotList = getPlotListKinematics( inTree, sampleName, plotList )
    if args.kinematicDistributions:         plotList = getPlotListKinematicDistributions( inTree, sampleName, plotList )
    if args.energyLayers:                   plotList = getPlotListEnergyLayers( inTree, sampleName, plotList )
    if args.energyLayersLength:             plotList = getPlotListEnergyLayersLength( inTree, sampleName, plotList )
    if args.truth:                          plotList = getPlotListTruth( inTree, sampleName, plotList )
#      if args.truthDetail:                  plotList = getPlotListTruthDetail( inTree, sampleName, plotList )
    if args.do_massPartonPlots:             plotList = getPlotListMassPartons( inTree, sampleName, plotList )
#    if args.userDefinedPlots:                plotList = getPlotListUserDefined( inTree, sampleName, plotList )
    if args.jetConstituents:                plotList = getPlotListJetConstituents( inTree, sampleName, plotList )
    if args.jetConstituentsAll:             plotList = getPlotListJetConstituentsAll( inTree, sampleName, plotList )

    if args.jetCaloQuant:                   plotList = getPlotListCaloQuant( inTree, sampleName, plotList )

    if args.jetTrackQuant or args.jetTrackQuantAll: plotList = getPlotListTrackQuant( inTree, sampleName, plotList )

    if args.wideJets:                       plotList = getPlotListWideJets( inTree, sampleName, plotList )
    if args.quarkGluon:                     plotList = getPlotListQuarkGluon( inTree, sampleName, plotList )
    if args.letterKinematicPlots:           plotList = getPlotListLetterKinematicPlots( inTree, sampleName, plotList )

    if args.quickStudy:                     plotList = getPlotListQuickStudy( inTree, sampleName, plotList )

    if args.jetTrack:                       plotList = getPlotListTrack( inTree, sampleName, plotList )
    if args.plotAll or args.plotAllVector:  plotList = getPlotListAll( inTree, sampleName, plotList )
    plotList = getPlotListBranchList( inTree, sampleName, getBranchList( sampleName ), plotList )



    plotList['events_passed'] = makeTH1D("events_passed", sampleName, 2,-0.5 , 1.5,   "Jet #eta"    )


    #Check plot list for duplicates and remove
#    plotList = removeDuplicatesfromPlotList( plotList )
#   Remove variables not found in mc14... really on necessary for plotAll scenarios
# Need to add a default to turn on fillJets for the relevant studies


    if(args.v): print plotList
    print plotList

    if(args.v):
      print("\n plots" )
      for varName in plotList:
        print( varName + "\t" + plotList[varName].GetName() + "\t" + plotList[varName].GetTitle() )
      print("\n" )
#      print("\n 2D plots" )
#      for varName in plotList2D: print( varName )
#      print("\n" )






#    plotList['eventCutFlow'].Fill(6,weight)
#    cutflow_all = recordedCutflow.GetBinContent(1)
#    cutflow_NPV = recordedCutflow.GetBinContent(1)
#    cutflow_Trigger = recordedCutflow.GetBinContent(1)
#    cutflow_TriggerEfficiency = recordedCutflow.GetBinContent(1)
#    cutflow_mcCleaning = recordedCutflow.GetBinContent(1)
#
#

    recordedCutflow.SetName("event_cutflow_0")
    recordedCutflow.GetXaxis().FindBin("Apply GRL")
    recordedCutflow.GetXaxis().FindBin("HLT_j360")
    recordedCutflow.GetXaxis().FindBin("lbn")
    recordedCutflow.GetXaxis().FindBin("cleaning")
    recordedCutflow.GetXaxis().FindBin("LJetPt")
    recordedCutflow.GetXaxis().FindBin("mjjMin")
    recordedCutflow.GetXaxis().FindBin("yStarMax")
    recordedCutflow.GetXaxis().FindBin("yBoost")
    recordedCutflow.GetXaxis().FindBin("etaLT2p8")


    fillHists(inTree, sampleName, sampleEvents, plotList, recordedCutflow)





    #------------------------------------------
    ## make the output file and write the histograms to it
#    outFileName = args.outDir + "/" + args.studyInfo + args.histType + "." + sampleName + ".root"
    outFileName = args.outFileName
    outFile = ROOT.TFile(outFileName, 'RECREATE')
    print outFile, outFileName
    outFile.cd()

    for varName in plotList:
      plotList[varName].Write()
      if(args.v): print(varName)

    #recordedCutflow.Write()




######### useful functions #########
def makeTH1D(varName, sampleName, nbins, xmin, xmax, xlabel, ylabel="Number of Jets"):
  name = args.histType + "_" + varName + "_" + sampleName
  if args.v: print("Adding 1D : " + name)
  title = varName # TITLE is always the name of the branch if want to use "a_" option
  hist = ROOT.TH1D( name, title, nbins, xmin, xmax )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist

def makeTH2D(varName, sampleName, xnbins, xmin, xmax, ynbins, ymin, ymax, xlabel, ylabel):
  name = args.histType + "_" + varName + "_" + sampleName
  title = varName
  hist = ROOT.TH2D( name, title, xnbins, xmin, xmax, ynbins, ymin, ymax )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist

def makeTH2DxArray(varName, sampleName, xbins, ynbins, ymin, ymax, xlabel, ylabel):
  name = args.histType + "_" + varName + "_" + sampleName
  title = varName
  hist = ROOT.TH2D( name, title, len(xbins)-1, array.array('d', xbins), ynbins, ymin, ymax )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist


def makeTH2DyArray(varName, sampleName, xnbins,xmin, xmax, ybins, xlabel, ylabel):
  name = args.histType + "_" + varName + "_" + sampleName
  title = varName
  hist = ROOT.TH2D( name, title,  xnbins, xmin, xmax, len(ybins)-1, array.array('d', ybins) )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist

#------ Antonio's additions
def makeTH2DxyArray(varName, sampleName, xbinedges, ybinedges, xlabel, ylabel):
  name = args.histType + "_" + varName + "_" + sampleName
  if args.v: print("Adding TH2D : " + name)
  title = varName
  hist = ROOT.TH2D( name, title, len(xbinedges)-1, array.array('d',xbinedges), len(ybinedges)-1, array.array('d',ybinedges) )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist

def makeTProfile(varName, sampleName, nbins, xmin, xmax, xlabel):
  name = args.histType + "_" + varName + "_" + sampleName
  if args.v: print("Adding Profile : " + name)
  title = varName # TITLE is always the name of the branch if want to use "a_" option
  hist = ROOT.TProfile( name, title, nbins, xmin, xmax )
  hist.GetXaxis().SetTitle(xlabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist

def makeTProfileArray(varName, sampleName, xedges, xlabel):
  name = args.histType + "_" + varName + "_" + sampleName
  if args.v: print("Adding Profile : " + name)
  title = varName # TITLE is always the name of the branch if want to use "a_" option
  hist = ROOT.TProfile( name, title, len(xedges)-1, array.array('d',xedges) )
  hist.GetXaxis().SetTitle(xlabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist
#-------

#------ Caterina's addition
def makeTProfile2DArray(varName, sampleName, xedges, yedges, xlabel, ylabel):
  name = args.histType + "_" + varName + "_" + sampleName
  if args.v: print("Adding Profile2D : " + name)
  title = varName # TITLE is always the name of the branch if want to use "a_" option
  hist = ROOT.TProfile2D( name, title, len(xedges)-1, array.array('d',xedges), len(yedges)-1, array.array('d',yedges) )
  hist.GetXaxis().SetTitle(xlabel)
  hist.GetYaxis().SetTitle(ylabel)
  hist.Sumw2()
  #hist.SetDirectory(0)
  return hist
#-------

def getMassHist(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getMassHist(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getMassHistMorphing(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getMassHist(name, "GeVbins")
  hist.SetTitle(varName)
  return hist

def getMassHistTeV(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getMassHistTeV(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getJetPtHistTeV(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetPtHistTeV(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getChiHist(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getChiHist(name)
  hist.SetTitle(varName)
  return hist

def getJetPtHist(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetPtHist(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getJetEnHist(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetEnHist(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getJetEtaPhiHist(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetEtaPhiHist(name)
  hist.SetTitle(varName)
  return hist

def getJetEtaHist(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetEtaHist(name)
  hist.SetTitle(varName)
  return hist

def getJetPhiHist(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetPhiHist(name)
  hist.SetTitle(varName)
  return hist

def getJetMassHist(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetMassHist(name)
  hist.SetTitle(varName)
  return hist

#Punch Through Studies
def getNSegments(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getNSegments(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getJetPt_nSegments(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetPt_nSegments(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getJetEta_nSegments(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetEta_nSegments(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getJetPhi_nSegments(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetPhi_nSegments(name, "13TeV")
  hist.SetTitle(varName)
  return hist

def getJetEMScaleEta_nSegments_TruthEg1(varName, sampleName):
  name = args.histType + "_" + varName + "_" + sampleName
  hist = plotUtils.getJetEMScaleEta_nSegments_TruthEg1(name, "13TeV")
  hist.SetTitle(varName)
  return hist


# return parton label as string
def getQG( pdgid ):
  if pdgid == 21:
    return "g"
  else:
    return "q"



######################################################
##### TWO FUNCTIONS FOLLOW USED TO DEFINE HISTS  #####
##### YOU MUST TAKE CARE TO FILL EACH PLOT ADDED #####
##### FILLING IN DONE IN THE FUCNTION JUST AFTER #####
#####                                            #####
#####   IF THE PLOT IS DIRECTLY FROM ONE BRANCH  #####
#####   THEN START THE VARIABLE NAME WITH a_     #####
#####   AND IT WILL BE AUTOMATICLLY FILLED       #####
######################################################
#def removeDuplicatesfromPlotList( inList ):
##  # order preserving
#  noDuplicates = {}
#  [noDuplicates[name] for name in inList if not noDuplicates.count(name)]
#  return noDuplicates
##  set = Set( inList )
##  return list( set )

def getPlotListPunchThrough( tree, sampleName, plotList ):
    endsGM = plotUtils.getHistExtrema(tree,"jet__GhostMuonSegmentCount","min")

    #plotList['jet__GhostMuonSegmentCount'] = makeTH1D("jet__GhostMuonSegmentCount", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet__GhostMuonSegmentCount","min"), plotUtils.getHistExtrema(tree,"jet__GhostMuonSegmentCount","max"), "N_{Segments}" )
    plotList['jet__GhostMuonSegmentCount'] = makeTH1D("jet__GhostMuonSegmentCount", sampleName, args.nBins, ends[0], ends[1], "N_{Segments}" )

    #endsGM = plotUtils.getHistExtrema(tree,"jet__GhostMuonSegmentCount","min")
    endspt = plotUtils.getHistExtrema(tree,"jet_pt","max")
    plotList['jet_pt__jet__GhostMuonSegmentCount'] = makeTH2D("jet_pt__jet__GhostMuonSegmentCount", sampleName, args.nBins, endspt[0], endspt[1], args.nBins, endsGM[0], endsGM[1], "jet p_{T}", "N_{Segments}")

    endseta = plotUtils.getHistExtrema(tree,"jet_eta","min")
    plotList['jet_eta__jet__GhostMuonSegmentCount'] = makeTH2D("jet_eta__jet__GhostMuonSegmentCount", sampleName, args.nBins, endseta[0], endseta[1], args.nBins, endsGM[0], endsGM[1], "jet #eta", "N_{Segments}")

    endsphi = plotUtils.getHistExtrema(tree,"jet_phi","min")
    plotList['jet_phi__jet__GhostMuonSegmentCount'] = makeTH2D("jet_phi__jet__GhostMuonSegmentCount", sampleName, args.nBins, endsphi[0], endsphi[1], args.nBins, endsGM[0], endsGM[1], "jet #phi", "N_{Segments}")

    endsemScaleEta = plotUtils.getHistExtrema(tree,"jet_emScaleEta","min")
    plotList['jet_emScaleEta__jet__GhostMuonSegmentCount'] = makeTH2D("jet_emScaleEta__jet__GhostMuonSegmentCount", sampleName, args.nBins, endsemScaleEta[0] , endsemScaleEta[1], args.nBins, endsGM[0], endsGM[1], "detector #eta", "N_{Segments}")

    endsE = plotUtils.getHistExtrema(tree, "jet_E","min")
    plotList['jet_E__jet__GhostMuonSegmentCount'] = makeTH2D("jet_E__jet__GhostMuonSegmentCount", sampleName, args.nBins, endsE[0],endsE[1],  args.nBins, endsGM[0], endsGM[1], "jet E [GeV]", "N_{Segments}")

    pTBinning = plotUtils.getPtBinsForJets()
    for i in range( 0,len(pTBinning) -1 ):
      pTLabel = "pT_"+str(pTBinning[i])+"_"+str(pTBinning[i+1])
      pTLabel.replace(".","p")
      plotList['average_jet__GhostMuonSegmentCount_for'+pTLabel+'_vs_eta'] = makeTProfileArray("average_jet__GhostMuonSegmentCount_for"+pTLabel+"_vs_eta", sampleName, plotUtils.getJetAbsEtaBins(), "detector #eta")

    plotList['ptasym12__first_jet__GhostMuonSegmentCount'] = makeTH2D("ptasym12__first_jet__GhostMuonSegmentCount", sampleName, 20,-1,1, args.nBins, endsGM[0], endsGM[1], "(p_{T,2} - p_{T,1})/(p_{T,2} + p_{T,1})", "First Jet GhostMuonSegmentCount")
    plotList['ptasym12__second_jet__GhostMuonSegmentCount'] = makeTH2D("ptasym12__second_jet__GhostMuonSegmentCount", sampleName, 20,-1,1, args.nBins, endsGM[0], endsGM[1], "(p_{T,2} - p_{T,1})/(p_{T,2} + p_{T,1})", "Second Jet GhostMuonSegmentCount")


#  plotList['jet__GhostMuonSegmentCount'] = getNSegments("nSegments", sampleName)
#  plotList['jet_pt__jet__GhostMuonSegmentCount'] = getJetPt_nSegments("nSegmentsvJetPt", sampleName)
#  plotList['jet_eta__jet__GhostMuonSegmentCount'] = getJetEta_nSegments("nSegmentsvJetEta", sampleName)
#  plotList['jet_phi__jet__GhostMuonSegmentCount'] = getJetPhi_nSegments("nSegmentsvJetPhi", sampleName)
#  plotList['jet_emScaleEta__jet__GhostMuonSegmentCount'] = getJetEMScaleEta_nSegments_TruthEg1("punch5", sampleName)
    return plotList

def getPlotListSensitivity( tree, sampleName, plotList ):
#  plotList['a_mjj']   = getMassHist("mjj", sampleName)
  plotList['mjjj']   = getMassHist("mjjj", sampleName)
  return plotList


def getPlotListQuickStudy( tree, sampleName, plotList ):
    plotList['mjj'] = getMassHistTeV("mjj", sampleName)
#    plotList['L1CaloCheck'] = makeTH1D("L1CaloCheck",   sampleName, 120,  0.0, 5.0e3, "Jet E x (1-EMFrac - HECFrac)" )
    return plotList

def getPlotListQuarkGluon( tree, sampleName, plotList ):

    #print sampleName
    if args.morphing:
      print "IN morphing section"
      plotList['mjj'] = getMassHistMorphing("mjj", sampleName)
      plotList['QQ_mjj'] = getMassHistMorphing("QQ_mjj",sampleName)
      plotList['QG_mjj'] = getMassHistMorphing("QG_mjj",sampleName)
      plotList['GG_mjj'] = getMassHistMorphing("GG_mjj",sampleName)

      plotList['truthQQ_mjj'] = getMassHistMorphing("truthQQ_mjj",sampleName)
      plotList['truthQG_mjj'] = getMassHistMorphing("truthQG_mjj",sampleName)
      plotList['truthGG_mjj'] = getMassHistMorphing("truthGG_mjj",sampleName)
    else:
      plotList['mjj'] = getMassHistTeV("mjj", sampleName)
      plotList['QQ_mjj'] = getMassHistTeV("QQ_mjj", sampleName)
      plotList['QG_mjj'] = getMassHistTeV("QG_mjj", sampleName)
      plotList['GG_mjj'] = getMassHistTeV("GG_mjj", sampleName)

      plotList['truthQQ_mjj'] = getMassHistTeV("truthQQ_mjj", sampleName)
      plotList['truthQG_mjj'] = getMassHistTeV("truthQG_mjj", sampleName)
      plotList['truthGG_mjj'] = getMassHistTeV("truthGG_mjj", sampleName)


      plotList['mjj_GeV'] = getMassHistMorphing("mjj_GeV", sampleName)
      plotList['QQ_mjj_GeV'] = getMassHistMorphing("QQ_mjj_GeV",sampleName)
      plotList['QG_mjj_GeV'] = getMassHistMorphing("QG_mjj_GeV",sampleName)
      plotList['GG_mjj_GeV'] = getMassHistMorphing("GG_mjj_GeV",sampleName)

      plotList['truthQQ_mjj_GeV'] = getMassHistMorphing("truthQQ_mjj_GeV",sampleName)
      plotList['truthQG_mjj_GeV'] = getMassHistMorphing("truthQG_mjj_GeV",sampleName)
      plotList['truthGG_mjj_GeV'] = getMassHistMorphing("truthGG_mjj_GeV",sampleName)


    #plotList['jet_NumTrkPt500PV'] = makeTH1D("jet_NumTrkPt500PV", sampleName, 80, -0.5, 79.5, "Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['QQ_leading_nTrk500']    =  makeTH1D("QQ_leading_nTrk500",   sampleName, 80, -0.5, 79.5, "QQ Number of Tracks, p_{T}^{trk}>500 MeV from PV") 
    plotList['QQ_subleading_nTrk500'] =  makeTH1D("QQ_subleading_nTrk500",sampleName, 80, -0.5, 79.5, "QQ Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['QG_leading_nTrk500']    =  makeTH1D("QG_leading_nTrk500",   sampleName, 80, -0.5, 79.5, "QG Number of Tracks, p_{T}^{trk}>500 MeV from PV") 
    plotList['QG_subleading_nTrk500'] =  makeTH1D("QG_subleading_nTrk500",sampleName, 80, -0.5, 79.5, "QG Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['GG_leading_nTrk500']    =  makeTH1D("GG_leading_nTrk500",   sampleName, 80, -0.5, 79.5, "GG Number of Tracks, p_{T}^{trk}>500 MeV from PV") 
    plotList['GG_subleading_nTrk500'] =  makeTH1D("GG_subleading_nTrk500",sampleName, 80, -0.5, 79.5, "GG Number of Tracks, p_{T}^{trk}>500 MeV from PV")

    
    plotList['truthQ_nTrk500']    =  makeTH1D("truthQ_nTrk500",   sampleName, 80, -0.5, 79.5, "truthQ Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['truthG_nTrk500']    =  makeTH1D("truthG_nTrk500",   sampleName, 80, -0.5, 79.5, "truthG Number of Tracks, p_{T}^{trk}>500 MeV from PV")

    QGTrackptBinning = plotUtils.getQGptBins()
    for i in range( 0,len(QGTrackptBinning)-1 ):
      minPt = QGTrackptBinning[i]
      maxPt = QGTrackptBinning[i+1]
      QGmassLabel = ("pT_%d_%d" % (int(math.floor(minPt)),int(math.floor(maxPt))))
      plotList['truthQ_nTrk500_for_'+QGmassLabel] =  makeTH1D("truthQ_nTrk500_for_"+QGmassLabel, sampleName, 80, -0.5, 79.5, "truthQ Number of Tracks, p_{T}^{trk}>500 MeV from PV")
      plotList['truthG_nTrk500_for_'+QGmassLabel] =  makeTH1D("truthG_nTrk500_for_"+QGmassLabel, sampleName, 80, -0.5, 79.5, "truthG Number of Tracks, p_{T}^{trk}>500 MeV from PV")


    return plotList

def getPlotListWideJets( tree, sampleName, plotList ):
    for j in [1,2]:
        plotList['cluster'+str(j)+'_FSR_njets'] = makeTH1D("cluster"+str(j)+"_FSR_njets", sampleName, 20, -0.5, 19.5, "Jet Cluster Multiplicity (include lead)")
        plotList['cluster'+str(j)+'_FSR_pt'] = getJetPtHist("cluster"+str(j)+"_FSR_pt", sampleName)
        plotList['cluster'+str(j)+'_FSR_phi'] = makeTH1D("cluster"+str(j)+"_FSR_phi", sampleName, 120, -3.2, 3.2,   "Jet #phi"    )
        plotList['cluster'+str(j)+'_FSR_eta'] = makeTH1D("cluster"+str(j)+"_FSR_eta", sampleName, 120, -4.8, 4.8,   "Jet #eta"    )
        plotList['cluster'+str(j)+'_FSR_E'] = makeTH1D("jet_E",   sampleName, 120,  0.0, 3.0e3, "Jet E [GeV]" )
        plotList['cluster'+str(j)+'_FSR_M']  = getJetMassHist("cluster"+str(j)+"_FSR_M", sampleName)
        plotList['wideJet'+str(j)+'_pt'] = getJetPtHist("wideJet"+str(j)+"_pt", sampleName)
        plotList['wideJet'+str(j)+'_phi'] = makeTH1D("wideJet"+str(j)+"_phi", sampleName, 120, -3.2, 3.2,   "wideJet"+str(j)+" #phi"    )
        plotList['wideJet'+str(j)+'_eta']  = makeTH1D("wideJet"+str(j)+"_eta", sampleName, 120, -4.8, 4.8,   "wideJet"+str(j)+" #eta"    )
        plotList['wideJet'+str(j)+'_E'] = makeTH1D("wideJet"+str(j)+"_E",   sampleName, 120,  0.0, 3.0e3, "wideJet"+str(j)+" E [GeV]" )
        plotList['wideJet'+str(j)+'_M']  = getJetMassHist("wideJet"+str(j)+"_M", sampleName)  #makeTH1D("jet_M", sampleName, 60, 0.0, 120, "jet mass [GeV]")
    plotList['wideJets_mjj'] = getMassHist("wideJets_mjj", sampleName)
    plotList['wideJets_pTjj'] = getJetPtHist("wideJets_pTjj", sampleName)
    plotList['wideJets_deltaPhi'] = makeTH1D("wideJets_deltaPhi", sampleName, 120, -10, 10, "wideJets_deltaPhi")
    plotList['wideJets_deltaEta'] = makeTH1D("wideJets_deltaEta", sampleName, 120, -10, 10, "wideJets_deltaEta")
    plotList['wideJets_yStar'] = makeTH1D("wideJets_yStar", sampleName, 120, -1.8, 1.8, "wideJets_yStar")
    plotList['wideJets_yBoost'] = makeTH1D("wideJets_yBoost", sampleName, 120, -4, 4, "wideJets_yBoost")
    return plotList


#def getPlotListCutFlow( tree, sampleName, plotList ):
#  plotList['eventCutFlow']   =  makeTH1D("event_CutFlow", sampleName, 15, -0.5,14.5 , "Event CutFlow")
#  plotList['eventCutFlow_unweighted'] = makeTH1D("event_CutFlow_unweighted", sampleName, 11, -0.5,10.5 , "Event CutFlow unweighted")
#  return plotList

def getPlotListCleaning( tree, sampleName, plotList ):
  plotList['jet_BchCorrCell'] = makeTH1D("jet_BchCorrCell", sampleName, 50, -0.1, 1.1, "BCH CORR CELL")
  plotList['jet_HECQuality'] = makeTH1D("jet_HECQuality", sampleName, 50, -1.1, 1.1, "HEC Quality")
  plotList['jet_LArQuality'] = makeTH1D("jet_LArQuality", sampleName, 50, 0.0, 1.1, "LAr Quality fraction")
  plotList['jet_AverageLArQF'] = makeTH1D("jet_AverageLArQF", sampleName, 50, -1.1, 1.1, "Mean LAr Quality")
  plotList['jet_LArBadHVNCell'] = makeTH1D("jet_LArBadHVNCell", sampleName, 50, -1.1, 1.1, "LAr Bad HVN Cell")
  plotList['jet_LArBadHVEnergyFrac'] = makeTH1D("jet_LArBadHVEnergyFrac", sampleName, 50, -1.1, 1.1, "LAr Bad HV Energy Fraction")
  plotList['jet_Timing'] = makeTH1D("jet_Timing", sampleName, 120, -12., 12., "jet Timing [ns]")
  plotList['jet_NegativeE'] = makeTH1D("jet_NegativeE", sampleName, 80, -70., 10., "jet Negative E [GeV]")
  plotList['jet_N90Constituents'] = makeTH1D("jet_N90Constituents", sampleName, 25, -0.5, 24.5, "N90 Constituents")

  endsTiming = plotUtils.getHistExtrema(tree,"jet_Timing","min")
  plotList['jet_eta__Timing'] = makeTH2D("jet_eta__Timing", sampleName, 80, -4, 4, args.nBins, endsTiming[0], endsTiming[1], "Jet #eta", "Jet timing [ns]")

  endspt = plotUtils.getHistExtrema(tree,"jet_pt","max")
  plotList['jet_pt__Timing'] = makeTH2D("jet_pt__Timing", sampleName, args.nBins, endspt[0], endspt[1], args.nBins, endsTiming[0], endsTiming[1], "Jet p_{T} [GeV]", "Jet timing [ns]")
  return plotList


def getPlotListEnergyLayers( tree, sampleName, plotList ):
  if args.v : print "in getPlotListEnergyLayers"

  plotList['jet__EMFrac'] = makeTH1D("jet__EMFrac", sampleName, 50, -0.1, 1.4, "EM fraction")
  plotList['jet__HECFrac'] = makeTH1D("jet__HECFrac", sampleName, 20, -0.1, 1.1, "HEC fraction")
  plotList['jet__FracSamplingMax'] = makeTH1D("jet__FracSamplingMax", sampleName, 50,0, 1, "Maximum fraction of jet energy deposited in a layer")
  plotList['jet__FracSamplingMaxIndex'] = makeTH1D("jet__FracSamplingMaxIndex", sampleName, 25,0, 25, "Layer of maximum fractional jet energy deposit")

  endspt = plotUtils.getHistExtrema(tree,"jet_pt","max")
  plotList['jet_pt__EMFrac']  = makeTH2D("jet_pt__EMFrac", sampleName, args.nBins, endspt[0], endspt[1], 24, 0, 1.2, "jet p_{T} [GeV]", "EM Fraction")
  endsE = plotUtils.getHistExtrema(tree,"jet_E","min")
  plotList['jet_Eem__EMFrac']  = makeTH2D("jet_Eem__EMFrac", sampleName, args.nBins, endsE[0], endsE[1], 24, 0, 1.2, "E^{jet}_{EM} [GeV]", "EM Fraction")

  plotList['jet_emScaleEta__Eem'] = makeTH2D("jet_emScaleEta__Eem", sampleName, 48, -4.8, 4.8, args.nBins, endsE[0], endsE[1],"detector #eta", "Jet E^{EM} [GeV]")

  plotList['jet_eta__Eem'] = makeTH2D("jet_eta__Eem", sampleName, 48, -4.8, 4.8, args.nBins, endsE[0], endsE[1],"#eta", "Jet E^{EM} [GeV]")

  #eta jet plots
  etaBinning = plotUtils.getJetAbsEtaBins()
  for i in range( 0,len(etaBinning) -1 ):
      minEta = etaBinning[i]
      maxEta = etaBinning[i+1]
      etaLabel = "_tot_eta_"+str(minEta)+"_"+str(maxEta)
      etaLabel = etaLabel.replace(".","p")
      plotList['jet__EMFrac'+etaLabel] = makeTH1D("jet__EMFrac"+etaLabel, sampleName, 50, -0.1, 1.4, "EM fraction")
      plotList['jet_Eem__EMFrac'+etaLabel]  = makeTH2D("jet_Eem__EMFrac"+etaLabel, sampleName, args.nBins, endsE[0], endsE[1], 24, 0, 1.2, "E^{jet}_{EM} [GeV]", "EM Fraction")


      plotList['jet__HECFrac'+etaLabel] = makeTH1D("jet__HECFrac"+etaLabel, sampleName, 20, -0.1, 1.1, "HEC fraction")
      plotList['jet_pt__EMFrac'+etaLabel]  = makeTH2D("jet_pt__EMFrac"+etaLabel, sampleName, args.nBins, endspt[0], endspt[1], 24, 0, 1.2, "jet p_{T} [GeV]", "EM Fraction")
      plotList['jet_E__EMFrac'+etaLabel]  = makeTH2D("jet_E__EMFrac"+etaLabel, sampleName, args.nBins, endsE[0], endsE[1], 24, 0, 1.2, "jet E [GeV]", "EM Fraction")



  for iSL in range(0, len(sampleLayerList)):
      labelEta = '_eta_'+str(sampleLayerEtaRange[iSL][0]).replace(".","p")+"_"+str(sampleLayerEtaRange[iSL][-1]).replace(".","p")

      plotList['jet_'+sampleLayerList[iSL]+labelEta] = makeTH1D("jet_"+sampleLayerList[iSL]+labelEta, sampleName, sampleBins[iSL], sampleMin[iSL], sampleMax[iSL], 'E^{jet}_{EM} in '+sampleLayerList[iSL]+' [GeV]')
      plotList['jet_'+sampleLayerList[iSL]+'overEem'+labelEta] = makeTH1D("jet_"+sampleLayerList[iSL]+'overEem'+labelEta, sampleName, 50, 0., 0.5, 'Fraction E^{jet}_{EM} in '+sampleLayerList[iSL]+' [GeV]')
      plotList['jet_Eem__'+sampleLayerList[iSL]+'overEem'+labelEta]  = makeTH2D("jet_Eem__"+sampleLayerList[iSL]+'overEem'+labelEta,     sampleName, args.nBins, endsE[0], endsE[1], 100, 0., 0.5, "E^{jet}_{EM} [GeV]", "E^{jet}_{EM} in "+sampleLayerList[iSL]+" / E^{jet}_{EM}")

      plotList['jet_pt__'+sampleLayerList[iSL]+labelEta]  = makeTH2D("jet_pt__"+sampleLayerList[iSL]+labelEta,     sampleName, args.nBins, endspt[0], endspt[1], sampleBins[iSL], sampleMin[iSL], sampleMax[iSL], "Jet p_{T} [GeV]", "E^{jet}_{EM} in "+sampleLayerList[iSL]+" [GeV]")

      plotList['jet_emScaleEta__'+sampleLayerList[iSL]+labelEta] = makeTH2D('jet_emScaleEta__'+sampleLayerList[iSL]+labelEta, sampleName, 20, sampleLayerEtaRange[iSL][0], sampleLayerEtaRange[iSL][-1], sampleBins[iSL], sampleMin[iSL], sampleMax[iSL],"detector #eta", 'E^{jet}_{EM} in '+sampleLayerList[iSL]+' [GeV]')
      plotList['jet_emScaleEta__'+sampleLayerList[iSL]+'overEem'+labelEta] = makeTH2D("jet_emScaleEta__"+sampleLayerList[iSL]+"overEem"+labelEta, sampleName, 20, sampleLayerEtaRange[iSL][0], sampleLayerEtaRange[iSL][-1], 50,0, 1,"detector #eta", "E^{jet}_{EM} in "+sampleLayerList[iSL]+" / E^{jet}_{EM}")


  return plotList

def getPlotListEnergyLayersLength( tree, sampleName, plotList ):
  if args.v : print "in getPlotListEnergyLayersLength"
  ybins = plotUtils.LogSpace(-7, 4, 300)
  plotList['energyLayersLength_Layers'] =makeTH2DyArray("energyLayersLength_Layers",  sampleName, 11,0,11, ybins,"","E_{layer}/(E_{calo}L_{layer}#lambda) [mm^{-1}]" )
  etaBinning = plotUtils.getJetAbsEtaBins()
  for i in range( 0,len(etaBinning) -1 ):
    minEta = etaBinning[i]
    maxEta = etaBinning[i+1]
    etaLabel = "tot_eta_"+str(minEta)+"_"+str(maxEta)
    etaLabel = etaLabel.replace(".","p")
    plotList['energyLayersLength_Layers_'+etaLabel] =makeTH2DyArray("energyLayersLength_Layers_"+etaLabel,  sampleName, 11,0,11, ybins,"","E_{layer}/(E_{calo}L_{layer}#lambda) [mm^{-1}]" )

    plotList['energyLayersLength_Layers_etaLimited'] =makeTH2DyArray("energyLayersLength_Layers_etaLimited",  sampleName, 11,0,11, ybins,"","E_{layer}/(E_{calo}L_{layer}#lambda) [mm^{-1}]" )

  return plotList


def getPlotListLetterKinematicPlots( tree, sampleName, plotList ):
    plotList['first_jet_pt']  = getJetPtHistTeV("first_jet_pt", sampleName)
    plotList['second_jet_pt']  = getJetPtHistTeV("second_jet_pt", sampleName)

    if args.morphing:
        plotList['mjj'] = getMassHistMorphing("mjj",sampleName )
    else:
        plotList['mjj'] = getMassHistTeV("mjj", sampleName)

    return plotList

def getPlotListKinematics( tree, sampleName, plotList ):
  plotList['jet_M']  = getJetMassHist("jet_M", sampleName)  #makeTH1D("jet_M", sampleName, 60, 0.0, 120, "jet mass [GeV]")
  plotList['jet_pt']   = getJetPtHist("jet_pt", sampleName)
  plotList['jet_eta']  = makeTH1D("jet_eta", sampleName, 120, -4.8, 4.8,   "Jet #eta"    )
  plotList['jet_rapidity']  = makeTH1D("jet_rapidity", sampleName, 120, -4.8, 4.8,   "Jet rapidity"    )
  plotList['jet_phi']  = makeTH1D("jet_phi", sampleName, 120, -3.2, 3.2,   "Jet #phi"    )
  plotList['jet_E']    = makeTH1D("jet_E",   sampleName, 120,  0.0, 3.0e3, "Jet E [GeV]" )
  plotList['jet_emScaleE']    = makeTH1D("jet_emScaleE",   sampleName, 120,  0.0, 6.0e3, "Jet E [GeV]" )
  plotList['jet_emScaleEta'] = makeTH1D("jet_emScaleEta", sampleName, 120, -4.8, 4.8, "detector #eta")
  plotList['jet_emScalePhi']  = makeTH1D("jet_emScalePhi", sampleName, phiBins, -phiRange, phiRange,   "Detector #phi"    )
  plotList['jet_eta__phi'] = makeTH2D("jet_eta__phi", sampleName, 120, -4.8, 4.8, 120, -3.2, 3.2, "Jet #eta", "Jet #phi" )
  plotList['jet_eta__pt'] = makeTH2DyArray("jet_eta__pt", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet #eta", "Jet p_{T} [GeV]" )
  plotList['jet_emScaleEta__pt'] = makeTH2DyArray("jet_emScaleEta__pt", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet detector #eta", "Jet p_{T} [GeV]")
  plotList['jet_emScaleEta__emScalePhi'] = makeTH2D("jet_emScaleEta__emScalePhi", sampleName, 120, -4.8, 4.8, phiBins, -phiRange, phiRange, "Detector #eta", "Detector #phi" )
  plotList['jet_eta__E'] = makeTH2DyArray("jet_eta__E", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet #eta", "Jet E [GeV]" )
  plotList['jet_emScaleEta__E'] = makeTH2DyArray("jet_emScaleEta__E", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet detector #eta", "Jet E [GeV]")


  #plotList['MHT'] = makeTH1D("MHT", sampleName, 240, 0,4000,   "MHT")
  plotList['HT'] = makeTH1D("HT", sampleName, 240, 0,4000,   "HT")
  #plotList['MHT_div_sqrtHT']  = makeTH1D("MHT_div_sqrtHT_", sampleName, 120, 0,100,   "MHT/sqrtHT ")
  pTBinning = plotUtils.getPtBinsForJets()
  for i in range( 0,len(pTBinning) -1 ):
    pTLabel = "pT_"+str(pTBinning[i])+"_"+str(pTBinning[i+1])
    pTLabel.replace(".","p")
    plotList['MHT_div_sqrtHT_'+pTLabel]  = makeTH1D("MHT_div_sqrtHT_"+pTLabel, sampleName, 120, 0,100,   "MHT/sqrtHT "+pTLabel)

  plotList['jet_Timing'] = makeTH1D("jet_Timing", sampleName, 120, -30., 30., "jet Timing [ns]")
  endsTiming = plotUtils.getHistExtrema(tree,"jet_Timing","min")
  plotList['jet_eta__Timing'] = makeTH2D("jet_eta__Timing", sampleName, 80, -4, 4, args.nBins, endsTiming[0], endsTiming[1], "Jet #eta", "Jet timing [ns]")

#  if not args.typicalVariables:
#    plotList['a_mjj']   = getMassHist("mjj", sampleName)
#    plotList['a_yStar'] = makeTH1D("yStar", sampleName, 120, -1.8, 1.8, "y*")
#    plotList['a_njets'] = makeTH1D("njets", sampleName, 20, -0.5, 19.5, "Jet Multiplicity")
#    plotList['njetsPass'] = makeTH1D("njetsPass", sampleName, 20, -0.5, 19.5, "Jet Multiplicity Passing Selection")
#    plotList['a_deltaPhi'] = makeTH1D("deltaPhi", sampleName, 60, 0, 3.2, "#Delta#phi(jet_{1},jet_{2})")

  return plotList


def getPlotListKinematicDistributions( tree, sampleName, plotList ):
  # collecting all the chi hists in one mass bin loop
  if args.chiVariables:
      chiMassBinning = plotUtils.getChiMassBins()
      for i in range( 0,len(chiMassBinning) ):
          if chiMassBinning[i] < 2250: continue
          if i==len(chiMassBinning)-1 :
              massLabel = "mjj_underflow"
          else :
              minM = chiMassBinning[i]
              maxM = chiMassBinning[i+1]
              massLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))

          #standard (should be in standard analysis)
          plotList['chi_for_'+massLabel] = getChiHist("chi_for_"+massLabel, sampleName)
          #pile-up checks
          plotList['average_chi_for_'+massLabel+'_vs_mu']   = makeTProfileArray("average_chi_for_"+massLabel+"_vs_mu", sampleName, plotUtils.getMuBins(), "lumi block")
          plotList['NPV__chi_for_'+massLabel] = makeTH2DxyArray("NPV__chi_for_"+massLabel, sampleName, plotUtils.getNPVBins(), plotUtils.getChiBins(), "NPV", "#chi")
          plotList['mu__chi_for_'+massLabel] = makeTH2DxyArray("mu__chi_for_"+massLabel, sampleName, plotUtils.getMuBins(), plotUtils.getChiBins(), "#mu", "#chi")
          #trigger checks
          plotList['chi_for_'+massLabel+'_for_L1_J75'] = getChiHist("chi_for_"+massLabel+"_for_L1_J75", sampleName)
          plotList['chi_for_'+massLabel+'_for_L1_J100'] = getChiHist("chi_for_"+massLabel+"_for_L1_J100", sampleName)
          plotList['chi_for_'+massLabel+'_for_HLT_j360'] = getChiHist("chi_for_"+massLabel+"_for_HLT_j360", sampleName)
          plotList['chi_for_'+massLabel+'_for_HLT_j400'] = getChiHist("chi_for_"+massLabel+"_for_HLT_j400", sampleName)
          #run and event number checks (data only)
          if "data" in sampleName:
              runBins = plotUtils.getRunBins("2015")
              numLBNbins = 15 # 1440 bins = every 1 day run every 60 sec
              plotList['average_chi_for_'+massLabel+'_vs_run']   = makeTProfileArray("average_chi_for_"+massLabel+"_vs_run", sampleName, runBins, "run number" )
              plotList['average_chi_for_'+massLabel+'_vs_lbn']   = makeTProfileArray("average_chi_for_"+massLabel+"_vs_lbn", sampleName, [x*10 for x in range(0,numLBNbins+1)], "lumi block")

          #make plots of asymmetries in chi bins
          plotList['ptasym12_for_'+massLabel] = makeTH1D("ptasym12_for_"+massLabel, sampleName, 100, -1, 1, "(p_{T,2} - p_{T,1})/(p_{T,2} + p_{T,1})")
          plotList['ptasym23_for_'+massLabel] = makeTH1D("ptasym23_for_"+massLabel, sampleName, 100, -1, 1, "(p_{T,2} - p_{T,3})/(p_{T,2} + p_{T,3})")
          plotList['ptasym13_for_'+massLabel] = makeTH1D("ptasym13_for_"+massLabel, sampleName, 100, -1, 1, "(p_{T,1} - p_{T,3})/(p_{T,1} + p_{T,3})")

          plotList['first_jet_eta__second_jet_eta_for_'+massLabel]   = makeTH2DxyArray("first_jet_eta__second_jet_eta_for_"+massLabel, sampleName, plotUtils.getJetEtaBins(), plotUtils.getJetEtaBins(), "first jet eta", "second jet eta")

      #make plots for eta map of leading/subleading
      plotList['average_ptasym12_for_first_jet_eta__second_jet_eta'] = makeTProfile2DArray("average_ptasym12_for_first_jet_eta__second_jet_eta", sampleName, plotUtils.getJetEtaBins(), plotUtils.getJetEtaBins(), "first jet eta", "second jet eta")
      plotList['average_ptasym23_for_first_jet_eta__second_jet_eta'] = makeTProfile2DArray("average_ptasym23_for_first_jet_eta__second_jet_eta", sampleName, plotUtils.getJetEtaBins(), plotUtils.getJetEtaBins(), "first jet eta", "second jet eta")
      plotList['average_ptasym13_for_first_jet_eta__second_jet_eta'] = makeTProfile2DArray("average_ptasym13_for_first_jet_eta__second_jet_eta", sampleName, plotUtils.getJetEtaBins(), plotUtils.getJetEtaBins(), "first jet eta", "second jet eta")

  if "data" in sampleName:
      runBins = plotUtils.getRunBins("2015")
      minMass = args.cut_mjjMin
      maxMass = args.cut_mjjMax
      numLBNbins = 15 # 1440 bins = every 1 day run every 60 sec
      plotList['average_mjj_vs_run']   = makeTProfileArray("average_mjj_vs_run", sampleName, runBins, "run number" )
      plotList['average_mjj_vs_lbn']   = makeTProfileArray("average_mjj_vs_lbn", sampleName, [x*10 for x in range(0,numLBNbins+1)], "lumi block")
      #plotList['average_chi_vs_run']   = makeTProfileArray("average_chi_vs_run", sampleName, runBins, "run number" )
      #plotList['average_chi_vs_lbn']   = makeTProfileArray("average_chi_vs_lbn", sampleName, [x*10 for x in range(0,numLBNbins+1)], "lumi block")
  # average vs x plots that also make sense for mc
  plotList['average_mjj_vs_mu']   = makeTProfileArray("average_mjj_vs_mu", sampleName, plotUtils.getMuBins(), "lumi block")
#plotList['average_chi_vs_mu']   = makeTProfileArray("average_chi_vs_mu", sampleName, plotUtils.getMuBins(), "lumi block")

  plotList['mjj_for_yStarLt0p6'] = getMassHist("mjj_for_yStarLt0p6", sampleName)
  plotList['mjj_for_yStarGt0p6'] = getMassHist("mjj_for_yStarGt0p6", sampleName)

  # m13 and m23 with binning that (should be) above 1 TeV to avoid unblinding the future >=3 jet analyses
  plotList['m23'] = getMassHist("m23", sampleName) #, "UnprescaledMass13TeV")
  plotList['m13'] = getMassHist("m13", sampleName) #, "UnprescaledMass13TeV")

  # pt asymmetries
  plotList['ptasym12'] = makeTH1D("ptasym12", sampleName, 100, -1, 1, "(p_{T,2} - p_{T,1})/(p_{T,2} + p_{T,1})")
  plotList['ptasym13'] = makeTH1D("ptasym13", sampleName, 100, -1, 1, "(p_{T,3} - p_{T,1})/(p_{T,3} + p_{T,1})")
  plotList['ptasym23'] = makeTH1D("ptasym23", sampleName, 100, -1, 1, "(p_{T,3} - p_{T,2})/(p_{T,3} + p_{T,2})")
  plotList['jetDEta12'] = makeTH1D("jetDEta12", sampleName, 100, -10, 10, "#eta_{2} - #eta_{1}")
  plotList['jetDEta13'] = makeTH1D("jetDEta13", sampleName, 100, -10, 10, "#eta_{3} - #eta_{1}")
  plotList['jetDEta23'] = makeTH1D("jetDEta23", sampleName, 100, -10, 10, "#eta_{3} - #eta_{2}")
  plotList['jetDPhi12'] = makeTH1D("jetDPhi12", sampleName, 100, -3.2, 3.2, "#phi_{2} - #phi_{1}")
  plotList['jetDPhi13'] = makeTH1D("jetDPhi13", sampleName, 100, -3.2, 3.2, "#phi_{3} - #phi_{1}")
  plotList['jetDPhi23'] = makeTH1D("jetDPhi23", sampleName, 100, -3.2, 3.2, "#phi_{3} - #phi_{2}")
  plotList['jetDR12'] = makeTH1D("jetDR12", sampleName, 100, 0, 10, "#Delta R_{1,2}")
  plotList['jetDR13'] = makeTH1D("jetDR13", sampleName, 100, 0, 10, "#Delta R_{1,3}")
  plotList['jetDR23'] = makeTH1D("jetDR23", sampleName, 100, 0, 10, "#Delta R_{2,3}")


  plotList['first_jet_pt']  = getJetPtHist("first_jet_pt", sampleName)
  plotList['first_jet_pt'].GetXaxis().SetTitle("Jet 1 p_{T} [GeV]")
  plotList['first_jet_eta'] = getJetEtaHist("first_jet_eta", sampleName)
  plotList['first_jet_eta'].GetXaxis().SetTitle("Jet 1 #eta")
  plotList['first_jet_phi'] = getJetPhiHist("first_jet_phi", sampleName)
  plotList['first_jet_phi'].GetXaxis().SetTitle("Jet 1 #phi")
  plotList['first_jet_m'] = getJetMassHist("first_jet_m", sampleName)
  plotList['first_jet_m'].GetXaxis().SetTitle("Jet 1 mass [GeV]")
  plotList['first_jet_en'] = getJetEnHist("first_jet_en", sampleName)
  plotList['first_jet_en'].GetXaxis().SetTitle("Jet 1 E [GeV]")
  plotList['first_jet_eta__first_jet_phi'] = getJetEtaPhiHist("first_jet_eta__first_jet_phi", sampleName)
  plotList['first_jet_eta__first_jet_pt'] = makeTH2DyArray("first_jet_eta__first_jet_pt", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 1 #eta", "Jet 1 p_{T} [GeV]")
  plotList['first_jet_emScaleEta__first_jet_pt'] = makeTH2DyArray("first_jet_emScaleEta__first_jet_pt", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 1 detector #eta", "Jet 1 p_{T} [GeV]")
  plotList['first_jet_emScaleEta__first_jet_E'] = makeTH2DyArray("first_jet_emScaleEta__first_jet_E", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 1 #eta", "Jet 1 E [GeV]" )
  plotList['first_jet_eta__mjj'] = makeTH2DxyArray("first_jet_eta__mjj", sampleName, plotUtils.getJetEtaBins(), plotUtils.getMassBins(), "Jet 1 #eta", "Dijet mass [GeV]" )
  plotList['first_jet_phi__mjj'] = makeTH2DxyArray("first_jet_phi__mjj", sampleName, plotUtils.getJetPhiBins(), plotUtils.getMassBins(), "Jet 1 #phi", "Dijet mass [GeV]" )
  plotList['first_jet_pt__mjj'] = makeTH2DxyArray("first_jet_pt__mjj", sampleName, plotUtils.getJetPtBins() , plotUtils.getMassBins(), "Jet 1 p_{T}", "Dijet mass [GeV]" )

  plotList['second_jet_pt'] = getJetPtHist("second_jet_pt", sampleName)
  plotList['second_jet_eta'] = getJetEtaHist("second_jet_eta", sampleName)
  plotList['second_jet_phi'] = getJetPhiHist("second_jet_phi", sampleName)
  plotList['second_jet_m'] = getJetMassHist("second_jet_m", sampleName)
  plotList['second_jet_en'] = getJetEnHist("second_jet_en", sampleName)

  plotList['second_jet_pt'].GetXaxis().SetTitle("Jet 2 p_{T} [GeV]")
  plotList['second_jet_eta'].GetXaxis().SetTitle("Jet 2 #eta")
  plotList['second_jet_phi'].GetXaxis().SetTitle("Jet 2 #phi")
  plotList['second_jet_m'].GetXaxis().SetTitle("Jet 2 mass [GeV]")
  plotList['second_jet_en'].GetXaxis().SetTitle("Jet 2 E [GeV]")

  plotList['second_jet_eta__second_jet_phi'] = getJetEtaPhiHist("second_jet_eta__second_jet_phi", sampleName)
  plotList['second_jet_eta__second_jet_pt'] = makeTH2DyArray("second_jet_eta__second_jet_pt", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 2 #eta", "Jet 2 p_{T} [GeV]")
  plotList['second_jet_emScaleEta__second_jet_pt'] = makeTH2DyArray("second_jet_emScaleEta__second_jet_pt", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 2 detector #eta", "Jet 2 p_{T} [GeV]")
  plotList['second_jet_emScaleEta__second_jet_E'] = makeTH2DyArray("second_jet_emScaleEta__second_jet_E", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 2 #eta", "Jet 2 E [GeV]" )
  plotList['second_jet_eta__mjj'] = makeTH2DxyArray("second_jet_eta__mjj", sampleName, plotUtils.getJetEtaBins(), plotUtils.getMassBins(), "Jet 2 #eta", "Dijet mass [GeV]" )
  plotList['second_jet_phi__mjj'] = makeTH2DxyArray("second_jet_phi__mjj", sampleName, plotUtils.getJetPhiBins(), plotUtils.getMassBins(), "Jet 2 #phi", "Dijet mass [GeV]" )
  plotList['second_jet_pt__mjj'] = makeTH2DxyArray("second_jet_pt__mjj", sampleName, plotUtils.getJetPtBins(), plotUtils.getMassBins(), "Jet 2 p_{T}", "Dijet mass [GeV]" )

  plotList['third_jet_pt'] = getJetPtHist("third_jet_pt", sampleName)
  plotList['third_jet_eta'] = getJetEtaHist("third_jet_eta", sampleName)
  plotList['third_jet_phi'] = getJetPhiHist("third_jet_phi", sampleName)
  plotList['third_jet_m'] = getJetMassHist("third_jet_m", sampleName)
  plotList['third_jet_en'] = getJetEnHist("third_jet_en", sampleName)

  plotList['third_jet_pt'].GetXaxis().SetTitle("Jet 3 p_{T} [GeV]")
  plotList['third_jet_eta'].GetXaxis().SetTitle("Jet 3 #eta")
  plotList['third_jet_phi'].GetXaxis().SetTitle("Jet 3 #phi")
  plotList['third_jet_m'].GetXaxis().SetTitle("Jet 3 mass [GeV]")
  plotList['third_jet_en'].GetXaxis().SetTitle("Jet 3 E [GeV]")

  plotList['third_jet_eta__third_jet_phi'] = getJetEtaPhiHist("third_jet_eta__third_jet_phi", sampleName)
  plotList['third_jet_eta__third_jet_pt'] = makeTH2DyArray("third_jet_eta__third_jet_pt", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 3 #eta", "Jet 3 p_{T} [GeV]")
  plotList['third_jet_emScaleEta__third_jet_pt'] = makeTH2DyArray("third_jet_emScaleEta__third_jet_pt", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 3 detector #eta", "Jet 3 p_{T} [GeV]")
  plotList['third_jet_emScaleEta__third_jet_E'] = makeTH2DyArray("third_jet_emScaleEta__third_jet_E", sampleName, 120, -4.8, 4.8, plotUtils.getJetPtBins(), "Jet 3 #eta", "Jet 3 E [GeV]" )
  plotList['third_jet_eta__mjj'] = makeTH2DxyArray("third_jet_eta__mjj", sampleName, plotUtils.getJetEtaBins(), plotUtils.getMassBins(), "Jet 2 #eta", "Dijet mass [GeV]" )
  plotList['third_jet_phi__mjj'] = makeTH2DxyArray("third_jet_phi__mjj", sampleName, plotUtils.getJetPhiBins(), plotUtils.getMassBins(), "Jet 2 #phi", "Dijet mass [GeV]" )
  plotList['third_jet_pt__mjj'] = makeTH2DxyArray("third_jet_pt__mjj", sampleName, plotUtils.getJetPtBins(), plotUtils.getMassBins(), "Jet 3 p_{T}", "Dijet mass [GeV]" )


  plotList['first_jet_pt__second_jet_pt'] = makeTH2DxyArray('first_jet_pt__second_jet_pt', sampleName, plotUtils.getJetPtBins(), plotUtils.getJetPtBins(), "Jet 1 p_{T} [GeV]", "Jet 2 p_{T} [GeV]")
  plotList['first_jet_pt__third_jet_pt'] = makeTH2DxyArray('first_jet_pt__third_jet_pt', sampleName, plotUtils.getJetPtBins(), plotUtils.getJetPtBins(), "Jet 1 p_{T} [GeV]", "Jet 3 p_{T} [GeV]")
  plotList['second_jet_pt__third_jet_pt'] = makeTH2DxyArray('second_jet_pt__third_jet_pt', sampleName, plotUtils.getJetPtBins(), plotUtils.getJetPtBins(), "Jet 2 p_{T} [GeV]", "Jet 3 p_{T} [GeV]")

  # kinematics binned by trigger
  plotList['mjj_for_L1_J75'] = getMassHist("mjj_for_L1_J75", sampleName)
  plotList['mjj_for_L1_J100'] = getMassHist("mjj_for_L1_J100", sampleName)
  plotList['mjj_for_HLT_j360'] = getMassHist("mjj_for_HLT_j360", sampleName)
  plotList['mjj_for_HLT_j400'] = getMassHist("mjj_for_HLT_j400", sampleName)
#  plotList['chi_for_L1_J75'] = getChiHist("chi_for_L1_J75", sampleName)
#  plotList['chi_for_L1_J100'] = getChiHist("chi_for_L1_J100", sampleName)
#  plotList['chi_for_HLT_j360'] = getChiHist("chi_for_HLT_j360", sampleName)
#  plotList['chi_for_HLT_j400'] = getChiHist("chi_for_HLT_j400", sampleName)
  plotList['jet_pt_for_L1_J75'] = getJetPtHist("jet_pt_for_L1_J75", sampleName)
  plotList['jet_pt_for_L1_J100'] = getJetPtHist("jet_pt_for_L1_J100", sampleName)
  plotList['jet_pt_for_HLT_j360'] = getJetPtHist("jet_pt_for_HLT_j360", sampleName)
  plotList['jet_pt_for_HLT_j400'] = getJetPtHist("jet_pt_for_HLT_j400", sampleName)


  # pileup
  plotList['NPV__mjj'] = makeTH2DxyArray("NPV__mjj", sampleName,plotUtils.getNPVBins(),plotUtils.getMassBins(), "NPV", "m_{jj}")
  plotList['mu__mjj'] = makeTH2DxyArray("mu__mjj", sampleName, plotUtils.getMuBins(), plotUtils.getMassBins(), "#mu", "m_{jj}")
#  plotList['NPV__chi'] = makeTH2DxyArray("NPV__chi", sampleName, plotUtils.getNPVBins(), plotUtils.getChiBins(), "NPV", "#chi")
#  plotList['mu__chi'] = makeTH2DxyArray("mu__chi", sampleName, plotUtils.getMuBins(), plotUtils.getChiBins(), "#mu", "#chi")
  plotList['njets50GeV'] = makeTH1D("njets50GeV", sampleName, 10, 0, 10, "n_{jets}>50 GeV")
  plotList['NPV__njets50GeV'] = makeTH2DxArray("NPV__njets50GeV", sampleName, plotUtils.getNPVBins(), 10, 0, 10, "NPV", "n_{jets}>50 GeV")
  plotList['mu__njets50GeV'] = makeTH2DxArray("mu__njets50GeV", sampleName, plotUtils.getMuBins(), 10, 0, 10, "#mu", "n_{jets}>50 GeV")

  plotList['NPV__first_jet_pt'] = makeTH2DxyArray("NPV__first_jet_pt", sampleName, plotUtils.getNPVBins(), plotUtils.getJetPtBins(), "NPV", "First Jet p_{T}")
  plotList['mu__first_jet_pt'] = makeTH2DxyArray("mu__first_jet_pt", sampleName, plotUtils.getMuBins(), plotUtils.getJetPtBins(), "#mu", "First Jet p_{T}")
  plotList['NPV__second_jet_pt'] = makeTH2DxyArray("NPV__second_jet_pt", sampleName, plotUtils.getNPVBins(), plotUtils.getJetPtBins(), "NPV", "Second Jet p_{T}")
  plotList['mu__second_jet_pt'] = makeTH2DxyArray("mu__second_jet_pt", sampleName, plotUtils.getMuBins(), plotUtils.getJetPtBins(), "#mu", "Second Jet p_{T}")
  plotList['NPV__third_jet_pt'] = makeTH2DxyArray("NPV__third_jet_pt", sampleName, plotUtils.getNPVBins(), plotUtils.getJetPtBins(), "NPV", "Third Jet p_{T}")
  plotList['mu__third_jet_pt'] = makeTH2DxyArray("mu__third_jet_pt", sampleName, plotUtils.getMuBins(), plotUtils.getJetPtBins(), "#mu", "Third Jet p_{T}")

#  etaBinning = plotUtils.getJetEtaBins()
#  etaBinning = [0,0.8,1.2,1.8,2.1,2.8,3.1,4.9]
  #etaBinning = [0,0.8,1.2,1.3,1.6,2.1,2.8,3.1,4.9]
  etaBinning = plotUtils.getJetAbsEtaBins()

  for i in range( 0,len(etaBinning) -1 ):
      minEta = etaBinning[i]
      maxEta = etaBinning[i+1]
      etaLabel = "eta_"+str(minEta)+"_"+str(maxEta)
      etaLabel = etaLabel.replace(".","p")

      plotList['mjj_for_first_jet_'+etaLabel] = getMassHist("mjj_LJet_"+etaLabel, sampleName)
      plotList['mjj_for_second_jet_'+etaLabel] = getMassHist("mjj_NLJet_"+etaLabel, sampleName)

      plotList['jet_pt_'+etaLabel]  = getJetPtHist('jet_pt_'+etaLabel, sampleName)

      plotList['jet_pt_'+etaLabel].GetXaxis().SetTitle("Jet p_{T} det" + etaLabel)
      plotList['jet_emScalePt_'+etaLabel]  = getJetPtHist('jet_emScalePt_'+etaLabel, sampleName)
      plotList['jet_emScalePt_'+etaLabel].GetXaxis().SetTitle("Jet EM Scale p_{T} det" + etaLabel)
      plotList['jet_phi_'+etaLabel] = makeTH1D('jet_phi_'+etaLabel, sampleName, 120, -3.2, 3.2, "Jet #phi det"+etaLabel)
      if "JET" not in args.tree:  plotList['jet_NumTrkPt500PV_'+etaLabel] = makeTH1D("jet_NumTrkPt500PV_"+etaLabel, sampleName, 40, -0.5, 39.5, "N Track (p_{T}^{trk}>500 MeV from PV) det"+etaLabel)
      if "JET" not in args.tree:  plotList['jet_SumTrkPt500PV_'+etaLabel] = getJetPtHist("jet_SumTrkPt500PV_"+etaLabel, sampleName)
      if "JET" not in args.tree:  plotList['jet_SumTrkPt500PV_'+etaLabel].GetXaxis().SetTitle("jet Sum Trk p_{T} (p_{T}^{trk}>500 MeV from PV), det"+etaLabel)
      if "JET" not in args.tree:  plotList['jet_fracSumPtTrkPt500PV_'+etaLabel] = makeTH1D("jet_fracSumPtTrkPt500PV_"+etaLabel, sampleName, 50, 0., 2.,"jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>500 MeV from PV det"+etaLabel)
      if "JET" not in args.tree:  plotList['jet_TrackWidthPt500PV_'+etaLabel] = makeTH1D("jet_TrackWidthPt500PV_"+etaLabel, sampleName, 60, -1.1, 0.5, "Track Width, p_{T}^{trk}>500 MeV from PV det"+etaLabel)

      plotList['jet_pt__fracSumPtTrkPt500PV_'+etaLabel] = makeTH2DxArray("jet_pt__fracSumPtTrk500PV_"+etaLabel, sampleName, plotUtils.getJetPtBins(), 100, 0., 3., "jet p_{T} [GeV] det"+etaLabel, "jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>500 MeV from PV")

  pTBinning = plotUtils.getPtBinsForJets()
  for i in range( 0,len(pTBinning) -1 ):
      pTLabel = "pT_"+str(pTBinning[i])+"_"+str(pTBinning[i+1])
      pTLabel.replace(".","p")
      plotList['jet_eta_'+pTLabel]  = makeTH1D("jet_eta_"+pTLabel, sampleName, 120, -4.8, 4.8,   "Jet #eta "+pTLabel)
      plotList['first_jet_eta_'+pTLabel]  = makeTH1D("first_jet_eta_"+pTLabel, sampleName, 120, -4.8, 4.8,   "Jet #eta "+pTLabel)
      plotList['second_jet_eta_'+pTLabel]  = makeTH1D("second_jet_eta_"+pTLabel, sampleName, 120, -4.8, 4.8,   "Jet #eta "+pTLabel)
      plotList['jet_emScaleEta_'+pTLabel] = makeTH1D("jet_emScaleEta_"+pTLabel, sampleName, 120, -4.8, 4.8, "detector #eta "+pTLabel)
      plotList['jet_eta__fracSumPtTrkPt500PV_'+pTLabel] = makeTH2DxArray("jet_eta__fracSumPtTrk500PV_"+pTLabel, sampleName, plotUtils.getJetEtaBins(), 100, 0., 3., "jet #eta " + pTLabel, "jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>500 MeV from PV")

      plotList['first_jet_phi_'+pTLabel] = getJetPhiHist("first_jet_phi_"+pTLabel, sampleName)
      plotList['second_jet_phi_'+pTLabel] = getJetPhiHist("second_jet_phi_"+pTLabel, sampleName)
      plotList['jet_phi_'+pTLabel] = getJetPhiHist("jet_phi_"+pTLabel, sampleName)
      if "JET" not in args.tree:  plotList['jet_fracSumPtTrkPt500PV_'+pTLabel] = makeTH1D("jet_fracSumPtTrkPt500PV_"+pTLabel, sampleName, 50, 0., 2.,"jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>500 MeV from PV det"+pTLabel)

  plotList['HT2_first_jet_pt_plus_second_jet_pt'] = getJetPtHist("HT2_first_jet_pt_plus_second_jet_pt", sampleName)

  return plotList


def getPlotListBasicInfo( tree, sampleName, plotList ):
    plotList['averageInteractionsPerCrossing'] = makeTH1D("averageInteractionsPerCrossing", sampleName, 50, -0.6,49.4, "<#mu>")
    plotList['actualInteractionsPerCrossing'] = makeTH1D("actualInteractionsPerCrossing", sampleName, 50, -0.5, 49.5, "actual #mu")

    return plotList

def getPlotListChiVariables( tree, sampleName, plotList ):
#plotList['chi']     = getChiHist("chi", sampleName)
  if not args.kinematicDistributions:
    chiMassBinning = plotUtils.getChiMassBins()
    for i in range( 0,len(chiMassBinning) ):
      if i==len(chiMassBinning)-1 :
          massLabel = "mjj_underflow"
      else :
          minM = chiMassBinning[i]
          maxM = chiMassBinning[i+1]
          massLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))
      plotList['chi_for_'+massLabel] =  getChiHist("chi_for_"+massLabel, sampleName)
  return plotList

def getPlotListTypicalVariables( tree, sampleName, plotList ):

  #These histograms have a different name but the same content as the ones in kinematicDistributions: would need harmonizing
#  if not args.kinematicDistributions:
#    plotList['jet1_pt']   = getJetPtHist("jet1_pt", sampleName)
#    plotList['jet1_pt'].GetXaxis().SetTitle("Jet_{1} p_{T} [GeV]")
#    plotList['jet2_pt']   = getJetPtHist("jet2_pt", sampleName)
#    plotList['jet2_pt'].GetXaxis().SetTitle("Jet_{2} p_{T} [GeV]")
#    plotList['jet1_eta']  = makeTH1D("jet1_eta", sampleName, 120, -4.8, 4.8,   "Jet_{1} #eta"    )
#    plotList['jet2_eta']  = makeTH1D("jet2_eta", sampleName, 120, -4.8, 4.8,   "Jet_{2} #eta"    )

  #plotList['a_mjj']   = getMassHistTeV("mjj", sampleName)
  plotList['a_yStar'] = makeTH1D("yStar", sampleName, 120, -1.8, 1.8, "y*")
  plotList['a_njets'] = makeTH1D("njets", sampleName, 20, -0.5, 19.5, "Jet Multiplicity")
  plotList['njetsPass'] = makeTH1D("njetsPass", sampleName, 20, -0.5, 19.5, "Jet Multiplicity Passing Selection")
  plotList['a_deltaPhi'] = makeTH1D("deltaPhi", sampleName, 60, 0, 3.2, "#Delta#phi(jet_{1},jet_{2})")

  return plotList

def getPlotListFlavTag( tree, sampleName, plotList ):
  return plotList

def getPlotListTruth( tree, sampleName, plotList ):
  plotList['jet_truth_E'] = makeTH1D("jet_truth_E", sampleName, 240, 0, 7000, "Truth Jet Energy [GeV]")
  plotList['jet_truth_pt'] = makeTH1D("jet_truth_pt", sampleName, 240, 0, 6000, "Truth Jet p_{T} [GeV]")
  plotList['jet_truth_phi'] = makeTH1D("jet_truth_phi", sampleName, 120, -3.2,3.2, "Truth Jet #phi")
  plotList['jet_truth_eta'] = makeTH1D("jet_truth_eta", sampleName, 120, -4.8,4.8, "Truth Jet #eta")
  plotList['jet_GhostTruthAssociationFraction'] = makeTH1D("jet_GhostTruthAssociationFraction", sampleName, 50, -0.1, 1.1, "Fraction of truth jet p_{T} associated with jet p_{T}")
  plotList['jet_PartonTruthLabelID'] = makeTH1D("jet_PartonTruthLabelID", sampleName, 26, -10, 25, "Parton Truth Label ID")
  plotList['jet_ConeTruthLabelID'] = makeTH1D("jet_ConeTruthLabelID", sampleName, 21, 0, 20, "Cone Truth Label ID")
  plotList['jet_TruthLabelDeltaR_B'] = makeTH1D("jet_TruthLabelDeltaR_B", sampleName, 60, 0, 0.6, "B-Jet truth label #DeltaR values")
  plotList['jet_TruthLabelDeltaR_C'] = makeTH1D("jet_TruthLabelDeltaR_C", sampleName, 60, 0, 0.6, "C-Jet truth label #DeltaR values")
  plotList['jet_TruthLabelDeltaR_T'] = makeTH1D("jet_TruthLabelDeltaR_T", sampleName, 60, 0, 0.6, "Tau-Jet truth label #DeltaR values")
  return plotList

#def getPlotListTruthDetail( tree, sampleName, plotList ):
#  return plotList

def getPlotListMassPartons( tree, sampleName, plotList ):
# add plots for dijet mass looking at all possible
# combinations of incoming and outgoing partons
  partons = [ "q", "g" ]
  if args.do_massPartonPlots:
    for ipart in partons:
      for jpart in partons:
        incoming = ipart + jpart
        for upart in partons:
          for vpart in partons:
            outgoing = upart + vpart
            varName = "_of_"+ incoming + "To" + outgoing
            for k in range(0,3):
              if k==0:
                if incoming in ["qg","gq"]: incomingLocal = "qg"
                else: incomingLocal = incoming
                if outgoing in ["qg","gq"]: outgoingLocal = "qg"
                else: outgoingLocal = outgoing
                varName = "_of_" + incomingLocal + "To" + outgoingLocal
              elif k==1:
                if incoming in ["qg","gq"]: incomingLocal = "qg"
                else: incomingLocal = incoming
                varName = "_incoming_of_" + incomingLocal
              elif k==2:
                if outgoing in ["qg","gq"]: outgoingLocal = "qg"
                else: outgoingLocal = outgoing
                varName = "_outgoing_of_" + outgoingLocal

              plotList["mjj" + varName]  =  getMassHist("mjj" + varName, sampleName)
              plotList["first_jet_pt" + varName]  =  getJetPtHist("first_jet_pt" + varName, sampleName)
              plotList["second_jet_pt" + varName]  =  getJetPtHist("second_jet_pt" + varName, sampleName)
              plotList["njets50GeV" + varName]  =  makeTH1D("njets50GeV"+varName, sampleName, 10, 0, 10, "n_{jets}>50 GeV")
              plotList["jetDPhi12" + varName]  =  makeTH1D("jetDPhi12"+varName, sampleName, 100, -3.2, 3.2, "#phi_{2} - #phi_{1}")
              plotList["yStar" + varName]  =  makeTH1D("yStar" + varName, sampleName, 120, -1.8, 1.8, "y*")
              plotList["yBoost" + varName]  =  makeTH1D("yBoost" + varName, sampleName, 120, -1.8, 1.8, "yBoost")
              chiMassBinning = plotUtils.getChiMassBins()
              for i in range( 0,len(chiMassBinning) ):
                if chiMassBinning[i] < 2250: continue
                if i==len(chiMassBinning)-1 :
                    massLabel = "mjj_underflow"
                else :
                    minM = chiMassBinning[i]
                    maxM = chiMassBinning[i+1]
                    massLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))
                plotList['chi_for_'+massLabel+varName] =  getChiHist("chi_for_"+massLabel+varName, sampleName)

  del partons
  return plotList

def getPlotListJetConstituents( tree, sampleName, plotList ):
  plotList['jet_numConstituents'] = makeTH1D("jet_numConstituents", sampleName, 50, 0, 50, "Number of Constituents")

  endspt = plotUtils.getHistExtrema(tree,"jet_pt","min")
  plotList['jet_pt__numConstituents'] = makeTH2D("jet_pt__numConstituents", sampleName, args.nBins, endspt[0], endspt[1], 50, 0, 50, "Jet p_{T} [GeV]", "Number of Constituents")
  plotList['jet_eta__numConstituents'] = makeTH2D("jet_eta__numConstituents", sampleName, 120, -4, 4, 50, 0, 50, "Jet #eta", "Number of Constituents")
  plotList['jet_phi__numConstituents'] = makeTH2D("jet_phi__numConstituents", sampleName, 120, -3.2, 3.2, 50, 0, 50, "Jet #phi", "Number of Constituents")
  return plotList

def getPlotListJetConstituentsAll( tree, sampleName, plotList ):
  plotList['jet_fClus'] = makeTH1D("jet_fClus", sampleName, 50, 0, 1, "f_{clus}", "Number of Events")
  plotList['jet_constituent_e'] = makeTH1D("jet_constituent_e", sampleName, 100, 0.0, 70.0, "Jet Constituents E [GeV]")
  plotList['jet_constituent_pt'] = makeTH1D("jet_constituent_pt", sampleName, 100, 0.0, 70.0, "Jet Constituents p_{T} [GeV]")
  plotList['jet_constituent_eta'] = makeTH1D("jet_constituent_eta", sampleName, 120, -4.8, 4.8, "Jet Constituents #eta")
  plotList['jet_constituent_phi'] = makeTH1D("jet_constituent_phi", sampleName, 120, -3.2, 3.2, "Jet Constituents #phi")
  plotList['jet_numConstituents'] = makeTH1D("jet_numConstituents", sampleName, 50, 0, 50, "Number of Constituents")
  plotList['jet_constituent_Et'] = makeTH1D("jet_constituent_Et", sampleName, 100, 0.0, 50.0, "Jet Constituents Et [GeV]")
  plotList['jet_constituent_M'] = makeTH1D("jet_constituent_M", sampleName, 100, 0.0, 20.0, "Jet Constituents M [GeV]")

  endspt = plotUtils.getHistExtrema(tree,"jet_pt","min")
  plotList['jet_pt__numConstituents'] = makeTH2D("jet_pt__numConstituents", sampleName, args.nBins, endspt[0], endspt[1], 50, 0, 50, "Jet p_{T} [GeV]", "Number of Constituents")
  plotList['jet_eta__numConstituents'] = makeTH2D("jet_eta__numConstituents", sampleName, 120,-4,4, 50, 0, 50, "Jet #eta", "Number of Constituents")
  plotList['jet_phi__numConstituents'] = makeTH2D("jet_phi__numConstituents", sampleName, 120, -3.2, 3.2, 50, 0, 50, "Jet #phi", "Number of Constituents")
  return plotList

def getPlotListCaloQuant( tree, sampleName, plotList ):
  plotList['jet__Width'] = makeTH1D("jet__Width", sampleName, 30, 0, 0.3, "Jet Width")
  plotList['jet_CentroidR'] = makeTH1D("jet_CentroidR", sampleName, 120, 0., 6000., "Jet Centroid R")
  plotList['jet_minDeltaR'] = makeTH1D("jet_minDeltaR", sampleName, 60, 0, 6, "min #DeltaR(jet,jet)")
  #plotList['jet_pt__width'] = makeTH2DxArray("jet_pt__width", sampleName, [0,30,40,60,80,110,170,170, 200,300], 50,0,0.5, "Jet p_{T} [GeV]", "Jet Width")

  endspt = plotUtils.getHistExtrema(tree,"jet_pt","min")
  plotList['jet_pt__Width'] = makeTH2D("jet_pt__Width", sampleName, args.nBins, endspt[0], endspt[1], 30,0,0.3, "Jet p_{T} [GeV]", "Jet Width")
  plotList['jet_eta__Width'] = makeTH2D("jet_eta__Width", sampleName, 80,-4,4, 30,0,0.3, "Jet #eta", "Jet Width")
  plotList['jet_pt__CentroidR'] = makeTH2D("jet_pt__CentroidR", sampleName, args.nBins, endspt[0], endspt[1], 120, 0., 6000., "Jet p_{T} [GeV]", "Jet Centroid R")
  plotList['jet_eta__CentroidR'] = makeTH2D("jet_eta__CentroidR", sampleName, 80,-4,4, 120, 0., 6000., "Jet #eta", "Jet Centroid R")
  plotList['jet_pt__minDeltaR'] = makeTH2D("jet_pt__minDeltaR", sampleName, args.nBins, endspt[0], endspt[1], 60, 0, 6., "Jet p_{T} [GeV]", "min #DeltaR(jet,jet)")
  plotList['jet_eta__minDeltaR'] = makeTH2D("jet_eta__minDeltaR", sampleName, 80,-4,4, 60, 0, 6., "Jet #eta", "min #DeltaR(jet,jet)")
  return plotList

def getPlotListTrackQuant( tree, sampleName, plotList ):
  plotList['jet_NumTrkPt500PV'] = makeTH1D("jet_NumTrkPt500PV", sampleName, 80, -0.5, 79.5, "Number of Tracks, p_{T}^{trk}>500 MeV from PV")



  QGTrackMassBinning = plotUtils.getQGMassBins()
  for i in range( 0,len(QGTrackMassBinning)-1 ):
    minM = QGTrackMassBinning[i]
    maxM = QGTrackMassBinning[i+1]
    QGmassLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))
    plotList['jet_NumTrkPt500PV_for_'+QGmassLabel] =  makeTH1D("jet_NumTrkPt500PV_for_"+QGmassLabel, sampleName, 80, -0.5, 79.5, "Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthqq_SumNumTrkPt500PV_for_'+QGmassLabel] =  makeTH1D("jet_truthqq_SumNumTrkPt500PV_for_"+QGmassLabel, sampleName, 160, -0.5, 159.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthqg_SumNumTrkPt500PV_for_'+QGmassLabel] =  makeTH1D("jet_truthqg_SumNumTrkPt500PV_for_"+QGmassLabel, sampleName, 160, -0.5, 159.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthgg_SumNumTrkPt500PV_for_'+QGmassLabel] =  makeTH1D("jet_truthgg_SumNumTrkPt500PV_for_"+QGmassLabel, sampleName, 160, -0.5, 159.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthqq_DiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH1D("jet_truthqq_DiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 80, -0.5, 79.5, "Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthqg_DiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH1D("jet_truthqg_DiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 80, -0.5, 79.5, "Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthgg_DiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH1D("jet_truthgg_DiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 80, -0.5, 79.5, "Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    
    plotList['jet_truthqq_SumDiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH2D("jet_truthqq_SumDiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 160, -0.5, 159.5, 80, -0.5, 79.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV","Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthqg_SumDiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH2D("jet_truthqg_SumDiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 160, -0.5, 159.5, 80, -0.5, 79.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV","Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthgg_SumDiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH2D("jet_truthgg_SumDiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 160, -0.5, 159.5, 80, -0.5, 79.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV","Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")

    plotList['jet_truthqq_CoarseSumDiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH2D("jet_truthqq_CoarseSumDiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 32, -0.5, 159.5, 16, -0.5, 79.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV","Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthqg_CoarseSumDiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH2D("jet_truthqg_CoarseSumDiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 32, -0.5, 159.5, 16, -0.5, 79.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV","Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")
    plotList['jet_truthgg_CoarseSumDiffNumTrkPt500PV_for_'+QGmassLabel] =  makeTH2D("jet_truthgg_CoarseSumDiffNumTrkPt500PV_for_"+QGmassLabel, sampleName, 32, -0.5, 159.5, 16, -0.5, 79.5, "Sum Number of Tracks, p_{T}^{trk}>500 MeV from PV","Diff Number of Tracks, p_{T}^{trk}>500 MeV from PV")


  plotList['Event_Likelihood_JJ'] = makeTH1D("Event_Likelihood_JJ",sampleName,100,-5.,10.,"QG Likelihood")
  plotList['Event_Likelihood_truthqq'] = makeTH1D("Event_Likelihood_truthqq",sampleName,100,-5.,10.,"QG Likelihood")
  plotList['Event_Likelihood_truthqg'] = makeTH1D("Event_Likelihood_truthqg",sampleName,100,-5.,10.,"QG Likelihood")
  plotList['Event_Likelihood_truthgg'] = makeTH1D("Event_Likelihood_truthgg",sampleName,100,-5.,10.,"QG Likelihood")


    #getChiHist("chi_for_"+massLabel, sampleName)
  

  plotList['jet_SumPtTrkPt500PV'] = getJetPtHist("jet_SumPtTrkPt500PV", sampleName)
  plotList['jet_SumPtTrkPt500PV'].GetXaxis().SetTitle("jet Sum Trk p_{T}, p_{T}^{trk}>500 MeV from PV")
  plotList['jet_fracSumPtTrkPt500PV'] = makeTH1D("jet_fracSumPtTrkPt500PV", sampleName, 100, 0., 4.,"jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>500 MeV from PV")
  plotList['jet_TrackWidthPt500PV'] = makeTH1D("jet_TrackWidthPt500PV", sampleName, 60, -1.1, 0.5, "Track Width, p_{T}^{trk}>500 MeV from PV")

  plotList['jet_NumTrkPt1000PV'] = makeTH1D("jet_NumTrkPt1000PV", sampleName, 80, -0.5, 79.5, "Number of Tracks, p_{T}^{trk}>1000 MeV from PV")
  plotList['jet_SumPtTrkPt1000PV'] = getJetPtHist("jet_SumPtTrkPt1000PV", sampleName)
  plotList['jet_SumPtTrkPt1000PV'].GetXaxis().SetTitle("jet Sum Trk p_{T}, p_{T}^{trk}>1000 MeV from PV")
  plotList['jet_fracSumPtTrkPt1000PV'] = makeTH1D("jet_fracSumPtTrkPt1000PV", sampleName, 100, 0., 4.,"jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>1000 MeV from PV" )
  plotList['jet_TrackWidthPt1000PV'] = makeTH1D("jet_TrackWidthPt1000PV", sampleName, 70, -1.1, 0.6, "Track Width, p_{T}^{trk}>1000 MeV from PV")

  plotList['jet_JVFPV']      = makeTH1D("jet_JVFPV",      sampleName, 100, -1.1, 1.1, "Jet JVF PV")
  plotList['jet_Jvt']        = makeTH1D("jet_Jvt",        sampleName, 100, -0.2, 1.1, "Jet Jvt")
  plotList['jet_JvtJvfcorr'] = makeTH1D("jet_JvtJvfcorr", sampleName, 100, -1.1, 1.1, "Jet JvtJvfcorr")
  plotList['jet_JvtRpt']     = makeTH1D("jet_JvtRpt",     sampleName, 100, -0.2, 2, "Jet JvtRpt")

  plotList['jet_pt__fracSumPtTrkPt500PV'] = makeTH2DxArray("jet_pt__fracSumPtTrk500PV", sampleName, plotUtils.getJetPtBins(), 100, 0., 3., "jet p_{T} [GeV]", "jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>500 MeV from PV")
#  plotList['jet_eta__fracSumPtTrkPt500PV'] = makeTH2DxArray("jet_eta__fracSumPtTrk500PV", sampleName, plotUtils.getJetEtaBins(), 100, 0., 3., "jet p_{T} [GeV]", "jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>500 MeV from PV")


  plotList['jet_pt__fracSumPtTrkPt1000PV'] = makeTH2D("jet_pt__fracSumPtTrk1000PV", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 50, 0., 2., "jet p_{T} [GeV]","jet Sum Trk p_{T}/jet p_{T}, p_{T}^{trk}>1000 MeV from PV")

  plotList['jet_pt__NumTrkPt500PV'] = makeTH2D("jet_pt__NumTrk500PV", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 80, -0.5, 79.5, "jet p_{T} [GeV]","jet Num Trk p_{T}, p_{T}^{trk}>500 MeV from PV")
  plotList['jet_pt__NumTrkPt1000PV'] = makeTH2D("jet_pt__NumTrk1000PV", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 80, -0.5, 79.5, "jet p_{T} [GeV]","jet Num Trk p_{T}, p_{T}^{trk}>1000 MeV from PV")

  plotList['jet_pt__fracSumPtTrkPt500PV'] = makeTH2D("jet_pt__fracSumPtTrkPt500PV", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 100, 0., 4., "jet p_{T} [GeV]", "#splitline{jet Sum Trk p_{T}/jet p_{T}}{p_{T}^{trk}>500 MeV from PV}")
  plotList['jet_pt__fracSumPtTrkPt1000PV'] = makeTH2D("jet_pt__fracSumPtTrkPt1000PV", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 100, 0., 4., "jet p_{T} [GeV]","#splitline{jet Sum Trk p_{T}/jet p_{T}}{p_{T}^{trk}>1000 MeV from PV}")

  plotList['jet_pt__TrackWidthPt500PV'] = makeTH2D("jet_pt__TrackWidthPt500PV", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 80, -0.1, 0.7, "jet p_{T} [GeV]","Track Width, p_{T}^{trk}>500 MeV from PV")
  plotList['jet_pt__TrackWidthPt1000PV'] = makeTH2D("jet_pt__TrackWidthPt1000PV", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 80, -0.1, 0.7, "jet p_{T} [GeV]","Track Width, p_{T}^{trk}>1000 MeV from PV")

  plotList['jet_pt__JVFPV'] = makeTH2D("jet_pt__JVFPV", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 100, -1.1, 1.1, "jet p_{T} [GeV]","Jet JVF PV")
  plotList['jet_pt__Jvt'] = makeTH2D("jet_pt__Jvt", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 100, -0.2, 1.1, "jet p_{T} [GeV]","Jet Jvt")
  plotList['jet_pt__JvtJvfcorr'] = makeTH2D("jet_pt__JvtJvfcorr", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 100, -1.1, 1.1, "jet p_{T} [GeV]","Jet JvtJvfcorr")
  plotList['jet_pt__JvtRpt'] = makeTH2D("jet_pt__JvtRpt", sampleName, args.nBins, plotUtils.getHistExtrema(tree,"jet_pt","min")[0], plotUtils.getHistExtrema(tree,"jet_pt","max")[1], 100, -0.2, 2, "jet p_{T} [GeV]","Jet JvtRpt")

  return plotList

def getPlotListTrackQuantAll( tree, sampleName, plotList ):
  plotList['jet_fTrack']     = makeTH1D("jet_fTrack",     sampleName, 50, 0., 1.2, "f_{track}")
  plotList['jet_trackdR']    = makeTH1D("jet_trackdR",    sampleName, 50, 0., 1.3,"#DeltaR(trk_{1},trk_{2})")
  #plotList['jet_pt__fTrack'] = makeTH2DxArray("jet_pt__fTrack", sampleName, [0,30,40,60,80,100], 50, 0., 2., "jet p_{T} [GeV]", "f_{Track}")
  #plotList['jet_pt__dRtrk'] = makeTH2DxArray("jet_pt__dRtrk",   sampleName, [20,30,40,60,80,100], 50, 0., 1.3, "jet p_{T} [GeV]", "#DeltaR(trk1, trk2)")
  plotList['jet_pt__fTrack'] = makeTH2D("jet_pt__fTrack", sampleName, 120, 20, 500, 50, 0., 1.2, "jet p_{T} [GeV]", "f_{Track}")
  plotList['jet_pt__dRtrk'] = makeTH2D("jet_pt__dRtrk",   sampleName, 120, 20, 500, 50, 0., 1.3, "jet p_{T} [GeV]", "#DeltaR(trk1, trk2)")
  return plotList


def getPlotListTrack( tree, sampleName, plotList ):
  plotList['jet_GhostTrack_qOverP'] = makeTH1D("jet_GhostTrack_qOverP", sampleName, 40, -2.0, 2.0, "track q/p, GM Jet p_{T}^{trk}>500 MeV from PV", "Number of Constituents")
  plotList['jet_GhostTrack_pt'] = makeTH1D("jet_GhostTrack_pt", sampleName, 40, 0.0, 20.0, "track p_{T}, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_d0'] = makeTH1D("jet_GhostTrack_d0", sampleName, 80, -10.0, 10.0, "d_{0}, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_z0'] = makeTH1D("jet_GhostTrack_z0", sampleName, 80, -20.0, 20.0, "z_{0} wrt PV, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nPixelHits'] = makeTH1D("jet_GhostTrack_nPixelHits", sampleName, 10 , 0, 10, "Pixel Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nPixelSharedHits'] = makeTH1D("jet_GhostTrack_nPixelSharedHits", sampleName, 6, 0, 6, "Pixel Shared Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nPixelSplitHits'] = makeTH1D("jet_GhostTrack_nPixelSplitHits", sampleName, 6, 0, 6, "Pixel Split Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nSCTHits'] = makeTH1D("jet_GhostTrack_nSCTHits", sampleName, 12, 0, 12, "SCT Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nTRTHits'] = makeTH1D("jet_GhostTrack_nTRTHits", sampleName, 45, 0, 45, "TRT Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nInnermostPixelLayerHits'] = makeTH1D("jet_GhostTrack_nInnermostPixelLayerHits", sampleName, 4, 0, 4, "Innermost Pixel Layer Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nInnermostPixelLayerSplitHits'] = makeTH1D("jet_GhostTrack_nInnermostPixelLayerSplitHits", sampleName, 4, 0, 4, "Innermost Pixel Layer Split Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nInnermostPixelLayerSharedHits'] = makeTH1D("jet_GhostTrack_nInnermostPixelLayerSharedHits", sampleName, 4, 0, 4, "Innermost Pixel Layer Shared Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nNextToInnermostPixelLayerHits'] = makeTH1D("jet_GhostTrack_nNextToInnermostPixelLayerHits", sampleName, 4, 0, 4, "NextToInnermost Pixel Layer Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nNextToInnermostPixelLayerSplitHits'] = makeTH1D("jet_GhostTrack_nNextToInnermostPixelLayerSplitHits", sampleName, 4, 0, 4, "NextToInnermost Pixel Layer Split Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_nNextToInnermostPixelLayerSharedHits'] = makeTH1D("jet_GhostTrack_nNextToInnermostPixelLayerSharedHits", sampleName, 4, 0, 4, "NextToInnermost Pixel Layer Shared Hits, GM Jet p_{T}^{trk}>500 MeV from PV","Number of Constituents")
  plotList['jet_GhostTrack_DeltaR'] = makeTH1D("jet_GhostTrack_DeltaR", sampleName, 20, 0, 0.4, "#DeltaR(trk,jet)","Number of Constituents")
#
  plotList['jet_dRtrk__NTrack'] = makeTH2D("jet_dRtrk__NTrack",   sampleName, 20, 0, 0.4, 20, -0.5, 19.5, "#DeltaR(trk, jet)","N Tracks")
  plotList['jet_dRtrk__SumPt'] = makeTH2D("jet_dRtrk__SumPt",   sampleName, 20, 0, 0.4, 200, -0.5, 199.5, "#DeltaR(trk, jet)","Sum p_{T}")
  plotList['jet_dRtrk__pt'] = makeTH2D("jet_dRtrk__pt",   sampleName, 20, 0, 0.4, 200, -0.5, 199.5, "#DeltaR(trk, jet)","track p_{T}")
  plotList['jet_dRtrk__NPixelHits'] = makeTH2D("jet_dRtrk__NPixelHits",   sampleName, 20, 0, 0.4, 40, -0.5, 39.5, "#DeltaR(trk, jet)","N Pixel Hits")
  plotList['jet_dRtrk__NSCTHit'] = makeTH2D("jet_dRtrk__NSCTHit",   sampleName, 20, 0, 0.4, 40, -0.5, 39.5, "#DeltaR(trk, jet)","N SCT Hits")
  plotList['jet_dRtrk__NTRTHit'] = makeTH2D("jet_dRtrk__NTRTHit",   sampleName, 20, 0, 0.4, 70, -0.5, 69.5, "#DeltaR(trk, jet)","N TRT Hits")
  plotList['jet_dRtrk__NIMPLHit'] = makeTH2D("jet_dRtrk__NIMPLHit",   sampleName, 20, 0, 0.4, 20, -0.5, 19.5, "#DeltaR(trk, jet)","N Innermost Pixel Layer Hits")
  plotList['jet_dRtrk__NIMPLSharedHit'] = makeTH2D("jet_dRtrk__NIMPLSharedHit",   sampleName, 20, 0, 0.4, 20, -0.5, 19.5, "#DeltaR(trk, jet)","N Innermost Pixel Layer Shared Hits")
  plotList['jet_dRtrk__NIMPLSplitHit'] = makeTH2D("jet_dRtrk__NIMPLSplitHit",   sampleName, 20, 0, 0.4, 20, -0.5, 19.5, "#DeltaR(trk, jet)","N Innermost Pixel Layer Split Hits")
  plotList['jet_dRtrk__NntIMPLHit'] = makeTH2D("jet_dRtrk__NntIMPLHit",   sampleName, 20, 0, 0.4, 20, -0.5, 19.5, "#DeltaR(trk, jet)","N Next-to-Innermost Pixel Layer  Hits")
  plotList['jet_dRtrk__NntIMPLSharedHit'] = makeTH2D("jet_dRtrk__NntIMPLSharedHit",   sampleName, 20, 0, 0.4, 20, -0.5, 19.5, "#DeltaR(trk, jet)","N Next-to-Innermost Pixel Layer Shared Hits")
  plotList['jet_dRtrk__NntIMPLSplitHit'] = makeTH2D("jet_dRtrk__NntIMPLSplitHit",   sampleName, 20, 0, 0.4, 20, -0.5, 19.5, "#DeltaR(trk, jet)","N Next-to-Innermost Pixel Layer Split Hits")

  return plotList

def getPlotListProperties( tree, sampleName, plotList ):
  return plotList

######################################################

  # add plots for dijet mass looking at all possible
  # combinations of incoming and outgoing partons
#  partons = [ "q", "g" ]
#  if args.do_massPartonPlots:
#    for ipart in partons:
#      for jpart in partons:
#        incoming = ipart + jpart
#        for upart in partons:
#          for vpart in partons:
#            outgoing = upart + vpart
#            varName = "mjj" + incoming + "To" + outgoing
#            plotList[varName]  =  getMassHist(varName, sampleName)

def getBranchList(sampleName):


  # inTree.GetListOfBranches()
  # >>> for item in list:
  # ...     print item.GetName()

  isMC = True
  if "data" in sampleName: isMC = False


  print sampleName,isMC

  mc15a = True # when will this ever be false??
  if "mc15a" in args.file: mc15a = True
  if "mc15"  in args.file: mc15a = True

  plotWeights = False

  branchList = []
  if args.powheg: branchList.append("mcEventWeight")
  if (args.RWFile) > 0:
    reweightVariable = os.path.basename(args.RWFile).replace('.root','').replace('newStudy_reweight_','')
    branchList.append(reweightVariable)
    #branchList.append("NPV")
    #branchList.append("actualInteractionsPerCrossing")
    #branchList.append("averageInteractionsPerCrossing")
  if args.basicInfo:
    branchList.append("runNumber")
    branchList.append("eventNumber")
    branchList.append("lumiBlock")
    branchList.append("NPV")
    branchList.append("actualInteractionsPerCrossing")
    branchList.append("averageInteractionsPerCrossing")
    if isMC:
      branchList.append("mcEventNumber")
      branchList.append("mcChannelNumber")
      branchList.append("mcEventWeight")
      if "Sherpa" in sampleName:
        branchList.append("JZ_weight")

      if "e7142_s3126_r10724_p5003" in sampleName: 
        branchList.append("weight_nEv")
      branchList.append("jet_PartonTruthLabelID")
    if plotWeights:
      branchList.append("weight")
      branchList.append("weight_xs")
      if mc15a: branchList.append("weight_prescale")  #mc15a only
  if args.typicalVariables:
#    branchList.append("yStar")
    branchList.append("yBoost")
    branchList.append("mjj")
    branchList.append("pTjj")
    branchList.append("m3j")
    branchList.append("deltaPhi")
    global njetVarName
    branchList.append(njetVarName)
    branchList.append("jet_pt")
    branchList.append("jet_eta")
    if "JET" not in args.tree:  branchList.append("jet_SumPtTrkPt500PV")
    if "JET" not in args.tree:  branchList.append("jet_NumTrkPt500PV")
    if "JET" not in args.tree:  branchList.append("jet_TrackWidthPt500PV")
#    branchList.append("averageInteractionsPerCrossing")
  if args.kinematics:
    branchList.append("jet_E")
    branchList.append("jet_pt")
    branchList.append("jet_phi")
    branchList.append("jet_eta")
#    branchList.append("jet_emScaleEta")
    branchList.append("averageInteractionsPerCrossing")
  if args.cleaning:
    branchList.append("jet_Timing")
    branchList.append("jet_LArQuality")
    branchList.append("jet_HECQuality")
    branchList.append("jet_NegativeE")
    branchList.append("jet_AverageLArQF")
    branchList.append("jet_BchCorrCell")
    branchList.append("jet_N90Constituents")
    branchList.append("jet_LArBadHVEnergyFrac")
    branchList.append("jet_LArBadHVNCell")

  if args.energyLayers:
    branchList.append("jet_E")
    branchList.append("jet_emScaleE")
    global jet__EMFracVarName
    branchList.append(jet__EMFracVarName)
    global jet__HECFracVarName
    branchList.append(jet__HECFracVarName)
    global jet__FracSamplingMaxVarName
    branchList.append(jet__FracSamplingMaxVarName)
    global jet__FracSamplingMaxIndexVarName
    branchList.append(jet__FracSamplingMaxIndexVarName)
    branchList.append("jet_SumPtTrkPt500PV")
    branchList.append("jet_emScaleEta")
    branchList.append("jet_pt")


  if args.kinematicDistributions:
    if "JET" not in args.tree:  branchList.append("jet_SumPtTrkPt500PV")
    if "JET" not in args.tree:  branchList.append("jet_NumTrkPt500PV")
    if "JET" not in args.tree:  branchList.append("jet_TrackWidthPt500PV")
##    branchList.append("jet_pt")
##    branchList.append("jet_emScalePt")
#    #branchList.append("jet_emScaleE")

    #branchList.append("jet_LowEtConstituentsFrac") # an empty branch - not in xAODs?
  if args.energyLayersLength:
    #global jet__GhostMuonSegmentCountVarName
    global jet__GhostMuonSegmentCountVarName;
    branchList.append(jet__GhostMuonSegmentCountVarName )  #branchList.append( "jet__GhostMuonSegmentCount" )
    global jet__FracSamplingMaxIndexVarName
    branchList.append( jet__FracSamplingMaxIndexVarName )
    branchList.append( "jet_E" )
    branchList.append("jet_emScalePt")
    branchList.append("jet_emScaleE")
    branchList.append("jet_pt")
    if isMC: branchList.append( "jet_truth_E" )

  if args.punchThrough:
    global jet__GhostMuonSegmentCountVarName
    if mc15a: branchList.append( jet__GhostMuonSegmentCountVarName)  #mc15a only
  if args.flavTag:
    if mc15a: branchList.append("jet_SV0")  #mc15a only
    if mc15a: branchList.append("jet_SV1")  #mc15a only
    if mc15a: branchList.append("jet_IP3D")  #mc15a only
    if mc15a: branchList.append("jet_SV1IP3D")  #mc15a only
    if mc15a: branchList.append("jet_MV1")  #mc15a only
    if mc15a: branchList.append("jet_MV2c00")  #mc15a only
    if mc15a: branchList.append("jet_MV2c20")  #mc15a only
  if args.truth and isMC:
    branchList.append("jet_truth_E")
    branchList.append("jet_truth_pt")
    branchList.append("jet_truth_phi")
    branchList.append("jet_truth_eta")
    #branchList.append("jet_TruthCount") # check what this is before including
    if mc15a: branchList.append("pdgId1")  #mc15a only
    if mc15a: branchList.append("pdgId2")  #mc15a only
#   if mc15a: branchList.append("pdfId1")  #mc15a only
#   if mc15a: branchList.append("pdfId2")  #mc15a only
    if mc15a: branchList.append("x1")  #mc15a only
    if mc15a: branchList.append("x2")  #mc15a only
    if mc15a: branchList.append("xf1")  #mc15a only
    if mc15a: branchList.append("xf2")  #mc15a only
#    if args.truthDetail:
    if mc15a: branchList.append("jet_ConeTruthLabelID")  #mc15a only
    branchList.append("jet_TruthLabelDeltaR_B")
    branchList.append("jet_TruthLabelDeltaR_C")
    branchList.append("jet_TruthLabelDeltaR_T")
    if mc15a: branchList.append("jet_PartonTruthLabelID")  #mc15a only
    if mc15a: branchList.append("jet_GhostTruthAssociationFraction")  #mc15a only
  if args.jetConstituents or args.jetConstituentsAll:
    branchList.append("jet_numConstituents")
  if args.jetConstituentsAll:
    branchList.append("jet_constituent_pt")
    branchList.append("jet_constituent_eta")
    branchList.append("jet_constituent_phi")
    branchList.append("jet_constituent_e")
  if args.jetCaloQuant:
    branchList.append("jet__Width")
    branchList.append("jet_CentroidR")
    branchList.append("jet_minDeltaR")

  if args.jetTrackQuantAll:
    '''
    branchList.append("jet_NumTrkPt500PV")
    branchList.append("jet_SumPtTrkPt500PV")
    branchList.append("jet_TrackWidthPt500PV")
    branchList.append("jet_NumTrkPt1000PV")
    branchList.append("jet_SumPtTrkPt1000PV")
    branchList.append("jet_TrackWidthPt1000PV")
    branchList.append("jet_JVFPV")
    branchList.append("jet_Jvt")
    branchList.append("jet_JvtJvfcorr")
    branchList.append("jet_JvtRpt")
    '''
    branchList.append("jet_GhostTrack_pt")
    branchList.append("jet_GhostTrack_eta")
    branchList.append("jet_GhostTrack_phi")

  if args.jetTrack:
    branchList.append("jet_GhostTrack_pt")
    branchList.append("jet_GhostTrack_qOverP")
    branchList.append("jet_GhostTrack_eta")
    branchList.append("jet_GhostTrack_phi")
    branchList.append("jet_GhostTrack_d0")
    branchList.append("jet_GhostTrack_z0")
    branchList.append("jet_GhostTrack_nPixelHits")
    branchList.append("jet_GhostTrack_nSCTHits")
    branchList.append("jet_GhostTrack_nTRTHits")
    branchList.append("jet_GhostTrack_nPixelSharedHits")
    branchList.append("jet_GhostTrack_nPixelSplitHits")
    branchList.append("jet_GhostTrack_nInnermostPixelLayerHits")
    branchList.append("jet_GhostTrack_nInnermostPixelLayerSharedHits")
    branchList.append("jet_GhostTrack_nInnermostPixelLayerSplitHits")
    branchList.append("jet_GhostTrack_nNextToInnermostPixelLayerHits")
    branchList.append("jet_GhostTrack_nNextToInnermostPixelLayerSharedHits")
    branchList.append("jet_GhostTrack_nNextToInnermostPixelLayerSplitHits")

  if args.kinematicDistributions:
    branchList.append("passedTriggers")
    branchList.append("runNumber")
    branchList.append("lumiBlock")
    branchList.append("NPV")
    branchList.append("actualInteractionsPerCrossing")
    branchList.append("averageInteractionsPerCrossing")
    branchList.append("jet_SumPtTrkPt500PV")
    branchList.append("jet_NumTrkPt500PV")
    branchList.append("jet_TrackWidthPt500PV")
    branchList.append("jet_emScalePt")
    branchList.append("jet_E")
    branchList.append("jet_pt")
    branchList.append("jet_phi")
    branchList.append("jet_eta")
    branchList.append("yStar")

#jet_TruthLabelID  #mc14 only!!
  return branchList
#---------

def getPlotListAll( tree, sampleName, plotList ):
   #if want all plots, add those which are missing
  branchList = []
  for thisBranch in tree.GetListOfBranches():
    branchName = thisBranch.GetName()
    if branchName      in plotList: continue
    if "a_"+branchName in plotList: continue
    if args.plotAll:
        if not "vector" in tree.GetLeaf(branchName).GetTypeName():
            branchList.append( branchName )
    if args.plotAllVector:
        if "vector" in tree.GetLeaf(branchName).GetTypeName():
            branchList.append( branchName )
  plotList = getPlotListBranchList( tree, sampleName, branchList, plotList )
  return plotList

def getPlotListBranchList( tree, sampleName, branchList, plotList ):
  # do not duplicate ones we already listed above
  for ibranch in branchList:
    #print("**** "+ibranch)
    # put an "a_" in front to make know if this is plot that can be "automatically" filled
#    plotList[ "a_"+ibranch ] = makeTH1D(ibranch, sampleName, args.nBins, tree.GetMinimum(ibranch), tree.GetMaximum(ibranch), "")
    if ibranch      in plotList: continue
    if "a_"+ibranch in plotList: continue
    if not tree.GetLeaf( ibranch ):
        print("No branch named " + ibranch)
        continue
    if not "vector" in tree.GetLeaf( ibranch ).GetTypeName():  branchVarName = "a_"+ibranch
    if "vector" in tree.GetLeaf( ibranch ).GetTypeName():  branchVarName = ibranch
    # branches which should not have an automaticlly made plot
    if "jet_EnergyPerSampling"    in ibranch: continue

    if "_constituents" in ibranch:
        ends = plotUtils.getHistExtrema(tree,ibranch,"min")
        plotList[ branchVarName ] = makeTH1D(ibranch, sampleName, args.nBins, ends[0], ends[1], ibranch, "Number of Constituents")
    elif "_GhostTrack" in ibranch:
        ends = plotUtils.getHistExtrema(tree,ibranch,"min")
        plotList[ branchVarName ] = makeTH1D(ibranch, sampleName, args.nBins, ends[0], ends[1], ibranch, "Number of Tracks")
    elif "jet_" in ibranch or "pdg" in ibranch:
        ends = plotUtils.getHistExtrema(tree,ibranch,"min")
        #print("+++++ "+ibranch)
        plotList[ branchVarName ] = makeTH1D(ibranch, sampleName, args.nBins, ends[0], ends[1], ibranch)
    else: #args.basicInfo or args.typicalVariables:
        ends = plotUtils.getHistExtrema(tree,ibranch,"min")
        plotList[ branchVarName ] = makeTH1D(ibranch, sampleName, args.nBins, ends[0], ends[1], ibranch, "Number of Events")

  del branchList
  return plotList








#tree.SetBranchAddress(c.GetAlias("ngoodVertices"), ROOT.AddressOf(tree, "ngoodVertices"))




######################################################
#####      FUNCTION USED TO FILL HISTOGRAMS      #####
##### YOU MUST TAKE CARE TO FILL EACH PLOT ADDED #####
#####   IF THE PLOT IS DIRECTLY FROM A BRANCH    #####
#####   THEN START THE VARIABLE NAME WITH a_     #####
#####   AND IT WILL BE AUTOMATICLLY FILLED HERE  #####
######################################################
def fillHists(tree, sampleName, sampleEvents, plotList, recordedCutflow ):

  ### Choose Branches to Activate ###
  global jet__FracSamplingMaxIndexVarName
  branchList_eventLevelCuts = ["yStar","yBoost","jet_pt","mjj", "lumiBlock", "runNumber", "NPV","averageInteractionsPerCrossing","jet_clean_passLooseBad", jet__FracSamplingMaxIndexVarName]
  #branchList_eventLevelCuts+=["MHT",]
  #branchList_eventLevelCuts += ["jet_eta"]
  branchList_eventLevelCuts += ["jet_Timing"]

  if args.powheg: branchList_eventLevelCuts.append("mcEventWeight")
  if len(args.triggers)>0: branchList_eventLevelCuts.append("passedTriggers")
  if args.mbts: branchList_eventLevelCuts.append("passedTriggers")
  if args.rd0filled: branchList_eventLevelCuts.append("passedTriggers")
#  #CD: adding both passLooseBad and LooseBad branches is a temporary workaround for different branch naming in ntuples available as of 27/06/2015
#  branchList_jetLevelCuts = ["jet_eta","jet_rapidity", "jet_clean_passLooseBad", "jet_clean_LooseBad", "jet__FracSamplingMaxIndex", "jet_NumTrkPt500PV"]
  branchList_jetLevelCuts = ["jet_eta","jet_rapidity", "jet_clean_passLooseBad",jet__FracSamplingMaxIndexVarName, "jet_NumTrkPt500PV"]
  if args.tileGap: branchList_jetLevelCuts += [jet__FracSamplingMaxIndexVarName]
  if args.truthMatch or args.puMatch: branchList_jetLevelCuts += ["jet_truth_pt"]
  if args.truthB or args.truthC or args.truthL: branchList_jetLevelCuts += ["jet_ConeTruthLabelID"]
  #Activate only branches of interest (include wieghts!) and turn others off   NOT WORKING!!!
  tree.SetBranchStatus('*',0)
  activateBranchList = []
  if args.energyLayers:
    activateBranchList.append( "jet_EnergyPerSampling" )
  if args.energyLayersLength:
    activateBranchList.append( "jet_EnergyPerSampling" )
  if args.jetTrackQuantAll:
    activateBranchList.append( "jet_GhostTrack_pt" )
    activateBranchList.append( "jet_GhostTrack_eta" )
    activateBranchList.append( "jet_GhostTrack_phi" )
  if args.jetTrack:
    activateBranchList.append( "jet_GhostTrack_pt" )
    activateBranchList.append( "jet_GhostTrack_qOverP" )
    activateBranchList.append( "jet_GhostTrack_phi" )
    activateBranchList.append( "jet_GhostTrack_eta" )
    activateBranchList.append( "jet_GhostTrack_d0" )
    activateBranchList.append( "jet_GhostTrack_z0" )
    activateBranchList.append( "jet_GhostTrack_nPixelHits" )
    activateBranchList.append( "jet_GhostTrack_nSCTHits" )
    activateBranchList.append( "jet_GhostTrack_nTRTHits" )
    activateBranchList.append( "jet_GhostTrack_nInnermostPixelLayerHits" )
    activateBranchList.append( "jet_GhostTrack_nInnermostPixelLayerSplitHits" )
    activateBranchList.append( "jet_GhostTrack_nInnermostPixelLayerSharedHits" )
    activateBranchList.append( "jet_GhostTrack_nNextToInnermostPixelLayerHits" )
    activateBranchList.append( "jet_GhostTrack_nNextToInnermostPixelLayerSplitHits" )
    activateBranchList.append( "jet_GhostTrack_nNextToInnermostPixelLayerSharedHits" )
  if args.do_massPartonPlots:
    activateBranchList.append( "deltaPhi" )
    activateBranchList.append( "chi" )
  if args.wideJets:
    activateBranchList.append( "jet_phi" )
    activateBranchList.append( "jet_E" )
    activateBranchList.append( "jet_eta" )
  if args.sensitivity:
    activateBranchList.append( "jet_phi" )
    activateBranchList.append( "jet_E" )

  if args.quickStudy:
    activateBranchList.append("jet_E")
    global jet__EMFracVarName
    activateBranchList.append(jet__EMFracVarName)
    global jet__HECFracVarName
    activateBranchList.append(jet__HECFracVarName)


  for varName in plotList:
    if "__" in varName:
      (var1 , var2 ) = varName.split("__")
      activateBranchList.append( var1 )
      activateBranchList.append( var2 )
    else: activateBranchList.append( varName )
    activateBranchList.append( varName )
    for varName in branchList_eventLevelCuts:  activateBranchList.append( varName )
    for varName in branchList_jetLevelCuts:  activateBranchList.append( varName )

  ## Remove repeated variables ##
  activateBranchList = list(set(activateBranchList))
  if args.v: print activateBranchList

  ##### Activate the chosen branches #####

#  for thisBranch in tree.GetListOfBranches():
#    branchName = thisBranch.GetName()
#    if "weight" in branchName: tree.SetBranchStatus(branchName,1)
#    if "a_"+branchName in activateBranchList: tree.SetBranchStatus(branchName,1)
#    if branchName in activateBranchList:
#      tree.SetBranchStatus(branchName,1)
##      tree.SetBranchAddress(tree.GetAlias(branchName), ROOT.AddressOf(tree, branchName))
#  if args.do_massPartonPlots:
#        tree.SetBranchStatus("jet_PartonTruthLabelID",1)
#        tree.SetBranchStatus("pdgId1",1)
#        tree.SetBranchStatus("pdgId2",1)
#  if args.v: print activateBranchList
#  if args.v: print branchList_eventLevelCuts
#  del activateBranchList

  ## Create list of variables ##
  variableList = {}
  branchTypes = {}
  branchList = []
  for thisBranch in tree.GetListOfBranches():
    branchName = thisBranch.GetName()
    branchType = thisBranch.GetListOfLeaves().At(0).GetTypeName()
    #print(branchName,branchType)
    if( not ("weight" in branchName) and
        not (branchName in activateBranchList) and
        not ("a_"+branchName in activateBranchList) and
        not (args.do_massPartonPlots and (branchName == "jet_PartonTruthLabelID" or branchName == "pdgId1" or branchName == "pdgId2")) ):
      continue
    if "vector<vector<int> >" in branchType:
      variableList[branchName] = std.vector(std.vector('int'))()
    elif "vector<vector<float> >" in branchType:
      variableList[branchName] = std.vector(std.vector('float'))()
    elif "vector<vector<string> >" in branchType:
      variableList[branchName] = std.vector(std.vector('string'))()
    elif "vector<int>" in branchType:
      variableList[branchName] = std.vector('int')()
    elif "vector<float>" in branchType:
      variableList[branchName] = std.vector('float')()
    elif "vector<string>" in branchType:
      variableList[branchName] = std.vector('string')()
    elif "Int_t" in branchType:
      variableList[branchName] = array.array('i', [0])
    elif "Long64_t" in branchType:
      variableList[branchName] = array.array('L', [0])
    elif "Float_t" in branchType:
      variableList[branchName] = array.array('f', [0])
    elif "Double_t" in branchType:
      variableList[branchName] = array.array('d', [0])
    else:
      print "Type of ", branchName, "(", branchType, ") Not recognized!!"
      continue

    branchList.append(branchName)
    branchTypes[branchList[-1]] = branchType
    tree.SetBranchStatus(branchList[-1], 1)
    tree.SetBranchAddress(branchList[-1], variableList[branchList[-1]])

  if args.v: print variableList

  isMC = True
  if "data" in sampleName: isMC = False

  if isMC and len(args.RWFile) > 0:
    reweightVariable = '_'.join(os.path.basename(args.RWFile).replace('.root','').split('_')[:2]) #remove 'newStudy_reweight_'
    rwFile = ROOT.TFile(args.RWFile, 'READ')
    rwHist = rwFile.Get('reweight_'+reweightVariable)
    rwHist.SetDirectory(0)
    rwFile.Close()


  mjjmax = 0

  count = 0
  nEvents = tree.GetEntries()

  # QG selection

  print("selecting "+args.QGselection+" events")
  QGSelectionValue = -1000
  if args.QGselection=="QQ":
      QGSelectionValue=2
  elif args.QGselection=="QG":
      QGSelectionValue=1
  elif args.QGselection=="GG":
      QGSelectionValue=0

  print("selecting "+args.truthQGselection+" events")
  truthQGselectionValue = -1000
  if isMC:
    if args.truthQGselection=="QQ":
        truthQGselectionValue=2
    elif args.truthQGselection=="QG":
        truthQGselectionValue=1
    elif args.truthQGselection=="GG":
        truthQGselectionValue=0



  print(" Tree contains " + str(nEvents) + " events")
  if args.maxEvents > 0 and nEvents>args.maxEvents: nEvents = args.maxEvents
  tStart = time.time()
  while tree.GetEntry(count) and count < nEvents:
    count += 1
    #print(count)
#    if count > 5000:
#      break
    if (count%1e3 == 0):
      tNow = time.time()
      t = tNow - tStart
#      print("\t"+sampleName + " : " + str(count) + " events in " + "{:.1f}".format(t) + " sec (" + "{:.1f}".format(count/t) + " evt/sec)")
      print("\t"+sampleName + " : " + str(count) + " " + str(count/(t+0.1)) + " evt/sec")

    if args.powheg and "JZ5" in args.file and variableList["mcEventWeight"][0] >= 352220000:
      print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Cutting out powheg bad event "
      continue







#NOT CORRECT FOR RW
    #------------------------------------------
    ## event weight
    weight = 1
    if isMC: 
      weight = variableList["weight"][0] * args.lumi / (sampleEvents)
      if ("e7142_s3126_r10724_p5003" in sampleName):
          weight = variableList["weight_nEv"][0]
      elif ("H7" in sampleName) :
          weight = variableList["mcEventWeight"][0]* variableList["JZ_weight"][0]
      elif ("SherpaCSS" in sampleName) :
          weight = variableList["mcEventWeight"][0]* variableList["JZ_weight"][0]
      elif  ("SherpaLund" in sampleName) :
          weight = variableList["mcEventWeight"][0]* variableList["JZ_weight"][0]
      elif (weight==0) and "Sherpa" in sampleName:
        weight = variableList["mcEventWeight"][0] * args.lumi / (sampleEvents)
    else: weight = args.lumi

    if weight==0 and isMC: weight=1


    if isMC and truthQGselectionValue>-1:
      testTruthValue = -1
      outgoing = getQG( variableList["jet_PartonTruthLabelID"].at(0) ) + getQG( variableList["jet_PartonTruthLabelID"].at(1) )
      if outgoing in ["qg","gq"]: 
        testTruthValue=1 
      elif outgoing in ["qq"]:
        testTruthValue=2
      elif outgoing in ["gg"]:
        testTruthValue=0
      if testTruthValue != truthQGselectionValue : continue



    if args.v: 
      print("eventLevelCuts")
      print(count,eventLevelCuts(variableList,plotList,weight,QGSelectionValue,recordedCutflow))

    # make sure cuts are consistant 


    if eventLevelCuts(variableList,plotList,weight,QGSelectionValue,recordedCutflow) == False: continue


    #apply k-factor for NLO correction
    if args.applyNLOCorrection and args.resonanceCuts:
      weight *= getNLOKFactor(variableList["mjj"][0])
      #print '\n\tapplying NLO correction: %s'%getNLOKFactor(variableList["mjj"][0])

    #apply k-factor for EW correction
    if args.applyEWCorrection and args.resonanceCuts:
      weight *= getEWKFactor(variableList["mjj"][0])
      #print '\tapplying EW correction: %s'%getEWKFactor(variableList["mjj"][0])

    if args.applyNLOCorrection and args.applyEWCorrection and args.angularCuts:
      chi = math.exp( math.fabs(2.0 * variableList["yStar"][0]))
      weight *= getNLO_kKappaFactors(variableList["mjj"][0],chi)

    #------------------------------------------

    args.correctLargeEtaJets = float(args.correctLargeEtaJets)
    if (args.correctLargeEtaJets > 0):
      for ijet in range(0,variableList["jet_pt"].size()):
        if math.fabs(variableList["jet_eta"][ijet])>1.2:
          variableList["jet_pt"][ijet] *= args.correctLargeEtaJets
          variableList["jet_E"][ijet] *= args.correctLargeEtaJets




    reorderedJets, newJetPts = [], []
    for ijet in range(variableList["jet_pt"].size()):
      newJetPts.append(variableList["jet_pt"][ijet])

    reorderedJets = [index for (pt,index) in sorted(zip(newJetPts,range(len(newJetPts))), reverse=True)]
#    if reorderedJets[0] > reorderedJets[1]:
#      print "Reordered !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"


    if args.v: print("jetCuts")
    nJetPass = 0
    selectedJets = []

    for ijet in reorderedJets:
    #for ijet in range(variableList["jet_pt"].size()):
      if args.cleanJet and not jetLevelCuts(variableList,ijet): break
      if not jetLevelCuts(variableList,ijet): continue
      nJetPass += 1
      if nJetPass > args.nJetToPlot: continue
      selectedJets.append( ijet )
    #if nJetPass == 0: continue
    if nJetPass < args.cut_NJet: continue
    if args.v: print("nPass "+str(nJetPass))

    if args.cleanJet and not args.noCleanEvent and not jetLevelCuts(variableList,ijet): continue

#    #------------------------------------------
    if isMC and len(args.RWFile) > 0:
      if reweightVariable.startswith("jet") and reweightVariable[3].isdigit():
        whichRWJet = int(reweightVariable[3])-1
        changedReweightVar = reweightVariable[:3]+reweightVariable[4:]
        weight *= rwHist.GetBinContent( rwHist.FindBin( variableList[changedReweightVar][selectedJets[whichRWJet]] ) )
      else:
        weight *= rwHist.GetBinContent( rwHist.FindBin( variableList[reweightVariable][0] ) )



#    if args.kinematicDistributions and args.cut_yStarMax == 0.6:
#        if math.fabs(variableList["yStar"][0])>0.6:
#            yStarCutStoreMax = args.cut_yStarMax
#            yStarCutStoreMin = args.cut_yStarin
#            args.cut_yStarMin = -99
#            args.cut_yStarMax = -99
#            if eventLevelCuts(variableList) == True: plotList['mjj_for_yStarGt0p6'].Fill(variableList["mjj"][0],weight)
#            args.cut_yStarMax = yStarCutStoreMax
#            args.cut_yStarMin = yStarCutStoreMin
#        elif math.fabs(variableList["yStar"][0])<0.6:
#            if eventLevelCuts(variableList) == True: plotList['mjj_for_yStarLt0p6'].Fill(variableList["mjj"][0],weight)




    if variableList["mjj"][0] > mjjmax:
        mjjmax = variableList["mjj"][0]
        if args.v : print mjjmax


    ######################################################
    #####           EVENT LEVEL PLOTS                #####
    ######################################################

    if args.sensitivity:

      if variableList["jet_pt"].size() < 3: continue
      jet1 = ROOT.TLorentzVector()
      jet2 = ROOT.TLorentzVector()
      jet3 = ROOT.TLorentzVector()
#      jet4 = ROOT.TLorentzVector()

      if math.fabs(variableList["jet_eta"][0]) > 2.8: continue
      if math.fabs(variableList["jet_eta"][1]) > 2.8: continue
      if math.fabs(variableList["jet_eta"][2]) > 2.8: continue
#      if math.fabs(variableList["jet_eta"][3]) > 2.8: continue

      jet1.SetPtEtaPhiE( variableList["jet_pt"][0], variableList["jet_eta"][0], variableList["jet_phi"][0], variableList["jet_E"][0])
      jet2.SetPtEtaPhiE( variableList["jet_pt"][1], variableList["jet_eta"][1], variableList["jet_phi"][1], variableList["jet_E"][1])
      jet3.SetPtEtaPhiE( variableList["jet_pt"][2], variableList["jet_eta"][2], variableList["jet_phi"][2], variableList["jet_E"][2])
#      jet4.SetPtEtaPhiE( variableList["jet_pt"][3], variableList["jet_eta"][3], variableList["jet_phi"][3], variableList["jet_E"][3])

      mjjj = (jet1 + jet2 + jet3).M()
      plotList['mjjj'].Fill(mjjj,weight)

    scale = 1000.;
    if args.morphing: scale = 1.
    if args.quarkGluon:

      quarkQuarkEvent = False 
      quarkGluonEvent = False 
      gluonGluonEvent = False


      if args.quarkGluonOverlap:
        print " Overlap: "+str(args.quarkGluonOverlap)
        print " Gluon Selection "+str(args.gluonTrackSlope)+" "+str(args.gluonParam)+str(args.gluonTrackOffset)
        print " Quark Selection "+str(args.quarkTrackSlope)+" "+str(args.quarkParam)+str(args.quarkTrackOffset)
        print " Quark Jet 0 "+str(getQuarkSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]))
        print " Quark Jet 1 "+str(getQuarkSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]))
        print " Gluon Jet 0 "+str(getGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]))
        print " Gluon Jet 1 "+str(getGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]))
        print variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]
        print variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]
        quarkQuarkEvent = getQuarkSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getQuarkSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1])
        gluonGluonEvent = getGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1])
      if args.quarkGluonOverlap:
        print " Overlap: "+str(args.quarkGluonOverlap)
        print " Gluon Selection "+str(args.gluonTrackSlope)+" "+str(args.gluonParam)+str(args.gluonTrackOffset)
        print " Quark Selection "+str(args.quarkTrackSlope)+" "+str(args.quarkParam)+str(args.quarkTrackOffset)
        print " Quark Jet 0 "+str(getQuarkSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]))
        print " Quark Jet 1 "+str(getQuarkSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]))
        print " Gluon Jet 0 "+str(getGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]))
        print " Gluon Jet 1 "+str(getGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]))
        print variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]
        print variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]
        quarkQuarkEvent = getQuarkSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getQuarkSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1])
        gluonGluonEvent = getGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1])

        quarkGluonEvent = ( getQuarkSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1])) or \
           (getGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getQuarkSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]))
      
        print "QQ "+str(quarkQuarkEvent)
        print "QG "+str(quarkGluonEvent)
        print "GG "+str(gluonGluonEvent)
      else:
        quarkGluonChoice = 0
        quarkGluonChoice+= getQuarkGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0])
        quarkGluonChoice+= getQuarkGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1])
        if quarkGluonChoice == 0:
          gluonGluonEvent = True 
        elif quarkGluonChoice == 1:
          quarkGluonEvent = True 
        else:
          quarkQuarkEvent = True 




      plotList['mjj'].Fill(variableList["mjj"][0]/scale,weight)
      plotList['mjj_GeV'].Fill(variableList["mjj"][0],weight)
      if gluonGluonEvent:
        plotList['GG_mjj'].Fill(variableList["mjj"][0]/scale,weight)
        plotList['GG_mjj_GeV'].Fill(variableList["mjj"][0],weight)
        plotList['GG_leading_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][0],weight)
        plotList['GG_subleading_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][1],weight)
      if quarkGluonEvent:
        plotList['QG_mjj'].Fill(variableList["mjj"][0]/scale,weight)
        plotList['QG_mjj_GeV'].Fill(variableList["mjj"][0],weight)
        plotList['QG_leading_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][0],weight)
        plotList['QG_subleading_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][1],weight)
      if quarkQuarkEvent:
        plotList['QQ_mjj'].Fill(variableList["mjj"][0]/scale,weight)
        plotList['QQ_mjj_GeV'].Fill(variableList["mjj"][0],weight)
        plotList['QQ_leading_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][0],weight)
        plotList['QQ_subleading_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][1],weight)


      #QQ_leading_nTrk500

      if isMC:
        QGTrackptBinning = plotUtils.getQGptBins()
        QGmassLabel = None
        for i in range( 0,len(QGTrackptBinning)-1 ):
          minPt = QGTrackptBinning[i]
          maxPt = QGTrackptBinning[i+1]
          if variableList["jet_pt"][0]>=minPt and variableList["jet_pt"][0]<maxPt:
            QGmassLabel = ("pT_%d_%d" % (int(math.floor(minPt)),int(math.floor(maxPt))))
            break

        if getQG( variableList["jet_PartonTruthLabelID"].at(0)) == "q":
          plotList['truthQ_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][0],weight)
          if QGmassLabel is not None:
            plotList['truthQ_nTrk500_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][0],weight)
        else: 
          plotList['truthG_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][0],weight)
          if QGmassLabel is not None:
            plotList['truthG_nTrk500_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][0],weight)


        QGmassLabel = None
        for i in range( 0,len(QGTrackptBinning)-1 ):
          minPt = QGTrackptBinning[i]
          maxPt = QGTrackptBinning[i+1]
          if variableList["jet_pt"][1]>=minPt and variableList["jet_pt"][1]<maxPt:
            QGmassLabel = ("pT_%d_%d" % (int(math.floor(minPt)),int(math.floor(maxPt))))
            break

        if getQG( variableList["jet_PartonTruthLabelID"].at(1)) == "q":
          plotList['truthQ_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][1])
          if QGmassLabel is not None:
            plotList['truthQ_nTrk500_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1],weight)
        else: 
          plotList['truthG_nTrk500'].Fill(variableList["jet_NumTrkPt500PV"][1],weight)

        sum500 = abs(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0])
        diff500 = abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0])
        likeValue = getLikelihood(variableList["mjj"][0],sum500,diff500)
        plotList['Event_Likelihood_JJ'].Fill(likeValue,weight)

        QGTrackMassBinning = plotUtils.getQGMassBins()
        QGmassLabel = None 
        for i in range( 0,len(QGTrackMassBinning)-1 ):
            minM = QGTrackMassBinning[i]
            maxM = QGTrackMassBinning[i+1]
            if variableList["mjj"][0]>=minM and variableList["mjj"][0]<maxM:
              QGmassLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))
              break     
        outgoing = getQG( variableList["jet_PartonTruthLabelID"].at(0) ) + getQG( variableList["jet_PartonTruthLabelID"].at(1) )
        if outgoing in ["qg","gq"]: 
          plotList['truthQG_mjj'].Fill(variableList["mjj"][0]/scale,weight)
          plotList['truthQG_mjj_GeV'].Fill(variableList["mjj"][0],weight)
          plotList['Event_Likelihood_truthqg'].Fill(likeValue,weight)
          if QGmassLabel is not None:
            plotList['jet_truthqg_SumNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],weight)
            plotList['jet_truthqg_DiffNumTrkPt500PV_for_'+QGmassLabel].Fill(abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)
            plotList['jet_truthqg_SumDiffNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)
            plotList['jet_truthqg_CoarseSumDiffNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)

        elif outgoing in ["qq"]:
          plotList['truthQQ_mjj'].Fill(variableList["mjj"][0]/scale,weight)
          plotList['truthQQ_mjj_GeV'].Fill(variableList["mjj"][0],weight)
          plotList['Event_Likelihood_truthqq'].Fill(likeValue,weight)
          if QGmassLabel is not None:
            plotList['jet_truthqq_SumNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],weight)
            plotList['jet_truthqq_DiffNumTrkPt500PV_for_'+QGmassLabel].Fill(abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)
            plotList['jet_truthqq_SumDiffNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)
            plotList['jet_truthqq_CoarseSumDiffNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)


        elif outgoing in ["gg"]:
          plotList['truthGG_mjj'].Fill(variableList["mjj"][0]/scale,weight)
          plotList['truthGG_mjj_GeV'].Fill(variableList["mjj"][0],weight)
          plotList['Event_Likelihood_truthgg'].Fill(likeValue,weight)
          if QGmassLabel is not None:
            plotList['jet_truthgg_SumNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],weight)
            plotList['jet_truthgg_DiffNumTrkPt500PV_for_'+QGmassLabel].Fill(abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)
            plotList['jet_truthgg_SumDiffNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)
            plotList['jet_truthgg_CoarseSumDiffNumTrkPt500PV_for_'+QGmassLabel].Fill(variableList["jet_NumTrkPt500PV"][1]+variableList["jet_NumTrkPt500PV"][0],abs(variableList["jet_NumTrkPt500PV"][1]-variableList["jet_NumTrkPt500PV"][0]),weight)






    if args.letterKinematicPlots:
      if args.v : print("weight"+str(variableList["mjj"][0])+" "+str(weight))
      plotList['first_jet_pt'].Fill(variableList["jet_pt"][0]/1000.0,weight)
      plotList['second_jet_pt'].Fill(variableList["jet_pt"][1]/1000.0,weight)

      continue


    if args.quickStudy:
      plotList['mjj'].Fill(variableList["mjj"][0],weight)

      plotList['events_passed'].Fill(1,weight)

#      valueMax = -999.0
#      for i in range(variableList["jet_pt"].size()):
#        value = variableList["jet_E"][i]*(1.0 - variableList["jet__EMFrac"][i] - variableList["jet__HECFrac"][i])
#        if value > valueMax: valueMax = value
#
#      if valueMax>0: plotList['L1CaloCheck'].Fill(valueMax)
      continue


    if args.wideJets:

#      if variableList["jet_pt"][0] < 410: continue

      jet1 = ROOT.TLorentzVector()
      jet2 = ROOT.TLorentzVector()
      jet = ROOT.TLorentzVector()

      jet1.SetPtEtaPhiE( variableList["jet_pt"][0], variableList["jet_eta"][0], variableList["jet_phi"][0], variableList["jet_E"][0])
      jet2.SetPtEtaPhiE( variableList["jet_pt"][1], variableList["jet_eta"][1], variableList["jet_phi"][1], variableList["jet_E"][1])

      jet1List, jet2List = [], []
      for i in range(variableList["jet_pt"].size()):
        if i == 0 or i == 1: continue
        jet.SetPtEtaPhiE( variableList["jet_pt"][i], variableList["jet_eta"][i], variableList["jet_phi"][i], variableList["jet_E"][i])
        dR1=jet.DeltaR(jet1)
        dR2=jet.DeltaR(jet2)
        if dR1 <= args.cut_wideJetdR or dR2 <= args.cut_wideJetdR:
          if dR1 <= dR2:
            jet1 += jet
            jet1List.append( i )
#            plotList['cluster'+str(1)+'_FSR_M'].Fill( jet.M() , weight)
          else:
            jet2 += jet
            jet2List.append( i )
#            plotList['cluster'+str(2)+'_FSR_M'].Fill( jet.M() , weight)

      wideJets = ROOT.TLorentzVector()
      wideJets = jet1+jet2
      wideJets_yStar = (jet1.Rapidity()-jet2.Rapidity())/2

      if math.fabs( wideJets_yStar ) > 0.6: continue
#      if wideJets.M() < 1100: continue


      for j in [1,2]:
        if j==1:
          jetList = jet1List
          jet = jet1
        elif j==2:
          jetList = jet2List
          jet = jet2
        plotList['cluster'+str(j)+'_FSR_njets'].Fill( len(jetList) , weight)
        for ijet in jetList:
          plotList['cluster'+str(j)+'_FSR_pt'].Fill( variableList["jet_pt"][ijet] , weight)
          plotList['cluster'+str(j)+'_FSR_phi'].Fill( variableList["jet_phi"][ijet] , weight)
          plotList['cluster'+str(j)+'_FSR_eta'].Fill( variableList["jet_eta"][ijet] , weight)
          plotList['cluster'+str(j)+'_FSR_E'].Fill( variableList["jet_E"][ijet] , weight)

        plotList['wideJet'+str(j)+'_pt'].Fill( jet.Pt() , weight)
        plotList['wideJet'+str(j)+'_phi'].Fill( jet.Phi(), weight)
        plotList['wideJet'+str(j)+'_eta'].Fill( jet.Eta() , weight)
        plotList['wideJet'+str(j)+'_E'].Fill( jet.E() , weight)
        plotList['wideJet'+str(j)+'_M'].Fill( jet.M() , weight)


      plotList['wideJets_mjj'].Fill( wideJets.M(), weight)
      plotList['wideJets_pTjj'].Fill( wideJets.Pt(), weight)
      plotList['wideJets_deltaPhi'].Fill( jet1.Phi() - jet2.Phi() , weight)
      plotList['wideJets_deltaEta'].Fill( jet1.Eta() - jet2.Eta(), weight)
      plotList['wideJets_yStar'].Fill( wideJets_yStar , weight)
      plotList['wideJets_yBoost'].Fill( (jet1.Rapidity()+jet2.Rapidity())/2 , weight)

#      plotList['deltaMjj'].Fill(wide_mjj)





    if args.kinematics:
      HT = 0
      countJet = 0
      for ijet in selectedJets:
        countJet += 1
        HT += variableList["jet_pt"].at(ijet)


      if variableList["mjj"][0]   > 5470:
        if variableList["jet_NumTrkPt500PV"][0] < 5 or variableList["jet_NumTrkPt500PV"][1] < 5: 
          print "High Mass, Low Track Event: m= "+str(variableList["mjj"][0])+" n500 jet 1 "+str(variableList["jet_NumTrkPt500PV"][0])+" jet 2 "+str(variableList["jet_NumTrkPt500PV"][1])+" Run: "+str(variableList["runNumber"][0])+" Event: "+str(variableList["eventNumber"][0])

#      if math.fabs(variableList["MHT"][0]/math.sqrt(HT))<20:  continue

#      jet1 = ROOT.TLorentzVector()
#      jet2 = ROOT.TLorentzVector()
#      jet1.SetPtEtaPhiE( variableList["jet_pt"][0], variableList["jet_eta"][0], variableList["jet_phi"][0], variableList["jet_E"][0])
#      jet2.SetPtEtaPhiE( variableList["jet_pt"][1], variableList["jet_eta"][1], variableList["jet_phi"][1], variableList["jet_E"][1])
#      if math.fabs(jet2.DeltaPhi(jet1)) < 2.8: continue

      plotList['HT'].Fill( HT, weight )
      #plotList['MHT'].Fill( math.fabs(variableList["MHT"][0]), weight )
      #if math.sqrt(HT) > 0:
      #    plotList['MHT_div_sqrtHT'].Fill( math.fabs(variableList["MHT"][0]/math.sqrt(HT)), weight )
      pTBinning = plotUtils.getPtBinsForJets()
              #I should/will move to plotUtils.py
              # PUT IN APPROPIATE ETA BIN
      for i in range( 0,len(pTBinning)-1 ):
        minPt = float(pTBinning[i])
        maxPt = float(pTBinning[i+1])
        if variableList["jet_pt"][0]< minPt: continue
        if variableList["jet_pt"][0]>=maxPt: continue
        pTLabel = "pT_"+str(pTBinning[i])+"_"+str(pTBinning[i+1])
        #if math.sqrt(HT) > 0:
        #    #plotList['MHT_div_sqrtHT_'+pTLabel].Fill( math.fabs(variableList["MHT"][0]/math.sqrt(HT)), weight )



    plotList['events_passed'].Fill(1,weight)
    #------------------------------------------
    ## "automatic" variables of EVENT LEVEL QUANTITIES
    if args.v: print("Fill auto plots")
    for varName in plotList:
      if not varName.startswith("a_"): continue
      branchName = varName.replace("a_","")
      # do not plot branches which are vectors here
      # plots from vectors need to pass the individual jet cuts
      if "njet" in branchName:
          branchName = njetVarName
      #print(varName)
      #print(branchName)
      #print(variableList)
      #print(type(variableList[branchName]))
      #if "vector" in type(variableList[branchName]).__name__: continue
      #print "Filling " + branchName + " " + str(variableList[branchName][0])
      try:
          plotList[varName].Fill( variableList[branchName][0], weight )
      except:
          #print(varName,branchName)
          if branchName == "deltaPhi":
              delPhi = variableList["jet_phi"][0] - variableList["jet_phi"][1]
              delPhi = (delPhi + 2.*math.pi)%(2*math.pi)
              plotList[varName].Fill(delPhi, weight )

    if args.basicInfo:
      plotList['actualInteractionsPerCrossing'].Fill(variableList['actualInteractionsPerCrossing'][0], weight)
      plotList['averageInteractionsPerCrossing'].Fill(variableList['averageInteractionsPerCrossing'][0], weight)



# LEADING AND SUBLEADING JET QUANTITIES WILL ONLY BE DONE IN KINEMATiC DISTRIBUTIONS
#    if args.typicalVariables:
#      if args.v: print("Fill typicalVariables")
##      if not args.kinematicDistributions:
#      if len(selectedJets) > 0:
#        plotList['jet1_pt'].Fill( variableList["jet_pt"][selectedJets[0]], weight )
#        plotList['jet1_eta'].Fill( variableList["jet_eta"][selectedJets[0]], weight )
#      if len(selectedJets) > 1:
#        plotList['jet2_pt'].Fill( variableList["jet_pt"][selectedJets[1]], weight )
#        plotList['jet2_eta'].Fill( variableList["jet_eta"][selectedJets[1]], weight )


    if args.chiVariables:
      #plotList['chi'].Fill( math.exp( math.fabs(2.0 * variableList["yStar"][0]) ), weight )
      # find the right mass bin for chi
      if not args.kinematicDistributions:
        chiMassBinning = plotUtils.getChiMassBins()
        massLabel = "mjj_underflow"
        for i in range(0,len(chiMassBinning)-1):
          minM = chiMassBinning[i]
          maxM = chiMassBinning[i+1]
          if variableList["mjj"][0]>=minM and variableList["mjj"][0]<maxM:
             massLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))
             break
        plotList['chi_for_'+massLabel].Fill( math.exp( math.fabs(2.0 * variableList["yStar"][0]) ), weight )



    ###############################################################
    #####  USER DEFINED EVENT LEVEL PLOTS NOT ALREADY FILLED  #####
    ###############################################################

    #------------------------------------------
    ## plot depending on incoming and outgoing parton flavor
    if isMC and args.do_massPartonPlots:
      if args.v: print("Fill mass parton plots")
      incoming = getQG( variableList["pdgId1"][0] ) + getQG( variableList["pdgId2"][0] )
      outgoing = getQG( variableList["jet_PartonTruthLabelID"].at(0) ) + getQG( variableList["jet_PartonTruthLabelID"].at(1) )
      for k in range(0,3):
        if k==0:
          if incoming in ["qg","gq"]: incomingLocal = "qg"
          else: incomingLocal = incoming
          if outgoing in ["qg","gq"]: outgoingLocal = "qg"
          else: outgoingLocal = outgoing
          varName = "_of_" + incomingLocal + "To" + outgoingLocal
        elif k==1:
          if incoming in ["qg","gq"]: incomingLocal = "qg"
          else: incomingLocal = incoming
          varName = "_incoming_of_" + incomingLocal
        elif k==2:
          if outgoing in ["qg","gq"]: outgoingLocal = "qg"
          else: outgoingLocal = outgoing
          varName = "_outgoing_of_" + outgoingLocal
        plotList["mjj" + varName].Fill( variableList["mjj"][0], weight )
        plotList["first_jet_pt" + varName].Fill( variableList["jet_pt"][0], weight )
        plotList["second_jet_pt" + varName].Fill( variableList["jet_pt"][1], weight )
        #   count njets>50 GeV
        njets50 = 0
        for ijet in range(0,variableList["jet_pt"].size()):
          if variableList["jet_pt"][ijet]>50.:
              njets50 = njets50+1
        plotList["njets50GeV" + varName].Fill( njets50, weight)
        plotList["jetDPhi12" + varName].Fill( variableList["deltaPhi"][0], weight )
        plotList["yStar" + varName].Fill( variableList["yStar"][0], weight )
        plotList["yBoost" + varName].Fill( variableList["yBoost"][0], weight )
        chiMassBinning = plotUtils.getChiMassBins()
        massLabel = "mjj_underflow"
        for i in range(0,len(chiMassBinning)-1):
          if chiMassBinning[i] < 2250: continue
          minM = chiMassBinning[i]
          maxM = chiMassBinning[i+1]
          if variableList["mjj"][0]>=minM and variableList["mjj"][0]<maxM:
              massLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))
              break


    #those are all event-level quantities, so let's fill them before going into the jet loop

    if args.punchThrough:
      if variableList["jet_pt"].size()>=2:
        global jet__GhostMuonSegmentCountVarName
        ptasym12 = (variableList["jet_pt"][1]-variableList["jet_pt"][0])/(variableList["jet_pt"][1]+variableList["jet_pt"][0])
        plotList['ptasym12__first_jet__GhostMuonSegmentCount'].Fill(ptasym12,variableList[ jet__GhostMuonSegmentCountVarName][0], weight)
        plotList['ptasym12__second_jet__GhostMuonSegmentCount'].Fill(ptasym12,variableList[ jet__GhostMuonSegmentCountVarName][1], weight)

    if args.kinematicDistributions:
#      print "Kinematic distributions: filling histograms"
      chi = math.exp( math.fabs(2.0 * variableList["yStar"][0]) )
      jet1 = ROOT.TLorentzVector()
      jet2 = ROOT.TLorentzVector()
      jet3 = ROOT.TLorentzVector()
      m23 = 0
      m13 = 0
      ptasym12 = -99
      ptasym13 = -99
      ptasym23 = -99
      dphi12 = -99
      dphi13 = -99
      dphi23 = -99
      deta12 = -99
      deta13 = -99
      deta23 = -99
      dr12 = -99
      dr13 = -99
      dr23 = -99

      # find the right mass bin for chi
      if args.chiVariables:
        chiMassBinning = plotUtils.getChiMassBins()
        massLabel = "mjj_underflow"
        for i in range(0,len(chiMassBinning)-1):
          if chiMassBinning[i] < 2250: continue
          minM = chiMassBinning[i]
          maxM = chiMassBinning[i+1]
          if variableList["mjj"][0]>=minM and variableList["mjj"][0]<maxM:
            massLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))
            break

      if variableList["jet_pt"].size()>0:
        jet1.SetPtEtaPhiE( variableList["jet_pt"][0], variableList["jet_eta"][0], variableList["jet_phi"][0], variableList["jet_E"][0])
      if variableList["jet_pt"].size()>1:
        jet2.SetPtEtaPhiE( variableList["jet_pt"][1], variableList["jet_eta"][1], variableList["jet_phi"][1], variableList["jet_E"][1])
        ptasym12 = (variableList["jet_pt"][1]-variableList["jet_pt"][0])/(variableList["jet_pt"][1]+variableList["jet_pt"][0])
        dphi12 = jet2.DeltaPhi(jet1)
        deta12 = jet2.Eta() - jet1.Eta()
        dr12 = jet2.DeltaR(jet1)
      if variableList["jet_pt"].size()>2:
        jet3.SetPtEtaPhiE( variableList["jet_pt"][2], variableList["jet_eta"][2], variableList["jet_phi"][2], variableList["jet_E"][2])
        ptasym13 = (variableList["jet_pt"][2]-variableList["jet_pt"][0])/(variableList["jet_pt"][2]+variableList["jet_pt"][0])
        ptasym23 = (variableList["jet_pt"][2]-variableList["jet_pt"][1])/(variableList["jet_pt"][2]+variableList["jet_pt"][1])
        dphi13 = jet3.DeltaPhi(jet1)
        deta13 = jet3.Eta() - jet1.Eta()
        dr13 = jet3.DeltaR(jet1)
        dphi23 = jet3.DeltaPhi(jet2)
        deta23 = jet3.Eta() - jet2.Eta()
        dr23 = jet3.DeltaR(jet2)
        s23=jet2+jet3
        s13=jet1+jet3
        m23=s23.M()
        m13=s13.M()

      if not isMC:
        plotList['average_mjj_vs_run'].Fill(variableList["runNumber"][0],variableList["mjj"][0],weight)
        plotList['average_mjj_vs_lbn'].Fill(variableList["lumiBlock"][0],variableList["mjj"][0],weight)
        if args.chiVariables: plotList['average_chi_for_'+massLabel+'_vs_run'].Fill(variableList["runNumber"][0],chi,weight)
        if args.chiVariables: plotList['average_chi_for_'+massLabel+'_vs_lbn'].Fill(variableList["lumiBlock"][0],chi,weight)

      plotList['average_mjj_vs_mu'].Fill(variableList["averageInteractionsPerCrossing"][0],variableList["mjj"][0],weight)
      if args.chiVariables: plotList['average_chi_for_'+massLabel+'_vs_mu'].Fill(variableList["averageInteractionsPerCrossing"][0],chi,weight)

#cannot do this here because event level cuts have already been applied, which include yStar<0.6
#         if math.fabs(variableList["yStar"][0])<0.6:
#             plotList['mjj_for_yStarLt0p6'].Fill(variableList["mjj"][0],weight)
#         else:
#             plotList['mjj_for_yStarGt0p6'].Fill(variableList["mjj"][0],weight)

      if args.chiVariables: plotList['chi_for_'+massLabel].Fill(chi,weight)

      # m13 and m23 with binning that (should be) above 1 TeV to avoid unblinding the future >=3 jet analyses
      plotList['m23'].Fill(m23,weight)
      plotList['m13'].Fill(m13,weight)

      # pt asymmetries
      plotList['ptasym12'].Fill(ptasym12,weight)
      plotList['ptasym13'].Fill(ptasym13,weight)
      plotList['ptasym23'].Fill(ptasym23,weight)
      plotList['jetDEta12'].Fill(deta12,weight)
      plotList['jetDEta13'].Fill(deta13,weight)
      plotList['jetDEta23'].Fill(deta23,weight)
      plotList['jetDPhi12'].Fill(dphi12,weight)
      plotList['jetDPhi13'].Fill(dphi13,weight)
      plotList['jetDPhi23'].Fill(dphi23,weight)
      plotList['jetDR12'].Fill(dr12,weight)
      plotList['jetDR13'].Fill(dr13,weight)
      plotList['jetDR23'].Fill(dr23,weight)



      if args.chiVariables:
        plotList['ptasym12_for_'+massLabel].Fill(ptasym12,weight)
        plotList['ptasym23_for_'+massLabel].Fill(ptasym23,weight)
        plotList['ptasym13_for_'+massLabel].Fill(ptasym13,weight)
        if variableList["jet_pt"].size()>1:
          plotList['first_jet_eta__second_jet_eta_for_'+massLabel].Fill(jet1.Eta(), jet2.Eta(), weight)
          plotList['average_ptasym12_for_first_jet_eta__second_jet_eta'].Fill(jet1.Eta(), jet2.Eta(), ptasym12, weight)
          plotList['average_ptasym23_for_first_jet_eta__second_jet_eta'].Fill(jet1.Eta(), jet2.Eta(), ptasym23, weight)
          plotList['average_ptasym13_for_first_jet_eta__second_jet_eta'].Fill(jet1.Eta(), jet2.Eta(), ptasym13, weight)

      if variableList["jet_pt"].size()>0:
        plotList['first_jet_pt'].Fill(jet1.Pt(),weight)
        plotList['first_jet_eta'].Fill(jet1.Eta(),weight)
        plotList['first_jet_phi'].Fill(jet1.Phi(),weight)
        plotList['first_jet_m'].Fill(jet1.M(),weight)
        plotList['first_jet_en'].Fill(jet1.E(),weight)
        plotList['first_jet_eta__first_jet_phi'].Fill(jet1.Eta(),jet1.Phi(),weight)
        plotList['first_jet_eta__first_jet_pt'].Fill(jet1.Eta(),jet1.Pt(),weight)
        plotList['first_jet_emScaleEta__first_jet_pt'].Fill(variableList["jet_emScaleEta"][0],jet1.Pt(),weight)
        plotList['first_jet_emScaleEta__first_jet_E'].Fill(variableList["jet_emScaleEta"][0],jet1.E(),weight)
        plotList['first_jet_eta__mjj'].Fill(jet1.Eta(),variableList["mjj"][0],weight)
        plotList['first_jet_pt__mjj'].Fill(jet1.Pt(),variableList["mjj"][0],weight)
        plotList['first_jet_phi__mjj'].Fill(jet1.Phi(),variableList["mjj"][0],weight)

      if variableList["jet_pt"].size()>1:
        plotList['second_jet_pt'].Fill(jet2.Pt(),weight)
        plotList['second_jet_eta'].Fill(jet2.Eta(),weight)
        plotList['second_jet_phi'].Fill(jet2.Phi(),weight)
        plotList['second_jet_m'].Fill(jet2.M(),weight)
        plotList['second_jet_en'].Fill(jet2.E(),weight)
        plotList['second_jet_eta__second_jet_phi'].Fill(jet2.Eta(),jet2.Phi(),weight)
        plotList['second_jet_eta__second_jet_pt'].Fill(jet2.Eta(),jet2.Pt(),weight)
        plotList['second_jet_emScaleEta__second_jet_pt'].Fill(variableList["jet_emScaleEta"][1],jet2.Pt(),weight)
        plotList['second_jet_emScaleEta__second_jet_E'].Fill(variableList["jet_emScaleEta"][1],jet2.E(),weight)
        plotList['second_jet_eta__mjj'].Fill(jet2.Eta(),variableList["mjj"][0],weight)
        plotList['second_jet_phi__mjj'].Fill(jet2.Phi(),variableList["mjj"][0],weight)
        plotList['second_jet_pt__mjj'].Fill(jet2.Pt(),variableList["mjj"][0],weight)
        plotList['first_jet_pt__second_jet_pt'].Fill(jet1.Pt(),jet2.Pt(),weight)
        plotList['HT2_first_jet_pt_plus_second_jet_pt'].Fill(jet1.Pt()+jet2.Pt(),weight)

      if variableList["jet_pt"].size()>2:
        plotList['third_jet_pt'].Fill(jet3.Pt(),weight)
        plotList['third_jet_eta'].Fill(jet3.Eta(),weight)
        plotList['third_jet_phi'].Fill(jet3.Phi(),weight)
        plotList['third_jet_m'].Fill(jet3.M(),weight)
        plotList['third_jet_en'].Fill(jet3.E(),weight)
        plotList['third_jet_eta__third_jet_phi'].Fill(jet3.Eta(),jet3.Phi(),weight)

        plotList['third_jet_eta__third_jet_pt'].Fill(jet3.Eta(),jet3.Pt(),weight)
        plotList['third_jet_emScaleEta__third_jet_pt'].Fill(variableList["jet_emScaleEta"][2],jet3.Pt(),weight)
        plotList['third_jet_emScaleEta__third_jet_E'].Fill(variableList["jet_emScaleEta"][2],jet3.E(),weight)
        plotList['third_jet_eta__mjj'].Fill(jet3.Eta(),variableList["mjj"][0],weight)
        plotList['third_jet_phi__mjj'].Fill(jet3.Phi(),variableList["mjj"][0],weight)
        plotList['third_jet_pt__mjj'].Fill(jet3.Pt(),variableList["mjj"][0],weight)
        plotList['first_jet_pt__third_jet_pt'].Fill(jet1.Pt(),jet3.Pt(),weight)
        plotList['second_jet_pt__third_jet_pt'].Fill(jet2.Pt(),jet3.Pt(),weight)

      # kinematics binned by trigger
      if "passedTriggers" in variableList :
        if 'L1_J75' in variableList["passedTriggers"]:
          plotList['mjj_for_L1_J75'].Fill(variableList["mjj"][0],weight)
          if args.chiVariables: plotList['chi_for_'+massLabel+'_for_L1_J75'].Fill(chi,weight)
          for ijet in range(variableList["jet_pt"].size()):
            plotList['jet_pt_for_L1_J75'].Fill(variableList["jet_pt"][ijet],weight)
        if 'L1_J100' in variableList["passedTriggers"]:
          plotList['mjj_for_L1_J100'].Fill(variableList["mjj"][0],weight)
          if args.chiVariables: plotList['chi_for_'+massLabel+'_for_L1_J100'].Fill(chi,weight)
          for ijet in range(variableList["jet_pt"].size()):
            plotList['jet_pt_for_L1_J100'].Fill(variableList["jet_pt"][ijet],weight)
        if 'HLT_j360' in variableList["passedTriggers"]:
          plotList['mjj_for_HLT_j360'].Fill(variableList["mjj"][0],weight)
          if args.chiVariables: plotList['chi_for_'+massLabel+'_for_HLT_j360'].Fill(chi,weight)
          for ijet in range(variableList["jet_pt"].size()):
             plotList['jet_pt_for_HLT_j360'].Fill(variableList["jet_pt"][ijet],weight)
        if 'HLT_j400' in variableList["passedTriggers"]:
          plotList['mjj_for_HLT_j400'].Fill(variableList["mjj"][0],weight)
          if args.chiVariables:  plotList['chi_for_'+massLabel+'_for_HLT_j400'].Fill(chi,weight)
          for ijet in range(variableList["jet_pt"].size()):
            plotList['jet_pt_for_HLT_j400'].Fill(variableList["jet_pt"][ijet],weight)

      # pileup
      plotList['NPV__mjj'].Fill(variableList["NPV"][0],variableList["mjj"][0],weight)
      plotList['mu__mjj'].Fill(variableList["averageInteractionsPerCrossing"][0],variableList["mjj"][0],weight)
      if args.chiVariables:  plotList['NPV__chi_for_'+massLabel].Fill(variableList["NPV"][0],chi,weight)
      if args.chiVariables: plotList['mu__chi_for_'+massLabel].Fill(variableList["averageInteractionsPerCrossing"][0],chi,weight)
      #count njets>50 GeV
      njets50 = 0
      for ijet in range(0,variableList["jet_pt"].size()):
        if variableList["jet_pt"][ijet]>50.:
          njets50 = njets50+1
      plotList['njets50GeV'].Fill(njets50,weight)
      plotList['NPV__njets50GeV'].Fill(variableList["NPV"][0],njets50,weight)
      plotList['mu__njets50GeV'].Fill(variableList["averageInteractionsPerCrossing"][0],njets50,weight)

      if variableList["jet_pt"].size()>0:
        plotList['NPV__first_jet_pt'].Fill(variableList["NPV"][0],variableList["jet_pt"][0],weight)
        plotList['mu__first_jet_pt'].Fill(variableList["averageInteractionsPerCrossing"][0],variableList["jet_pt"][0],weight)
      if variableList["jet_pt"].size()>1:
        plotList['NPV__second_jet_pt'].Fill(variableList["NPV"][0],variableList["jet_pt"][1],weight)
        plotList['mu__second_jet_pt'].Fill(variableList["averageInteractionsPerCrossing"][0],variableList["jet_pt"][1],weight)
      if variableList["jet_pt"].size()>2:
        plotList['NPV__third_jet_pt'].Fill(variableList["NPV"][0],variableList["jet_pt"][2],weight)
        plotList['mu__third_jet_pt'].Fill(variableList["averageInteractionsPerCrossing"][0],variableList["jet_pt"][2],weight)

      #eta region plots
      if variableList["jet_pt"].size()>1:

        #etaBinning = [0,0.8,1.2,1.8,2.1,2.8,3.1,4.9]  # This must match what was in the get plotlist section above where you made the histograms
        #I should/will move to plotUtils.py
        #originally: [0,0.8,1.2,1.3,1.6,2.1,2.8,3.1,4.9], rebinned for stats
        etaBinning = plotUtils.getJetAbsEtaBins()

#bin in eta by LEAD jet
        for i in range( 0,len(etaBinning)-1 ):
          minEta = etaBinning[i]
          maxEta = etaBinning[i+1]
          #if variableList["jet_eta"].at(0)>=minEta and variableList["jet_eta"].at(0)<maxEta:
          if math.fabs(variableList["jet_emScaleEta"].at(0))>=minEta and math.fabs(variableList["jet_emScaleEta"].at(0))<maxEta:
            etaLabel = "eta_"+str(minEta)+"_"+str(maxEta)
            etaLabel = etaLabel.replace(".","p")
            plotList['mjj_for_first_jet_'+etaLabel].Fill(variableList["mjj"][0],weight)
            break

#bin in eta by SECOND jet
        for i in range( 0,len(etaBinning)-1 ):
          minEta = etaBinning[i]
          maxEta = etaBinning[i+1]
          #if variableList["jet_eta"].at(1)>=minEta and variableList["jet_eta"].at(1)<maxEta:
          if math.fabs(variableList["jet_emScaleEta"].at(1))>=minEta and math.fabs(variableList["jet_emScaleEta"].at(1))<maxEta:
            etaLabel = "eta_"+str(minEta)+"_"+str(maxEta)
            etaLabel = etaLabel.replace(".","p")
            plotList['mjj_for_second_jet_'+etaLabel].Fill(variableList["mjj"][0],weight)
            break

#------------------------------------------

#    plotList2D['mjj__yStar'].Fill( entry.mjj, entry.yStar, weight )


                                 #  plotList['layer_v_energy'] = makeTH2D()
                                 #  plotList['fracJetEnergy_v_layer'] = makeTH2D("fracJetEnergy per layer")
                                 #  plotList['jetPt_v_layer'] = makeTH2D("jetPt per layer")
                                 #  plotList['percentJetEnergy_v_layer'] = makeTH2D("Jet energy \% per layer")
                                 #also need to plot jet variations

                                 #cleaning

    ######################################################
    #####             JET LEVEL PLOTS                #####
    ######################################################
    if not args.fillJetPlots: continue    #not args.plotAllVector and
    if args.v: print("Filling jet plots")
    #------------------------------------------

    #------------------------------------------
    ## apply jet cuts on top of event level cuts
    countJet = 0
    #for ijet in range(variableList["jet_pt"].size()):
    for ijet in selectedJets:
      #if not jetLevelCuts(variableList,ijet): continue
      countJet += 1

      if args.punchThrough : #and countJet == 1:
        if args.v: print("Plot punchThrough")
        global jet__GhostMuonSegmentCountVarName

        plotList['jet__GhostMuonSegmentCount'].Fill(variableList[ jet__GhostMuonSegmentCountVarName].at(ijet),weight)
        plotList['jet_pt__jet__GhostMuonSegmentCount'].Fill(variableList["jet_pt"].at(ijet),variableList[ jet__GhostMuonSegmentCountVarName].at(ijet),weight)
        plotList['jet_eta__jet__GhostMuonSegmentCount'].Fill(variableList["jet_eta"].at(ijet),variableList[ jet__GhostMuonSegmentCountVarName].at(ijet),weight)
        plotList['jet_phi__jet__GhostMuonSegmentCount'].Fill(variableList["jet_phi"].at(ijet),variableList[ jet__GhostMuonSegmentCountVarName].at(ijet),weight)
        plotList['jet_emScaleEta__jet__GhostMuonSegmentCount'].Fill(variableList["jet_emScaleEta"].at(ijet),variableList[ jet__GhostMuonSegmentCountVarName].at(ijet),weight)

        pTBinning = plotUtils.getPtBinsForJets()
        for i in range( 0,len(pTBinning)-1 ):
          minPt = float(pTBinning[i])
          maxPt = float(pTBinning[i+1])
          if variableList["jet_pt"].at(ijet)< minPt: continue
          if variableList["jet_pt"].at(ijet)>=maxPt: continue
          pTLabel = "pT_"+str(pTBinning[i])+"_"+str(pTBinning[i+1])
          plotList['average_jet__GhostMuonSegmentCount_for'+pTLabel+'_vs_eta'].Fill(variableList["jet_emScaleEta"].at(ijet),variableList[ jet__GhostMuonSegmentCountVarName].at(ijet),weight)


      #------------------------------------------
#      ## "automatic" variables of JET LEVEL QUANTITIES
#      for varName in plotList1D:
#        if not varName.startswith("a_"): continue
#        branchName = varName.replace("a_","")
#        # plot branches which are vectors here
#        # plots from vectors need to pass the individual jet cuts
#        if not "vector" in tree.GetLeaf( branchName ).GetTypeName(): continue
#	### TODO - figure out how to do this for a vector
##        plotList1D[varName].Fill( entry.+branchName+[ijet], weight )
##        plotList1D[varName].Fill( tree.GetLeaf( branchName )[0].GetValue(), weight )


        ########################################################
        #####                JET LEVEL PLOTS               #####
        ########################################################

      if (args.energyLayersLength or args.plotAllVector) and countJet < 2: ##could get rid of ijet < 2 requirment and just use cut
        if args.v: print("Plot energyLayersLength")

        etaForLayer = [[0,1.52],[0,1.475],[0,1.475],[0,1.475],[0,1.0],[0,0.9],[0,0.8]]
        layerList = [0,1,2,3,12,13,14]
        #layerNames = ["EM Barrel PreSampler", "EM Barrel Layer 1", "EM Barrel Layer 2","EM Barrel Layer 3","Tile Barrel Layer 0","Tile Barrel Layer 1", "Tile Barrel Layer 2"]
        layerLengths = [11., 90.627802691,337.219730944,42.152466368,321.283783784,878.175675675,385.54054054]
        interactionLengths = [0.036184211, 0.311184211, 1.157894737, 0.144736842, 1.5, 4.1, 1.8]
        alllayerNames =  ["PreSamplerB", "EMB1", "EMB2","EMB3","PreSamplerE", "EME1", "EME2","EME3","HEC0", "HEC1", "HEC2", "HEC3", "TileBar0", "TileBar1", "TileBar2", "TileGap1", "TileGap2", "TileGap3", "TileExt0", "TileExt1", "TileExt2", "FCAL0", "FCAL1", "FCAL2"]
        if args.cut_caloLayerName!= "None": layer = alllayerNames.index(args.cut_caloLayerName)
        else: layer="None"
        if args.cut_caloLayerName != "None" and variableList[jet__FracSamplingMaxIndexVarName][ijet] != layer: continue

        ECalo = (variableList["jet_emScaleE"][ijet])/1000.
        for lay in range(7):
            plotList['energyLayersLength_Layers'].Fill(lay, variableList["jet_EnergyPerSampling"][ijet][layerList[lay]]/(ECalo*layerLengths[lay]*interactionLengths[lay]), weight)
            etaMin = etaForLayer[lay][0]
            etaMax = etaForLayer[lay][1]
            if math.fabs(variableList["jet_emScaleEta"].at(ijet)) > etaMin and math.fabs(variableList["jet_emScaleEta"].at(ijet)) <= etaMax:
                plotList['energyLayersLength_Layers_etaLimited'].Fill(lay, variableList["jet_EnergyPerSampling"][ijet][layerList[lay]]/(ECalo*layerLengths[lay]*interactionLengths[lay]), weight)

        global jet__GhostMuonSegmentCountVarName

        plotList['energyLayersLength_Layers'].Fill(7, variableList[ jet__GhostMuonSegmentCountVarName][ijet], weight)
        plotList['energyLayersLength_Layers'].Fill(8, variableList["jet_E"][ijet], weight)
        plotList['energyLayersLength_Layers_etaLimited'].Fill(7, variableList[ jet__GhostMuonSegmentCountVarName][ijet], weight)
        plotList['energyLayersLength_Layers_etaLimited'].Fill(8, variableList["jet_E"][ijet], weight)
        if isMC:
          plotList['energyLayersLength_Layers'].Fill(9, variableList["jet_truth_E"][ijet], weight)
          plotList['energyLayersLength_Layers'].Fill(10, variableList["jet_E"][ijet]/variableList["jet_truth_E"][ijet], weight)
          plotList['energyLayersLength_Layers_etaLimited'].Fill(9, variableList["jet_truth_E"][ijet], weight)
          plotList['energyLayersLength_Layers_etaLimited'].Fill(10, variableList["jet_E"][ijet]/variableList["jet_truth_E"][ijet], weight)
        else:
          plotList['energyLayersLength_Layers'].Fill(9, 0)
          plotList['energyLayersLength_Layers'].Fill(10, 0)
          plotList['energyLayersLength_Layers_etaLimited'].Fill(9, 0)
          plotList['energyLayersLength_Layers_etaLimited'].Fill(10, 0)



         #eta binned plots
        etaBinning = plotUtils.getJetAbsEtaBins()
        for i in range( 0,len(etaBinning)-1 ):
          minEta = etaBinning[i]
          maxEta = etaBinning[i+1]

          if math.fabs(variableList["jet_emScaleEta"].at(ijet))< minEta: continue
          if math.fabs(variableList["jet_emScaleEta"].at(ijet))>=maxEta: continue
          etaLabel = "tot_eta_"+str(minEta)+"_"+str(maxEta)
          etaLabel = etaLabel.replace(".","p")
 #          if args.cut_caloLayerName!= "None": layer = alllayerNames.index(args.cut_caloLayerName)
 #          else: layer="None"
 #          if args.cut_caloLayerName != "None" and variableList["jet__FracSamplingMaxIndex"][ijet] != layer: continue
          ECalo = (variableList["jet_emScaleE"][ijet])/1000.
          global jet__GhostMuonSegmentCountVarName

          for lay in range(7):
            plotList['energyLayersLength_Layers_'+etaLabel].Fill(lay, variableList["jet_EnergyPerSampling"][ijet][layerList[lay]]/(ECalo*layerLengths[lay]*interactionLengths[lay]), weight)
            plotList['energyLayersLength_Layers_'+etaLabel].Fill(7, variableList[ jet__GhostMuonSegmentCountVarName][ijet], weight)
            plotList['energyLayersLength_Layers_'+etaLabel].Fill(8, variableList["jet_E"][ijet], weight)
          if isMC:
            plotList['energyLayersLength_Layers_'+etaLabel].Fill(9, variableList["jet_truth_E"][ijet], weight)
            plotList['energyLayersLength_Layers_'+etaLabel].Fill(10, variableList["jet_E"][ijet]/variableList["jet_truth_E"][ijet], weight)
          else:
            plotList['energyLayersLength_Layers_'+etaLabel].Fill(9, 0)
            plotList['energyLayersLength_Layers_'+etaLabel].Fill(10, 0)


      if args.kinematics or args.plotAllVector:
        if args.v: print("Plot kinematics")
        plotList['jet_E'].Fill( variableList["jet_E"][ijet], weight )
        plotList['jet_pt'].Fill( variableList["jet_pt"][ijet], weight )
        plotList['jet_phi'].Fill( variableList["jet_phi"][ijet], weight )
        plotList['jet_rapidity'].Fill( variableList["jet_rapidity"][ijet], weight )
        plotList['jet_eta'].Fill( variableList["jet_eta"][ijet], weight )
        plotList['jet_emScaleEta'].Fill( variableList["jet_emScaleEta"][ijet], weight )
        plotList['jet_emScaleE'].Fill( variableList["jet_emScaleE"][ijet]/1000., weight )
        plotList['jet_emScalePhi'].Fill( variableList["jet_emScalePhi"][ijet], weight )
        mass =  variableList["jet_E"][ijet]*variableList["jet_E"][ijet] - variableList["jet_pt"][ijet]*variableList["jet_pt"][ijet]*math.cosh(variableList["jet_eta"][ijet])*math.cosh(variableList["jet_eta"][ijet])
        if mass < 0: mass *= -1.0
        mass = math.sqrt(mass)
        plotList['jet_M'].Fill( mass, weight)

        plotList['jet_eta__phi'].Fill( variableList["jet_eta"][ijet], variableList["jet_phi"][ijet], weight )
        plotList['jet_eta__pt'].Fill(  variableList["jet_eta"][ijet], variableList["jet_pt"][ijet], weight )
        plotList['jet_emScaleEta__pt'].Fill( variableList["jet_emScaleEta"][ijet], variableList["jet_pt"][ijet], weight )
        plotList['jet_eta__E'].Fill(  variableList["jet_eta"][ijet], variableList["jet_E"][ijet], weight )
        plotList['jet_emScaleEta__E'].Fill( variableList["jet_emScaleEta"][ijet], variableList["jet_E"][ijet], weight )
        plotList['jet_emScaleEta__emScalePhi'].Fill( variableList["jet_emScaleEta"][ijet], variableList["jet_emScalePhi"][ijet], weight )


#        plotList['jet_Timing'].Fill( variableList["jet_Timing"][ijet], weight )
#        plotList['jet_eta__Timing'].Fill(variableList["jet_eta"][ijet],variableList["jet_Timing"][ijet],weight)

      if args.cleaning or args.plotAllVector:
        if args.v: print("Plot cleaning")
        plotList['jet_Timing'].Fill( variableList["jet_Timing"][ijet], weight )
        plotList['jet_LArQuality'].Fill( variableList["jet_LArQuality"][ijet]/65535, weight )
        plotList['jet_HECQuality'].Fill( variableList["jet_HECQuality"][ijet], weight )
        plotList['jet_NegativeE'].Fill( variableList["jet_NegativeE"][ijet], weight )
        plotList['jet_AverageLArQF'].Fill( variableList["jet_AverageLArQF"][ijet], weight )
        plotList['jet_BchCorrCell'].Fill( variableList["jet_BchCorrCell"][ijet], weight )
        plotList['jet_N90Constituents'].Fill( variableList["jet_N90Constituents"][ijet], weight )
        plotList['jet_LArBadHVEnergyFrac'].Fill( variableList["jet_LArBadHVEnergyFrac"][ijet], weight )
        plotList['jet_LArBadHVNCell'].Fill( variableList["jet_LArBadHVNCell"][ijet], weight )
        plotList['jet_eta__Timing'].Fill(variableList["jet_eta"][ijet],variableList["jet_Timing"][ijet],weight)
        plotList['jet_pt__Timing'].Fill(variableList["jet_pt"][ijet],variableList["jet_Timing"][ijet],weight)

      if args.energyLayers or args.plotAllVector:

        #CD: redeclaring as I want to touch as little as possible of the rest
        layerListWithLength = [0,1,2,3,12,13,14]
        #layerNames = ["EM Barrel PreSampler", "EM Barrel Layer 1", "EM Barrel Layer 2","EM Barrel Layer 3","Tile Barrel Layer 0","Tile Barrel Layer 1", "Tile Barrel Layer 2"]
        layerLengths = {0:11., 1:90.627802691,2:337.219730944,3:42.152466368,12:321.283783784,13:878.175675675,14:385.54054054}
        interactionLengths = {0:0.036184211, 1:0.311184211, 2:1.157894737, 3:0.144736842, 12:1.5, 13:4.1, 14:1.8}

        if args.v: print("Plot energyLayers")

        global jet__HECFracVarName
        plotList['jet__HECFrac'].Fill( variableList[jet__HECFracVarName][ijet], weight )
        global jet__EMFracVarName
        plotList['jet__EMFrac'].Fill( variableList[jet__EMFracVarName][ijet], weight )
        global FracSamplingMaxVarName
        plotList['jet__FracSamplingMax'].Fill( variableList[jet__FracSamplingMaxVarName][ijet], weight )
        global jet__FracSamplingMaxIndexVarName
        plotList['jet__FracSamplingMaxIndex'].Fill( variableList[jet__FracSamplingMaxIndexVarName][ijet], weight )
        plotList['jet_pt__EMFrac'].Fill( variableList["jet_pt"][ijet], variableList[jet__EMFracVarName][ijet], weight)
        plotList['jet_Eem__EMFrac'].Fill( variableList["jet_emScaleE"][ijet]/1000., variableList[jet__EMFracVarName][ijet], weight)

        #eta binned plots
        etaBinning = plotUtils.getJetAbsEtaBins()
        for i in range( 0,len(etaBinning)-1 ):
          minEta = etaBinning[i]
          maxEta = etaBinning[i+1]

          if math.fabs(variableList["jet_emScaleEta"].at(ijet))< minEta: continue
          if math.fabs(variableList["jet_emScaleEta"].at(ijet))>=maxEta: continue
          etaLabel = "_tot_eta_"+str(minEta)+"_"+str(maxEta)
          etaLabel = etaLabel.replace(".","p")

          plotList['jet__EMFrac'+etaLabel].Fill( variableList[jet__EMFracVarName][ijet], weight )
          plotList['jet_Eem__EMFrac'+etaLabel].Fill( variableList[jet__EMFracVarName][ijet]/1000., variableList[jet__EMFracVarName][ijet], weight)

          plotList['jet__HECFrac'+etaLabel].Fill( variableList[jet__HECFracVarName][ijet], weight )
          plotList['jet_pt__EMFrac'+etaLabel].Fill( variableList["jet_pt"][ijet], variableList[jet__EMFracVarName][ijet], weight)
          plotList['jet_E__EMFrac'+etaLabel].Fill( variableList["jet_E"][ijet], variableList[jet__EMFracVarName][ijet], weight)



        for iSL in range(0, len(sampleLayerList)):
          labelEta = '_eta_'+str(sampleLayerEtaRange[iSL][0]).replace(".","p")+"_"+str(sampleLayerEtaRange[iSL][-1]).replace(".","p")
          if abs(variableList["jet_emScaleEta"].at(ijet)) < sampleLayerEtaRange[iSL][0] or  abs(variableList["jet_emScaleEta"].at(ijet)) > sampleLayerEtaRange[iSL][-1]: continue


          plotList['jet_'+sampleLayerList[iSL]+labelEta].Fill( (variableList["jet_EnergyPerSampling"].at(ijet)).at(iSL), weight)
          #print type(variableList["jet_EnergyPerSampling"])
          #print type(variableList["jet_EnergyPerSampling"].at(ijet))

          #plotList['jet_'+sampleLayerList[iSL]+labelEta].Fill( variableList["jet_EnergyPerSampling"][ijet][iSL], weight)
          #plotList['jet_'+sampleLayerList[iSL]+'overEem'+labelEta].Fill( variableList["jet_EnergyPerSampling"][ijet][iSL]/(variableList["jet_emScaleE"][ijet]/1000.), weight)

          #plotList['jet_Eem__'+sampleLayerList[iSL]+'overEem'+labelEta].Fill( variableList["jet_emScaleE"][ijet]/1000., (variableList["jet_EnergyPerSampling"].at(ijet)).at(iSL)/(variableList["jet_emScaleE"][ijet]/1000.), weight)
          #plotList['jet_pt__'+sampleLayerList[iSL]+labelEta].Fill( variableList["jet_pt"].at(ijet), (variableList["jet_EnergyPerSampling"].at(ijet)).at(iSL), weight)
          #plotList['jet_emScaleEta__'+sampleLayerList[iSL]+labelEta].Fill( variableList["jet_emScaleEta"][ijet], (variableList["jet_EnergyPerSampling"].at(ijet)).at(iSL), weight)
          #plotList['jet_emScaleEta__'+sampleLayerList[iSL]+'overEem'+labelEta].Fill( variableList["jet_emScaleEta"][ijet], (variableList["jet_EnergyPerSampling"].at(ijet)).at(iSL)/(variableList["jet_emScaleE"][ijet]/1000.), weight)


      if args.flavTag or args.plotAllVector:
        if args.v: print("Plot flavTag")
        plotList['jet_SV0'].Fill( variableList["jet_SV0"].at(ijet), weight )
        plotList['jet_SV1'].Fill( variableList["jet_SV1"].at(ijet), weight )
        plotList['jet_IP3D'].Fill( variableList["jet_IP3D"].at(ijet), weight )
        plotList['jet_SV1IP3D'].Fill( variableList["jet_SV1IP3D"].at(ijet), weight )
        plotList['jet_MV1'].Fill( variableList["jet_MV1"].at(ijet), weight )
        plotList['jet_MV2c00'].Fill( variableList["jet_MV2c00"].at(ijet), weight )
        plotList['jet_MV2c20'].Fill( variableList["jet_MV2c20"].at(ijet), weight )

      if isMC and ( args.truth or args.plotAllVector ):
        if args.v: print("Plot truth")
        plotList['jet_truth_E'].Fill( variableList["jet_truth_E"].at(ijet), weight )
        plotList['jet_truth_pt'].Fill( variableList["jet_truth_pt"].at(ijet), weight )
        plotList['jet_truth_phi'].Fill( variableList["jet_truth_phi"].at(ijet), weight )
        plotList['jet_truth_eta'].Fill( variableList["jet_truth_eta"].at(ijet), weight )
#        plotList['jet_TruthCount'].Fill( variableList["jet_TruthCount"].at(ijet), weight )
#        if args.truthDetail or args.plotAllVector:
        plotList['jet_ConeTruthLabelID'].Fill( variableList["jet_ConeTruthLabelID"].at(ijet), weight )
        plotList['jet_TruthLabelDeltaR_B'].Fill( variableList["jet_TruthLabelDeltaR_B"].at(ijet), weight )
        plotList['jet_TruthLabelDeltaR_C'].Fill( variableList["jet_TruthLabelDeltaR_C"].at(ijet), weight )
        plotList['jet_TruthLabelDeltaR_T'].Fill( variableList["jet_TruthLabelDeltaR_T"].at(ijet), weight )
        plotList['jet_PartonTruthLabelID'].Fill( variableList["jet_PartonTruthLabelID"].at(ijet), weight )
        plotList['jet_GhostTruthAssociationFraction'].Fill( variableList["jet_GhostTruthAssociationFraction"].at(ijet), weight )


      if args.jetConstituents or args.jetConstituentsAll or args.plotAllVector:
        if args.v: print("Plot jetConstituents")
        plotList['jet_numConstituents'].Fill( variableList["jet_numConstituents"].at(ijet), weight )
        plotList['jet_pt__numConstituents'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_numConstituents"].at(ijet), weight )
        plotList['jet_eta__numConstituents'].Fill( variableList["jet_eta"].at(ijet), variableList["jet_numConstituents"].at(ijet), weight )
        plotList['jet_phi__numConstituents'].Fill( variableList["jet_phi"].at(ijet), variableList["jet_numConstituents"].at(ijet), weight )

      if args.jetConstituentsAll or args.plotAllVector:
        if args.v: print("Plot jetConstituentsAll")
       # clusPt=0
        leadConstE = 0
        for icluster in range(len(variableList["jet_constituent_pt"].at(ijet))):
          plotList['jet_constituent_pt'].Fill( variableList["jet_constituent_pt"].at(ijet)[icluster], weight )
          plotList['jet_constituent_eta'].Fill( variableList["jet_constituent_eta"].at(ijet)[icluster], weight )
          plotList['jet_constituent_phi'].Fill( variableList["jet_constituent_phi"].at(ijet)[icluster], weight )
          plotList['jet_constituent_e'].Fill( variableList["jet_constituent_e"].at(ijet)[icluster], weight )
          jConst = ROOT.TLorentzVector()
          jConst.SetPtEtaPhiE( variableList["jet_constituent_pt"].at(ijet)[icluster], variableList["jet_constituent_eta"].at(ijet)[icluster], variableList["jet_constituent_phi"].at(ijet)[icluster], variableList["jet_constituent_e"].at(ijet)[icluster])
          plotList['jet_constituent_Et'].Fill( jConst.Et(), weight )
          plotList['jet_constituent_M'].Fill( jConst.M(), weight )
          if variableList["jet_constituent_e"].at(ijet)[icluster] > leadConstE:
              leadConstE = variableList["jet_constituent_e"].at(ijet)[icluster]
        #  clusPt += variableList["jet_constituent_pt"].at(ijet)[icluster]
        #if len(variableList["jet_constituent_pt"].at(ijet)) > 0:
        plotList['jet_fClus'].Fill( leadConstE/variableList["jet_E"].at(ijet), weight )

      if args.jetCaloQuant or args.plotAllVector:
        if args.v: print("Plot jetCaloQuant")
        plotList['jet__Width'].Fill( variableList["jet__Width"].at(ijet), weight )
        plotList['jet_pt__Width'].Fill( variableList["jet_pt"].at(ijet), variableList["jet__Width"].at(ijet), weight )
        plotList['jet_eta__Width'].Fill( variableList["jet_eta"].at(ijet), variableList["jet__Width"].at(ijet), weight )
        plotList['jet_CentroidR'].Fill( variableList["jet_CentroidR"].at(ijet), weight )
        plotList['jet_pt__CentroidR'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_CentroidR"].at(ijet), weight )
        plotList['jet_eta__CentroidR'].Fill( variableList["jet_eta"].at(ijet), variableList["jet_CentroidR"].at(ijet), weight )
        plotList['jet_minDeltaR'].Fill( variableList["jet_minDeltaR"].at(ijet), weight )
        plotList['jet_pt__minDeltaR'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_minDeltaR"].at(ijet), weight )
        plotList['jet_eta__minDeltaR'].Fill( variableList["jet_eta"].at(ijet), variableList["jet_minDeltaR"].at(ijet), weight )

      if args.jetTrackQuant or args.jetTrackQuantAll or args.plotAllVector:
        QGmassLabel = None 
        if ijet < 2:
          if args.v: print("Plot jetTrackQuant")
          plotList['jet_NumTrkPt500PV'].Fill( variableList["jet_NumTrkPt500PV"].at(ijet), weight )
          QGTrackMassBinning = plotUtils.getQGMassBins()
          for i in range( 0,len(QGTrackMassBinning)-1 ):
            minM = QGTrackMassBinning[i]
            maxM = QGTrackMassBinning[i+1]
            if variableList["mjj"][0]>=minM and variableList["mjj"][0]<maxM:
              QGmassLabel = ("mjj_%d_%d" % (int(math.floor(minM)),int(math.floor(maxM))))
              break
          if QGmassLabel is not None:
            plotList['jet_NumTrkPt500PV_for_'+QGmassLabel].Fill( variableList["jet_NumTrkPt500PV"].at(ijet), weight )
            

        plotList['jet_SumPtTrkPt500PV'].Fill( variableList["jet_SumPtTrkPt500PV"].at(ijet), weight )
        plotList['jet_fracSumPtTrkPt500PV'].Fill( variableList["jet_SumPtTrkPt500PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight )
        plotList['jet_TrackWidthPt500PV'].Fill( variableList["jet_TrackWidthPt500PV"].at(ijet), weight )
        plotList['jet_NumTrkPt1000PV'].Fill( variableList["jet_NumTrkPt1000PV"].at(ijet), weight )
        plotList['jet_SumPtTrkPt1000PV'].Fill( variableList["jet_SumPtTrkPt1000PV"].at(ijet), weight )
        plotList['jet_fracSumPtTrkPt1000PV'].Fill( variableList["jet_SumPtTrkPt1000PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight )
        plotList['jet_TrackWidthPt1000PV'].Fill( variableList["jet_TrackWidthPt1000PV"].at(ijet), weight )
        plotList['jet_JVFPV'].Fill( variableList["jet_JVFPV"].at(ijet), weight )
        plotList['jet_Jvt'].Fill( variableList["jet_Jvt"].at(ijet), weight )
        plotList['jet_JvtJvfcorr'].Fill( variableList["jet_JvtJvfcorr"].at(ijet), weight )
        plotList['jet_JvtRpt'].Fill( variableList["jet_JvtRpt"].at(ijet), weight )

        plotList['jet_pt__fracSumPtTrkPt500PV'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_SumPtTrkPt500PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight)
#        plotList['jet_eta__fracSumPtTrkPt500PV'].Fill( variableList["jet_eta"].at(ijet), variableList["jet_SumPtTrkPt500PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight)
        plotList['jet_pt__fracSumPtTrkPt1000PV'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_SumPtTrkPt1000PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight)
        plotList['jet_pt__NumTrkPt500PV'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_NumTrkPt500PV"].at(ijet), weight )
        plotList['jet_pt__NumTrkPt1000PV'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_NumTrkPt1000PV"].at(ijet), weight )

        plotList['jet_pt__TrackWidthPt500PV'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_TrackWidthPt500PV"].at(ijet), weight)
        plotList['jet_pt__TrackWidthPt1000PV'].Fill( variableList["jet_pt"].at(ijet), variableList["jet_TrackWidthPt500PV"].at(ijet), weight)

        plotList['jet_pt__JVFPV'].Fill( variableList["jet_pt"].at(ijet),  variableList["jet_JVFPV"].at(ijet), weight )
        plotList['jet_pt__Jvt'].Fill( variableList["jet_pt"].at(ijet),  variableList["jet_Jvt"].at(ijet), weight )
        plotList['jet_pt__JvtJvfcorr'].Fill( variableList["jet_pt"].at(ijet),  variableList["jet_JvtJvfcorr"].at(ijet), weight )
        plotList['jet_pt__JvtRpt'].Fill( variableList["jet_pt"].at(ijet),  variableList["jet_JvtRpt"].at(ijet), weight )

      if args.jetTrackQuantAll or args.plotAllVector:
        if args.v: print("Plot jetTrackQuantAll")
# until we have corrected derivations
        trkPt=0
        for itrk in range(len(variableList["jet_GhostTrack_pt"].at(ijet))):
          trkPt += variableList["jet_GhostTrack_pt"].at(ijet).at(itrk)
        plotList['jet_fTrack'].Fill( trkPt/variableList["jet_pt"].at(ijet), weight )
        plotList['jet_pt__fTrack'].Fill( variableList["jet_pt"].at(ijet), trkPt/variableList["jet_pt"].at(ijet), weight )
        if len(variableList["jet_GhostTrack_pt"].at(ijet)) > 1:
          deta = variableList["jet_GhostTrack_eta"].at(ijet)[0] - variableList["jet_GhostTrack_eta"].at(ijet)[1]
          dphi = variableList["jet_GhostTrack_phi"].at(ijet)[0] - variableList["jet_GhostTrack_phi"].at(ijet)[1]
          dphi = ROOT.TVector2.Phi_mpi_pi( dphi )
          plotList['jet_trackdR'].Fill( math.sqrt( deta*deta + dphi*dphi ), weight )
          plotList['jet_pt__dRtrk'].Fill( variableList["jet_pt"].at(ijet), math.sqrt( deta*deta + dphi*dphi ), weight )


      if args.jetTrack or args.plotAllVector:
        if args.v: print("Plot jetTrack")
        ## need arrays to count up the various quantities
        nslot = int(20/0.4)
        step  = 1.0/nslot
        arNTrack = [0] * nslot
        arSumPT  = [0] * nslot

        nTrk = 0
        for itrk in range(len(variableList["jet_GhostTrack_pt"].at(ijet))):
          #nTrk += 1
          plotList['jet_GhostTrack_qOverP'].Fill( variableList["jet_GhostTrack_qOverP"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_pt'].Fill( variableList["jet_GhostTrack_pt"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_eta'].Fill( variableList["jet_GhostTrack_eta"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_phi'].Fill( variableList["jet_GhostTrack_phi"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_d0'].Fill( variableList["jet_GhostTrack_d0"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_z0'].Fill( variableList["jet_GhostTrack_z0"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nPixelHits'].Fill( variableList["jet_GhostTrack_nPixelHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nPixelSharedHits'].Fill( variableList["jet_GhostTrack_nPixelSharedHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nPixelSplitHits'].Fill( variableList["jet_GhostTrack_nPixelSplitHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nSCTHits'].Fill( variableList["jet_GhostTrack_nSCTHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nTRTHits'].Fill( variableList["jet_GhostTrack_nTRTHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nInnermostPixelLayerHits'].Fill( variableList["jet_GhostTrack_nInnermostPixelLayerHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nInnermostPixelLayerSplitHits'].Fill( variableList["jet_GhostTrack_nInnermostPixelLayerSplitHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nInnermostPixelLayerSharedHits'].Fill( variableList["jet_GhostTrack_nInnermostPixelLayerSharedHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nNextToInnermostPixelLayerHits'].Fill( variableList["jet_GhostTrack_nNextToInnermostPixelLayerHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nNextToInnermostPixelLayerSplitHits'].Fill( variableList["jet_GhostTrack_nNextToInnermostPixelLayerSplitHits"].at(ijet).at(itrk) , weight)
          plotList['jet_GhostTrack_nNextToInnermostPixelLayerSharedHits'].Fill( variableList["jet_GhostTrack_nNextToInnermostPixelLayerSharedHits"].at(ijet).at(itrk) , weight)

          deta = variableList["jet_GhostTrack_eta"].at(ijet).at(itrk) - variableList["jet_eta"].at(ijet)
          dphi = variableList["jet_GhostTrack_phi"].at(ijet).at(itrk) - variableList["jet_phi"].at(ijet)
          dphi = ROOT.TVector2.Phi_mpi_pi( dphi )
          dR = math.sqrt( deta*deta + dphi*dphi )
          plotList['jet_GhostTrack_DeltaR'].Fill( dR, weight )

          # count the various quantities
          islot = int(dR/step) # floored quotient
          arNTrack[islot] += 1
          arSumPT[islot] += variableList["jet_GhostTrack_pt"].at(ijet).at(itrk)

          plotList['jet_dRtrk__pt'].Fill(dR,              variableList["jet_GhostTrack_pt"].at(ijet).at(itrk), weight)
          plotList['jet_dRtrk__NPixelHits'].Fill(dR,      variableList["jet_GhostTrack_nPixelHits"].at(ijet).at(itrk)                            ,weight)
          plotList['jet_dRtrk__NSCTHit'].Fill(dR,         variableList["jet_GhostTrack_nSCTHits"].at(ijet).at(itrk)                              ,weight)
          plotList['jet_dRtrk__NTRTHit'].Fill(dR,         variableList["jet_GhostTrack_nTRTHits"].at(ijet).at(itrk)                              ,weight)
          plotList['jet_dRtrk__NIMPLHit'].Fill(dR,        variableList["jet_GhostTrack_nInnermostPixelLayerHits"].at(ijet).at(itrk)              ,weight)
          plotList['jet_dRtrk__NIMPLSharedHit'].Fill(dR,  variableList["jet_GhostTrack_nInnermostPixelLayerSharedHits"].at(ijet).at(itrk)        ,weight)
          plotList['jet_dRtrk__NIMPLSplitHit'].Fill(dR,   variableList["jet_GhostTrack_nInnermostPixelLayerSplitHits"].at(ijet).at(itrk)         ,weight)
          plotList['jet_dRtrk__NntIMPLHit'].Fill(dR,      variableList["jet_GhostTrack_nNextToInnermostPixelLayerHits"].at(ijet).at(itrk)        ,weight)
          plotList['jet_dRtrk__NntIMPLSharedHit'].Fill(dR,variableList["jet_GhostTrack_nNextToInnermostPixelLayerSharedHits"].at(ijet).at(itrk)  ,weight)
          plotList['jet_dRtrk__NntIMPLSplitHit'].Fill(dR, variableList["jet_GhostTrack_nNextToInnermostPixelLayerSplitHits"].at(ijet).at(itrk)   ,weight)

        # done loop over tracks so now fill 2Ds for ntrk and sum pT
        for i in range(nslot):
          idR = (i + 0.5)*step
          plotList['jet_dRtrk__NTrack'].Fill(idR,           arNTrack[i],    weight)
          plotList['jet_dRtrk__SumPt'].Fill(idR,            arSumPT[i],     weight)

      if args.kinematicDistributions:
        #etaBinning = [0,0.8,1.2,1.8,2.1,2.8,3.1,4.9]  # This must match what was in the get plotlist section above where you made the histograms
        #etaBinning = [0,0.8,1.2,1.3,1.6,2.1,2.8,3.1,4.9]
        etaBinning = plotUtils.getJetAbsEtaBins()

        #I should/will move to plotUtils.py
        # PUT IN APPROPIATE ETA BIN
        for i in range( 0,len(etaBinning)-1 ):
          minEta = etaBinning[i]
          maxEta = etaBinning[i+1]
#          if variableList["jet_eta"].at(ijet)< minEta: continue
#          if variableList["jet_eta"].at(ijet)>=maxEta: continue
          if math.fabs(variableList["jet_emScaleEta"].at(ijet))< minEta: continue
          if math.fabs(variableList["jet_emScaleEta"].at(ijet))>=maxEta: continue
          etaLabel = "eta_"+str(minEta)+"_"+str(maxEta)
          etaLabel = etaLabel.replace(".","p")
          plotList['jet_pt_'+etaLabel].Fill(  variableList["jet_pt"].at(ijet), weight)
          #!!plotList['jet_emScalePt_'+etaLabel].Fill(  variableList["jet_emScalePt"].at(ijet), weight)
          plotList['jet_phi_'+etaLabel].Fill( variableList["jet_phi"].at(ijet), weight)

          if "JET" not in args.tree: plotList['jet_NumTrkPt500PV_'+etaLabel].Fill( variableList["jet_NumTrkPt500PV"].at(ijet), weight)
          if "JET" not in args.tree: plotList['jet_SumTrkPt500PV_'+etaLabel].Fill( variableList["jet_SumPtTrkPt500PV"].at(ijet), weight)
          if "JET" not in args.tree: plotList['jet_fracSumPtTrkPt500PV_'+etaLabel].Fill( variableList["jet_SumPtTrkPt500PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight)
          if "JET" not in args.tree: plotList['jet_pt__fracSumPtTrkPt500PV_'+etaLabel].Fill( variableList["jet_pt"].at(ijet), variableList["jet_SumPtTrkPt500PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight)
          if "JET" not in args.tree: plotList['jet_TrackWidthPt500PV_'+etaLabel].Fill( variableList["jet_TrackWidthPt500PV"].at(ijet), weight)

        pTBinning = plotUtils.getPtBinsForJets()
        #I should/will move to plotUtils.py
        # PUT IN APPROPIATE ETA BIN
        for i in range( 0,len(pTBinning)-1 ):
          minPt = float(pTBinning[i])
          maxPt = float(pTBinning[i+1])
          if variableList["jet_pt"].at(ijet)< minPt: continue
          if variableList["jet_pt"].at(ijet)>=maxPt: continue
          pTLabel = "pT_"+str(pTBinning[i])+"_"+str(pTBinning[i+1])
          plotList['jet_eta_'+pTLabel].Fill( variableList["jet_eta"].at(ijet), weight )
          if ijet==0: plotList['first_jet_eta_'+pTLabel].Fill( variableList["jet_eta"].at(ijet), weight )
          if ijet==1: plotList['second_jet_eta_'+pTLabel].Fill( variableList["jet_eta"].at(ijet), weight )
          plotList['jet_emScaleEta_'+pTLabel].Fill( variableList["jet_emScaleEta"].at(ijet), weight )
          if "JET" not in args.tree: plotList['jet_eta__fracSumPtTrkPt500PV_'+pTLabel].Fill( variableList["jet_eta"].at(ijet), variableList["jet_SumPtTrkPt500PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight)

          if ijet==0: plotList['first_jet_phi_'+pTLabel].Fill( variableList["jet_phi"].at(ijet), weight )
          if ijet==1: plotList['second_jet_phi_'+pTLabel].Fill( variableList["jet_phi"].at(ijet), weight )
          plotList['jet_phi_'+pTLabel].Fill( variableList["jet_phi"].at(ijet), weight )
          if "JET" not in args.tree:  plotList['jet_fracSumPtTrkPt500PV_'+pTLabel].Fill( variableList["jet_SumPtTrkPt500PV"].at(ijet)/variableList["jet_pt"].at(ijet), weight)

#      if args.kinematics:
#
#      if args.energyLayers:
#
#
#      if args.punchThrough:
#
#      if args.truth:
#        if args.truthDetail:






        #############################################################
        #####  USER DEFINED JET LEVEL PLOTS NOT ALREADY FILLED  #####
        #############################################################



  if args.v:
    for var in plotList:
      print("Filled plots")
      print(var + " -> " + str(plotList[var].Integral()))


  return
#
#def passEnergyLayerCuts(tree, entry, ijet, layer = 0):
#  passCut = True
#  if args.cut_jetEtaMin > -1 and fabs(entry.jet_eta[ijet])  < args.cut_jetEtaMin: passCut = False
#  if args.cut_jetEtaMax > -1 and fabs(entry.jet_eta[ijet])  > args.cut_jetEtaMax: passCut = False
#  if args.cut_jetEMin > -1 and fabs(entry.jet_E[ijet])  < args.cut_jetEMin: passCut = False
#  if args.cut_jetEMax > -1 and fabs(entry.jet_E[ijet])  > args.cut_jetEMax: passCut = False
#  if args.caloLayerName != "None" and entry.jet__FracSamplingMaxIndex[ijet] != layer: passCut = False
#  return passCut
#

def eventLevelCuts(variableList,plotList,weight,QGSelectionValue,recordedCutflow):
    recordedCutflow.Fill("Apply GRL",weight)
    ####### EVENT LEVEL CUTS #####
#    plotList['eventCutFlow'].Fill(12,weight)
#    plotList['eventCutFlow_unweighted'].Fill(0)
# pile-up generation cut - make sure it happens every time is requested


#    if variableList["jet_eta"].size() > 1:
#      if math.fabs(variableList["jet_eta"][1]) > 0.8: return False
      #if variableList["jet_eta"][1] > 0.8: return False
      #if variableList["jet_eta"][1] < 0.0: return False
      #if variableList["jet_eta"][1] < -0.8: return False
      #if variableList["jet_eta"][1] >  0.0: return False

#    time_cut = 5
#    if math.fabs( variableList["jet_Timing"][0] < time_cut ): return False
#      if variableList["jet_Timing"].size()>0
#        if math.fabs( variableList["jet_Timing"][1] < time_cut ): return False
#      if variableList["jet_Timing"].size()>1
#        if math.fabs( variableList["jet_Timing"][2] < time_cut ): return False

    if args.mbts:
      passedTriggerList = list( variableList["passedTriggers"] )
      if not any( "L1_MBTS_1_1" == thisTrigger for thisTrigger in passedTriggerList ):
        return False
    if args.rd0filled:
      passedTriggerList = list( variableList["passedTriggers"] )
      if not any( "L1_RD0_FILLED" == thisTrigger for thisTrigger in passedTriggerList ):
        return False

    ## If passCut is still true, set it to false by default and only return it to
    ## True if it passed any of the triggers in args.triggers
    if len(args.triggers) > 0:
      passCut = False #By default
      passedTriggerList = list( variableList["passedTriggers"] )
      if any(thisTrigger in passedTriggerList for thisTrigger in args.triggers):
        passCut = True
      if not passCut: return False

    recordedCutflow.Fill("HLT_j420",weight)

    if args.lbn:
#      if variableList["runNumber"][0] == 271421 and variableList["lumiBlock"][0] <  64 : return False
#      if variableList["runNumber"][0] == 271421 and variableList["lumiBlock"][0] > 409 : return False
#      if variableList["runNumber"][0] == 271421 :
#        if variableList["lumiBlock"][0] > 137 and variableList["lumiBlock"][0] < 139 : return False
#        if variableList["lumiBlock"][0] > 139 and variableList["lumiBlock"][0] < 141 : return False
      recordedCutflow.Fill("lbn",weight)
#    plotList['eventCutFlow'].Fill(6,weight)
#    plotList['eventCutFlow_unweighted'].Fill(6)




    if not args.noCleanEvent :
      if variableList["jet_clean_passLooseBad"][0] == False : return False
      if variableList["jet_pt"].size()>1 and variableList["jet_clean_passLooseBad"][1] == False : return False
      if variableList["jet_pt"].size()>2 and variableList["jet_clean_passLooseBad"][2] == False : return False
#      and variableList["jet_pt"][2] > 0.3*variableList["jet_pt"][1] : return False
      recordedCutflow.Fill("cleaning",weight)

#    plotList['eventCutFlow'].Fill(7,weight)
#    plotList['eventCutFlow_unweighted'].Fill(7)


    if args.cut_NJet     > -1 and variableList["jet_pt"].size()       < args.cut_NJet:     return False
    if args.cut_LJetPt   > -1 and variableList["jet_pt"].at(0)         < args.cut_LJetPt:   return False
    if args.cut_LJetPtMax   > -1 and variableList["jet_pt"].at(0)         > args.cut_LJetPtMax:   return False
    recordedCutflow.Fill("LJetPt",weight)
    if args.cut_NLJetPt  > -1 and variableList["jet_pt"].at(1)         < args.cut_NLJetPt:  return False

#    plotList['eventCutFlow'].Fill(8,weight)
#    plotList['eventCutFlow_unweighted'].Fill(8)

    if args.cut_mjjMin	 > -1 and variableList["mjj"][0]         	  <= args.cut_mjjMin:   return False
    recordedCutflow.Fill("mjjMin",weight)

#    plotList['eventCutFlow'].Fill(9,weight)
#    plotList['eventCutFlow_unweighted'].Fill(9)


    if args.cut_yStarMin >= 0 and math.fabs(variableList["yStar"][0])  < args.cut_yStarMin: return False
    if args.cut_yStarMax >= 0 and math.fabs(variableList["yStar"][0])  > args.cut_yStarMax: return False
    recordedCutflow.Fill("yStarMax",weight)

#    plotList['eventCutFlow'].Fill(10,weight)
#    plotList['eventCutFlow_unweighted'].Fill(10)

    if args.cut_yBoost 	 > -1 and math.fabs(variableList["yBoost"][0]) > args.cut_yBoost:   return False
    recordedCutflow.Fill("yBoost",weight)

    if args.cut_jetEtaMax > -1 and math.fabs(variableList["jet_eta"].at(0))      > args.cut_jetEtaMax: return False
    if args.cut_jetEtaMax > -1 and math.fabs(variableList["jet_eta"].at(1))      > args.cut_jetEtaMax: return False
    recordedCutflow.Fill("etaLT2p8",weight)

#    plotList['eventCutFlow'].Fill(11,weight)
#    plotList['eventCutFlow_unweighted'].Fill(11)

    if args.cut_mjjMax	 > -1 and variableList["mjj"][0]         	  > args.cut_mjjMax:   return False
    if args.cut_NPVMax	 > -1 and variableList["NPV"][0]         	  > args.cut_NPVMax:   return False

    if QGSelectionValue>-1 :
      


      if args.quarkGluonOverlap:
        

        if QGSelectionValue==2: return (getQuarkSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getQuarkSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]))
        if QGSelectionValue==0: return (getGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]))
        testValue = getQuarkSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1])

        return ( getQuarkSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1])) or \
           (getGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0]) and getQuarkSelection(variableList["jet_pt"][1],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][1]))
        
      else:
      
        quarkGluonChoice = 0   # GG == 0, QG ==1, QQ == 2
        quarkGluonChoice+= getQuarkGluonSelection(variableList["jet_pt"][0],variableList["mjj"][0],variableList["jet_NumTrkPt500PV"][0])
        quarkGluonChoice+= getQuarkGluonSelection(variableList["jet_pt"][1],variableList["mjj"][0], variableList["jet_NumTrkPt500PV"][1])
        if quarkGluonChoice!=QGSelectionValue   : return False



#    pass_NPV_max += 1
#    if not args.truthOnlyInput:
#      if args.cut_AvgMuMin > -1 and variableList["averageInteractionsPerCrossing"][0] < args.cut_AvgMuMin: return False




    #additional cut for sensitivity studies
    if args.sensitivity:
        if args.cut_massDropMin > -1.0 or args.cut_massDropMax > -1.0:
#            print "NEED TO CHECK THIS PORTION"
            j1 = TLorentzVector()
            j1.SetPtEtaPhiE(variableList["jet_pt"].at(0),variableList["jet_eta"].at(0),variableList["jet_phi"].at(0),variableList["jet_E"].at(0))
            Rc = 0.0
            dR = 1 #fsqrt(pow(entry.jet_eta[0] - entry.jet_eta[1],2) + pow(entry.jet_phi[0] - entry.jet_phi[1]),2))
            massDrop = j1.M()/variableList["mjj"][0]*(dR - Rc)
            if args.cut_massDropMin > -1 and massDrop < args.cut_massDropMin: return False
            if args.cut_massDropMax > -1 and massDrop > args.cut_massDropMax: return False

    return True

def jetLevelCuts(variableList,ijet):
    ####### JET LEVEL CUTS #####
    if args.cut_jetPtMin  > -1 and variableList["jet_pt"].at(ijet)       < args.cut_jetPtMin:  return False
    if args.cut_jetPtMax  > -1 and variableList["jet_pt"].at(ijet)       > args.cut_jetPtMax:  return False
    if args.cut_jetEtaMin > -1 and math.fabs(variableList["jet_eta"].at(ijet))      < args.cut_jetEtaMin: return False
    if args.cut_jetEtaMax > -1 and math.fabs(variableList["jet_eta"].at(ijet))      > args.cut_jetEtaMax: return False
    if args.cut_jetYMin   > -1 and math.fabs(variableList["jet_rapidity"].at(ijet)) < args.cut_jetYMin:   return False
    if args.cut_jetYMax   > -1 and math.fabs(variableList["jet_rapidity"].at(ijet)) > args.cut_jetYMax:   return False
    if args.cut_jetDetEtaMin   > -1 and math.fabs(variableList["jet_emScaleEta"].at(ijet)) < args.cut_jetDetEtaMin:   return False
    if args.cut_jetDetEtaMax   > -1 and math.fabs(variableList["jet_emScaleEta"].at(ijet)) > args.cut_jetDetEtaMax:   return False
    if args.cut_jetNumTrkPt500PVMin   > -1 and variableList["jet_NumTrkPt500PV"].at(ijet) < args.cut_jetNumTrkPt500PVMin: return False
    if args.cleanJet :
      if variableList["jet_clean_passLooseBad"].at(ijet) == 0:    return False
    if args.tileGap            and variableList[jet__FracSamplingMaxIndexVarName]  == 17:         return False
    if args.cut_jetMV2c20Min > -1 and variableList["jet_MV2c20"].at(ijet) < args.cut_jetMV2c20Min: return False
    # truth matched HardScatter vs !HardScatter
    if args.truthMatch   and variableList["jet_truth_pt"].at(ijet) < 0: return False
    if args.puMatch      and variableList["jet_truth_pt"].at(ijet) > 0: return False
    # jet flavor: B vs C vs L using b-tagging moment - should be same as Run I
    # HadronConeExclTruthLabelID is run II label -- need to update!
    if args.truthB and math.fabs(variableList["jet_ConeTruthLabelID"].at(ijet)) != 5: return False
    if args.truthC and math.fabs(variableList["jet_ConeTruthLabelID"].at(ijet)) != 4: return False
    if args.truthL and math.fabs(variableList["jet_ConeTruthLabelID"].at(ijet)) != 0: return False
    return True





#------------------------------------------
def getNLO_kKappaFactors(mjj,chi):
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))

    #retrieve input kK-factors file
    inputFileName = localdir+'/data/angular_selection_corrections/kKappaFactors.root'
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input NLO kKappa-factors file: %s'%inputFileName)
    f = ROOT.TFile(inputFileName)

    print "new"
    print chi
    print float(mjj)

    #get kK-factors histogram
    mass_window = [2500,2800,3100,3400,3700,4000,4300,4600,4900,5400,6500,8000,10000,15000]

    correction_factor = 0
    for i in range(0,len(mass_window)-1):
        if mjj >= mass_window[i] and mjj < mass_window[i+1]:
            h = f.Get('kKappaFactors'+str(mass_window[i]))
            correction_factor = h.GetBinContent( h.FindBin( float(chi) ) )
            break

#    if not h:
#        raise SystemExit('\n***ERROR*** couldn\'t find input NLO k-factors histogram')

    #return kK-factor

    print mass_window[i]
    return correction_factor



#------------------------------------------
def getNLOKFactor(mjj):
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))

    #retrieve input k-factors file
    inputFileName = localdir+'/data/NLO.kfactors.root'
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input NLO k-factors file: %s'%inputFileName)
    f = ROOT.TFile(inputFileName)

    #get k-factors histogram
    h = f.Get('NLOkfactors')
    if not h:
        raise SystemExit('\n***ERROR*** couldn\'t find input NLO k-factors histogram')

    #return k-factor
    print "new"
    print h.GetBinContent( h.FindBin( float(mjj) ) )
    print mjj
    print float(mjj)

    return h.GetBinContent( h.FindBin( float(mjj) ) )

#------------------------------------------
def getEWKFactor(mjj):
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))

    #retrieve input k-factors file
    inputFileName = localdir+'/data/EW.kfactors.root'
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input EW k-factors file: %s'%inputFileName)
    f = ROOT.TFile(inputFileName)

    #get k-factors histogram
    h = f.Get('EWkfactors')
    if not h:
        raise SystemExit('\n***ERROR*** couldn\'t find input EW k-factors histogram')

    #return k-factor
    return h.GetBinContent( h.FindBin( float(mjj) ) )

def getQuarkGluonSelection(pt,mjj,ntrack):
    # G = 0 Q = 1
    #nTrackMax = 65
    #nTrackMin = 8

    value = pt 
    if args.quarkGluonParam=="mjj":
      value = mjj
    elif args.quarkGluonParam=="lnpt":
      value = math.log(pt)
    elif args.quarkGluonParam=="lnpt2":
      #print "cross check"
      value = math.log(pt)

    value = pt 
    if args.quarkGluonParam=="mjj":
      value = mjj
    elif args.quarkGluonParam=="lnpt":
      value = math.log(pt)

    type = 0
    #SigmoidnTrack = int(nTrackMax / (1 + ROOT.TMath.Exp(-0.003*(pt-1500.))))+7
    #SigmoidnTrack = int(0.02*pt);
    SigmoidnTrack = (args.quarkGluonTrackSlope*value + args.quarkGluonTrackOffset)
    if args.quarkGluonParam=="lnpt2": 
      SigmoidnTrack += args.quarkGluonRootSlope*math.sqrt(value)
      #print "Check " + str(SigmoidnTrack)
    if ntrack < SigmoidnTrack: type = 1
    return type


def getLikelihood(mjj, sumN500,diffN500):
    if mjj>SumLikeRatioMaxX: mjj=SumLikeRatioMaxX
    if mjj<SumLikeRatioMinX: mjj = SumLikeRatioMinX

    if diffN500>100:diffN500=100
    if sumN500<10:sumN500=10
    if sumN500>120:sumN500=120

    SumLikeRatio.SetParameter(0,SumRatioParam_0.Eval(mjj))
    SumLikeRatio.SetParameter(1,SumRatioParam_1.Eval(mjj))
    SumLikeRatio.SetParameter(2,SumRatioParam_2.Eval(mjj))
    SumLikeRatio.SetParameter(3,SumRatioParam_3.Eval(mjj))

    DiffLikeRatio.SetParameter(0,DiffRatioParam_0.Eval(mjj))
    DiffLikeRatio.SetParameter(1,DiffRatioParam_1.Eval(mjj))
    DiffLikeRatio.SetParameter(2,DiffRatioParam_2.Eval(mjj))
    DiffLikeRatio.SetParameter(3,DiffRatioParam_3.Eval(mjj))

    like = 1. 
    like *= SumLikeRatio.Eval(sumN500)
    like *= DiffLikeRatio.Eval(diffN500)

    if like < 1e-9:
        like=1e-9
    like = math.log(like)
    return like


def getGluonSelection(pt,mjj,ntrack):
    # G = 0 Q = 1
    
    returnValue = False 

    value = pt 
    if args.gluonParam=="mjj":
      value = mjj
    elif args.gluonParam=="lnpt":
      value = math.log(pt)
    elif args.gluonParam=="lnpt2":
      value = math.log(pt)


    
    SigmoidnTrack = (args.gluonTrackSlope*value + args.gluonTrackOffset)
    if args.gluonParam=="lnpt2": SigmoidnTrack += args.gluonRootSlope*math.sqrt(value)
    if ntrack > SigmoidnTrack: returnValue = True
    return returnValue

def getQuarkSelection(pt,mjj,ntrack):
  
  returnValue = False 

  value = pt 
  if args.quarkParam=="mjj":
    value = mjj
  elif args.quarkParam=="lnpt":
    value = math.log(pt)
  elif args.quarkParam=="lnpt2":
    value = math.log(pt)



  SigmoidnTrack = (args.quarkTrackSlope*value + args.quarkTrackOffset)
  if args.quarkParam=="lnpt2": SigmoidnTrack += args.quarkRootSlope*math.sqrt(value)
  if ntrack < SigmoidnTrack: returnValue = True
  return returnValue


#------------------------------------------

if __name__ == "__main__":
    main()
