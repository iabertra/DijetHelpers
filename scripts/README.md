# Dijet Helpers

This version of DijetHelpers is being adapted for use in the QuarkGluon
selection code. The main functional change has been adding a routine to
select subsets of the data based on the number of 500 MeV tracks
pointing to a jet. Currently this is given by 
```math
\begin{aligned}
\text{Gluon Like} \qquad\qquad& n_{500} \ge \text{int} \left(0.002 \times p_T \right) + 7  \\
\text{Quark Like} \qquad\qquad& n_{500}  <  \text{int} \left(0.002 \times p_T \right) + 7 \\
\end{aligned}
```

Unfortunately, for some reason the tree's have been modified and the MC
pythia tree has slightly different branches to the data for 2015/16 as
reconstructed with R21. I have made several changes to `plotNtuple.py`
and `plotUtils.py` to cope with this.

I have also found that the code runs very slowly on the MC sample due to
some automated scripts (`getHistExtrema(tree,branch,type)` that determine the maximum and minimum values of
the data to be plotted. This is problematic in two ways:
1. It means that histograms from different files have different maxima and minima which means they cannot be merged.
2. For very large files the method is very slow (and run twice)..
I have modified both `plotNtuple.py` and `plotUtils.py` so that
`getHistExtrema(tree,branch,type)` returns a lit with maximum and minima
so it is only called once and populated the defaults more completely to
speed up the code. 

The `--quarkGluon` tag is used to fill the appropriate histograms. 
To create a simulation of the expected background using the available Pythia samples 
you can run the following command: 

```
python DijetHelpers/scripts/makeStandardHistograms.py -b --outDir AnalysisResonance_Pythia_QG_2018_04_20_2 \
--cut_caloLayerName EMB1 --studies letterKinematicPlots \
--pathToTrees /eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijet/inputs/CurrentMC_2018/QCD_Pythia8/ \
--tags mc16_13TeV \
--cut_LJetPt 440 \
--cut_jetPtMin 60 \
--cut_yStarMax 0.6 \
--trigger HLT_j380 \
--cut_mjjMin 1100 \
--applyNLOCorrection --applyEWCorrection --quarkGluon --tree outTree --outputTag nominal

```

The script has also been modified to create histograms appropriate for us in creating the morphs 
required for setting limits. Here we use the command line option `--morphing` to create histograms with 1 GeV bins from 
0 to 10 TeV. 

```
python DijetHelpers/scripts/makeStandardHistograms.py -b --outDir AnalysisQStar_MC_linear --cut_caloLayerName EMB1 --studies letterKinematicPlots  --pathToTrees  QStar --tags mc15_13TeV+QStar_0110_tree --cut_LJetPt 440 --cut_jetPtMin 60 --cut_yStarMax 0.6 --trigger HLT_j380 --cut_mjjMin 500   --cut_NLJetPt 60   --basicInfo --lumi 1 --quarkGluon --morphing 
```

22 May 2018 