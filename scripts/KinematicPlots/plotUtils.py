###############################################
#  plotUtils.py                               #
#                                             #
#  purpose: collect common plotting functions #
#                                             #
###############################################

import ROOT
from array import array
import re, sys, os, math

##############################################################################################
##############################################################################################
# LIST OF FUNCTIONS BELOW AND DESCRIPTION
#  getMassBins : get an array with the standard binning for the mJJ histogram
#  getMassHist : get histogram using the standard binning for the mJJ histogram
#  printHist   : dump hist content
#  checkHistPoissonianErrors : compare errors to sqrt(N) - print out only
#  setHistPoissonianErrors : set bin errors to sqrt(N)
#  roundHistogram : round histogram bin content to int - fill bin i N_i times with weight 1
#  getSignalMass : get signal mass tag from a string - NEEDS UPDATING
#  getQStarXSec  : get QStar XSEC - currently 8 TeV - NEEDS UPDATING
#  getQBHXSec    : get QBH XSEC - currently 8 TeV - NEEDS UPDATING
#  getScaleFactor: get xsec * lumi / nEvents - lumi and nEvents are inputs
#  getHistogramContainingPercentage : NEED TO READ
#  getEffectiveEntriesHistogram : returns hist w/ N event (weight=1) with equal error to input hist
#  createPseudoDataHist : fill hist from PDF crated from input hist
#  getDataLikeHist : given effective entries and input hist at desired lumi, give hist with weight=1 with shape and lumi of input hist
##############################################################################################
##############################################################################################

###############################################
# Get an array which defines the bin boundaries for the standard dijet mass plot
def getMassBins( opt = "13TeV" ):

  binLowE = []

  if "13TeV" in opt:
    # original binning - 8 TeV + some guesses afterwards
    #binLowE = [96.0, 105, 116, 127, 138, 151, 164, 177, 192, 207, 222, 239, 256, 274, 293, 312, 333, 354, 376, 400, 424, 449, 475, 502, 530, 558, 587, 618, 650, 682, 716, 750, 787, 825, 865, 906, 949, 994, 1040, 1088, 1138, 1189, 1242, 1297, 1353, 1410, 1469, 1531, 1595, 1663, 1733, 1807, 1884, 1964, 2047, 2132, 2221, 2313, 2407, 2503, 2602, 2703, 2807, 2916, 3028, 3146, 3267, 3394, 3525, 3660, 3801, 3946, 4096, 4250, 4409, 4571, 4738, 4909, 5085, 5267, 5453, 5644, 5843, 6066, 6311, 6566, 6831, 7107, 7394, 7692, 8003, 8326, 8662, 9012, 9376, 9755, 10149, 10559, 10985, 11429, 11890, 12370, 12870, 13390, 13930, 14493]
    # binning 1st pass in collaboration with SM jet group
    binLowE = [0, 30, 70, 110, 160, 210, 260, 310,  370, 440, 510, 590, 670, 760, 850, 950, 1060, 1180, 1310, 1450, 1600, 1760, 1940, 2120, 2330, 2550, 2780, 3040, 3310, 3610, 3930, 4270, 4640, 5040, 5470, 5940, 6440, 7000, 7495, 8104, 8735, 9353, 10000]
    #Ning Binning
    #binLowE = [0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,3300,3400,3500,3600,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,4700,4800,4900,5000]

  if "8TeV" in opt:
    binLowE = [0.0,20.0,40.0,60.0,80.0,100.0,120.0,140.0,160.0,180.0,200.0,216.0,234.0,253.0,272.0,294.0,316.0,339.0,364.0,390.0,417.0,445.0,474.0,504.0,535.0,566.0,599.0,633.0,668.0,705.0,743.0,782.0,822.0,864.0,907.0,952.0,999.0,1048.0,1098.0,1150.0,1203.0,1259.0,1316.0,1376.0,1437.0,1501.0,1567.0,1635.0,1706.0,1779.0,1854.0,1932.0,2012.0,2095.0,2181.0,2269.0,2360.0,2454.0,2551.0,2650.0,2753.0,2860.0,2970.0,3084.0,3202.0,3324.0,3450.0,3581.0,3716.0,3855.0,3999.0,4149.0,4303.0,4463.0,4629.0,4800.0,4977.0,5160.0,5350.0,5546.0,5748.0,5958.0,6174.0,6397.0,6628.0,6866.0,7112.0,7366.0,7628.0,7898.0,8177.0]
    #Ning Binning
    #binLowE = [0,100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000,3100,3200,3300,3400,3500,3600,3700,3800,3900,4000,4100,4200,4300,4400,4500,4600,4700,4800,4900,5000]

  return binLowE

###############################################
# Get an array which defines the bin boundaries for the standard chi distribution plot
def getChiBins():

  binLowE = [1., 1.34986, 1.82212, 2.4596, 3.32012, 4.48169, 6.04965, 8.16617, 11.0232, 14.8797, 20.0855, 30.]

  return binLowE

###############################################
def getMassHist( name, opt = "13TeV" ):
  """Get an empty histogram with the binning for the standard dijet mass plot
  """
  bins = getMassBins( opt )
  hist = ROOT.TH1D( name, name, len(bins)-1, array('f', bins) )
  hist.Sumw2()
  return hist

###############################################
# TODO: add x axis labels in constructor here
def get2DHist( name, xAxisBins, xAxisTitle, yAxisBins, yAxisTitle ):
  """make a 2D histograms with the specified binning for the x and y axis respectively
  """
  hist = ROOT.TH2D( name, name, len(xAxisBins)-1, array('f', xAxisBins), len(yAxisBins)-1, array('f', yAxisBins) )
  hist.GetXaxis().SetTitle(xAxisTitle)
  hist.GetYaxis().SetTitle(yAxisTitle)
  hist.Sumw2()
  return hist

###############################################

#******************************************
def printHist (hist):
    """print histogram content
    """
    for bin in xrange(1,hist.GetNbinsX()+1):
        print "bin %5s = %10.4f +/- %10.4f"%(bin, hist.GetBinContent(bin), hist.GetBinError(bin))

#******************************************
def checkHistPoissonianErrors (hist):
    """check if the errors of the histogram are Poissonian
    """
    for bin in xrange(1,hist.GetNbinsX()+1):
        if hist.GetBinContent(bin) != 0:
            if hist.GetBinError(bin) != math.sqrt(hist.GetBinContent(bin)):
                print "bin %5s = %10.4f +/- %10.4f\t->\t%10.4f"%(bin, hist.GetBinContent(bin), hist.GetBinError(bin), math.sqrt(hist.GetBinContent(bin)))
            else:
                print "bin %5s is ok"%bin
        else:
            print "bin %5s is empty"%bin

#******************************************
def setHistPoissonianErrors (hist, debug=False):
    """set Poissonian errors
    """
    for bin in xrange(1,hist.GetNbinsX()+1):
        hist.SetBinError(bin, math.sqrt(hist.GetBinContent(bin)))
        if debug:
            print "bin %5s = %10.4f +/- %10.4f"%(ii, hist.GetBinContent(bin), hist.GetBinError(bin))
    return hist

#******************************************
def roundHistogram (hist):
    """round the histogram bin content to integer numbers; NOTE fill histogram one entry at a time
    """
    newHist = ROOT.TH1D("newHist","newHist",hist.GetXaxis().GetNbins(),hist.GetXaxis().GetXbins().GetArray())
    newHist.SetDefaultSumw2(ROOT.kTRUE)
    for bin in xrange(1,hist.GetNbinsX()+1):
        bincontent = int(round(hist.GetBinContent(bin)))
        for iBinCont in xrange(bincontent):
            newHist.Fill(newHist.GetBinCenter(bin))
    return newHist

#******************************************
#def getSignalMass(name):
#    if "ExcitedQ" in name:
#        regEx = re.compile("ExcitedQ(.*)Lambda")
#    elif "BlackMax" in name:
#        regEx = re.compile("MthMD(.*)\.e2303")
#    else:
#        regEx = re.compile("_14TeV(.*)_AU2")
#
#    match = regEx.search(name)
#    sample = match.group(1)
#    return sample
#
##******************************************
#def getQStarXSec(mass):
#    #cross sections are in nb
#    sampleDict = {}
#    sampleDict["2000"]  = 3.680e-02
#    sampleDict["3000"]  = 2.985e-03
#    sampleDict["4000"]  = 3.534e-04
#    sampleDict["5000"]  = 4.833e-05
#    sampleDict["6000"]  = 7.047e-06
#    sampleDict["7000"]  = 1.039e-06
#    sampleDict["8000"]  = 1.626e-07
#    sampleDict["9000"]  = 3.186e-08
#    sampleDict["10000"] = 9.205e-09
#    sampleDict["11000"] = 3.653e-09
#    sampleDict["12000"] = 1.684e-09
#    sampleDict["13000"] = 8.456e-10
#
#    #return cross section in nb
#    #print 'QStar [%s GeV] cross section: %s nb'%(mass, str(sampleDict[mass]/1e3))
#    return sampleDict[mass]
#
##******************************************
##function taken from CommonFunctions.py
#def getQBHXSec(mass):
#	#cross sections are in pb
#    sampleDict = {}
#    sampleDict["2000"] = 2.570e+03
#    sampleDict["3000"] = 2.194e+02
#    sampleDict["4000"] = 2.742e+01
#    sampleDict["5000"] = 3.850e+00
#    sampleDict["6000"] = 5.507e-01
#    sampleDict["7000"] = 7.374e-02
#    sampleDict["8000"] = 8.615e-03
#    sampleDict["9000"] = 8.318e-04
#    sampleDict["10000"] = 5.754e-05
#    sampleDict["11000"] = 2.431e-06
#    sampleDict["12000"] = 4.606e-08
#
#    #return cross section in nb
#    #print 'QBH [%s GeV] cross section: %s nb'%(mass, str(sampleDict[mass]/1e3))
#    return sampleDict[mass]/1e3
#
##******************************************
#def getScaleFactor(model, mass, lumi, nEvents):
#    #print "lumi = %s nb^-1"%lumi
#    if model == "QStar":
#        return (getQStarXSec(mass) * lumi) / nEvents
#    elif model == "QBH":
#        return (getQBHXSec(mass) * lumi) / nEvents
#    else:
#        raise SystemExit("\n***ERROR*** unknown signal sample: %s"%name)

#******************************************
def getHistogramContainingPercentage(hist, percentage, lowBin=-999, highBin=-999):

    if lowBin == -999 : lowBin = 1
    if highBin == -999 : highBin = hist.GetNbinsX()+1

    nbins = hist.GetNbinsX()
    if (nbins==0) : return None

    integral = hist.Integral()
    if (integral==0) : return None

    firstBin = 1
    while (hist.GetBinContent(firstBin)==0) : firstBin = firstBin+1

    lastBin = nbins
    while (hist.GetBinContent(lastBin)==0) : lastBin = lastBin-1

    binsToScan = lastBin - firstBin
    if (binsToScan==0) : return None

    scanStep = binsToScan/100
    if (scanStep<1) : scanStep=1

    thisPercentage=0
    thisInterval=0

    remember1=1
    remember2=1

    #a really big interval to start with.
    smallestInterval= hist.GetBinLowEdge(lastBin) + hist.GetBinWidth(lastBin) - hist.GetBinLowEdge(firstBin) + 1e12

    rememberThisPercentage=0

    for bin1 in xrange(firstBin, lastBin+1, scanStep) :
        for bin2 in xrange(bin1, lastBin+1, scanStep) :

            #print "bins ", bin1, " to ", bin2
            thisPercentage = (hist.Integral(bin1,bin2))/integral
            thisInterval = hist.GetBinLowEdge(bin2) +  hist.GetBinWidth(bin2) - hist.GetBinLowEdge(bin1)
            #print " -. contain ", thisPercentage

            if thisPercentage >= percentage :
                if thisInterval < smallestInterval :
                    remember1=bin1
                    remember2=bin2
                    smallestInterval=thisInterval
                    rememberThisPercentage=thisPercentage
                break

    #print "best interval are bins : ", remember1, " , ", remember2
    #print " which span ", smallestInterval, "GeV"
    lowBin=remember1
    highBin=remember2

    #now make the histogram
    choppedHist = hist.Clone()
    choppedHist.Reset("ICE")
    for iBin in xrange(1, hist.GetNbinsX()+1) :
        if iBin < lowBin : continue
        if iBin > highBin : continue
        choppedHist.SetBinContent(iBin, hist.GetBinContent(iBin))
        choppedHist.SetBinError(iBin, hist.GetBinError(iBin))

    #print "bins [" , lowBin, "," , highBin , "] contain " , rememberThisPercentage*100, "percent of the signal"
    return choppedHist

#******************************************
def getEffectiveEntriesHistogram(hist, name = "hee"):
    hee = ROOT.TH1D(name,name,hist.GetXaxis().GetNbins(),hist.GetXaxis().GetXbins().GetArray())
    for bin in xrange(1,hist.GetNbinsX()+1):
        if hist.GetBinError(bin) != 0.:
            nee = pow(hist.GetBinContent(bin), 2) /  pow(hist.GetBinError(bin), 2)
        else:
            nee = 0.
        hee.SetBinContent(bin, nee)
        hee.SetBinError(bin, math.sqrt(nee))
    return hee

#******************************************
def createPseudoDataHist(inHist):

    #nEvents = inHist.GetSumOfWeights()
    nEvents = inHist.Integral()
    #nEvents = inHist.GetEntries() * lumi

    print "number of events: %s"%nEvents
    random = ROOT.TRandom3(1986)
    gRandom = random

    normalizedMassDist = inHist.Clone("normalized_mjj")
    normalizedMassDist.Scale(1./normalizedMassDist.Integral())
    nEventsFluctuated = random.Poisson(floor(nEvents))
    pseudoData = inHist.Clone("pseudo_data")
    pseudoData.Reset()
    pseudoData.FillRandom(normalizedMassDist, int(nEventsFluctuated))

    return pseudoData

#******************************************
def getDataLikeHist( eff, scaled, name, jobSeed = 10 ):
  dataLike = ROOT.TH1D(name, name, eff.GetXaxis().GetNbins(),eff.GetXaxis().GetXbins().GetArray())
  dataLike.SetDirectory(0)
  #random number generator
  rand3 = ROOT.TRandom3(1986) #1986 #ORIGINAL

  #Bin upper edge must be above this limit
  massLimit = 1000.

  #loop over bins
  for bin in xrange(1,eff.GetNbinsX()+1):
    #enough effective entries?
    nee = eff.GetBinContent(bin)
    if nee >= scaled.GetBinContent(bin) and scaled.GetXaxis().GetBinUpEdge(bin) > massLimit:

      #set seed
      #NOTE the seed for each bin must be always the same
      binSeed = int( round( eff.GetBinCenter(bin) + jobSeed*1e5 ))
      #print '  bin %s\t seed = %i'%(bin, binSeed)
      rand3.SetSeed(binSeed)

      #get data-like bin content by drawing entries
      #NOTE weights are poissonian by construction
      for jj in xrange( int( round( nee ) ) ):
        if rand3.Uniform() < scaled.GetBinContent(bin) / nee :
          dataLike.Fill(dataLike.GetBinCenter(bin))

  return dataLike

#******************************************
def getLumiFromFileName(fileName):
    #get luminosity value from file name
    keys = fileName.split('.')
    for ii in xrange(len(keys)):
        if keys[ii] == 'ifb':
            return keys[ii-1]
    raise SystemExit('\n***ERROR*** couldn\'t find luminosity for file: %s'%fileName)

#******************************************
def getSignalMassFromFileName(fileName):
    #get luminosity value from file name
    keys = fileName.split('.')
    for ii in xrange(len(keys)):
        if keys[ii] == 'GeV':
            return keys[ii-1]
    raise SystemExit('\n***ERROR*** couldn\'t find signal mass for file: %s'%fileName)

#******************************************
def getSignalModelFromFileName(fileName):
    #get luminosity value from file name
    keys = fileName.split('.')
    for ii in xrange(len(keys)):
        if keys[ii] == 'GeV':
            return keys[ii-2]
    raise SystemExit('\n***ERROR*** couldn\'t find signal model for file: %s'%fileName)

#******************************************
def getSampleInfo(path, dsid, i):
    #given a DSID, get the number of generated events from a table
    #table is samples.info
    #'i' is the i-th entry in the sample info table
    dsid = str(dsid)
    f = open(os.environ['ROOTCOREBIN']+'/../DijetResonanceAlgo/data/XsAcc_13TeV.txt', 'r')
    lines = f.read().splitlines()
    for line in lines:
        if line.startswith('#'): line = line[1:]
        info = line.split()
        if len(info) == 0: continue
        if info[0] == dsid:
            return info[i]
    print '\n***WARNING*** DSID %s not found in the library\n'%dsid
    return 0

#******************************************
def getSampleCrossSection(path, dsid):
    #given a DSID, get the cross-section
    return getSampleInfo(path, dsid, 1)

#******************************************
def getSampleAcceptance(path, dsid):
    #given a DSID, get the acceptance
    return getSampleInfo(path, dsid, 2)

#******************************************
def getSampleNEntries(path, dsid):
    #given a DSID, get the number of generated events from a table
    return getSampleInfo(path, dsid, 3)

#******************************************
#def getRawTreeFileList(path, tag1, tag2):
#    #NOTE given a path and two tags (to identify the right dataset),
#    #return the list of all the 'tree' files
#    #NOTE 'path' should be the path to the gridOutput/ directory
#
#    path+='/rawDownload/'
#    fileList = []
#
#    for d in os.listdir(path):
#        if not os.path.isdir( os.path.join(path,d) ): continue
#        if not '_tree.root' in d: continue
#        if not tag1 in d: continue
#        if not tag2 in d: continue
#        #print '  %s'%d
#
#        for f in os.listdir( os.path.join(path,d) ):
#            if not '.tree.root' in f: continue
#            #print '    %s'%f
#            fileList.append(path+'/'+d+'/'+f)
#
#    fileList.sort()
#    return fileList

#******************************************
def getMergedTreeFileList(path, tags):
    #NOTE given a path and tags (to identify the right dataset),
    #return the list of all the merged 'tree' files
    #NOTE 'path' should be the path to the gridOutput directory


    path+='/'
    fileList = []
    for f in os.listdir(path):
        if not os.path.isfile( os.path.join(path,f) ): continue
        if not '_tree.root' in f: continue
        if all(tag in f for tag in tags):
          fileList.append( os.path.join(path,f) )
        #print '  %s'%f

    fileList.sort()
    return fileList

#******************************************
def getCutflowEntries(path, tags, dsid):
    #NOTe given a path, tags and the DSID
    #get the number of events in the first bin of the cutflow
    #NOTE this is a backup solution, the cutflow histogram should be in the tree file

    path += '/cutflow/'
    events = 0
    fileList = []

    for f in os.listdir(path):
        if not os.path.isfile( os.path.join(path,f) ): continue
        if not 'user.' in f: continue
        if not '.root' in f: continue
        if not str(dsid) in f: continue
        if all(tag in f for tag in tags):
          fileList.append( os.path.join(path,f) )

    if len(fileList) < 1:
        raise SystemExit('\n***EXIT*** no cutflow file found')

    elif len(fileList) > 1:
        raise SystemExit('\n***EXIT*** too many cutflow files found')

    else:
        #print '  cutflow file: %s'%fileList[0]
        c = ROOT.TFile.Open(fileList[0],'READ')
        keys = c.GetListOfKeys()
        for key in keys:
            if key.ReadObj().GetTitle() == 'cutflow':
                events = key.ReadObj().GetBinContent(1)
                break

    return events
