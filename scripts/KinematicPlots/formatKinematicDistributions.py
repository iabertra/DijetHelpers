## other imports ##
import ROOT, plotUtils, math, os, sys, time

#******************************************
## set ATLAS style ##
import AtlasStyle
AtlasStyle.SetAtlasStyle()
#ROOT.gStyle.SetPalette(55)
#to avoid the z axis getting mangled
ROOT.gStyle.SetPadRightMargin(0.16);

#******************************************

inputFile = ROOT.TFile.Open("../../../Inputs/TestingTrees/dataLikeHists/dataLikeHistograms.QCDDiJet.v10.root","READ")
baseDirForPlots = "Plots"
ObjectBuffer = []

############
#Dictionaries
############

SelectionsDic = {
  
  "preselection" : "After preselection" , 
  "massSelection" : "Resonance selection" , 
  "angularSelection" : "Angular selection" , 
  "exclusiveOuterSelection" : "Exclusive outer events only" , 
  
  }

TreeTagsDic = {
  
  "Nominal" : "Nominal JES" , 
  "JET_GroupedNP_1__3down" : "-3#sigma, JES grouped NP 1" , 
  "JET_GroupedNP_1__3up" : "+3#sigma, JES grouped NP 1" , 
  "JET_GroupedNP_2__3down" : "-3#sigma, JES grouped NP 2" , 
  "JET_GroupedNP_2__3up" : "+3#sigma, JES grouped NP 2" , 
  "JET_GroupedNP_3__3down" : "-3#sigma, JES grouped NP 3" , 
  "JET_GroupedNP_3__3up" : "+3#sigma, JES grouped NP 3" , 
  "JET_RelativeNonClosure_MC12__3down" : "-3#sigma, JES MC12 non-closure" , #why do we even have this?
  "JET_RelativeNonClosure_MC12__3up" : "+3#sigma, JES MC12 non-closure" , 
  
  }

############
#Helper functions
############

def getNewCanvas(dividex = 0, dividey = 0) :
  #TODO: divide canvas in arbitrary pads
  c = ROOT.TCanvas()
  c.Divide(dividex, dividey)
  return c

def addLabels (x, y, selection, treeTag) :
  
  #slap ATLAS label on top
  AtlasStyle.ATLAS_LABEL(x, y, True)
  
  #identify where this plot is coming from
  selectionText = SelectionsDic[selection]
  treeTagText = TreeTagsDic[treeTag]
  #TODO: align the tag to the RHS of the ATLAS label, not by hand
  text = ROOT.TPaveText(x-0.1, y-0.08, x+0.238, y-0.01, "NDC NB")
  text.SetFillColor(0)
  #text = ROOT.TPaveText(0.5, 0.5, 0.8, 0.8, "NDC NB")
  text.AddText(selectionText+", "+treeTagText)
  text.Draw("same")
  ObjectBuffer.append(text)
  #add CoM, details, ...
  
  
def saveCanvas(c, directory, name = "test", fileFormat = "png") :
  #here we can make more file formats at the same time if needed
  c.SaveAs(directory+"/"+name+"."+fileFormat)
  del c
  
############
#Main code
############

##loop on directories of the file
for theDirectoryKey in inputFile.GetListOfKeys() :

  ############
  #Directory structure preparation
  ############
  
  #make directory structure for saving plots
  directoryName = theDirectoryKey.GetName()
  
  if not os.path.exists(baseDirForPlots+"/"+directoryName):
    os.makedirs(baseDirForPlots+"/"+directoryName)
  thisDirectory = inputFile.GetDirectory(directoryName)

  #keep information for file name - may be useful in latex
  treeTagName = directoryName.split("___")[0]
  selectionName = directoryName.split("___")[1]

  ############
  #Formatting for plots that need special formatting (don't loop over all keys)
  ############
  
  ###Plots that just need projections
  #TODO: make a function to project, maybe using a dictionary that is based on kinematicHistList.py?
  #Don't want to make projections of everything, too many plots - so maybe just leave them here
  
  ###Mjj histogram: project one of the histograms with fine mjj binning
  inputHistogram = inputFile.Get(directoryName+"/mjj_vs_etaLead")
  c = getNewCanvas()
  projectedHistogram = inputHistogram.ProjectionX()
  projectedHistogram.Draw()
  addLabels(0.58, 0.85, selectionName, treeTagName)
  saveCanvas(c, baseDirForPlots+"/"+directoryName, "eta_lead")    
  
  ###eta leading histogram: project one of the histograms with fine mjj binning
  inputHistogram = inputFile.Get(directoryName+"/mjj_vs_etaLead")
  c = getNewCanvas()
  projectedHistogram = inputHistogram.ProjectionY()
  ROOT.gPad.SetLogx()
  projectedHistogram.Draw()
  addLabels(0.58, 0.85, selectionName, treeTagName)
  saveCanvas(c, baseDirForPlots+"/"+directoryName, "mjj")    

  ###Plots that need special attention, like the chi plot
  
  ############
  #Plot formatting for all basic plots
  ############
  
  #loop on histograms within that directory
  for thePlotKey in thisDirectory.GetListOfKeys() :
    
    histogram = inputFile.Get(directoryName+"/"+thePlotKey.GetName())
    
    ###All 2D histograms: draw with COLZ option
    c = getNewCanvas()
    histogram.Draw("COLZ")
    #adding labels must go after drawing the histogram
    addLabels(0.58, 0.85, selectionName, treeTagName)
    saveCanvas(c, baseDirForPlots+"/"+directoryName, histogram.GetName())
    