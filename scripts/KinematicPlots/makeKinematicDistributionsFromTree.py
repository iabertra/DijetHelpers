#!/usr/bin/env python

#******************************************
#makeKinematicDistributionsFromTree.py
#******************************************

#******************************************
## argparse import, must be done before ROOT to preserve pyroot options ##
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--default", dest='default', action='store_true', default=False, help="Run the default configuration, expecting all QCD and QStar files")
parser.add_argument("--nominal", dest='nominal', action='store_true', default=False, help="Run on only the nominal tree")
parser.add_argument("--path", dest='path', default="/home/cate/Work/DijetSearches_Run2/20150422_MakeKinematicPlots/Inputs/TestingTrees/tree",
     help="Path to the input trees")
parser.add_argument("--tags", dest='tags', default="",
     help="Comma seperated list of tags. Input files if they contain all the tags")
parser.add_argument("--lumi", dest='lumi', default=1.0,
     type=float, help="Luminosity of the output sample")
parser.add_argument("--name", dest='outputName', default="QCDDiJet",
     help="Name of output file")
#CD: changed this from original script
parser.add_argument("--ystar", dest='yStarCut', default=0.6,
     type=float, help="yStar cut to apply")
parser.add_argument("--seed", dest='seed', default=10,
     type=int, help="Seed for getDataLikeHist")
args = parser.parse_args()

## other imports ##
import ROOT, plotUtils, math, os, sys, time
from kinematicHistList import * 
from math import exp

#******************************************
## set ATLAS style ##
import AtlasStyle
AtlasStyle.SetAtlasStyle()

#******************************************

def makeDataLikeFromTree(path, tags, lumi, outputName, yStarCut, seed):

  #------------------------------------------
  ## array of possible selections##
  Selections = ["preselection", "massSelection", "angularSelection", "exclusiveOuterSelection"] 

  print '\nmaking kinematic histograms'

  #------------------------------------------
  ## input parameters ##
  print '\nparameters:'
  print ' path:    %s'%path
  print ' tags:    %s'%tags
  print ' lumi [fb]: %s'%lumi
  print ' output name:    %s'%outputName

  tags = tags.split(',')
  #------------------------------------------
  ## get the files in path which match tags ##
  ## get list of merged output files ##
  inputFiles = plotUtils.getMergedTreeFileList(path, tags)

  print '\nmatching input files:'
  for file in inputFiles:
    print ' %s'%file

  ## make sure we have found some files ##
  if len(inputFiles) == 0:
    raise SystemExit('\n***EXIT*** no files found for tags: %s'%(tags))

  #------------------------------------------

  #------------------------------------------
  ## set error sum and overflow ##
  ROOT.TH1.SetDefaultSumw2()
  ROOT.TH1.StatOverflows()

  #------------------------------------------
  ## before creating histograms get all the different NP variations ##
  #open the first input file
  f = ROOT.TFile.Open(inputFiles[0],'READ')

  ## get list of trees with different NP variations ##
  trees = []
  treeTags = []
  print '\ntrees:'
  for key in f.GetListOfKeys():
    #if 'outTree' in key.GetName() and not 'JET' in key.GetName(): #for testing on Nominal only
    if 'outTree' in key.GetName() and (not args.nominal or 'outTree' == key.GetName()):
      print "Running on ", key.GetName()
      tree = key.GetName()
      trees.append(tree)
      treeTag = tree.lstrip('outTree')
      if treeTag == '':
        treeTag = 'Nominal'
      treeTags.append(treeTag)
      print ' %s \t%s'%(treeTag, tree)

  del f

  #------------------------------------------
  ## output file ##
  outputDir = path+'/../dataLikeHists/'
  print outputDir
  if not os.path.exists(outputDir):
    os.makedirs(outputDir)

  outputFile = ROOT.TFile.Open(outputDir+'/dataLikeHistograms.'+outputName+'.v'+str(args.seed)+'.root','UPDATE')

  ## create directory structure ##
  outputFile.cd()
  for treeTag in treeTags:
    for thisSelection in Selections :
      #TODO: find a better way to separate tree tag and selection?
      if not outputFile.GetDirectory(treeTag+"___"+thisSelection):
        outputFile.mkdir(treeTag+"___"+thisSelection)

  #------------------------------------------
  ## book histograms ##
  
  #those are for the vectors of vectors, when having loop on lumis, only for mjj
  scaledHistograms = []
  scaledMjjHists = []
  
  #this is for now a dictionary for vectors that contains the kinematic histograms for each of the trees
  #key = histogram name, value = dictionary containing the different selections with key = selection, value = histogram itself
  #it can also eventually be extended to plot different luminosities
  ScaledKinematicHistsDic = {}
  
  #loop on kinematic histograms we want to plot, make one dictionary to contain a histogram vector for each of the selections/tree
  for thisKinematicPlotName, thisKinematicPlotDic in KinematicPlotsDic.iteritems() :        
    ScaledKinematicHistsDic[thisKinematicPlotName] = {}
    #now loop on the selections
    for thisSelection in ["preselection", "massSelection", "angularSelection", "exclusiveOuterSelection"] :
      ScaledKinematicHistsDic[thisKinematicPlotName][thisSelection] = []
      #now loop on trees
      for itree, tree in enumerate(trees):
        xAxisBins = thisKinematicPlotDic["xAxisBins"]
        yAxisBins = thisKinematicPlotDic["yAxisBins"]
        xAxisTitle = thisKinematicPlotDic["xAxisTitle"]
        yAxisTitle = thisKinematicPlotDic["yAxisTitle"]
        #here we name all histograms the same for now, and when we write them out we put them in different directories
        print '\nNOTE the following "Replacing existing TH1" warnings are expected:'
        thisKinematicPlotHisto = plotUtils.get2DHist(thisKinematicPlotName, xAxisBins, xAxisTitle, yAxisBins, yAxisTitle)
        #here we could name according to the selection, but we already have the directory structure
        #thisKinematicPlotHisto.SetName(thisKinematicPlotName+"_"+thisSelection)
        ScaledKinematicHistsDic[thisKinematicPlotName][thisSelection].append(thisKinematicPlotHisto) 
      
  #print ScaledKinematicHistsDic
  #{'mjj_vs_chi': {'angularSelection': [], 'exclusiveOuterSelection': [], 'massSelection': [], 'preselection': []}}
  #where the vectors filled with one <ROOT.TH2D object ("mjj_vs_chi") at 0x27ec240> per tree 

  #this may be useful later...
  #Remove unecessary decimal points from lumi
  #if int(lumi) == lumi: 
    #lumi = int(lumi)
    #scaledMjjHists.append( plotUtils.getMassHist('mjj_Scaled_'+outputName+'_'+str(lumi).replace('.','p')+'fb', '13TeV') )

  #------------------------------------------
  ## loop over input files ##

  #acceptanceFile = open(outputDir+"/../Acceptances.txt", "w")

  for inputFile in inputFiles:
    #acceptanceFile.write( "File: "+os.path.basename(inputFile)+'\n' )

    #------------------------------------------
    ## open input file ##
    print '\nAdding file: %s'%os.path.basename(inputFile)
    f = ROOT.TFile.Open(inputFile,'READ')

    #------------------------------------------
    ## get number of events from cutflow ##
    sampleEvents = 0
    keys = f.GetListOfKeys()
    for key in keys:

      if 'cutflow' in key.GetName():
      #if 'cutflow' in key.GetName() and str(dsid) in key.GetName():
        sampleEvents = key.ReadObj().GetBinContent(1)
        continue

    if sampleEvents == 0:
      raise SystemExit('\n***WARNING*** no cutflow entries found')

    #------------------------------------------
    ## loop over trees ##
    for itree, tree in enumerate(trees):
      #------------------------------------------
      ## open tree ##
      print '  adding tree: %s'%tree
      t = f.Get(tree)

      if not t:
        print ' ***WARNING*** tree not found\n'
        continue

      #print "Acceptance of Tree ", tree, t.GetEntries(), "/", sampleEvents
      #acceptanceFile.write( "   Tree: "+tree+" "+str(t.GetEntries()) +' / '+str(sampleEvents)+'\n' )

      if t.GetEntries() == 0:
        print ' ***WARNING*** empty tree\n'
        continue

      #######
      ##Prepare the tree
      #######
      
      # turn off everything
      t.SetBranchStatus("*",0)
      
      #turn on the weight branch
      t.SetBranchStatus("weight",1)
      #turn on all the branches we're going to cut on
      t.SetBranchStatus("mjj",1)
      t.SetBranchStatus("yBoost",1)
      t.SetBranchStatus("yStar",1)
      t.SetBranchStatus("jet_pt",1)
      t.SetBranchStatus("jet_eta",1)

      #loop on the kinematic plots (note, not histograms!) we want to have
      for thisKinematicPlotName, thisKinematicPlotDic in KinematicPlotsDic.iteritems() :                
        for branchName in thisKinematicPlotDic["activeBranches"] :        
          # turn on necessary branches for this plot
          # TODO: is it better to turn on/off branches for each event? trade-off here is doing all plots in one single tree loop
          t.SetBranchStatus(branchName,1)
      
      #TODO: add possibility of printing cutflow somewhere

      #######
      ##Event loop
      #######        
      
      eventCounter = 0
      masseventCounter = 0
      for entry in t:
        
        #######
        ##Event selection shared by mass and angular
        #######
        #### TODO: make cuts options?
        
        # leading jet pT cut (already done at preselection level)
        if entry.jet_pt[0] < 400 : continue
        # leading and subleading jet eta cuts
        if abs(entry.jet_eta[0]) > 2.8 : continue
        if abs(entry.jet_eta[1]) > 2.8 : continue

        ##########
        #####Event selection, for mass and angular
        ##########
        
        passYBoostCut_angular = False
        passYStarCut_angular = False
        passYStarCut_mass = False
        isInInner_angular = False
        isInExclusiveOuter_angular = False
      
        # overall yStar cut
        if abs(entry.yStar) > 1.7: continue
        eventCounter = eventCounter+1

        passYStarCut_angular = True          

        #angular cuts, plus divide in inner/outer for FChi
        if abs(entry.yStar) < 0.6 : 
          isInInner_angular = True
          passYStarCut_mass = True
        else :
          isInExclusiveOuter_angular = True
        
        # yBoost cut
        if abs(entry.yBoost) <  1.1: 
          passYBoostCut_angular = True
          isInExclusiveOuter_angular = (isInExclusiveOuter_angular and passYBoostCut_angular)
        
        ##########
        #####Filling histograms
        ##########
        
        #loop on histograms we want to plot
        for thisKinematicPlotName, thisKinematicPlotDic in KinematicPlotsDic.iteritems() :
          
          xVariableFormula = thisKinematicPlotDic["xVariableFormula"]
          yVariableFormula = thisKinematicPlotDic["yVariableFormula"]
          xVariableValue = eval(xVariableFormula)
          yVariableValue = eval(yVariableFormula)  
          
          #now do the various selections separately
          #the reason we don't use the cut bools as weight is that the number of entries
          #is not going to correspond to the number of events passing the cut
          
          #"preselection" : no cuts beyond what we have already done above
          ScaledKinematicHistsDic[thisKinematicPlotName]["preselection"][itree].Fill(xVariableValue, yVariableValue, entry.weight*float(lumi) / sampleEvents)
                    
          #"massSelection", corresponding to inner
          if passYStarCut_mass :
            ScaledKinematicHistsDic[thisKinematicPlotName]["massSelection"][itree].Fill(xVariableValue, yVariableValue, entry.weight*float(lumi) / sampleEvents)
            
            
          #"angularSelection"
          if passYStarCut_angular and passYBoostCut_angular :
            ScaledKinematicHistsDic[thisKinematicPlotName]["angularSelection"][itree].Fill(xVariableValue, yVariableValue, entry.weight*float(lumi) / sampleEvents)
          
          #"exclusiveOuterSelection" :
          if isInExclusiveOuter_angular :
            ScaledKinematicHistsDic[thisKinematicPlotName]["exclusiveOuterSelection"][itree].Fill(xVariableValue, yVariableValue, entry.weight*float(lumi) / sampleEvents)          
          
                  
      #------------------------------------------
      #TODO: make them add up and do a proper cutflow
      #print '  Total events available in this file: %s'%t.GetEntries()
      #print '  Events passing preselection: %s'%t.GetEntries()
      #print '  Events passing mass selection: %s'%t.GetEntries()
      #print '  Events passing mass selection: %s'%t.GetEntries()
      
      del t

    f.Close()
    #acceptanceFile.close()
    
    #######
    ##Filling histograms
    #######
    
    #loop over the scaled kinematic histograms
    for thisKinematicPlotName, thisSelectionDic in ScaledKinematicHistsDic.iteritems():
      #print thisSelection, thisArrayOfHistograms
      for thisSelection in Selections :
        #note that we have made thisArrayOfHistograms index-parallel with the trees
        thisArrayOfHistograms = thisSelectionDic[thisSelection]
        #write the histogram in the right directory, using the selection and the tree tag
        for itree, treeTag in enumerate(treeTags):
          thisTreeAndSelectionHistogram = thisSelectionDic[thisSelection][itree]
          thisTreeAndSelectionDirectoryName = treeTag+"___"+thisSelection
          outputFile.cd(thisTreeAndSelectionDirectoryName)
          thisTreeAndSelectionHistogram.Write(thisTreeAndSelectionHistogram.GetName(), ROOT.TObject.kOverwrite)
          
    ####------------------------------------------
    ####END writing loop

  outputFile.Close()

#******************************************
if __name__ == '__main__':

  #------------------------------------------
  ## Do the default configuration ##
  if args.default:
   lumies = [1]
   #signalTypes = ["ExcitedQ2000","ExcitedQ2500","ExcitedQ3000","ExcitedQ3500","ExcitedQ4000","ExcitedQ4500","ExcitedQ5000","ExcitedQ5500","ExcitedQ6000","ExcitedQ7000"]

   for lumi in lumies:

    QCDTag = "Pythia8EvtGen_A14NNPDF23LO_jetjet"
    outputName = "QCDDiJet"
    makeDataLikeFromTree(args.path, QCDTag, lumi, outputName, args.yStarCut, args.seed)

    #for signalTag in signalTypes:
     #outputName = signalTag.replace("ExcitedQ", "QStar")
     #makeDataLikeFromTree(args.path, signalTag, lumi, outputName, args.yStarCut, args.seed)

  #------------------------------------------
  ## Do a single configuration ##
  else:
   makeDataLikeFromTree(args.path, args.tags, args.lumi, args.outputName, args.yStarCut, args.seed)

  print '\ndone'
