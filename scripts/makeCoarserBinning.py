#!/usr/bin/env python

#******************************************
#makeCoarserBinning.py

#Given a file with a list of histograms, create coarser binning
#******************************************

import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--file", dest='file', default="",
         help="Filename that contains the histogram")
parser.add_argument("--binType", dest='binType', default="Coarse13TeV",
         help="Binning Type used by plotUtils.py")
args = parser.parse_args()


#******************************************
#import stuff
import ROOT, plotUtils, math, os, sys, array

#******************************************
#set ATLAS style
import AtlasStyle
AtlasStyle.SetAtlasStyle()

#******************************************
def makeCoarserBinning(file, binType):

    print('\nmaking coarser histograms')

    #------------------------------------------
    #input parameters
    print '\nparameters:'
    print '  file:      %s'%file
    print '  bin type: %s'%binType

    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    # Get Binning #
    massBins = plotUtils.getMassBins( binType )
    nBins = len(massBins)-1
    newBins = array.array('d', massBins)


    #------------------------------------------
    #open input file
    f = ROOT.TFile.Open(file,'READ')
    outName = file.replace('.', '_Coarse.', 1) #Add Coarse to first field of name
    outFile = ROOT.TFile.Open(outName, 'UPDATE')

    if not f:
        raise SystemExit('\n***ERROR*** input file not found: %s'%file)

    ## Go through all systematics or histograms TDirectories ##
    for key in f.GetListOfKeys():
      obj = f.Get( key.GetName() )
      if type(obj) == ROOT.TDirectoryFile:
        outFile.mkdir( obj.GetName() ) #Create directory
        newDir = outFile.Get( obj.GetName() )

        for subKey in obj.GetListOfKeys():
          subObj = obj.Get( subKey.GetName() )
          if type(subObj) != ROOT.TH1F and type(subObj) != ROOT.TH1D:
            print "Error, type", type(subObj), "of", subObj.GetName(), "within TDirectory", obj.GetName(), "is not supported"
            continue


          newHist = subObj.Rebin(nBins, subObj.GetName(), newBins)
          newHist.SetDirectory(0)
          newDir.cd()
          newHist.Write("", ROOT.TObject.kOverwrite)

      elif type(obj) == ROOT.TH1F and type(obj) == ROOT.TH1D:
        newHist = obj.Rebin(nBins, "", newBins)
        newHist.SetDirectory(0)
        outFile.cd()
        newHist.Write("", ROOT.TObject.kOverwrite)

      else:
        print "Error, type", type(obj), "of", obj.GetName(), "is not supported"




#******************************************
if __name__ == '__main__':

    #------------------------------------------

    makeCoarserBinning( args.file, args.binType)

    #------------------------------------------
    print '\ndone'
