#!/usr/bin/env python
#####################################################################################################
# configureInputHists.py
#
# This script first creates combined QStar+QCD files for all QStar* files within PATH.
# It records the available luminosities during this process.
#
# Afterwards the script creates a file structure necessary for the Statistical analysis package,
# using only the scaled QStar files.  This transforms the original files structure:
#
# RunName (folder)
#   MassPoint (file)
#     Systematic_Variation (TDirectory)
#       mjj_HistType_MassPoint_Lumi (TH1D)
#
# to
#
# RunName (folder)
#   Lumi (folder)
#     MassPoint_Lumi (ROOT file)
#       mjj_MassPoint_Lumi_Systematic_Variation (TH1D)
#
#  For further information contact Jeff.Dandoy@cern.ch
#####################################################################################################

import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-v", dest='verbose', action='store_true', default=False, help="Verbose mode for debugging")
parser.add_argument("--path", dest='path', default="./gridOutput/dataLikeHists/",
         help="Directory with signal and background files")
parser.add_argument("--signal", dest='signalType', default="QStar",
         help="String to check for signal files")
parser.add_argument("--signalName", dest='signalName', default="",
         help="Generator name of the file to use for output.  Defaults to --signal")
parser.add_argument("--bkg", dest='bkgType', default="QCDDiJet",
         help="String to check for background file")
parser.add_argument("--noCombination", dest='noCombination', action='store_true', default=False, help="Do not combine signal and background")
args = parser.parse_args()

import ROOT, glob, os, time

################# Combine the Signal and Bkg Files ####################
def combinedSignalWithBkg():

  lumiList = [] #list of luminosities that exist

  ################## Grab QCD ########################
  QCDFileNames = glob.glob(args.path+"/*"+args.bkgType+".*root");
  if not len(QCDFileNames) == 1:
    print "ERROR, we found ", len(QCDFileNames), "potential QCD files"
    exit(0)
  else:
    QCDFile = ROOT.TFile.Open(QCDFileNames[0], "READ")


  ################## Grab Signal Files ########################
  signalFiles = glob.glob(args.path+"/dataLikeHistograms."+args.signalType+"*.root");
  if len(signalFiles) == 0:
    print "ERROR, we found no potential "+args.signalType+" files in "+args.path
    exit(0)

  for signalFileName in signalFiles:
    signalFile = ROOT.TFile.Open(signalFileName, "READ")
    combinedFile = ROOT.TFile.Open(os.path.dirname(signalFileName)+'/'+os.path.basename(signalFileName).replace(args.signalType, args.bkgType+'With'+args.signalType), "RECREATE")

    sysVars = signalFile.GetListOfKeys()
    #### For each systematic variation ####
    for sysVar in sysVars:
      if(args.verbose):  print "Systematics Directory", sysVar.GetName()
      # !! only do 1 sigma variation combination (only 1sigma made for QCD)
      if not sysVar.GetName().endswith("Nominal") and not sysVar.GetName().endswith("1down") and not sysVar.GetName().endswith("1up"):
        continue
      if "AFII" in sysVar.GetName():
        continue
      combinedFile.mkdir( sysVar.GetName() )
      combinedDir = combinedFile.Get(sysVar.GetName())

      signalDir = signalFile.Get(sysVar.GetName())
      # !! temporary replacement to combine different sigmas!
      #QCDDir = QCDFile.Get(sysVar.GetName().replace('3down','1down').replace('3up','1up') )
      QCDDir = QCDFile.Get(sysVar.GetName() )
      if not QCDDir:
        print "ERROR: Could not find QCD TDirectory with name", sysVar.GetName(), "in file ", QCDFileNames[0]
        exit(1)

      hists = signalDir.GetListOfKeys()
      for hist in hists:
        histName = hist.GetName()
        if(args.verbose):  print "     Copying histogram", histName

        ## Make sure this luminosity is in lumiList ##
        thisLumi = histName.split('_')[3]
        if not thisLumi in lumiList:
          lumiList.append(thisLumi)

        thisSignalHist = signalDir.Get(histName)

        ## The datalike signal histogram will not exist if it doesn't have enough effective entries
        if not thisSignalHist:
          continue

        thisSignalHist.SetName( (thisSignalHist.GetName()).replace(args.signalType, args.bkgType+'With'+args.signalType) )
        thisSignalHist.SetTitle( (thisSignalHist.GetName()).replace(args.signalType, args.bkgType+'With'+args.signalType) )

        ## Get equivalent QCD name ##
        QCDHistName = histName.split('_')
        QCDHistName[2] = args.bkgType
        QCDHistName = '_'.join(QCDHistName)
        thisQCDHist = QCDDir.Get(QCDHistName)
        if not thisQCDHist or thisQCDHist.GetEntries() == 0:
          continue
        if not thisQCDHist:
          print "ERROR: Could not find QCD histogram with name", QCDHistName, "in file/TDirectory:", QCDFileNames[0], "/", sysVar.GetName()
          exit(1)
        firstBin = thisQCDHist.FindFirstBinAbove(0)

        ## Need to set signal bins to zero if QCD doesn't have enough stats ##
        for iBin in range(1, firstBin+1):
          thisSignalHist.SetBinContent( iBin, 0. )
          thisSignalHist.SetBinError( iBin, 0. )

        thisSignalHist.Add ( thisQCDHist )
        thisSignalHist.SetDirectory( combinedDir )

    combinedFile.Write()
    combinedFile.Close()
    signalFile.Close()
  return lumiList

def getLumiListOnly():
  lumiList = [] #list of luminosities that exist

  ################## Grab Signal Files ########################
  signalFiles = glob.glob(args.path+"/dataLikeHistograms."+args.signalType+"*.root");
  if len(signalFiles) == 0:
    print "ERROR, we found no potential "+args.signalType+" files in "+args.path
    exit(0)

  for signalFileName in signalFiles:
    signalFile = ROOT.TFile.Open(signalFileName, "READ")
    sysVars = signalFile.GetListOfKeys()
    #### For each systematic variation ####
    for sysVar in sysVars:
      if(args.verbose):  print "Systematics Directory", sysVar.GetName()

      signalDir = signalFile.Get(sysVar.GetName())
      hists = signalDir.GetListOfKeys()

      for hist in hists:
        histName = hist.GetName()
        if(args.verbose):  print "     Copying histogram", histName

        ## Make sure this luminosity is in lumiList ##
        thisLumi = histName.split('_')[3]
        if not thisLumi in lumiList:
          lumiList.append(thisLumi)

    signalFile.Close()
  return lumiList

################# Group Signal Files for Statistical Code ####################
def arrangeSignalForStats(lumiList):
  for lumi in lumiList:
    if(args.verbose):  print "Making directory for luminosity", lumi
    if not os.path.exists( args.path+'/StatisticalHists/'+lumi ):
      os.makedirs(args.path+'/StatisticalHists/'+lumi)
    ################## Grab Signal Files ########################
    signalFiles = glob.glob(args.path+"/dataLikeHistograms."+args.signalType+"*.root");

    for signalFileName in signalFiles:
      signalFile = ROOT.TFile.Open(signalFileName, "READ")
      massPointName = os.path.basename(signalFileName).split('.')[1]
      massPointName = massPointName.replace(args.signalType, args.signalName)
      outFile = ROOT.TFile.Open(args.path+'/StatisticalHists/'+lumi+'/'+massPointName+'_'+lumi+'.root', "RECREATE")

      sysVars = signalFile.GetListOfKeys()
      #### For each systematic variation ####
      for sysVar in sysVars:
        sysName = sysVar.GetName()
        signalDir = signalFile.Get(sysName)

        hists = signalDir.GetListOfKeys()
        for hist in hists:
          if lumi == hist.GetName().split('_')[-1] and "Scaled" in hist.GetName():
            thisSignalHist = signalDir.Get(hist.GetName())
            thisSignalHist.SetName("mjj_"+massPointName+'_'+lumi+'_'+sysName)
            thisSignalHist.SetTitle("mjj_"+massPointName+'_'+lumi+'_'+sysName)
            thisSignalHist.SetDirectory( outFile )

      outFile.Write()
      outFile.Close()
      signalFile.Close()

if __name__ == "__main__":

    if len(args.signalName) == 0:
      args.signalName = args.signalType
    print "Beginning combination"
    if args.noCombination:
      lumiList = getLumiListOnly()
    else:
      lumiList = combinedSignalWithBkg()
    print "Done combination, beginning Stats structure"
    arrangeSignalForStats(lumiList)
    print "Finished configureInputHists.py"
