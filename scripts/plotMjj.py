#!/bin/python

#******************************************
#import stuff
import ROOT
import sys, os
import plotUtils

#******************************************
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def plotMjj(inputDataFilesPath, dataTags, inputMCFilesPath, MCTags, tag):

    print '\n******************************************'
    print 'plotting mjj distribution'
    print '\nparameters:'
    print '  input data files path: %s'%inputDataFilesPath
    print '  data tags:             %s'%dataTags
    print '  input MC files path:   %s'%inputMCFilesPath
    print '  MC tag:                %s'%MCTags
    print '  tag:                   %s'%tag

    #------------------------------------------
    #first things first
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))

    #------------------------------------------
    logx=True
    line = ROOT.TLine()
    line.SetLineColor(ROOT.kBlack)

    #------------------------------------------
    #get input data files
    dataTags = dataTags.split('+')
    dataFilesNames = plotUtils.getMergedTreeFileList(inputDataFilesPath, dataTags)
    
    if len(dataFilesNames) == 0:
        raise SystemExit('\n***EXIT*** no data files found for tags: %s'%(dataTags))
  
    print '\nmatching input data files:'
    for file in dataFilesNames:
        print ' %s'%file

    #------------------------------------------
    #get input MC files
    MCTags = MCTags.split('+')
    MCFilesNames = plotUtils.getMergedTreeFileList(inputMCFilesPath, MCTags)
    
    if len(MCFilesNames) == 0:
        raise SystemExit('\n***EXIT*** no MC files found for tags: %s'%(MCTags))
  
    print '\nmatching input MC files:'
    for file in MCFilesNames:
        print ' %s'%file
    
    #------------------------------------------
    #open data input files and get histograms
    print ''
    for ii in xrange(len(dataFilesNames)):
        if not os.path.isfile(dataFilesNames[ii]):
            raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%dataFilesNames[ii])
        f = ROOT.TFile(dataFilesNames[ii])

        #get list of histograms and pick mjj
        tkeys = f.GetListOfKeys()
        keys = []
        for tkey in tkeys:
            if 'Scaled_mjj_data15_13TeV' in tkey.GetName():
                mjjDataHistName = tkey.GetName()
        if mjjDataHistName:
            print 'mjj data hist: %s'%mjjDataHistName

        #clone the first hist of the list and then add the others
        if ii == 0:
            mjj_data = f.Get(mjjDataHistName).Clone('mjj_data')
            mjj_data.SetDirectory(0)
        else:
            mjj_data.Add(f.Get(mjjDataHistName), 1.)
        
        del f
            
    #------------------------------------------
    #open MC input files and get histograms
    print ''
    for ii in xrange(len(MCFilesNames)):
        if not os.path.isfile(MCFilesNames[ii]):
            raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%MCFilesNames[ii])
        f = ROOT.TFile(MCFilesNames[ii])

        #get list of histograms and pick mjj
        tkeys = f.GetListOfKeys()
        keys = []
        for tkey in tkeys:
            if 'Scaled_mjj_mc15_13TeV' in tkey.GetName():
                mjjMCHistName = tkey.GetName()
        if mjjMCHistName:
            print 'mjj MC hist: %s'%mjjMCHistName

        #clone the first hist of the list and then add the others
        if ii == 0:
            mjj_mc = f.Get(mjjMCHistName).Clone('mjj_mc')
            mjj_mc.SetDirectory(0)
        else:
            mjj_mc.Add(f.Get(mjjMCHistName))

        del f

    #------------------------------------------
    #print hist info
    print '\nhist info:\t\tdata\tMC'
    print 'integral:\t\t%s\t%s'%(mjj_data.Integral(), mjj_mc.Integral())
    print 'integral (width):\t%s\t%s'%(mjj_data.Integral('width'), mjj_mc.Integral('width'))
    print 'entries:\t%s\t%s'%(mjj_data.GetEntries(), mjj_mc.GetEntries())

    #------------------------------------------
    #scale MC to data
    mjj_mc.Scale(mjj_data.Integral()/mjj_mc.Integral())

    #------------------------------------------
    #print hist info
    print '\nhist info:\t\tdata\tMC'
    print 'integral:\t\t%s\t%s'%(mjj_data.Integral(), mjj_mc.Integral())
    print 'integral (width):\t%s\t%s'%(mjj_data.Integral('width'), mjj_mc.Integral('width'))
    print 'entries:\t%s\t%s'%(mjj_data.GetEntries(), mjj_mc.GetEntries())
    
    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    #canvas
    c = ROOT.TCanvas('c', 'c', 400, 50, 800, 600)

    #pads
    outpad = ROOT.TPad("extpad","extpad", 0., 0.,   1., 1.)
    pad1   = ROOT.TPad("pad1",  "pad1",   0., 0.33, 1., 1.)
    pad2   = ROOT.TPad("pad2",  "pad2",   0., 0.,   1., 0.33)

    #setup drawing options
    outpad.SetFillStyle(4000)#transparent

    pad1.SetBottomMargin(0.)#00001)#CHECK
    pad1.SetBorderMode(0)

    pad2.SetTopMargin(0.)#00001)#CHECK
    pad2.SetBottomMargin(0.3)
    pad2.SetBorderMode(0)

    pad1.Draw()
    pad2.Draw()
    outpad.Draw()

    #canvas and pads
    c = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    pad1 = ROOT.TPad("pad1","pad1",0, 0.5, 1, 1.0)#top
    pad2 = ROOT.TPad("pad2","pad2",0, 0.3, 1, 0.5)#central
    pad3 = ROOT.TPad("pad3","pad3",0, 0.0, 1, 0.3)#bottom
    
    pad1.SetTopMargin(0.08)
    pad1.SetBottomMargin(0.0)
    pad1.SetBorderMode(0)
    pad1.Draw()

    pad2.SetTopMargin(0.0)
    pad2.SetBottomMargin(0.0)
    pad2.SetBorderMode(0)
    pad2.Draw()

    pad3.SetTopMargin(0.0)
    pad3.SetBottomMargin(1./3.)
    pad3.SetBorderMode(0)
    pad3.Draw()
    
    #------------------------------------------
    #draw histograms
    pad1.cd()
    pad1.SetLogx(logx)
    pad1.SetLogy(1)
    pad1.SetGrid(1,1)

    mjj_data.GetYaxis().SetTitle("events/bin")
    mjj_data.GetYaxis().SetTitleFont(43)
    mjj_data.GetYaxis().SetTitleSize(20)
    mjj_data.GetYaxis().SetLabelFont(43)
    mjj_data.GetYaxis().SetLabelSize(20)
    mjj_data.GetXaxis().SetTitle("m_{jj} [GeV]")
    mjj_data.GetXaxis().SetTitleFont(43)
    mjj_data.GetXaxis().SetTitleSize(20)
    mjj_data.GetXaxis().SetLabelFont(43)
    mjj_data.GetXaxis().SetLabelSize(20)
    mjj_data.SetMarkerStyle(0)
    mjj_data.SetMarkerColor(ROOT.kRed)
    mjj_data.SetLineColor(ROOT.kRed)
    mjj_data.SetFillStyle(0)
    mjj_data.SetLineWidth(2)
    #ratio = mjj_data.Clone()#moved up here, before setting y-axis range
    mjj_data.Draw("p")#draw then set y-axis range
    #mjj_data.GetYaxis().SetRangeUser(0.1,ROOT.gPad.GetFrame().GetY2())#set y-axis low range #OK
    ymax = ROOT.gPad.GetFrame().GetY2()#TEST
    #print ymax

    mjj_mc.SetMarkerStyle(0)
    mjj_mc.SetMarkerColor(ROOT.kBlue)
    mjj_mc.SetLineColor(ROOT.kBlue)
    mjj_mc.SetFillStyle(0)
    mjj_mc.SetLineWidth(2)
    mjj_mc.GetYaxis().SetRangeUser(0.1,ymax)
    #mjj_mc.Draw("same p")
    
    hs = ROOT.THStack('hs','hs')
    hs.Add(mjj_data)
    hs.Add(mjj_mc)
    hs.Draw('nostack')

    hs.GetYaxis().SetTitle("events/bin")
    hs.GetYaxis().SetTitleFont(43)
    hs.GetYaxis().SetTitleSize(20)
    hs.GetYaxis().SetLabelFont(43)
    hs.GetYaxis().SetLabelSize(20)
    #print ROOT.gPad.GetFrame().GetY2()
    #hs.GetYaxis().SetRangeUser(0.1,ymax)#set y-axis low range #TEST
    
    hs.GetXaxis().SetTitle("m_{jj} [GeV]")
    hs.GetXaxis().SetLabelFont(43)
    hs.GetXaxis().SetLabelSize(20)
    hs.GetXaxis().SetLabelFont(43)
    hs.GetXaxis().SetLabelSize(20)

    hs.Draw('nostack')
    
    c.Update()

    #------------------------------------------
    #draw ratio
    pad2.cd()
    pad2.SetLogx(logx)
    pad2.SetLogy(0)
    pad2.SetGrid(1,1)

    ratio = mjj_data.Clone()#moved up
    ratio.Divide(mjj_mc)
    ratio.SetName('ratio')
    ratio.SetTitle('ratio')

    ratio.SetMarkerStyle(0)
    ratio.SetMarkerColor(ROOT.kBlack)
    ratio.SetLineColor(ROOT.kBlack)
    ratio.SetFillStyle(0)
    ratio.SetLineWidth(2)
    
    ratio.GetYaxis().SetTitle('data/MC')
    ratio.GetYaxis().SetTitleFont(43)
    ratio.GetYaxis().SetTitleSize(20)
    ratio.GetYaxis().SetLabelFont(43)
    ratio.GetYaxis().SetLabelSize(20)

    ratio.GetXaxis().SetTitle('m_{jj} [GeV]')
    ratio.GetXaxis().SetTitleFont(43)
    ratio.GetXaxis().SetTitleSize(20)
    ratio.GetXaxis().SetTitleOffset(4.0)
    ratio.GetXaxis().SetLabelFont(43)
    ratio.GetXaxis().SetLabelSize(20)
    ratio.GetYaxis().SetMoreLogLabels(1)

    ratio.Draw('E')
    ratio.SetAxisRange(0.0,2.0,'Y')

    lowx = ratio.GetBinLowEdge(1)
    highx = ratio.GetBinLowEdge(ratio.GetNbinsX()+1)
    line.DrawLine(lowx,1.,highx,1.)
    
    c.Update()

    #------------------------------------------
    #draw difference
    pad3.cd()
    pad3.SetLogx(logx)
    pad3.SetLogy(0)
    pad3.SetGrid(1,1)

    diff = mjj_data.Clone()
    diff.Add(mjj_mc, -1)
    diff.SetName('diff')
    diff.SetTitle('diff')

    diff.SetMarkerStyle(0)
    diff.SetMarkerColor(ROOT.kBlack)
    diff.SetLineColor(ROOT.kBlack)
    diff.SetFillStyle(0)
    diff.SetLineWidth(2)
    
    diff.GetYaxis().SetTitle('data - MC')
    diff.GetYaxis().SetTitleFont(43)
    diff.GetYaxis().SetTitleSize(20)
    diff.GetYaxis().SetLabelFont(43)
    diff.GetYaxis().SetLabelSize(20)

    diff.GetXaxis().SetTitle('m_{jj} [GeV]')
    diff.GetXaxis().SetTitleFont(43)
    diff.GetXaxis().SetTitleSize(20)
    diff.GetXaxis().SetTitleOffset(4.0)
    diff.GetXaxis().SetLabelFont(43)
    diff.GetXaxis().SetLabelSize(20)
    diff.GetYaxis().SetMoreLogLabels(1)

    diff.Draw('E')
    #diff.SetAxisRange(0.8,1.2,'Y')

    lowx = diff.GetBinLowEdge(1)
    highx = diff.GetBinLowEdge(diff.GetNbinsX()+1)
    line.DrawLine(lowx,0.,highx,0.)

    c.Update()
    
    #------------------------------------------
    #draw labels and legend
    pad1.cd()

    #ATLAS
    aX = 0.65
    aY = 0.80
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
    a.DrawLatex(aX,aY,'ATLAS')
		
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    p.DrawLatex(aX+0.13,aY,'internal')
    
    #note
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(43)
    n.SetTextColor(1)
    n.SetTextSize(20)
    n.DrawLatex(aX,aY-0.10,'#sqrt{s} = 13 TeV, 50 ns')
    n.DrawLatex(aX,aY-0.20,'L_{int} = 0.072 fb^{-1}')

    #legend
    lXmin = aX
    lXmax = aX+0.15
    lYmin = aY-0.25
    lYmax = aY-0.35
    l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(15)
    l.AddEntry(mjj_data,'data','pl')
    l.AddEntry(mjj_mc,'Pythia NLO+EW','pl')
    l.Draw()
    
    c.Update()
    c.WaitPrimitive()
    if tag == 'ok' or tag == 'note' or tag == '':
        c.SaveAs(localdir+'/figures/mjj.data.MC.pdf')
    else:
        c.SaveAs(localdir+'/figures/mjj.data.MC.'+tag+'.pdf')
    del c

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

#******************************************
if __name__ == "__main__":
    
    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 6:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \nplot mjj comparison \
            \nHOW TO: python -u plotMjj.py inputDataFilesPath dataTags inputMCFilesPath MCTags tag'
            %(len(sys.argv), 6))
    
    #------------------------------------------
    #input parameters
    inputDataFilesPath = sys.argv[1].strip()
    dataTags = sys.argv[2].strip()
    inputMCFilesPath = sys.argv[3].strip()
    MCTags = sys.argv[4].strip()
    tag = sys.argv[5].strip()

    plotMjj(inputDataFilesPath, dataTags, inputMCFilesPath, MCTags, tag)
    
    #------------------------------------------
    print '\ndone'
