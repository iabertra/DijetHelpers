#!/usr/bin/env python

###########################################################
# reweightTrees.py                                        #
# A short script to reweight TTrees by their initial      #
# event number.  Allows for directly combining TTrees.    #
# For more information contact Jeff.Dandoy@cern.ch        #
###########################################################

import glob, array, argparse
#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--pathToTrees", dest='pathToTrees', default="./gridOutput/tree", help="Path to the input trees")
parser.add_argument("--treeName", dest='treeName', default="outTree", help="Name of trees to be reweighted")

args = parser.parse_args()


from ROOT import *

def reweightTrees():
  files = glob.glob(args.pathToTrees+"/*.root")
  treeName = "outTree"

  for file in files:

    print "Reweighting file ", file

    inFile = TFile.Open(file, "UPDATE")
    tree = inFile.Get( args.treeName )

    for key in inFile.GetListOfKeys():
      if 'cutflow' in key.GetName() and 'weight' not in key.GetName():
        cutFlow = inFile.Get( key.GetName() )
    if not cutFlow:
      print "ERROR, no cutflow file found"
      exit(1)

    scaleFactor = 1./cutFlow.GetBinContent(1)

    newTree = tree.CloneTree(0)
    newTree.SetName("corrected_"+args.treeName)

    weight = array.array('f', [0])
    tree.SetBranchAddress("weight", weight)
    i = 0
    while tree.GetEntry(i):
      i += 1
      weight[0] = weight[0] * scaleFactor
      newTree.Fill()

    newTree.Write("", TObject.kOverwrite)


if __name__ == "__main__":
  reweightTrees()
  print "Finished reweightTrees()"
