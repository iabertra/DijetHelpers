#!/usr/bin/env python

##############################################################
#
# generatePseudoData.py
#
# Generate a data like mjj distribution for certain luminosities
# given a fit and it's parameters
#
##############################################################


import os, sys
from array import array
import time
import argparse
import copy
from math import sqrt, log, isnan, isinf

#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--minMjj", dest='minMjj', default=1500,
         type=int, help="Minimum Mjj mass to begin generating events")
parser.add_argument("--comEnergy", dest='comEnergy', default=13,
         type=int, help="Center of Mass Energy")
parser.add_argument("--effectiveLumi", dest='effectiveLumi', default=30,
         help="Effective Luminosity to scale to")
parser.add_argument("--fitToUse", dest='fitToUse', default="STD",
         help="Single fit name to use")
parser.add_argument("--params", dest='params', default=[0.067, 12.29, -2.55, 0.8], #Edited STD
#parser.add_argument("--params", dest='params', default=[0.335, 12.29, -2.55, 0.49], #original STD
#parser.add_argument("--params", dest='params', default=[0.0118, 9.1, -4.98], #original CDF
         type=int, nargs='+', help="List of parameters for the function.  This should correspond to a fit to 1 fb^{-1}")
parser.add_argument("--systematic", dest='systematic', default="Nominal",
         help="Name of systematic directory to put in results")
parser.add_argument("--seed", dest='seed', default=10,
         type=int, help="Seed for generating events")

args = parser.parse_args()


from ROOT import *
import DijetFuncs
from Minuit2Fit import FitWithRetries, Chi2LogFit, groupFNC, GetPVal, PoissonFluctuate
sys.path.insert(0, '../scripts/')
import plotUtils

def main():

  ## We want a small cushion ##
  args.lumi = args.lumi+0.1
  ## Initialize space ##
  DijetFuncs.initializeGlobals()

  ## Setup Naming convention ##
  #histType = 'PseudoData'
  #sampleType = 'QCDDiJet'
  histType = 'Scaled'
  sampleType = args.fitToUse

  ## Get binning ##
  outputHist = plotUtils.getMassHist( "outputHist", str(args.comEnergy)+"TeV" ) #binning histogram to be saved in output
  startBin = outputHist.FindBin( args.minMjj )

  ###Choose which functions to fit ###
  fList = DijetFuncs.getFits( [args.fitToUse] )

  ### Setup Function, assuming for 1fb ###
  DijetFuncs.thisFunction = TF1(fList["fNames"][0], fList["fEqs"][0], outputHist.GetXaxis().GetBinLowEdge(1), outputHist.GetXaxis().GetBinUpEdge( outputHist.GetNbinsX()+1) )
  for iParam in range(len(fList["fParams"][0])):
    DijetFuncs.thisFunction.SetParameter(iParam, args.params[iParam] )

  newFile = TFile.Open("pseudoData_"+args.fitToUse+"_v"+str(args.seed)+".root", "RECREATE")
  newFile.mkdir( args.systematic )
  newFile.cd( args.systematic )

  rand3 = TRandom3(1986)

  for lumi in args.lumies:
    ## Set name for this Lumi ##
    outputHist.SetName('mjj_'+histType+'_'+sampleType+'_'+str(lumi).replace('.','p')+'fb')
    outputHist.SetTitle('mjj_'+histType+'_'+sampleType+'_'+str(lumi).replace('.','p')+'fb')

    ## Set content of each bin ##
    for iBin in range(startBin, outputHist.GetNbinsX()+1):

      # We want effective entries to be just above bin content, but not small enough to give incorrect poisson statistics in the datalike step #
      # so we set effective entries = N + 20*sqrt(N), or allow for 20 sigma variations
      # effective entries = N**2 / err**2
      # therefore we set err = N / sqrt(N + 20*sqrt(N))
      nomCont = DijetFuncs.thisFunction.Integral(outputHist.GetXaxis().GetBinLowEdge(iBin), outputHist.GetXaxis().GetBinUpEdge(iBin))
      err = nomCont / sqrt( nomCont + 20* sqrt(nomCont) )

      outputHist.SetBinContent(iBin, nomCont)
      outputHist.SetBinError(iBin, err)

      ##### The following is for making DataLike ####
      #### Instead use makeDataLikeFromHist for this! ###
      ##Effective entries should be at least 3 times the nominal to get good poisson values
      #effectiveEntries = max(thisNomContent * 3, 100000)

      #binSeed = int( round( outputHist.GetBinCenter(iBin) + args.seed*1e5 ))
      #rand3.SetSeed(binSeed)

      #for event in xrange(int(round(effectiveEntries))):
      #  if rand3.Uniform() < thisNomContent / effectiveEntries:
      #    outputHist.Fill( outputHist.GetBinCenter( iBin ) )

      #if iBin == startBin:
      #  print "Bin ", iBin, "Scaled - Poisson: ", thisNomContent, '-', outputHist.GetBinContent( iBin)

    outputHist.Write()
  newFile.Close()

if __name__ == "__main__":
  main()
  print "Finished main()"

