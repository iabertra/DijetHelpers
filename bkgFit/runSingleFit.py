#!/usr/bin/env python

#######################################################################################
# runSingleFit.py
#
# This is a script for running singleFit.py on all the expected mjj histograms.
# Lists to loop over include:
#   files: a set of different input files
#   fitsToUse: The fits to use
#   systematics: Systematic directories in the files
#   histNameVariations:  Potential variations to the input histograms (i.e. luminosity)
#   fitVariations:  Potential variations to fits (i.e. tolerance)
#       This variation should both change the fitting and include an addition
#       to the output name.
#
#######################################################################################


import subprocess, os, time, sys, glob
import datetime
import ROOT

def main():
  f_updateData = False
  f_newFitFiles = False
  comString = '13TeV'
  f_smartMassWindow = False # use a specific mass window around the input signal
  f_chooseMassWindow = False
  f_test = False

  f_pseudoData = False
  f_scaled = False
  f_data = True
  f_allSystematics = False
  f_noDirStructure = False # If histograms are directly in the file, and not in systematic TDir structure

  f_tada = False
  f_findLuminosities = True

  #histNameBase = "m13"
  histNameBase = "mjj"
  #histNameBase = "mbj"

  minFitMass = "1099"
  maxFitMass = "10000"


#  ##### mgjjj TADA Options #####
#  f_tada = True
#  comString = 'Mgjjj'
#  histNameBase = "mgjjj"
#  minFitMass = "1300"
#  maxFitMass = "10000"
#  #maxFitMass = "4500"
#  f_findLuminosities = False

  ########### files ##########

#  files = glob.glob("input/CurrentData_IBLOff/*.root")
#  luminosities = ["_3p57fb"]
  files = glob.glob("input/CurrentData_Full/*.root")
#  luminosities = ["_3p34fb"]
#  files = glob.glob("input/Data_v4/*.root")


  f_bTagVary = False
  bTags = ["_85WP", "_85o85WP", "_77WP", "_77o77WP", "_70WP", "_70o70WP", "_60WP", "_60o60WP"]


#  if f_updateData:
#    # find done luminosities
#    # remove from luminosity string
#    f_findLuminosities = False
#    luminosities = ['_0p38fb', '_0p41fb', '_0p45fb', '_0p52fb']

  ## For Lumi Based Histogram Names (default) ##
  #f_findLuminosities = False
  #luminosities = ["_2fb"]
  #luminosities = ["_0p01fb", "_0p1fb", "_0p2fb", "_0p3fb"]
  #luminosities = ["_0p01fb", "_0p1fb", "_0p3fb", "_0p5fb", "_0p7fb", "_1fb", "_3fb", "_10fb", "_20fb"]
  #luminosities = ["_0p1fb", "_0p3fb", "_0p5fb", "_0p7fb", "_1fb", "_3fb", "_5fb", "_7fb", "_10fb", "_15fb", "_20fb", "_30fb"]

  ## exclusion run ##
#  pValThreshold = '0.01'
#  pValThreshold = '0.05'
#  pValThreshold = '0.07'
#  pValThreshold = '0.1'
#  pValThreshold = '0.15'
#  files = glob.glob("input/WithData/dataLikeHists_MC15_DoubleBinning_"+str(pValThreshold).replace('.','p')+"/*v1/*ranged*.root")
  #files = glob.glob("input/WithData/dataLikeHists_MC15_DoubleBinning_Nom/*v1/*.root")


  ########### Fits to Use ###############
  #fitsToUse = ["NEW7", "NEW8"]
  fitsToUse = ["CDF"]
  #fitsToUse = ["CDF", "STD", "ADD", "BTH", "NEW1", "NEW2", "NEW3", "NEW4", "NEW5", "NEW6"]

  ########### Options for varying fit parameters ##########
  fitVariations = [""] #None
  ## For varying fit tolerance ##
  #tolerances = [0.001, 0.01, 0.1, 1, 10]
  #fitVariations = []
  #for tolerance in tolerances:
  #  fitVariations.append( ' --tolerance '+str(tolerance)+' --fitVariation _'+str(tolerance).replace('.','p')+'tol' )

  ## If doing pseudoEvents
#  numPseudo = 0
  numPseudo = 1000 #2 minutes per 100 pseudo

  ################### Automatic Changes ######################

  ########### Histogram names ###########
  # Histogram name is histNameBase+sampleNames[iFile]+thisHistVariation

  ## Base Name ##
  if (f_pseudoData):
    histNameBase += "_PseudoData"
  elif (f_data):
    histNameBase += "_Data"
  elif (f_scaled):
    histNameBase += "_Scaled"
  elif (f_tada):
    histNameBase += "_TADA"
  else:
    histNameBase += "_DataLike"


  ## Histogram name is based on sampleNames ##
  sampleNames = [] #For new format
  for thisFile in files:
    if (f_pseudoData):
      sampleNames.append("_QCDDiJet")  #PseudoData
    else:
      thisFile = os.path.basename(thisFile).split('.')
      sampleNames.append('_'+thisFile[1])



  ## Format Histogram Names ##
  histNameVariations = []
  if f_bTagVary:
    for thisBTag in bTags:
      histNameVariations.append( thisBTag )
  elif not f_findLuminosities:
    for thisLumi in luminosities:
      histNameVariations.append( thisLumi )

  ########### Systematics Dir Structure###############
  if( f_noDirStructure):
    systematics = [""] #None
  elif (f_allSystematics):
    systematics = [
      "Nominal",
      "JET_GroupedNP_1__1down",
      "JET_GroupedNP_1__1up",
      "JET_GroupedNP_2__1down",
      "JET_GroupedNP_2__1up",
      "JET_GroupedNP_3__1down",
      "JET_GroupedNP_3__1up",
    ]
  else:
    systematics = ["Nominal"]


  pids = []
  logFiles = []
  NCORES = 6
  if not os.path.exists("logs/fitting/"):
    os.makedirs("logs/fitting/")

  for iFile, thisFile in enumerate(files):
    if not os.path.exists("logs/fitting/{0}/".format(os.path.basename(thisFile)[:-5])):
      os.makedirs("logs/fitting/{0}/".format(os.path.basename(thisFile)[:-5]))
    if 'ranged' in thisFile:
      f_chooseMassWindow = True
    else:
      f_chooseMassWindow = False


    if f_findLuminosities:
      histNameVariations = []
      luminosities = findLuminosities(thisFile, histNameBase, f_noDirStructure)
      print "In new file ", thisFile, " found luminosities ", luminosities
      latestLumiSet = set(luminosities)

      if f_updateData:
        luminosities = findLuminosities(thisFile[:-5]+'/STD.root', histNameBase, f_noDirStructure)
        prevLumiSet = set(luminosities)
        latestLumiSet = [latestLumi for latestLumi in latestLumiSet if not any(prevLumi in latestLumi for prevLumi in prevLumiSet)]

      print "Running only on new luminosities ", latestLumiSet

      for thisLumi in latestLumiSet:
        histNameVariations.append( thisLumi ) # new format

    for thisFit in fitsToUse:
      for iHist, thisHistVariation in enumerate(histNameVariations):
        for thisSys in systematics:
          for thisVariation in fitVariations:

            ## Wait for job if cores are all used or the output file is in use ##
            while len(pids) >= NCORES:
            #while len(pids) >= NCORES or any(os.path.basename(thisFile)[:-5] in logFile.name and thisFit in logFile.name for logFile in logFiles):
              wait_completion(pids, logFiles)

            #sendString = 'python singleFit.py -b --f_chi2 --comString '+str(comString)+' '
            sendString = 'python singleFit.py -b --f_chi2Log --comString '+str(comString)+' --numPseudo '+str(numPseudo)+' '

            thisHist = histNameBase+sampleNames[iFile]
            if (f_bTagVary):
              thisHist += luminosities[0]
            thisHist += thisHistVariation

            sendString += '--file '+thisFile+' --histName '+thisHist+' --fitToUse '+thisFit+' '
            logFile="logs/fitting/{0}/{1}_{2}".format(os.path.basename(thisFile)[:-5], thisHist.replace('/','_'), thisFit)

            parallelOutputName =  "_TMP_"+thisHist.replace('/','_')

            ### Other options ###
            if( len(thisSys) > 0 ):
              sendString += '--systematic '+thisSys+' '
              logFile += "_{0}".format(thisSys)
              parallelOutputName += '_'+thisSys
            if( len(thisVariation) > 0 ):
              sendString += thisVariation+' '
              logFile += "_{0}".format(thisVariation.split(' ')[-1])
              parallelOutputName += '_'+thisVariation.split(' ')[-1]


            ### Choose fit range based on input signal ###
            if f_smartMassWindow:
              thisMass = int(thisHist[thisHist.find("QStar")+5:thisHist.find("QStar")+9])
              sendString += "--massRanges "+minFitMass+" "+str(thisMass-500)+" "+str(thisMass+500)+" "+maxFitMass+" "
            else:
              sendString += "--minMjj "+minFitMass+" --maxMjj "+maxFitMass+" " #1598

            if f_chooseMassWindow:
              lowBin, highBin = 0,0


              ##### Get exlusion file name #####
              ##### There is quite a bit of string gymnastics here, not for the faint of heart
              ##### This does not care about background type, only that filename and histname field conventions are correct
              exclusionSignalType = "QStar"
              ## ranged string must be last field of '_' in first field of '.' in the file name
              ## (i.e. dataLikeHistograms_ranged4500.QCDDiJet.root)
              exclRunInfo = os.path.basename(thisFile).split('.')[0]
              exclSampleInfo = os.path.basename(thisFile).split('.')[1]
              exclFileNameEnd = os.path.basename(thisFile).split('.',2)[-1]

              if not 'ranged' in exclRunInfo:
                exclusionFileName = thisFile
                exclusionHistName = thisHist
              else:
                exclusionFileName = os.path.dirname(thisFile).split('/')[-1]+'/'
                signalNumber = exclRunInfo.split('_')[-1]
                signalNumber = signalNumber.replace('ranged', '')

                # no ranged number, just remove it
                # i.e. dataLikeHistograms_ranged.QCDDiJetWithQStar4000.root -> i.e. dataLikeHistograms.QCDDiJetWithQStar4000.root
                if len(signalNumber) == 0:
                  exclusionFileName += '_'.join(exclRunInfo.split('_')[:-1])+'.'+exclSampleInfo+'.'+exclFileNameEnd
                  exclusionHistName = thisHist
                # else replicate QStar file name
                # i.e. dataLikeHistograms_ranged4000.QCDDiJet.root -> dataLikeHistograms.QCDDiJetWithQStar4000.root
                else:
                  exclusionFileName += '_'.join(exclRunInfo.split('_')[:-1])+'.'+exclSampleInfo+'With'+exclusionSignalType+signalNumber+'.'+exclFileNameEnd
                  histParts = thisHist.split('_')
                  exclusionHistName = histParts[0]+'_'+histParts[1]+'_'+histParts[2]+'With'+exclusionSignalType+signalNumber+'_'+histParts[3]


              #### get exclusion range #####
              with open("exclusionList_pValComparison.txt", "r") as exclusionFile:
                for line in exclusionFile:
                  ## if correct file, histogram and p-value
                  if exclusionFileName in line and exclusionHistName in line and pValThreshold in line:
                    results = next(exclusionFile).split(' ')
                    pValue = results[4]
                    if float(pValue) < 0.05:
                      lowBin = int(results[0])-1
                      highBin = int(results[1])+1
              if lowBin != 0:
                sendString += "--excludeBins "+str(lowBin)+" "+str(highBin)+" "

            ## Only run ranged if we have chosen an exclusion region
            if f_chooseMassWindow and not "excludeBins" in sendString:
              continue
            #%%%%%%%%%%%%% End Exclusion %%%%%%%%%%%%%%%%%%%%%%%%%%

            ### Creates seperate output files that are combined at the end.  Allows for parallelization
            ### by avoiding writing to the same file simultaneously
            sendString += "--outNameAppend "+parallelOutputName

            # tada #
#            sendString += " --excludeBins 53 57 "

            logFile += ".txt"


            print "Starting job", sendString
            if not f_test:
              res = submit_local_job(sendString, logFile)
              pids.append(res[0])
              logFiles.append(res[1])

  wait_all(pids, logFiles)
  for f in logFiles:
    f.close()

  if not f_test:
    ### Merge output files ###
    for iFile, thisFile in enumerate(files):
      for thisFit in fitsToUse:
        #if files exist
        if glob.glob(thisFile[:-5]+'/*'+thisFit+'*.root'):
          if f_newFitFiles:
            os.system('mv '+thisFile[:-5]+'/'+thisFit+'.root '+thisFile[:-5]+'/old_'+thisFit+'_'+str(time.time()).split('.')[0]+'.root ')
            os.system('hadd '+thisFile[:-5]+'/'+thisFit+'.root '+thisFile[:-5]+'/'+thisFit+'_TMP*.root')
            os.system('rm '+thisFile[:-5]+'/'+thisFit+'_TMP*.root')
          else: #keep old fit file
            os.system('mv '+thisFile[:-5]+'/'+thisFit+'.root '+thisFile[:-5]+'/'+thisFit+'_TMPGOOD.root')
            os.system('hadd '+thisFile[:-5]+'/'+thisFit+'.root '+thisFile[:-5]+'/'+thisFit+'_TMP*.root')
            os.system('rm '+thisFile[:-5]+'/'+thisFit+'_TMP*.root')


def submit_local_job(exec_sequence, logfilename):
  #os.system("rm -f "+logfilename)
  output_f=open(logfilename, 'w')
  pid = subprocess.Popen(exec_sequence, shell=True, stderr=output_f, stdout=output_f)
  time.sleep(0.1)  #Wait to prevent opening / closing of several files

  return pid, output_f

def wait_completion(pids, logFiles):
  print """Wait until the completion of one of the launched jobs"""
  while True:
    for pid in pids:
      if pid.poll() is not None:
        print "\nProcess", pid.pid, "has completed"
        logFiles.pop(pids.index(pid)).close()  #remove logfile from list and close it
        pids.remove(pid)

        return
    print ".",
    sys.stdout.flush()
    time.sleep(3) # wait before retrying

def wait_all(pids, logFiles):
  print """Wait until the completion of all launched jobs"""
  while len(pids)>0:
    wait_completion(pids, logFiles)
  print "All jobs finished!"

def findLuminosities(fileName, histNameBase, f_noDirStructure):
  file = ROOT.TFile.Open(fileName, "read")
  if not f_noDirStructure:
    thisDir = file.Get("Nominal")
  else:
    thisDir = file

  luminosities = []
  for key in thisDir.GetListOfKeys():
    if histNameBase in key.GetName():
      luminosities.append( '_'+key.GetName().split('_')[-1] )

  return luminosities

if __name__ == "__main__":
  print "Starting fitting at", datetime.datetime.now().time()
  main()
  print "Finished fitting at", datetime.datetime.now().time()



