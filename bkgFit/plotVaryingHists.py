#!/usr/bin/env python

####################################
# plotSingleFits.py
#
# For plotting TFitResults from singleFit.py onto the fitted data
#
# jeff.dandoy@cern.ch
####################################

import os, sys, time, argparse, copy, glob
from array import array
from math import sqrt, log, isnan, isinf, fabs, factorial



#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("-v", dest='verbose', action='store_true', default=False, help="Verbose Mode")
parser.add_argument("--massRanges", dest='massRanges', default=-1,
      type=int, nargs='+', help="List of mass ranges for fitting and generating purposes")
parser.add_argument("--file", dest='file', default="input/input_DataLikePythiaDijet.root",
      help="Input file name")
parser.add_argument("--histName", dest='histName', default="mjj_DataLikePythiaDiJet",
      help="string for histName to use")
parser.add_argument("--nominalFit", dest='nominalFit', default="CDF",
      help="Set the Nominal Fit for all comparisons")
parser.add_argument("--outDir", dest='outDir', default="plots",
      help="Name of output directory for plots")
parser.add_argument("--comEnergy", dest='comEnergy', default=13,
         type=int, help="Center of Mass Energy")
parser.add_argument("--systematic", dest='systematic', default="",
         help="Systematic directory to look for histograms")
parser.add_argument("--bannedFits", dest='bannedFits', default="NEW10,NEW11",
         help="Comma separated list of fits to ignore")
parser.add_argument("--comparison", dest='comparison', default="None", #fb
         help="String of type comparison between histograms.  For default this is None but should be set to fb for luminosity.  \
         The filename will be searched for this string, and the float before it and after the \
         precedding '_' will be used for the x-axis.  For example, by default the filename file_10fb_info.root \
         will be listed as instance 10 for x-axis title fb. '-' will be converted to '.'")

args = parser.parse_args()

## Import ROOT and helper code ##
import ROOT
sys.path.insert(0, '../scripts/')
import AtlasStyle, DijetFuncs
import plotUtils

def main():

  ###### Setup Area ########
  AtlasStyle.SetAtlasStyle()
  ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 2000") #Ignore TCanvas::Print info
  args.outDir += '/'+args.systematic
  print args.outDir, " is made"
  if not os.path.exists(args.outDir):
    os.makedirs(args.outDir)
  if not os.path.exists(args.outDir+"/ToyValues"):
    os.makedirs(args.outDir+"/ToyValues")
#  gStyle.SetOptStat(0)

  outName = os.path.basename(args.file)[:-5] #remove .root

  minMjj = maxMjj = -1
  binList = []

  ## Load global variables for fits ##
  DijetFuncs.initializeGlobals()
  DijetFuncs.comEnergy = args.comEnergy*1000.

  #### Get the Proper Fit Information for each Fit available ####
  #Items are fNames, fDisplayNames, fParams, fMin, fMax, fSteps, fEqs, fColors, fEnergy
  fitFiles = sorted(glob.glob(args.file[:-5]+'/*'))

  bannedFitList = args.bannedFits.split(',')
  for bannedFit in bannedFitList:
    fitFiles = [fitFile for fitFile in fitFiles if not bannedFit in fitFile]

  print "Running on files ", fitFiles
  fList = DijetFuncs.getFits( fitFiles )
  print "Fit Types are", fList["fNames"]

  if not args.nominalFit in fList["fNames"]:
    print "ERROR, the nominal fit ", args.nominalFit, " was not in the list of fit functions"
    exit(1)
  iNominalFit = fList["fNames"].index(args.nominalFit)

  nextFit = "STD"
  iNextFit = fList["fNames"].index(nextFit)

  #### Get the input histograms ####
  # Add data histograms that contain the args.histName string
  dataHists = []
  inFile = ROOT.TFile.Open(args.file, "READ")

  if len(args.systematic) > 0:
    args.systematic = args.systematic.rstrip('/')
    thisDir = inFile.Get(args.systematic)
    if not thisDir:
      print "ERROR, couldn't find systematic directory ", args.systematic, " in ", args.file
      exit(0)
  else:
    thisDir = inFile

  for histKey in thisDir.GetListOfKeys():
    if args.histName in histKey.GetName():
#      if "3fb" in histKey.GetName() or "10fb" in histKey.GetName() or "20fb" in histKey.GetName():
#        continue
      if "o" in histKey.GetName().split('_')[-1]:
        continue
      dataHists.append(histKey.ReadObj())
      dataHists[-1].SetDirectory(0) #Detach from inFile
  inFile.Close()

  #### Get binning if the user requested a specific range ####
  if args.massRanges is not -1:
    minMjj = []
    maxMjj = []
    for iRange in range(len(args.massRanges)/2):
      minMjj.append( args.massRanges[2*iRange] )
      maxMjj.append( args.massRanges[2*iRange+1] )

    for iBin in range(1, dataHists[0].GetNbinsX()+1):
      for iMass in range(len(minMjj)):
        if dataHists[0].GetXaxis.GetBinLowEdge(iBin) > minMjj[iMass] and dataHists[0].GetXaxis.GetBinUpEdge(iBin) < maxMjj[iMass]:
          binList.append(iBin)
          continue
    minMjj = binHist.GetXaxis().GetBinLowEdge( binList[0] )
    maxMjj = binHist.GetXaxis().GetBinUpEdge( binList[-1] )
    numPointsFit = len(binList)

  ###### Define ordering for multi histogram plots ######
  comparisonValues = []
  f_logX = False   #Always false now
#  if not args.comparison == "fb" and not args.comparison == "pb":
#    f_logX = False
  for iData, thisData in enumerate( dataHists ):
    if not args.comparison in thisData.GetName():
      print "ERROR, comparison", args.comparison, "is not in the histogram name", thisData.GetName()
      exit(1)


    comparisonString = thisData.GetName().split('_')[-1]
    if args.comparison == "fb" or args.comparison == "pb":
      float(comparisonString[:-len(args.comparison)].replace('p','.'))
      comparisonValues.append( float(comparisonString[:-len(args.comparison)].replace('p','.')) )
    else:
      comparisonValues.append( iData )
  print "comparison values ", comparisonValues

  ### Remap dataHists so that their order increases the comparison value ###
  dataHists = [hist for (value, hist) in sorted(zip(comparisonValues, dataHists)) ]
  comparisonValues = sorted(comparisonValues)

  if args.comparison == "fb":
    args.comparison = "Luminosity (fb^{-1})"
  elif args.comparison == "pb":
    args.comparison = "Luminosity (pb^{-1})"
  elif args.comparison == "tol":
    args.comparison = "Fit Tolerance"

  ############### Load fitResults and pValue TTrees into memory ############
  fitDOF = [ [] for i in range(len(dataHists)) ] #2D [hist][fit]
  NLLs = [ [] for i in range(len(dataHists)) ] #2D [hist][fit]
  Chi2s = [ [] for i in range(len(dataHists)) ] #2D [hist][fit]
  PVals = [ [] for i in range(len(dataHists)) ] #2D [hist][fit]
  ToyVals = [ [] for i in range(len(dataHists)) ] #3D [hist][fit][toy values]

  #-- For each fit
  for iFit, thisFit in enumerate(fList["fNames"]):
    print "Opening Fit File ", args.file[:-5]+"/"+thisFit+".root"
    fitFile = ROOT.TFile.Open(args.file[:-5]+"/"+thisFit+".root","READ")

    ## Switch to systematics dir ##
    if len(args.systematic) > 0:
      args.systematic = args.systematic.rstrip('/')
      thisDir = fitFile.Get(args.systematic)
      if not thisDir:
        print "ERROR, couldn't find systematic directory ", args.systematic, " in ", args.file[:-5]+'/'+thisFit+'.root'
        exit(0)
      args.systematic += '/'

    ## If not previously specified, get the binning used from the first hist ##
    if minMjj is -1:
      binHist = fitFile.Get(args.systematic+"BinHist")
      for iBin in range(1, binHist.GetNbinsX()+1):
        if binHist.GetBinContent(iBin) > 0:
          binList.append(iBin)
      minMjj = binHist.GetXaxis().GetBinLowEdge( binList[0] )
      maxMjj = binHist.GetXaxis().GetBinUpEdge( binList[-1] )
      numPointsFit = len(binList)

    ############# Calculate NLLs and Pvals ######################
    #--- For each data file
    for iData, thisData in enumerate( dataHists ):

      ## Get FitResultNLL ##
      thisResultNLL = fitFile.Get( args.systematic+"FitResultLogL_"+thisFit+"_"+thisData.GetName() )
      if thisResultNLL:
        #fitResultsNLL[iData].append( copy.copy(thisResultNLL) ) #Detach from inFil
        NLLs[iData].append(  thisResultNLL.MinFcnValue() )
      else:
        print "Couldn't find TFitFunction FitResultLogL_"+thisFit+"_"+thisData.GetName(), ", Skipping it"
        NLLs[iData].append( -1. )

      ## Get FitResultChi2 ##
      thisResultChi2 = fitFile.Get( args.systematic+"FitResultChi2_"+thisFit+"_"+thisData.GetName() )
      if thisResultChi2:
        #fitResultsChi2[iData].append( copy.copy(thisResultChi2) ) #Detach from inFil
        Chi2s[iData].append(  thisResultChi2.MinFcnValue() )
        fitDOF[iData].append( thisResultChi2.Ndf() )
      else:
        print "Couldn't find TFitFunction FitResultChi2_"+thisFit+"_"+thisData.GetName(), ", Skipping it"
        Chi2s[iData].append( -1. )
        fitDOF[iData].append( -1. )

      toyValues = []
      thisTree = fitFile.Get( args.systematic+"LogLToys_"+thisFit+"_"+thisData.GetName() )
      if thisTree: #If it exists
        badValues = 0
        for event in thisTree:
          if not isnan(event.logLValue) and not isinf(event.logLValue):
            toyValues.append(event.logLValue)
          else:
            badValues += 1;
        if badValues > 0:
          print "Found", badValues, " Nan or Inf in ", args.systematic+"LogLToys_"+thisFit+"_"+thisData.GetName()
      else:
        print "Couldn't find TTree ", args.systematic+"LogLToys_"+thisFit+"_"+thisData.GetName()
      ToyVals[iData].append( toyValues )

      # Need NLL result
      if thisResultNLL and len(toyValues) > 0:
        PVals[iData].append( len([x for x in toyValues if x > thisResultNLL.MinFcnValue()])/float(len(toyValues)) )
      else:
        PVals[iData].append( -1. )



    fitFile.Close()


  ######################### Calculate F-test statistics ################################
  FStatistic = [ [] for i in range(len(dataHists)) ] #2D [hist][fit]
  FPVals = [ [] for i in range(len(dataHists)) ] #2D [hist][fit]

  for iData, thisData in enumerate( dataHists ):
    for iFit, thisFit in enumerate(fList["fNames"]):

      if iFit == iNominalFit:  #Ignore the nominal fit
        FStatistic[iData].append( -1. )
        FPVals[iData].append( -1. )
        continue

      if( len(fList["fParams"][iNominalFit]) < len(fList["fParams"][iFit]) ):
        iFit1 = iNominalFit
        iFit2 = iFit
      else:
        iFit1 = iFit
        iFit2 = iNominalFit
      if (len(fList["fParams"][iFit2]) - len(fList["fParams"][iFit1])) != 0:
        thisDiff = (Chi2s[iData][iFit1] - Chi2s[iData][iFit2]) / ( len(fList["fParams"][iFit2]) - len(fList["fParams"][iFit1]) )
      else:
        thisDiff = Chi2s[iData][iFit1] - Chi2s[iData][iFit2]
      FStatistic[iData].append( thisDiff / (Chi2s[iData][iFit2] / (numPointsFit - len(fList["fParams"][iFit2])) ) )
      FPVals[iData].append( 1. - ROOT.Math.fdistribution_cdf( FStatistic[iData][-1],  len(fList["fParams"][iFit2]) - len(fList["fParams"][iFit1]), numPointsFit - len(fList["fParams"][iFit2]) ) )

      if(args.verbose):
        print "     lumi/iFit ", comparisonValues[iData], iFit, fList["fNames"][iFit], "Chi2/FStatistic is ", Chi2s[iData][iFit], FStatistic[iData][iFit]

  ######################### Calculate test statistics ###################################3#
  testStatistic = [ [] for i in range(len(dataHists)) ] #2D [hist][fit]
  WilkStatistic = [ [] for i in range(len(dataHists)) ] #2D [hist][fit]

  for iData, thisData in enumerate( dataHists ):
    for iFit, thisFit in enumerate(fList["fNames"]):
      testStatistic[iData].append( 2*(NLLs[iData][iNominalFit] - NLLs[iData][iFit]) )
      #WilkStatistic[iData].append( ROOT.TMath.Prob(2*(NLLs[iData][iNominalFit] - NLLs[iData][iFit]), abs(len(fList["fParams"][iFit]) - len(fList["fParams"][iNominalFit])) ))

      WilkStatistic[iData].append( ROOT.TMath.Prob(2*abs(NLLs[iData][iFit] - NLLs[iData][iNominalFit]), abs(len(fList["fParams"][iFit]) - len(fList["fParams"][iNominalFit])) ))
      ## If both fits in error
      if (NLLs[iData][iNominalFit] > 10000. and NLLs[iData][iFit] > 10000.):
        testStatistic[iData][-1] = 0.
      ## If nominal fit in error
      elif (NLLs[iData][iNominalFit] > 10000.):
        testStatistic[iData][-1] = 10.
      ## If this fit in error
      elif (NLLs[iData][iFit] > 10000.):
        testStatistic[iData][-1] = -10.

      if(args.verbose):
        print "     lumi/iFit ", comparisonValues[iData], iFit, fList["fNames"][iFit], "NLL/testStatistic is ", NLLs[iData][iFit], testStatistic[iData][iFit], WilkStatistic[iData][iFit]

  ################################# Begin Plotting #######################################

  canv1 = ROOT.TCanvas()
  if (f_logX):
    canv1.SetLogx(0)
  pad1 = ROOT.TPad("pad1","pad1",0,0,0.84,1)
  pad1.SetRightMargin(0)
  leg = ROOT.TLegend(0.84,.3,1,.75,"")
  leg.SetFillStyle(0)

  ## ATLAS labeling ##
  AtlasStyle.ATLAS_LABEL(0.85,0.9, 1, "")
  AtlasStyle.myText(0.88,0.85,1,"#scale[0.9]{Internal}");
  AtlasStyle.myText(0.85,0.8,1,"#scale[0.8]{#sqrt{s} = "+str(args.comEnergy)+" TeV}");

  ##################### Plot Test Statistic vs luminosity ###########################
  print "Test Statistic vs Luminosity plot"
  if (f_logX):
    pad1.SetLogx()
  pad1.Draw()
  pad1.cd()

  minVal = min( min(i) for i in testStatistic)
  maxVal = max( max(i) for i in testStatistic)
  minVal = minVal - abs(.15*minVal) - 1
  maxVal = maxVal + abs(.15*maxVal) + 1

  ## Setup Zero Line ##
  oneLine = ROOT.TF1("zl1","0", comparisonValues[0]*.9, comparisonValues[-1]*1.1 )
  oneLine.SetLineWidth(1)
  oneLine.SetLineStyle(7)
  oneLine.SetLineColor(ROOT.kBlack)
  oneLine.SetMaximum(maxVal)
  oneLine.SetMinimum(minVal)
  oneLine.SetTitle("Test Statistic;"+args.comparison+";Test Statistic (NLL_{Nom} - NLL_{Test}) ")
  topLine = ROOT.TF1("zl1","2.5", .0001, 35 )
  topLine.SetLineWidth(1)
  topLine.SetLineStyle(3)
  topLine.SetLineColor(ROOT.kBlack)
  botLine = ROOT.TF1("zl1","-1.5", .0001, 35 )
  botLine.SetLineWidth(1)
  botLine.SetLineStyle(3)
  botLine.SetLineColor(ROOT.kBlack)

  oneLine.Draw()
  topLine.Draw("same")
  botLine.Draw("same")


  # Potential addition of status warnings to points, create a new tgraph with encompassing values
  testStatGraphs = []
  for iFit, thisFit in enumerate(fList["fNames"]):

    if iFit == iNominalFit:  #Ignore the nominal fit
      continue

    testStatGraphs.append(ROOT.TGraph(len(dataHists)))
    testStatGraphs[-1].SetName("Graph_"+fList["fNames"][iFit])
    testStatGraphs[-1].SetTitle("Test Statistic [2*(NLL_{0} - NLL_{1}) ];"+args.comparison+";Test Statistic (NLL_{Nom} - NLL_{Test}")
    testStatGraphs[-1].SetMarkerColor(fList["fColors"][iFit])
    testStatGraphs[-1].SetMarkerSize(1)
    testStatGraphs[-1].SetMarkerStyle(3)
    testStatGraphs[-1].SetLineColor(fList["fColors"][iFit])

    for iData, thisData in enumerate( dataHists ):
      testStatGraphs[-1].SetPoint(iData, comparisonValues[iData], testStatistic[iData][iFit] )

    leg.AddEntry(testStatGraphs[-1], fList["fDisplayNames"][iFit], "p")
    testStatGraphs[-1].Draw("PLsame")

  canv1.cd()
  leg.Draw()

  ## Save Plot ##
  canv1.Print(args.outDir+"/testStatistic_"+outName+"_"+args.histName+".png")
  canv1.Print(args.outDir+"/testStatistic_"+outName+"_"+args.histName+".eps")
  outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
  canv1.SetName("testStatistic_"+outName+"_"+args.histName)
  canv1.Write("", ROOT.TObject.kOverwrite)
  outFile.Close()

  ##################### Plot Wilk's Statistic vs luminosity ###########################
  print "Wilk's Statistic vs Luminosity plot"
  leg.Clear()
  pad1.Clear()
  if (f_logX):
    pad1.SetLogx()
  pad1.SetLogy()
  pad1.Draw()
  pad1.cd()

  ## Setup Zero Line ##
  WilksLine = ROOT.TF1("WilksLine","0.05", comparisonValues[0]*.9, comparisonValues[-1]*1.1)
  WilksLine.SetLineWidth(1)
  WilksLine.SetLineStyle(3)
  WilksLine.SetLineColor(ROOT.kBlack)
  WilksLine.SetMaximum(1.)
  WilksLine.SetMinimum(0.001)
  WilksLine.SetTitle("Wilks's Statistic;"+args.comparison+";Wilks's Statistic ")
  WilksLine.Draw()



  # Potential addition of status warnings to points, create a new tgraph with encompassing values
  WilkStatGraphs = []
  for iFit, thisFit in enumerate(fList["fNames"]):

    if iFit == iNominalFit:  #Ignore the nominal fit
      continue

    WilkStatGraphs.append(ROOT.TGraph(len(dataHists)))
    WilkStatGraphs[-1].SetName("Graph_"+fList["fNames"][iFit])
    WilkStatGraphs[-1].SetMarkerColor(fList["fColors"][iFit])
    WilkStatGraphs[-1].SetMarkerSize(1)
    WilkStatGraphs[-1].SetMarkerStyle(3)
    WilkStatGraphs[-1].SetLineColor(fList["fColors"][iFit])

    for iData, thisData in enumerate( dataHists ):
      WilkStatGraphs[-1].SetPoint(iData, comparisonValues[iData], WilkStatistic[iData][iFit] )

    leg.AddEntry(WilkStatGraphs[-1], fList["fDisplayNames"][iFit], "p")
    WilkStatGraphs[-1].Draw("PLsame")

  canv1.cd()
  leg.Draw()
  AtlasStyle.myText(0.1, 0.05,1, "#scale[0.8]{Wilks's = %3.3f}" %WilkStatistic[-1][iNextFit]);

  ## Save Plot ##
  canv1.Print(args.outDir+"/WilkStatistic_"+outName+"_"+args.histName+".png")
  canv1.Print(args.outDir+"/WilkStatistic_"+outName+"_"+args.histName+".eps")
  outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
  canv1.SetName("WilkStatistic_"+outName+"_"+args.histName)
  canv1.Write("", ROOT.TObject.kOverwrite)
  outFile.Close()
  canv1.Clear("D")

  ##################### Plot F Statistic vs luminosity ###########################
  print "F-Statistic vs Luminosity plot"
  # This can only be done if functions are nested
  leg.Clear()
  pad1.Clear()
  if (f_logX):
    pad1.SetLogx()
  pad1.SetLogy(0)
  pad1.Draw()
  pad1.cd()

  minVal = min( min(i) for i in FStatistic)
  maxVal = max( max(i) for i in FStatistic)
  minVal = minVal - abs(.15*minVal) - 1
  maxVal = maxVal + abs(.15*maxVal) + 1

  ## Setup Zero Line ##
  oneLine.SetMaximum(maxVal)
  oneLine.SetMinimum(minVal)
  oneLine.SetTitle("F Test Statistic;"+args.comparison+";F Statistic")
  oneLine.Draw()

  FStatGraphs = []
  for iFit, thisFit in enumerate(fList["fNames"]):

    if iFit == iNominalFit:  #Ignore the nominal fit
      continue

    FStatGraphs.append(ROOT.TGraph(len(dataHists)))
    FStatGraphs[-1].SetName("Graph_"+fList["fNames"][iFit])
    FStatGraphs[-1].SetTitle("F Statistic;"+args.comparison+";F Statistic")
    FStatGraphs[-1].SetMarkerColor(fList["fColors"][iFit])
    FStatGraphs[-1].SetMarkerSize(1)
    FStatGraphs[-1].SetMarkerStyle(3)
    FStatGraphs[-1].SetLineColor(fList["fColors"][iFit])

    for iData, thisData in enumerate( dataHists ):
      FStatGraphs[-1].SetPoint(iData, comparisonValues[iData], FStatistic[iData][iFit] )

    leg.AddEntry(FStatGraphs[-1], fList["fDisplayNames"][iFit], "p")
    FStatGraphs[-1].Draw("PLsame")

  canv1.cd()
  leg.Draw()

  ## Save Plot ##
  canv1.Print(args.outDir+"/FStatistic_"+outName+"_"+args.histName+".png")
  canv1.Print(args.outDir+"/FStatistic_"+outName+"_"+args.histName+".eps")
  outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
  canv1.SetName("FStatistic_"+outName+"_"+args.histName)
  canv1.Write("", ROOT.TObject.kOverwrite)
  outFile.Close()

  ##################### Plot F PVal vs luminosity ###########################
  print "F P-Value vs Luminosity plot"
  # This can only be done if functions are nested
  leg.Clear()
  pad1.Clear()
  if (f_logX):
    pad1.SetLogx()
  pad1.SetLogy()
  pad1.Draw()
  pad1.cd()

  ## Setup Zero Line ##
  oneLine.SetMaximum(1.)
  oneLine.SetMinimum(0.001)
  oneLine.SetTitle("1 - FPValue;"+args.comparison+"; 1 - FPValue")
  oneLine.Draw()

  FPValGraphs = []
  for iFit, thisFit in enumerate(fList["fNames"]):

    if iFit == iNominalFit:  #Ignore the nominal fit
      continue

    FPValGraphs.append(ROOT.TGraph(len(dataHists)))
    FPValGraphs[-1].SetName("Graph_"+fList["fNames"][iFit])
    FPValGraphs[-1].SetMarkerColor(fList["fColors"][iFit])
    FPValGraphs[-1].SetLineColor(fList["fColors"][iFit])
    FPValGraphs[-1].SetMarkerSize(1)
    FPValGraphs[-1].SetMarkerStyle(3)

    for iData, thisData in enumerate( dataHists ):
      FPValGraphs[-1].SetPoint(iData, comparisonValues[iData], FPVals[iData][iFit] )

    leg.AddEntry(FPValGraphs[-1], fList["fDisplayNames"][iFit], "p")
    FPValGraphs[-1].Draw("PLsame")

  canv1.cd()
  leg.Draw()


  ## Save Plot ##
  canv1.Print(args.outDir+"/FPVal_"+outName+"_"+args.histName+".png")
  canv1.Print(args.outDir+"/FPVal_"+outName+"_"+args.histName+".eps")
  outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
  canv1.SetName("FPVal_"+outName+"_"+args.histName)
  canv1.Write("", ROOT.TObject.kOverwrite)
  outFile.Close()

  ############## Plot NLL vs luminosity ##################
  print "NLL vs Luminosity plot"
  leg.Clear()
  pad1.Clear()
  if (f_logX):
    pad1.SetLogx()
  pad1.SetLogy(0)
  pad1.Draw()
  pad1.cd()

  NLLGraphs = []

  for iFit, thisFit in enumerate(fList["fNames"]):

    NLLGraphs.append(ROOT.TGraph(len(dataHists)))
    NLLGraphs[-1].SetTitle("Negative Log-Likelihood;"+args.comparison+";Negative Log-Likelihood")
    NLLGraphs[-1].SetMarkerColor(fList["fColors"][iFit])
    NLLGraphs[-1].SetMarkerSize(1)
    NLLGraphs[-1].SetMarkerStyle(3)
    NLLGraphs[-1].SetLineColor(fList["fColors"][iFit])

    for iData, thisData in enumerate( dataHists ):
      NLLGraphs[-1].SetPoint(iData, comparisonValues[iData], NLLs[iData][iFit] )

    leg.AddEntry(NLLGraphs[-1], fList["fDisplayNames"][iFit], "p")
    if iFit == 0:
      NLLGraphs[-1].Draw("")
    else:
      NLLGraphs[-1].Draw("PLsame")


  canv1.cd()
  leg.Draw()

  ## Save Plot ##
  canv1.Print(args.outDir+"/NLL_"+outName+"_"+args.histName+".png")
  canv1.Print(args.outDir+"/NLL_"+outName+"_"+args.histName+".eps")
  outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
  canv1.SetName("NLL_"+outName+"_"+args.histName)
  canv1.Write("", ROOT.TObject.kOverwrite)
  outFile.Close()

  ############## Plot Chi2 vs luminosity ##################
  print "Chi2 vs Luminosity plot"
  leg.Clear()
  pad1.Clear()
  if (f_logX):
    pad1.SetLogx()
  pad1.SetLogy()
  pad1.Draw()
  pad1.cd()

  Chi2Graphs = []

  for iFit, thisFit in enumerate(fList["fNames"]):

    Chi2Graphs.append(ROOT.TGraph(len(dataHists)))
    Chi2Graphs[-1].SetTitle("#Chi^{2};"+args.comparison+";#Chi^{2}")
    Chi2Graphs[-1].SetMarkerColor(fList["fColors"][iFit])
    Chi2Graphs[-1].SetMarkerSize(1)
    Chi2Graphs[-1].SetMarkerStyle(3)
    Chi2Graphs[-1].SetLineColor(fList["fColors"][iFit])

    for iData, thisData in enumerate( dataHists ):
      Chi2Graphs[-1].SetPoint(iData, comparisonValues[iData], Chi2s[iData][iFit] )

    leg.AddEntry(Chi2Graphs[-1], fList["fDisplayNames"][iFit], "p")
    if iFit == 0:
      Chi2Graphs[-1].Draw("")
    else:
      Chi2Graphs[-1].Draw("PLsame")


  canv1.cd()
  leg.Draw()

  ## Save Plot ##
  canv1.Print(args.outDir+"/Chi2_"+outName+"_"+args.histName+".png")
  canv1.Print(args.outDir+"/Chi2_"+outName+"_"+args.histName+".eps")
  outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
  canv1.SetName("Chi2_"+outName+"_"+args.histName)
  canv1.Write("", ROOT.TObject.kOverwrite)
  outFile.Close()

  ############## Plot PVals vs luminosity ##################
  print "PVals vs Luminosity plot"
  leg.Clear()
  pad1.Clear()
  if (f_logX):
    pad1.SetLogx()
  pad1.SetLogy(0)
  pad1.Draw()
  pad1.cd()

  PValGraphs = []
  for iFit, thisFit in enumerate(fList["fNames"]):

    PValGraphs.append(ROOT.TGraph(len(dataHists)))
    PValGraphs[-1].SetTitle("P-Value;"+args.comparison+";P-Value")
    PValGraphs[-1].SetMarkerColor(fList["fColors"][iFit])
    PValGraphs[-1].SetMarkerSize(1)
    PValGraphs[-1].SetMarkerStyle(3)
    PValGraphs[-1].SetLineColor(fList["fColors"][iFit])
    PValGraphs[-1].SetMinimum(0.)
    PValGraphs[-1].SetMaximum(1.)

    for iData, thisData in enumerate( dataHists ):
      if PVals[iData] >= 0:
        PValGraphs[-1].SetPoint(iData, comparisonValues[iData], PVals[iData][iFit] )

    leg.AddEntry(PValGraphs[-1], fList["fDisplayNames"][iFit], "p")
    if iFit == 0:
      PValGraphs[-1].Draw("")
    else:
      PValGraphs[-1].Draw("PLsame")

  canv1.cd()
  leg.Draw()
  AtlasStyle.myText(0.1, 0.05,1, "#scale[0.8]{P-value = %3.3f}" %PVals[-1][iNominalFit]);

  ## Save Plot ##
  canv1.Print(args.outDir+"/Pvalue_"+outName+"_"+args.histName+".png")
  canv1.Print(args.outDir+"/Pvalue_"+outName+"_"+args.histName+".eps")
  outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
  canv1.SetName("Pvalue_"+outName+"_"+args.histName)
  canv1.Write("", ROOT.TObject.kOverwrite)
  outFile.Close()
  canv1.Clear("D")

  ############## Plot PVal differences vs luminosity ##################
  print "PVal Differences vs Luminosity plot"
  leg.Clear()
  pad1.Clear()
  if (f_logX):
    pad1.SetLogx()
  pad1.SetLogy(0)
  pad1.Draw()
  pad1.cd()
  oneLine.SetMaximum(0.5)
  oneLine.SetMinimum(-0.5)
  oneLine.GetYaxis().SetTitle(" P-Value Difference")
  oneLine.Draw()

  PValDiffGraphs = []
  for iFit, thisIFit in enumerate(fList["fNames"]):
    for jFit, thisJFit in enumerate(fList["fNames"]):
      if jFit <= iFit:
        continue

      thisColor = plotUtils.getGenericColor( len(PValDiffGraphs), factorial( len(fList["fNames"]) - 1) )
      PValDiffGraphs.append(ROOT.TGraph(len(dataHists)))
      PValDiffGraphs[-1].SetTitle("P-Value Difference;"+args.comparison+";P-Value Difference")
      PValDiffGraphs[-1].SetMarkerSize(1)
      PValDiffGraphs[-1].SetMarkerStyle(3)

      PValDiffGraphs[-1].SetMarkerColor(thisColor)
      PValDiffGraphs[-1].SetLineColor(thisColor)

      for iData, thisData in enumerate( dataHists ):
        if PVals[iData] >= 0:
          PValDiffGraphs[-1].SetPoint(iData, comparisonValues[iData], PVals[iData][jFit]-PVals[iData][iFit] )

      leg.AddEntry(PValDiffGraphs[-1], "#splitline{"+fList["fDisplayNames"][jFit]+" vs}{ "+fList["fDisplayNames"][iFit]+" }", "p")
      PValDiffGraphs[-1].Draw("PLsame")

  canv1.cd()
  leg.Draw()

  ## Save Plot ##
  canv1.Print(args.outDir+"/PvalueDiff_"+outName+"_"+args.histName+".png")
  canv1.Print(args.outDir+"/PvalueDiff_"+outName+"_"+args.histName+".eps")
  outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
  canv1.SetName("PvalueDiff_"+outName+"_"+args.histName)
  canv1.Write("", ROOT.TObject.kOverwrite)
  outFile.Close()

  ############## Plot Toy best fit values for all luminosities ##################
  print "Toy values for all Luminosity plot"

  for iData, thisData in enumerate( dataHists ):
    for iFit, thisFit in enumerate(fList["fNames"]):

      if ( len(ToyVals[iData]) >= iFit and  ToyVals[iData][iFit]):
        leg.Clear()
        canv1.Clear()
        canv1.cd()

        leg = ROOT.TLegend(0.80,.35,1,.5,"")
        leg.SetFillStyle(0)
        ## ATLAS labeling ##
        AtlasStyle.ATLAS_LABEL(0.85,0.9, 1, "")
        AtlasStyle.myText(0.88,0.85,1,"#scale[0.9]{Internal}");
        AtlasStyle.myText(0.85,0.8,1,"#scale[0.8]{#sqrt{s} = "+str(args.comEnergy)+" TeV}");

        AtlasStyle.myText(0.81,0.7,1,"#scale[0.8]{"+fList["fDisplayNames"][iFit]+" Fit}");
        if "Luminosity" in args.comparison:
         AtlasStyle.myText(0.81, 0.65,1, "#scale[0.8]{#scale[0.7]{#int} L dt= "+str(comparisonValues[iData])+" fb^{-1}}");
        AtlasStyle.myText(0.81, 0.59,1, "#scale[0.8]{Pval = "+str(PVals[iData][iFit])+"}");

        pad1 = ROOT.TPad("pad1","pad1",0,0,0.80,1)
        pad1.SetRightMargin(0)
        pad1.SetLogy()
        pad1.Draw()
        pad1.cd()

        leg.Clear()
        pad1.Clear()
        pad1.Draw()
        pad1.cd()


        xMin = min( ToyVals[iData][iFit] ) - 5
        xMax = max( ToyVals[iData][iFit] ) + 5
        toyHist = ROOT.TH1F( "toyVal_"+str(iData)+"_"+str(iFit), "toyVal_"+str(iData)+"_"+str(iFit), 50, xMin, xMax)
        toyHist.SetTitle("NLL of Toys vs Data; Negative Log Likelihood of Fit; Entries")
        toyHist.SetLineColor(ROOT.kBlack)
        toyHist.SetFillColor(ROOT.kAzure+1)


        for toyVal in ToyVals[iData][iFit]:
          toyHist.Fill( toyVal )
        leg.AddEntry(toyHist, "Pseudo", "lf")
        toyHist.Draw("")

        fitLine = ROOT.TLine(NLLs[iData][iFit], 0, NLLs[iData][iFit], toyHist.GetMaximum()+1)
        fitLine.SetLineWidth(2)
        fitLine.SetLineColor(ROOT.kRed)
        fitLine.Draw()
        leg.AddEntry(fitLine, "Actual", "l")

        canv1.cd()
        leg.Draw()

        ## Save Plot ##
        canv1.Print(args.outDir+"/ToyValues/ToyValues_"+outName+"_"+dataHists[iData].GetName()+'_'+fList["fNames"][iFit]+".png")
        canv1.Print(args.outDir+"/ToyValues/ToyValues_"+outName+"_"+dataHists[iData].GetName()+'_'+fList["fNames"][iFit]+".eps")
        outFile = ROOT.TFile(args.outDir+"/"+outName+".root", "UPDATE")
        canv1.SetName("ToyValues_"+outName+"_"+dataHists[iData].GetName()+'_'+fList["fNames"][iFit])
        canv1.Write("", ROOT.TObject.kOverwrite)
        outFile.Close()

  pad1.Clear()
  canv1.Clear()

  print "Done"

if __name__ == "__main__":
   main()
