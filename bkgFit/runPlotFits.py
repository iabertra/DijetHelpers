#!/usr/bin/env python

#######################################################################################
# runPlotFit.py
#
# This is a script for plotting the results of runSingleFit.py on all
# the expected TFitResults.
# Plotting will often be done against a changing value, and this value is assumed to be
# the last item in the '_' seperated histName
# Lists to loop over include:
#   files: a set of different input files
#   systematics: Systematic directories in the files
#   histNameVariations:  Potential variations to the input histograms (i.e. luminosity)
#
#   fitVariations:  Potential variations to fits (i.e. tolerance)
#       This variation should both change the fitting and include an addition
#       to the output name.
#
#######################################################################################

import subprocess, os, time, sys, glob

def main():

  f_plotSingleFits = True
  f_plotVaryingHists = False
  f_plotVaryingFits = False

  f_pseudoData = False
  f_scaled = False
  f_data = True
  f_tada = False
  f_allSystematics = False
  f_noDirStructure = False # If histograms are directly in the file, and not in systematic TDir structure

  f_toyError = True
  f_funcError = False

  ################ files ##################3

  #files = glob.glob("input/IBLOffData/*.root")
  files = glob.glob("input/CurrentData_Full/*.root")
  files += glob.glob("input/CurrentData_IBLOff/*.root")
#  files = glob.glob("input/Data_v4/*.root")
  ## exclusion run ##
  #files = glob.glob("input/WithData/dataLikeHists_MC15_DoubleBinning_*/*v1/*.root")

  ########### Histogram names ###########
  #histNameBase = "m23"
  histNameBase = "mjj"
  #histNameBase = "mbj"

#### TADA ###
#  files = glob.glob("input/Gamma3Jet/data_Mgjjj.2015.root")
#  histNameBase = "mgjjj"
#  f_tada = True

    ## Base Name ##
  if (f_pseudoData):
    histNameBase += "_PseudoData" #PseudoData
  elif (f_tada):
    histNameBase += "_TADA"
  elif (f_data):
    histNameBase += "_Data"
  elif (f_scaled):
    histNameBase += "_Scaled" #Scaled
  else:
    histNameBase += "_DataLike"

  sampleNames = [""]

  ########### Systematics Dir Structure###############
  if( f_noDirStructure):
    systematics = [""] #None
  elif (f_allSystematics):
    systematics = [
      "Nominal",
      "JET_GroupedNP_1__1down",
      "JET_GroupedNP_1__1up",
      "JET_GroupedNP_2__1down",
      "JET_GroupedNP_2__1up",
      "JET_GroupedNP_3__1down",
      "JET_GroupedNP_3__1up",
    ]
  else:
    systematics = ["Nominal"]

  ########## Comparison String #############
  # A string for comparing the test statistic of several fits
  # This must always be the last item in the histname (seperated by '_')
  # It must be a float followed by the comparisongString
  # I.E. fb for luminosity, or tol for fit tolerance
  comparison = ''
  comparison = 'fb'
  #comparison = 'WP'
  f_plotVaryingHists = True

  VaryFits = [""]
  ### For tolerance ###
 # comparison = 'tol'
 # f_plotVaryingFits = True
 # VaryFits = ["_0p001tol", "_0p01tol", "_0p1tol", "_1tol", "_10tol"]
 # histNameBase = "mjj_DataLike_QCDDiJet"
 # sampleNames = ["_0p1fb", "_0p3fb", "_0p5fb", "_0p7fb", "_1fb", "_3fb", "_5fb", "_7fb", "_10fb"]




  ######################### Begin Looping Over Jobs ##################
  pids = []
  logFiles = []
  NCORES = 1
  if not os.path.exists("logs/"):
    os.mkdir("logs/")

  ###### plotSingleFits #######
# Requires a fitVariation if we're doing it!!
  if(f_plotSingleFits):
    for thisFitVar in VaryFits:
      for thisSys in systematics:
        for thisSample in sampleNames:
          for thisFile in files:

            if len(pids) >= NCORES:
               wait_completion(pids)

            sendString = 'python plotSingleFits.py -b '
            #sendString += '--plotChi2 --noNLLPlot '
            logFile = "logs/plotting"
            sendString += '--file '+thisFile+' --outDir plots/'+thisFile[:-5]+' '
            #sendString += '--file '+thisFile+' --outDir plots/'+os.path.basename(thisFile).split('.')[0]+' '
            logFile += "_{0}".format(os.path.basename(thisFile)[:-5])

            histName = histNameBase+thisSample
            sendString += '--histName '+histName+' '
            logFile += "_{0}".format(histName)

            if len(thisSys) > 0:
              sendString += '--systematic '+thisSys+' '
              logFile += "_{0}".format(thisSys)

            if len(thisFitVar) > 0:
              sendString += '--fitVariation '+thisFitVar+' '
              logFile += "_{0}".format(thisFitVar)

            if f_toyError:
              sendString += '--toyError '
            if f_funcError:
              sendString += '--funcError '

            print "Starting job", sendString
            logFile += '.txt'

            res = submit_local_job(sendString, logFile)
            pids.append(res[0])
            logFiles.append(res[1])

  ###### plotVaryingHists #######
  if(f_plotVaryingHists):
    for thisSample in sampleNames:
      for thisSys in systematics:
        for thisFile in files:

          if len(pids) >= NCORES:
             wait_completion(pids)

          sendString = 'python plotVaryingHists.py -b '
          logFile = "logs/plotting"
          sendString += '--file '+thisFile+' --outDir plots/'+thisFile[:-5]+' '
          #sendString += '--file '+thisFile+' --outDir plots/'+os.path.basename(thisFile).split('.')[0]+' '
          logFile += "_{0}".format(os.path.basename(thisFile)[:-5])

          histName = histNameBase+thisSample
          sendString += '--histName '+histName+' '
          logFile += "_{0}".format(histName)

          if len(thisSys) > 0:
            sendString += '--systematic '+thisSys+' '
            logFile += "_{0}".format(thisSys)

          if len(comparison) > 0:
            sendString += '--comparison '+comparison+' '
            logFile += "_{0}".format(comparison)

          print "Starting job", sendString
          logFile += '.txt'

          res = submit_local_job(sendString, logFile)
          pids.append(res[0])
          logFiles.append(res[1])

  ###### plotVaryingFits #######
  if(f_plotVaryingFits):
    for thisFile in files:
      for thisSys in systematics:
        for thisSample in sampleNames:

          if len(pids) >= NCORES:
             wait_completion(pids)

          sendString = 'python plotVaryingFits.py -b '
          logFile = "logs/plotting"
          sendString += '--file '+thisFile+' --outDir plots/'+thisFile[:-5]+' '
          #sendString += '--file '+thisFile+' --outDir plots/'+os.path.basename(thisFile).split('.')[0]+' '
          logFile += "_{0}".format(os.path.basename(thisFile)[:-5])

          histName = histNameBase+thisSample
          sendString += '--histName '+histName+' '
          logFile += "_{0}".format(histName)

          if len(thisSys) > 0:
            sendString += '--systematic '+thisSys+' '
            logFile += "_{0}".format(thisSys)

          if len(comparison) > 0:
            sendString += '--comparison '+comparison+' '
            logFile += "_{0}".format(comparison)

          print "Starting job", sendString
          logFile += '.txt'

          res = submit_local_job(sendString, logFile)
          pids.append(res[0])
          logFiles.append(res[1])



  wait_all(pids)
  for f in logFiles:
    f.close()

def submit_local_job(exec_sequence, logfilename):
  #os.system("rm -f "+logfilename)
  output_f=open(logfilename, 'w')
  pid = subprocess.Popen(exec_sequence, shell=True, stderr=output_f, stdout=output_f)

  return pid, output_f

def wait_completion(pids):
  print """Wait until the completion of one of the launched jobs"""
  while True:
    for pid in pids:
      if pid.poll() is not None:
        print "\nProcess", pid.pid, "has completed"
        pids.remove(pid)
        return
    print ".",
    sys.stdout.flush()
    time.sleep(3) # wait before retrying

def wait_all(pids):
  print """Wait until the completion of all launched jobs"""
  while len(pids)>0:
    wait_completion(pids)
  print "All jobs finished!"

if __name__ == "__main__":
  main()



