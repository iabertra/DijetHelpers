#!/bin/python

from ROOT import *
import subprocess
import os

SamplesDic = {

   "CI": ("CI, Contact Interactions, constructive interference",
          "CI_minusLL",
          "CI_histograms",
          "nominal.mc15_13TeV.301339.Pythia8EvtGen_A14NNPDF23LO_CI_minusLL_Lambda7TeV_JZ3W.root",
          "mc15_13TeV_301339_Pythia8EvtGen_A14NNPDF23LO_CI_minusLL_Lambda7TeV",
          "Pythia8EvtGen_A14NNPDF23LO_CI_minusLL_Lambda7TeV",
          ),

}

Variables = ["chi_for_mjj_4000_4300"]#, "mjj", "m13", "first_jet_pt", "second_jet_pt", "third_jet_pt"]

for key, value in SamplesDic.iteritems() :

    selection = key
    title = value[0]
    sampleKey = value[1]
    directoryName = value[2]
    dummyFile = value[3]
    suffix =  value[4]
    fileSuffix = value[5]

    inputDirectory = "RawInputs/"+directoryName

    fout = TFile.Open("MergedHistograms_"+selection+".root","RECREATE")

    inputFile = TFile.Open(inputDirectory+"/"+dummyFile, "READ")\

    #loop on all files and add those that contain "Scaled_m23" and the like
    out = subprocess.check_output("ls "+inputDirectory+"|grep root", shell=True)

    fileList = out.split()

    print fileList

    for variable in Variables :

        print "Scaled_"+variable+"_"+suffix+"_JZ3W"
        histo = inputFile.Get("Scaled_"+variable+"_"+suffix+"_JZ3W")
        inputFile.ls()
        print histo
        merged_histo=histo.Clone()
        merged_histo.Reset("ICE")

        for fileName in fileList :

            fin = TFile.Open(inputDirectory+"/"+fileName, "READ")
            print fileName.split("_")
            #this is sample-dependent
            DSID = fileName.split("_")[1].split(".")[1]
            JXSlice = fileName.split("_")[6].split(".")[0]
            #if JXSlice != "JZ6W" : continue
            histoToAdd = fin.Get("Scaled_"+variable+"_mc15_13TeV_"+str(DSID)+"_"+fileSuffix+"_"+JXSlice)
            print "Scaled_"+variable+"_mc15_13TeV_"+str(DSID)+"_"+fileSuffix+"_"+JXSlice
            print histoToAdd
            print suffix
            histoToAdd.Print("all")
            merged_histo.Add(histoToAdd)
            fin.Get("Scaled_"+variable+"_mc15_13TeV_"+str(DSID)+"_"+fileSuffix+"_"+JXSlice).Print("all")
            fin.Close()

        fout.cd()
        merged_histo.SetName(variable+"_"+selection)
        merged_histo.Write(variable+"_"+selection)

    fout.ls()
    fout.Close()