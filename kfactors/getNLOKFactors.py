#!/bin/python
# -*- coding: utf-8 -*-

#******************************************
#get k-factors for Pythia8 dijet mass

#******************************************
#import stuff
import ROOT
import sys, os, array
import numpy as np

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def getKFactors(inputLOFileName, inputNLOCorrFileName, inputLOPSFileName,
                inputLOHistName, inputNLOCorrHistName, inputLOPSHistName,
                lumi, tag):

    print '\n******************************************\ncomparing mjj distributions'
    print '\nparameters:'
    print '  input LO file name:        %s'%inputLOFileName
    print '  input NLO corr. file name: %s'%inputNLOCorrFileName
    print '  input LO+PS file name:     %s'%inputLOPSFileName
    print '  hist LO name:              %s'%inputLOHistName
    print '  hist NLO corr. name:       %s'%inputNLOCorrHistName
    print '  hist LO+PS name :          %s'%inputLOPSHistName
    print '  lumi:                      %s'%lumi
    print '  tag:                       %s'%tag

    #------------------------------------------
    logx = True

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
    
    #------------------------------------------
    #open files
    if not os.path.isfile(inputLOFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input LO file: %s'%inputLOFileName)
    fLO = ROOT.TFile(inputLOFileName)

    if not os.path.isfile(inputNLOCorrFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input NLO correction file: %s'%inputNLOCorrFileName)
    fNLOC = ROOT.TFile(inputNLOCorrFileName)

    if not os.path.isfile(inputLOPSFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input LO+PS file: %s'%inputLOPSFileName)
    fLOPS = ROOT.TFile(inputLOPSFileName)

    #------------------------------------------
    #set weights
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get histograms
    hLO = fLO.GetDirectory('Nominal').Get(inputLOHistName)
    if not hLO:
        raise SystemExit('\n***ERROR*** couldn\'t find input LO histogram')

    #NOTE the NLO correction histogram has one more bin w.r.t LO
    hNLOC = fNLOC.GetDirectory('xSection').Get(inputNLOCorrHistName)
    if not hNLOC:
        raise SystemExit('\n***ERROR*** couldn\'t find input NLO correction histogram')

    hLOPS = fLOPS.GetDirectory('Nominal').Get(inputLOPSHistName)
    if not hLOPS:
        raise SystemExit('\n***ERROR*** couldn\'t find input LO+PS histogram')

    #------------------------------------------
    #apply NLO correction to LO histogram
    print '\napplying NLO correction to LO distribution'
    if hLO.GetNbinsX() != hNLOC.GetNbinsX():
        print '\n***WARNING*** LO and NLO correction histograms have different number of bins: %s vs. %s'%(hLO.GetNbinsX(), hNLOC.GetNbinsX())
        print '***WARNING*** using LO histogram as a reference for the binning'
        #raise SystemExit('***WARNING*** LO and NLO correction histograms have different number of bins: %s vs. %s'%(hLO.GetNbinsX(), hNLOC.GetNbinsX()))
        
    hNLO = ROOT.TH1D('mjj_NLO', 'mjj_NLO', hLO.GetXaxis().GetNbins(), hLO.GetXaxis().GetXbins().GetArray())
    for ii in range(1, hLO.GetNbinsX()+1):

        #OPTION 1: take into accout one bin shift
        '''
        print '\n  bin %s'%ii
        iic = ii+1
        lol   = hLO.GetBinLowEdge(ii) 
        loh   = hLO.GetBinLowEdge(ii+1)
        nlocl = hNLOC.GetBinLowEdge(ii+1)
        nloch = hNLOC.GetBinLowEdge(ii+2)
        print '    LO bin edges:        %s  %s'%(lol, loh)
        print '    NLO corr. bin edges: %s  %s'%(nlocl, nloch)
        if (lol != nlocl) or (loh != nloch):
            print '***WARNING*** LO and NLO correction histograms have different bin edges'
            break
        '''
        
        #OPTION 2: look for bin with same lower edge (more flexible, no need to know the bin shift)
        iic = hNLOC.FindBin( hLO.GetBinLowEdge(ii) )
        #if  (hLO.GetBinLowEdge(ii) !=  hNLOC.GetBinLowEdge(iic)) or (hLO.GetBinLowEdge(ii+1) !=  hNLOC.GetBinLowEdge(iic+1)):#DEFAULT
        if  not np.allclose(hLO.GetBinLowEdge(ii),   hNLOC.GetBinLowEdge(iic),   1e-6, 0.0) or \
            not np.allclose(hLO.GetBinLowEdge(ii+1), hNLOC.GetBinLowEdge(iic+1), 1e-6, 0.0):
            print '***WARNING*** LO and NLO correction histograms have different bin (%s) edges: %s - %s vs. %s - %s'%(ii, hLO.GetBinLowEdge(ii), hLO.GetBinLowEdge(ii+1), hNLOC.GetBinLowEdge(iic), hNLOC.GetBinLowEdge(iic+1))
            #print type(hLO.GetBinLowEdge(ii))
            break
        
        #Jeff #CHECK
        #content = hLO.GetBinContent(ii) * (1. + hNLOC.GetBinContent(ii))
        #error = hLO.GetBinError(ii) * (1. + hNLOC.GetBinContent(ii))

        #Pavel #OK
        if hNLOC.GetBinContent(ii) != 0.:
            content = hLO.GetBinContent(ii) / hNLOC.GetBinContent(iic)
        else:
            content = 0.
        if hNLOC.GetBinContent(ii) != 0.:
            error = hLO.GetBinError(ii) / hNLOC.GetBinContent(iic) #NOTE NLO corrections have no error
        else:
            error = 0.
        
        hNLO.SetBinContent(ii, content)
        hNLO.SetBinError(ii, error)

    #------------------------------------------
    #scale LOPS to same area as LO
    hLOPS.Scale(hLO.Integral() / hLOPS.Integral())

    #------------------------------------------
    #get k-factors
    hkf=hNLO.Clone()
    hkf.Divide(hLOPS)
    hkf.SetName('NLOkfactors')
    hkf.SetTitle('NLOkfactors')
    
    #------------------------------------------
    #save k-factors
    if tag == 'ok' or tag == 'note' or tag == '':
        outputFileName = localdir+'/results/NLO.kfactors.root'
    else:
        outputFileName = localdir+'/results/NLO.kfactors.'+tag+'.root'
    outputFile = ROOT.TFile.Open(outputFileName, 'RECREATE')
    outputFile.cd()
    hkf.Write()

    #------------------------------------------
    #canvas
    c = ROOT.TCanvas('c', 'c', 400, 50, 800, 600)

    #pads
    outpad = ROOT.TPad("extpad","extpad", 0., 0.,   1., 1.)
    pad1   = ROOT.TPad("pad1",  "pad1",   0., 0.33, 1., 1.)
    pad2   = ROOT.TPad("pad2",  "pad2",   0., 0.,   1., 0.33)

    #setup drawing options
    outpad.SetFillStyle(4000)#transparent

    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)

    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.3)
    pad2.SetBorderMode(0)

    pad1.Draw()
    pad2.Draw()
    outpad.Draw()

    #------------------------------------------
    #draw histograms
    pad1.cd()
    pad1.SetLogy(1)
    pad1.SetLogx(logx)
    pad1.SetGrid(1,1)

    hLO.GetYaxis().SetTitle("events/bin")
    hLO.GetYaxis().SetTitleFont(43)
    hLO.GetYaxis().SetTitleSize(20)
    hLO.GetYaxis().SetLabelFont(43)
    hLO.GetYaxis().SetLabelSize(20)
    hLO.GetXaxis().SetTitle("m_{jj} [GeV]")
    hLO.GetXaxis().SetTitleFont(43)
    hLO.GetXaxis().SetTitleSize(20)
    hLO.GetXaxis().SetLabelFont(43)
    hLO.GetXaxis().SetLabelSize(20)
    hLO.SetMarkerStyle(0)#24
    hLO.SetMarkerColor(ROOT.kRed)
    hLO.SetLineColor(ROOT.kRed)
    hLO.SetFillStyle(0)
    hLO.SetLineWidth(2)
    hLO.Draw("E")

    hLOPS.SetMarkerStyle(0)#25
    hLOPS.SetMarkerColor(ROOT.kBlue)
    hLOPS.SetLineColor(ROOT.kBlue)
    hLOPS.SetFillStyle(0)
    hLOPS.SetLineWidth(2)
    hLOPS.Draw("same E")
    
    hNLO.SetMarkerStyle(0)#26
    hNLO.SetMarkerColor(ROOT.kGreen+1)
    hNLO.SetLineColor(ROOT.kGreen+1)
    hNLO.SetFillStyle(0)
    hNLO.SetLineWidth(2)
    hNLO.Draw("same E")

    '''
    hs = ROOT.THStack('hs','hs')
    hs.Add(hLO)
    hs.Add(hLOPS)
    hs.Add(hNLO)
    hs.Draw('nostack')
    hs.GetYaxis().SetTitle("events/bin")
    hs.GetYaxis().SetTitleFont(43)
    hs.GetYaxis().SetTitleSize(20)
    hs.GetXaxis().SetTitle("m_{jj} [GeV]")
    hs.GetXaxis().SetLabelFont(43)
    hs.GetXaxis().SetLabelSize(20)
    '''
    c.Update()

    #------------------------------------------
    #draw ratio
    pad2.cd()
    
    pad2.SetLogy(0)
    pad2.SetLogx(logx)
    pad2.SetGrid(1,1)

    hkf.SetMarkerStyle(0)#27
    hkf.SetMarkerColor(ROOT.kBlack)
    hkf.SetLineColor(ROOT.kBlack)
    hkf.SetFillStyle(0)
    hkf.SetLineWidth(2)
    
    hkf.GetYaxis().SetTitle('#splitline{k-factors}{NLO/LO+PS}')
    hkf.GetYaxis().SetTitleFont(43)
    hkf.GetYaxis().SetTitleSize(20)
    hkf.GetYaxis().SetLabelFont(43)
    hkf.GetYaxis().SetLabelSize(20)

    hkf.GetXaxis().SetTitle('m_{jj} [GeV]')
    hkf.GetXaxis().SetTitleFont(43)
    hkf.GetXaxis().SetTitleSize(20)
    hkf.GetXaxis().SetTitleOffset(4.0)
    hkf.GetXaxis().SetLabelFont(43)
    hkf.GetXaxis().SetLabelSize(20)
    hkf.GetYaxis().SetMoreLogLabels(1)

    hkf.Draw('E')
    hkf.SetAxisRange(0.8,1.2,'Y')
    c.Update()
    
    #------------------------------------------
    #draw labels and legend
    pad1.cd()

    #ATLAS
    aX = 0.65
    aY = 0.85
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
    a.DrawLatex(aX,aY,'ATLAS')
		
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    p.DrawLatex(aX+0.13,aY,'internal')
    
    #note
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(42)
    n.SetTextColor(1)
    n.SetTextSize(0.04)
    n.DrawLatex(aX,aY-0.06,'#sqrt{s}=13 TeV')
    if float(lumi) > 0.:
        n.DrawLatex(aX,aY-0.14,'#intL dt = %s fb^{-1}'%lumi)
    #n.DrawLatex(aX,aY-0.18,'TEST')

    #legend
    lXmin = aX
    lXmax = aX+0.15
    lYmin = aY-0.35
    lYmax = aY-0.20
    l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(15)
    l.AddEntry(hLO,'LO','pl')
    l.AddEntry(hLOPS,'LO+PS','pl')
    l.AddEntry(hNLO,'NLO','pl')
    l.Draw()
    
    c.Update()
    #c.WaitPrimitive()
    if tag == 'ok' or tag == 'note' or tag == '':
        c.SaveAs(localdir+'/figures/NLO.kfactors.pdf')
    else:
        c.SaveAs(localdir+'/figures/NLO.kfactors.'+tag+'.pdf')
    del c

    outputFile.Write()
    outputFile.Close()

#******************************************
if __name__ == "__main__":

    #check input parameters
    if len(sys.argv) != 9:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
             \nHOW TO: python -u getKFactors.py inputLOFileName inputNLOCorrFileName inputLOPSFileName inputLOHistName inputNLOCorrHistName inputLOPSHistName lumi tag'
            %(len(sys.argv), 9))

    #get input parameters
    inputLOFileName = sys.argv[1].strip()
    inputNLOCorrFileName = sys.argv[2].strip()
    inputLOPSFileName = sys.argv[3].strip()
    inputLOHistName = sys.argv[4].strip()
    inputNLOCorrHistName = sys.argv[5].strip()
    inputLOPSHistName = sys.argv[6].strip()
    lumi = sys.argv[7].strip()
    tag = sys.argv[8].strip()

    #TEST
    #inputLOFileName = 'inputs/LO.root'
    #inputNLOCorrFileName = 'inputs/NLOCorr.root'
    #inputLOPSFileName = 'inputs/LOPS.root'
    #inputLOHistName = 'mjj_Scaled_QCDDiJet_1fb'
    ##inputNLOCorrHistName = 'ratioNLO_over_LO'#Jeff #CHECK
    #inputNLOCorrHistName = 'ratioLO_over_Full'#Pavel #OK
    #inputLOPSHistName = 'mjj_Scaled_QCDDiJet_1fb'
    #lumi = '1'
    #tag = sys.argv[1].strip()
    #END TEST
    
    #plot
    getKFactors(inputLOFileName, inputNLOCorrFileName, inputLOPSFileName, inputLOHistName, inputNLOCorrHistName, inputLOPSHistName, lumi, tag)
    print '\ndone'


