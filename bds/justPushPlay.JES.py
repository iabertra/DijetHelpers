#!/usr/bin/env python

#******************************************
#import stuff
import checkJES

#******************************************
if __name__ == '__main__':

    print '\n******************************************'
    print 'looping over fit function parameters'

    #inputFileName = '/atlas/data5/userdata/guescini/dijet/inputs/MC15a_20150623/dataLikeHists_v1/dataLikeHistograms.QCDDiJet.root'#Pythia
    inputFileName = '/atlas/data5/userdata/guescini/dijet/inputs/PowhegPythia8/Powheg_JZ_20150702/dataLikeHists_v1/dataLikeHistograms.QCDDiJet.root'#Powheg
    nParameters = ['3', '4', '5']
    luminosity = ['1']
    tag = 'Powheg.jes'
    
    #combine input parameters
    combinations = [(np, l) for l in luminosity for np in nParameters]
    
    #loop over combinations
    for combination in combinations:

        #print '  %s %s %s'%(combination[0], combination[1], combination[2])
        print '\n******************************************'
        print 'n parameters: %s'%combination[0]
        print 'luminosity:   %s'%combination[1]
        print 'tag:          %s'%tag

        checkJES.checkJES(inputFileName,
                        combination[1],
                        combination[0],
                        tag)
        print '\ndone'
        
    #------------------------------------------
    print '\n******************************************'
    print '\ndone looping over combinations'

    #time python checkJES.py /atlas/data5/userdata/guescini/dijet/inputs/MC15a_20150623/dataLikeHistograms/dataLikeHists_v1/dataLikeHistograms.QCDDiJet.root 1 3 testJES.01
