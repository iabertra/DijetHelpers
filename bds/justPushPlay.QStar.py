#!/usr/bin/env python

#******************************************
#import stuff
from findBarelyDetectableQStar import findBarelyDetectableQStar

#******************************************
if __name__ == '__main__':

    print '\n******************************************'
    print 'looping over q* mass values'

    tag = 'smooth'#poisson, smooth
    
    #combine input parameters
    smooth = [True]#,False]
    luminosity = ['1']#/fb #['1', '10']
    masses = ['5500']
    nParameters = ['3', '4', '5']
    combinations = [(s, l, m, np) for s in smooth for l in luminosity for m in masses for np in nParameters]
    #combinations = [(True, 1, 3000, 0.15, 4)]#TEST
    
    #loop over combinations
    for combination in combinations:
        
        print '\n******************************************'
        #print '  %s %s %s %s %s'%(combination[0], combination[1], combination[2], combination[3])
        print 'smooth:       %s'%combination[0]
        print 'luminosity:   %s'%combination[1]
        print 'signal mass:  %s'%combination[2]
        print 'n parameters: %s'%combination[3]
        print 'tag:          %s'%tag        
        out = findBarelyDetectableQStar( str(combination[2]),
                                          str(combination[1]),
                                          str(combination[3]),
                                          tag,
                                          combination[0],
                                          True)
        print '\ndone %s'%(list(out)) #n steps, n signal events, BH p-value
        
    #------------------------------------------
    print '\n******************************************'
    print '\ndone looping over combinations'
