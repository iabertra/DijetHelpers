#!/usr/bin/env python

#******************************************
#import stuff
import ROOT
import math, os, sys
import injectQStar
import runSearchPhase
import plotSearchPhase
import plotFuncRatio

#******************************************
#set ATLAS style #MOVED inside the functions
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()


#******************************************
def findBarelyDetectableQStarWilks(mass, lumi, nPar1, nPar2, tag):

    print '\n******************************************'
    print 'finding barely detectable q* and compare two fit functions'

    #------------------------------------------
    #input parameters
    nPar1 = int(nPar1)
    nPar2 = int(nPar2)
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')
    stag = mass+'.GeV.'+slumi+'.ifb.'+str(nPar1)+'.par1.'+str(nPar2)+'.par2'
    stag1 = mass+'.GeV.'+slumi+'.ifb.'+str(nPar1)+'.par'
    stag2 = mass+'.GeV.'+slumi+'.ifb.'+str(nPar2)+'.par'

    print '\nparameters:'
    print '  mass:          %s'%mass
    print '  lumi [ifb]:    %s'%lumi
    print '  n par. def.:   %s'%nPar1
    print '  n par. alt.:   %s'%nPar2
    print '  tag:           %s'%tag
    print '  string tag:    %s'%stag

    #------------------------------------------
    #TEST
    #raise SystemExit('\n***TEST*** exit')
    #------------------------------------------

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get input QCD histogram
    #NOTE this file and histogarms are created using the makeInputDijetMassDistributionFromFit.py script
    inputFileName = '/atlas/data5/userdata/guescini/dijet/inputs/MC15a_20150707/dataLikeHists_v1/dataLikeHistograms.QCDDiJet.root'
    inputHistDir = 'Nominal'
    inputHistName = 'mjj_Smooth_QCDDiJet_'+str(lumi)+'fb'

    print '\n  input file name: %s'%inputFileName
    print '  input hist dir: %s'%inputHistDir
    print '  input hist name: %s'%inputHistName

    #get QCD histogram
    f = ROOT.TFile.Open(inputFileName,'READ')
    if not f:
        raise SystemExit('\n***ERROR*** couldn\'t open input file')
    if len(inputHistDir) > 0:
        qcd = f.GetDirectory(inputHistDir).Get(inputHistName)
    else:
        qcd = f.Get(inputHistName)
    if not qcd:
        raise SystemExit('\n***ERROR*** couldn\'t find input histogram')
    
    #------------------------------------------
    #run search phase on QCD background only
    print '\n******************************************'
    print 'running search phase on QCD background only'

    #nPar1
    QCDtag1 = slumi+'.ifb.'+str(nPar1)+'.par'
    searchPhaseQCDOutputFileName1 = 'results/searchPhase.'+QCDtag1+'.QCD.'+tag+'.root'
    if os.path.isfile(searchPhaseQCDOutputFileName1):
        print '\n***WARNING*** QCD search phase results exist already (n par = %s)'%nPar1
    else:
        searchPhaseQCDOutputFileName1 = runSearchPhase.runSearchPhase(inputFileName, inputHistDir, inputHistName, lumi, nPar1, QCDtag1+'.QCD.'+tag)

    #nPar2
    QCDtag2 = slumi+'.ifb.'+str(nPar2)+'.par'
    searchPhaseQCDOutputFileName2 = 'results/searchPhase.'+QCDtag2+'.QCD.'+tag+'.root'
    if os.path.isfile(searchPhaseQCDOutputFileName2):
        print '\n***WARNING*** QCD search phase results exist already (n par = %s)'%nPar2
    else:
        searchPhaseQCDOutputFileName2 = runSearchPhase.runSearchPhase(inputFileName, inputHistDir, inputHistName, lumi, nPar2, QCDtag2+'.QCD.'+tag)

    #------------------------------------------
    #starting signal strength
    #NOTE start from half of the nominal cross section
    #norm = 0.5 #good for 3 TeV q* at 1/fb
    #norm = 1.5 #good for 4 TeV q* at 1/fb
    norm = 1.
    
    #------------------------------------------
    #LOOP 1
    #loop until: 0.010 < BH p-value < 0.015
    BHpvalHighMin = 0.010 #0.010
    BHpvalHighMax = 0.015 #0.015
    print '\n\n******************************************'
    print 'tuning q* injection INCLUDING signal range: %.3f < BH p-value < %.3f'%(BHpvalHighMin, BHpvalHighMax)
    print '******************************************'
    normHigh, bumpHunterPValueInitialHigh, bumpFoundHigh, injectedQStarFileName, bumpLowEdge1, bumpHighEdge1 = tuneQStarInjection(inputFileName, inputHistDir, inputHistName,
                                                                                        mass, norm, lumi, nPar1, stag1, 'includeSig.'+tag,
                                                                                        BHpvalHighMin, BHpvalHighMax, 100,
                                                                                        1.4, 0.8, #1.2, 0.9 #1.5, 0.8
                                                                                        False)
    
    if bumpHunterPValueInitialHigh < BHpvalHighMin or bumpHunterPValueInitialHigh > BHpvalHighMax:
        print '\n\n***WARNING*** signal injection tuning did NOT work'
        print '***WARNING*** initial BH p-value = %.4f'%bumpHunterPValueInitialHigh
        print '***WARNING*** BH range = %.4f - %.4f'%(BHpvalHighMin, BHpvalHighMax)
        print '  mass:          %s'%mass
        print '  lumi [ifb]:    %s'%lumi
        print '  n parameters:  %s'%nPar1
        print '  tag:           %s'%tag
        print '  string tag:    %s'%stag1
        print '  norm:          %s'%normHigh
        print '  bump found:    %s'%bumpFoundHigh
        
        #------------------------------------------
        #TEST
        #raise SystemExit('\n***EXIT*** exit')
        #------------------------------------------

    #run search with alternative fit function
    searchPhaseSignalIncludedOutputFile2 = runSearchPhase.runSearchPhase(injectedQStarFileName, '', 'mjj.QCD_QStar', lumi, nPar2, stag2+'.includeSig.'+tag)#DEFAULT #NOTE do not exclude any range a priori

    #------------------------------------------
    #LOOP 2
    #loop until: 0.005 < BH p-value < 0.010
    BHpvalLowMin = 0.005 #0.005
    BHpvalLowMax = BHpvalHighMin #0.010
    norm = normHigh
    print '\n\n******************************************'
    print 'tuning signal injection EXCLUDING signal range: %.3f < BH p-value < %.3f'%(BHpvalLowMin, BHpvalLowMax)
    print '******************************************'
    normLow, bumpHunterPValueInitialLow, bumpFoundLow, injectedQStarFileName, bumpLowEdge, bumpHighEdge = tuneQStarInjection(inputFileName, inputHistDir, inputHistName,
                                                                                    mass, norm, lumi, nPar1, stag1, 'excludeSig.'+tag,
                                                                                    BHpvalLowMin, BHpvalLowMax, 100,
                                                                                    1.05, 0.98, #1.1, 0.9
                                                                                    True)
    
    if bumpHunterPValueInitialLow < BHpvalLowMin or bumpHunterPValueInitialLow > BHpvalLowMax:
        print '\n\n***WARNING*** signal injection tuning did NOT work'
        print '***WARNING*** initial BH p-value = %.4f'%bumpHunterPValueInitialLow
        print '***WARNING*** BH range = %.4f - %.4f'%(BHpvalLowMin, BHpvalLowMax)
        print '  mass:          %s'%mass
        print '  lumi [ifb]:    %s'%lumi
        print '  n parameters:  %s'%nPar1
        print '  tag:           %s'%tag
        print '  string tag:    %s'%stag1
        print '  norm:          %s'%normLow
        print '  bump found:    %s'%bumpFoundLow
        
        #------------------------------------------
        #TEST
        #raise SystemExit('\n***EXIT*** exit')
        #------------------------------------------

    #run search with alternative fit function
    #searchPhaseSignalExcludedOutputFile2 = runSearchPhase.runSearchPhase(injectedQStarFileName, '', 'mjj.QCD_QStar', lumi, nPar2, stag2+'.excludeSig.'+tag)#DEFAULT
    searchPhaseSignalExcludedOutputFile2 = runSearchPhase.runSearchPhase(injectedQStarFileName, '', 'mjj.QCD_QStar', lumi, nPar2, stag2+'.excludeSig.'+tag, bumpLowEdge, bumpHighEdge)#TEST

    #------------------------------------------
    #plot results
    
    #plot SearchPhase results - signal included
    if (bumpHunterPValueInitialHigh > BHpvalHighMin or bumpHunterPValueInitialHigh < BHpvalHighMax):
        notes = []
        notes.append('m_{q*} = %.0f GeV'%float(mass))
        notes.append('sig. strength = %.3f'%float(normHigh))
        searchPhaseSignalIncludedOutputFile1 = 'results/searchPhase.'+stag1+'.includeSig.'+tag+'.root'
        plotSearchPhase.plotSearchPhase(searchPhaseSignalIncludedOutputFile1, notes)
        searchPhaseSignalIncludedOutputFile2 = 'results/searchPhase.'+stag2+'.includeSig.'+tag+'.root'
        plotSearchPhase.plotSearchPhase(searchPhaseSignalIncludedOutputFile2, notes)

    #plot SearchPhase results - signal excluded
    if (bumpHunterPValueInitialLow > BHpvalLowMin  or bumpHunterPValueInitialLow < BHpvalLowMax):
        notes = []
        notes.append('m_{q*} = %.0f GeV'%float(mass))
        notes.append('sig. strength = %.3f'%float(normLow))
        searchPhaseSignalExcludedOutputFile1 = 'results/searchPhase.'+stag1+'.excludeSig.'+tag+'.root'
        plotSearchPhase.plotSearchPhase(searchPhaseSignalExcludedOutputFile1, notes)
        searchPhaseSignalExcludedOutputFile2 = 'results/searchPhase.'+stag2+'.excludeSig.'+tag+'.root'
        plotSearchPhase.plotSearchPhase(searchPhaseSignalExcludedOutputFile2, notes)
        
    #plot functions ratios
    if (bumpHunterPValueInitialHigh > BHpvalHighMin or bumpHunterPValueInitialHigh < BHpvalHighMax) and (bumpHunterPValueInitialLow > BHpvalLowMin  or bumpHunterPValueInitialLow  < BHpvalLowMax):

        #------------------------------------------
        #TEST
        #print input parameters before plotting
        print '\n***TEST***'
        print 'parameters:'
        print '  QCD file name 1:            %s'%searchPhaseQCDOutputFileName1
        print '  QCD file name 2:            %s'%searchPhaseQCDOutputFileName2
        print '  signal file name (incl.) 1: %s'%searchPhaseSignalIncludedOutputFile1
        print '  signal file name (incl.) 2: %s'%searchPhaseSignalIncludedOutputFile2
        print '  signal file name (excl.) 1: %s'%searchPhaseSignalExcludedOutputFile1
        print '  signal file name (excl.) 2: %s'%searchPhaseSignalExcludedOutputFile2
        print '  model:                      %s'%'QStar'
        print '  mass:                       %s'%mass
        print '  width:                      %s'%0.
        print '  signal norm (incl.):        %s'%normHigh
        print '  signal norm (excl.):        %s'%normLow
        print '  lumi [ifb]:                 %s'%lumi
        print '  n parameters 1:             %s'%nPar1
        print '  n parameters 2:             %s'%nPar2
        print '  tag:                        %s'%tag
        #END TEST
        #------------------------------------------
        
        plotFuncRatio.plotDoubleFuncRatioWilks(searchPhaseQCDOutputFileName1,
                                                searchPhaseQCDOutputFileName2,
                                                searchPhaseSignalIncludedOutputFile1,
                                                searchPhaseSignalIncludedOutputFile2,
                                                searchPhaseSignalExcludedOutputFile1,
                                                searchPhaseSignalExcludedOutputFile2,
                                                'QStar',
                                                mass,
                                                0.,
                                                normHigh,
                                                normLow,
                                                lumi,
                                                nPar1,
                                                nPar2,
                                                tag)
    
    #------------------------------------------
    return (bumpHunterPValueInitialHigh, bumpHunterPValueInitialLow) #initial BH p-value signal included, initial BH p-value signal excluded


#******************************************
def tuneQStarInjection(inputFileName, inputHistDir, inputHistName,
                        mass, norm, lumi, nPar, stag, tag,
                        BHpvalMin=0.010, BHpvalMax=0.15, countMax=50,
                        increaseStep=1.6, decreaseStep=0.6, #1.75, 0.8 #1.5, 0.8 #1.6, 0.8
                        findBump=True):

    #------------------------------------------
    bumpHunterPValueInitial = 999.
    count = int(0)
    bumpFound = False

    #TEST
    normMax = -999.
    normMin = -999.
    rand3 = ROOT.TRandom3(1986) #1986
    
    #lists
    lBumpHunterPValue = []
    lNorm = []
    
    while (bumpHunterPValueInitial < BHpvalMin or bumpHunterPValueInitial > BHpvalMax) and count < countMax:

        print '\n\n******************************************'
        #print 'trial %s - signal peak normalization = %s'%(count, norm)
        print 'trial %s'%count
        print 'q* mass = %s'%mass
        print 'q* signal strength = %s'%norm
        if findBump:
            print 'excluding signal region'
        else:
            print 'including signal region'
        print '******************************************'

        #------------------------------------------
        #inject signal
        injectedQStarFileName = injectQStar.injectQStar(inputFileName, inputHistDir, inputHistName, mass, norm, lumi, str(nPar)+'.par.'+tag, True, False, True)#do NOT plot each step
        #injectedQStarFileName = injectQStar.injectQStar(inputFileName, inputHistDir, inputHistName, mass, norm, lumi, str(nPar)+'.par.'+tag, True, True, True)#plot injected spectrum before moving on

        #------------------------------------------
        #run search phase
        searchPhaseOutputFileName, bumpHunterPValueInitial, bumpHunterPValueFinal, bumpFound, chi2ndf, logLValue, bumpLowEdge, bumpHighEdge = runSearchPhase.runSearchPhase(injectedQStarFileName, '', 'mjj.QCD_QStar', lumi, nPar, stag+'.'+tag)
        print 'q* signal strength = %s'%norm

        #------------------------------------------
        #tune signal normalization
        #NOTE based on ***INITIAL*** BH p-value
        lBumpHunterPValue.append(bumpHunterPValueInitial)
        lNorm.append(norm)
        oldNorm = norm

        print '\n------------------------------------------'
        print 'initial BH p-values         = %s'%lBumpHunterPValue
        print 'q* signal strength = %s'%lNorm
        print 'loops = %s'%len(lBumpHunterPValue)
        print '------------------------------------------\n'

        #DEFAULT
        '''
        if bumpHunterPValueInitial > BHpvalMax:
            norm = norm*increaseStep      

        elif bumpHunterPValueInitial < BHpvalMin:
            norm = norm*decreaseStep
        '''
        
        #TEST
        #if bumpHunterPValueInitial > BHpvalMax:#DEFAULT
        if bumpHunterPValueInitial > BHpvalMax or float(bumpHighEdge) < 2000.:#TEST #CHECK #NOTE
            #increase norm
            normMin = norm
            if normMax<0:
                norm = norm*increaseStep
            else:
                #norm = (normMax + normMin)/2.
                mean = (normMax + normMin)/2.
                sigma = (normMax - normMin)/4.
                norm = rand3.Gaus(mean, sigma)

        elif bumpHunterPValueInitial < BHpvalMin:
            #decrease norm
            normMax = norm
            if normMin<0:
                norm = norm*decreaseStep
            else:
                #norm = (normMax + normMin)/2.
                mean = (normMax + normMin)/2.
                sigma = (normMax - normMin)/4.
                norm = rand3.Gaus(mean, sigma)
        #END TEST
        
        elif (findBump and bumpFound) or (not findBump and not bumpFound):
            injectedQStarFileName = injectQStar.injectQStar(inputFileName, inputHistDir, inputHistName, mass, norm, lumi, str(nPar)+'.par.'+tag, True, True, True)#OK, now plot
            break

        count+=1

    #------------------------------------------
    #check if injection has converged to the desired BH p-value range
    if count >= countMax and (bumpHunterPValueInitial < BHpvalMin or bumpHunterPValueInitial > BHpvalMax):
        print '\n***WARNING*** reached maximum number of trials (%s)'%countMax

    #------------------------------------------
    return norm, bumpHunterPValueInitial, bumpFound, injectedQStarFileName, bumpLowEdge, bumpHighEdge


#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 6:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u findBarelyDetectableQStarWilks.py mass lumi nPar1 nPar2 tag\n'\
            %(len(sys.argv),6))

    #------------------------------------------
    #get input parameters and run
    mass = sys.argv[1].strip()
    lumi = sys.argv[2].strip()
    nPar1 = sys.argv[3].strip()
    nPar2 = sys.argv[4].strip()
    tag = sys.argv[5].strip()

    out = findBarelyDetectableQStarWilks(mass, lumi, nPar1, nPar2, tag)
    
    #------------------------------------------
    print '\n******************************************'
    print '\ndone %s'%(list(out)) #n steps, n signal events, BH p-value
