#!/bin/python

#******************************************
#import stuff
import ROOT, sys, os
import raffaello

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()


#******************************************
def plotTomography(inputFileName):

    print '\n******************************************\nplotting search phase results tomography'
    print '\nparameters:'
    print '  input file name:      %s'%inputFileName

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
    
    #------------------------------------------
    #get info from input file name
    baseFileName = inputFileName.split('/')[-1]
    sBaseFileName = baseFileName.split('.')

    sigMass = 999.
    sigWidth = 999.
    luminosity = 999.
    fitFuncPar = 999
    
    for ii,s in enumerate(sBaseFileName):
        #print '%s %s'%(ii, s)
        if s == 'GeV':
            sigMass = float(sBaseFileName[ii-1])
        if s == 'width':
            sigWidth = float(sBaseFileName[ii-1].replace('p','.'))
        if s == 'ifb':
            luminosity = float(sBaseFileName[ii-1].replace('p','.'))
        if s == 'par':
            fitFuncPar = int(sBaseFileName[ii-1])

    print '\nsignal mass = %.1f'%sigMass
    print 'signal width = %.2f'%sigWidth
    print 'luminosity = %.1f'%luminosity
    print 'fit func. par. = %i'%fitFuncPar
    
    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%inputFileName)
    inputFile = ROOT.TFile(inputFileName)

    #------------------------------------------
    #set weights
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get SearchPhase results
    #histograms
    tomography = inputFile.Get("bumpHunterTomographyFromPseudoexperiments")

    #vector
    bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToData")
    #bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToDataInitial")#TEST #NO
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHigh')
    #bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHighInitial')#TEST #NO
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    bumpFoundVector = inputFile.Get("bumpFound")
    bumpFound = bumpFoundVector[0]
    
    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "bump window excluded? %s"%bumpFound
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas

    #------------------------------------------
    #define necessary quantities
    sBHpval = ('%.3f'%bumpHunterPValue).replace('.','p')
    #name = localdir+'/figures/+'.'.join( sBaseFileName[0:len(sBaseFileName)-1])#DEFAULT
    name = localdir+'/figures/tomography.'+'.'.join( sBaseFileName[1:len(sBaseFileName)-1])#TEST
    notes = []
    notes.append('sig. mass = %.0f GeV'%float(sigMass))
    notes.append('sig. width = %.0f%%'%(float(sigWidth)*100.))
    
    #------------------------------------------
    #plot
    raffaello.drawTomography(tomography,
                                'm_{jj} [GeV]',
                                'events',
                                'significance',
                                name,
                                luminosity,
                                13,
                                2000.,
                                14000.,
                                bumpHunterPValue,
                                fitFuncPar,
                                -1,
                                -1,
                                bumpLowEdge,
                                bumpHighEdge,
                                bumpFound,
                                False,
                                notes)
    

#******************************************
if __name__ == "__main__":

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 2:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \\nplot serach phase results from Statistical Analysis Bayesian tool \
            \nHOW TO: python -u plotSearchResults.py inputFileName'
            %(len(sys.argv), 2))
    
    #get input parameters
    inputFileName = sys.argv[1].strip()

    #plot
    plotTomography(inputFileName)
    print '\ndone'


"""
#******************************************
if __name__ == "__main__":

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 2:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \\nplot serach phase results from Statistical Analysis Bayesian tool \
            \nHOW TO: python -u plotSearchResults.py inputFileName'
            %(len(sys.argv), 2))
    
    #------------------------------------------
    #get input parameters
    inputFileName = sys.argv[1].strip()
    print '\n******************************************\nplotting search phase results'

    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%inputFileName)
    inputFile = ROOT.TFile(inputFileName)

    #------------------------------------------
    #first things first
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    tomography = inputFile.Get("bumpHunterTomographyFromPseudoexperiments")
    
    #------------------------------------------
    #canvas
    c = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    c.SetLogx(1)
    c.SetLogy(1)

    #------------------------------------------
    #draw
    tomography.SetLineColor(ROOT.kRed)
    tomography.SetTitle("");
    tomography.GetXaxis().SetTitle("m_{jj} [GeV]")
    tomography.GetXaxis().SetNdivisions(805,ROOT.kTRUE)
    #tomography.GetXaxis().SetMoreLogLabels()
    tomography.GetYaxis().SetTitle("BH p-value")
    tomography.GetYaxis().SetTitleOffset(1.2);
    #tomography.GetYaxis().SetMoreLogLabels()
    tomography.SetMarkerColor(ROOT.kRed)
    tomography.SetMarkerSize(0.2)
    tomography.Draw("AP");

    #------------------------------------------
    #position
    lXmin=0.64
    lXmax=0.94
    lYmin=0.45
    lYmax=0.85

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
    a.DrawLatex(lXmin,lYmax,'ATLAS')
        
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    p.DrawLatex(lXmin+0.13,lYmax,'internal')

    #notes
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(43)
    n.SetTextColor(1)
    n.SetTextSize(15)
    n.DrawLatex(lXmin,lYmax-0.04,'#sqrt{s} = %s TeV'%CME)
    if float(luminosity)>0.:
        n.DrawLatex(lXmin,lYmax-0.10,'#intLdt = %.1f fb^{-1}'%float(luminosity))
    n.DrawLatex(lXmin,lYmax-0.16,'bump range = %.0f - %.0f GeV'%(float(bumpLow), float(bumpHigh)))
    n.DrawLatex(lXmin,lYmax-0.20,'BH p-value = %.4f'%float(BHpval))
    if bumpFound:
        n.DrawLatex(lXmin,lYmax-0.24,'bump range excluded')
    else:
        n.DrawLatex(lXmin,lYmax-0.24,'bump range not excluded')            
    n.DrawLatex(lXmin,lYmax-0.28,'%s par. fit func.'%fitFuncPar)
    for ii, note in enumerate(notes):
        n.DrawLatex(lXmin,lYmax-0.32-0.04*ii,note)
    c.Update()

    #------------------------------------------    
    c.WaitPrimitive()
    """
