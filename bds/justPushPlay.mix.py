#!/usr/bin/env python

#******************************************
#import stuff
from checkMix import checkMix

#******************************************
if __name__ == '__main__':

    print '\n******************************************'
    print 'looping over mixing fractions and fit parameters'

    #inputFileName = '/atlas/data5/userdata/guescini/dijet/inputs/MC15a_20150623/dataLikeHists_v1/dataLikeHistograms.QCDDiJet.root'#Pythia
    inputFileName = '/atlas/data5/userdata/guescini/dijet/inputs/PowhegPythia8/Powheg_JZ_20150702/dataLikeHists_v1/dataLikeHistograms.QCDDiJet.root'#Powheg
    inputHistDir = 'Nominal'
    tag = 'Powheg.mixing'
    
    #combine input parameters
    luminosity = ['1']#/fb
    nParameters = ['3', '4', '5']
    fractions = [['2', '1', '1', '1'], ['1', '2', '1', '1'], ['1', '1', '2', '1'], ['1', '1', '1', '2'], ['1', '1', '1', '1']]
    combinations = [(l, f, np) for l in luminosity for f in fractions for np in nParameters]
    #combinations = [(1, ['1', '1', '1', '1'], 1)]#TEST
    
    #loop over combinations
    for combination in combinations:
        
        print '\n******************************************'
        #print '  %s %s %s'%(combination[0], combination[1], combination[2])
        print 'luminosity:   %s'%combination[0]
        print 'fractions:    %s'%combination[1][0]
        print 'fractions:    %s'%combination[1][1]
        print 'fractions:    %s'%combination[1][2]
        print 'fractions:    %s'%combination[1][3]
        print 'n parameters: %s'%combination[2]
        print 'tag:          %s'%tag
        out = checkMix( inputFileName,
                        inputHistDir,
                        str(combination[0]),
                        str(combination[2]),
                        str(combination[1][0]),
                        str(combination[1][1]),
                        str(combination[1][2]),
                        str(combination[1][3]),
                        tag)
        print '\ndone %s'%out
        
    #------------------------------------------
    print '\n******************************************'
    print '\ndone looping over fractions and fit parameters'
