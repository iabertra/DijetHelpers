#!/usr/bin/env python

#******************************************
#import stuff
from findBarelyDetectableQStarWilks import findBarelyDetectableQStarWilks

#******************************************
if __name__ == '__main__':

    print '\n******************************************'
    print 'looping over q* mass, parameters and luminosity'

    tag = 'wilks.34'
    
    #combine input parameters
    luminosity = ['1']#/fb
    masses = ['3000', '4000', '5000', '5500']
    nParameters = [['3','4']]
    combinations = [(l, m, np) for l in luminosity for m in masses for np in nParameters ]
    #combinations = [(1, 4000, ['3', '4'])]#TEST
    
    #loop over combinations
    for combination in combinations:
        
        #print '\n******************************************'
        #print '  %s %s %s %s'%(combination[0], combination[1], combination[2][0], combination[2][1])
        print 'luminosity:     %s'%combination[0]
        print 'signal mass:    %s'%combination[1]
        print 'n parameters 1: %s'%combination[2][0]
        print 'n parameters 2: %s'%combination[2][1]
        print 'tag:            %s'%tag
        out = findBarelyDetectableQStarWilks( str(combination[1]),
                                                str(combination[0]),
                                                str(combination[2][0]),
                                                str(combination[2][1]),
                                                tag)
        print '\ndone %s'%(list(out))
        
    #------------------------------------------
    print '\n******************************************'
    print '\ndone looping over combinations'
