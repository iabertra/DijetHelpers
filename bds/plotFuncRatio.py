#!/bin/python

#******************************************
#plot the ratio of two functions
#EXAMPLE python -u plotFuncRatio.py ../results/searchphase/emptysearchphase.200000.ipb.4par.seed.0.root ../results/searchphase/searchphase.QStar.6000.GeV.200000.ipb.4par.seed.0.root QBH 10000 500000 False 0.

#******************************************
#import stuff
import ROOT
import re, sys, os, math, shutil, fileinput

#******************************************
#set ATLAS style #MOVED inside functions
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def plotFuncRatio(qcdSearchFileName, sigSearchFileName, model, mass, width, lumi, nPar):

    print '\n******************************************'
    print 'plotting fit function ratio'

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 8:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \nplot search phase results including the bump identified by the BumpHunter \
            \nHOW TO: python -u plotSearchResults.py qcdSearchFileName sigSearchFileName model mass luminosity[pb^-1] nPar'
            %(len(sys.argv), 8))
    
    #------------------------------------------
    #input parameters
    qcdFileName = sys.argv[1].strip()
    sigFileName = sys.argv[2].strip()
    model = sys.argv[3].strip()
    mass = sys.argv[4].strip()
    width = sys.argv[5].strip()
    lumi = sys.argv[6].strip()
    nPar = sys.argv[7].strip()
    print '\nparameters:'
    print '  QCD file name:    %s'%qcdFileName
    print '  signal file name: %s'%sigFileName
    print '  model:            %s'%model
    print '  mass:             %s'%mass
    print '  width:            %s'%width
    print '  lumi [ifb]:       %s'%lumi
    print '  n parameters:     %s'%nPar

    #------------------------------------------
    #open SearchPhase QCD results file
    if not os.path.isfile(qcdFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find QCD input file')
    qcdFile = ROOT.TFile(qcdFileName)
    
    #------------------------------------------
    #open SearchPhase signal results file
    if not os.path.isfile(sigFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find signal input file')
    sigFile = ROOT.TFile(sigFileName)

    #------------------------------------------
    #first things first
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #define necessary quantities
    luminosity = float(lumi) #fb^-1
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')
    if model == 'QStar':
        stag = 'figures/functionratio.'+model+'.'+mass+'.GeV.'+slumi+'.ifb.'+nPar+'.par'
    else:
        stag = 'figures/functionratio.'+model+'.'+mass+'.GeV.'+str(width).replace('.','p')+'.width.'+slumi+'.ifb.'+nPar+'.par'
        
    #------------------------------------------
    #get SearchPhase results
    #histograms
    qcdBasicBackground            = qcdFile.Get("basicBkgFrom4ParamFit")
    qcdResidualHist               = qcdFile.Get("residualHist")

    sigBasicBackground            = sigFile.Get("basicBkgFrom4ParamFit")
    sigResidualHist               = sigFile.Get("residualHist")

    #signal bump
    sigBumpHunterPLowHigh = sigFile.Get('bumpHunterPLowHigh')
    #sigBumpHunterPLowHigh = sigFile.Get('bumpHunterPLowHighInitial')#TEST #NO
    #sigBumpHunterStatValue = sigBumpHunterPLowHigh[0]
    sigBumpLowEdge         = sigBumpHunterPLowHigh[1]
    sigBumpHighEdge        = sigBumpHunterPLowHigh[2]

    qcdBumpHunterPLowHigh = qcdFile.Get('bumpHunterPLowHigh')
    #qcdBumpHunterPLowHigh = qcdFile.Get('bumpHunterPLowHighInitial')#TEST #NO
    #qcdBumpHunterStatValue = qcdBumpHunterPLowHigh[0]
    qcdBumpLowEdge         = qcdBumpHunterPLowHigh[1]
    qcdBumpHighEdge        = qcdBumpHunterPLowHigh[2]

    #p-value
    sigBumpHunterStatOfFitToData = sigFile.Get("bumpHunterStatOfFitToData")
    #sigBumpHunterStatOfFitToData = sigFile.Get("bumpHunterStatOfFitToDataInitial")#TEST #NO
    #sigBumpHunterStatValue = sigBumpHunterStatOfFitToData[0]
    sigBumpHunterPValue    = sigBumpHunterStatOfFitToData[1]
    #sigBumpHunterPValueErr = sigBumpHunterStatOfFitToData[2]

    qcdBumpHunterStatOfFitToData = qcdFile.Get("bumpHunterStatOfFitToData")
    #qcdBumpHunterStatOfFitToData = qcdFile.Get("bumpHunterStatOfFitToDataInitial")#TEST #NO
    #qcdBumpHunterStatValue = qcdBumpHunterStatOfFitToData[0]
    qcdBumpHunterPValue    = qcdBumpHunterStatOfFitToData[1]
    #qcdBumpHunterPValueErr = qcdBumpHunterStatOfFitToData[2]

        
    '''
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas
    '''

    #------------------------------------------
    #plot

    #------------------------------------------
    #canvas and pads
    c1 = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    yd = 0.5 #bottom pad (pad2) height
    pad1 = ROOT.TPad("pad1","pad1",0, 0.7, 1, 1.0)#top
    pad2 = ROOT.TPad("pad2","pad2",0, 0.4, 1, 0.7)#central
    pad3 = ROOT.TPad("pad3","pad3",0, 0.0, 1, 0.4)#bottom
    
    pad1.SetTopMargin(0.08)
    pad1.SetBottomMargin(0.0)
    pad1.Draw()

    pad2.SetTopMargin(0.0)
    pad2.SetBottomMargin(0.0)
    pad2.Draw()

    pad3.SetTopMargin(0.0)
    pad3.SetBottomMargin(0.25) #CHECK
    pad3.Draw()

    #------------------------------------------
    #common stuff
    intervalLine = ROOT.TLine()
    intervalLine.SetLineColor(ROOT.kBlue)

    m = ROOT.TLatex()
    m.SetNDC()
    m.SetTextFont(43)
    m.SetTextColor(1)
    m.SetTextSize(15)

    #------------------------------------------
    #pad 1: functions ratio
    pad1.cd()
    pad1.Clear()
    pad1.SetLogx(0)
    pad1.SetLogy(0)

    #model name
    if model == 'QStar':
        modelName = 'q*'
    else:
        modelName = model

    #get ratio
    funcRatio = sigBasicBackground.Clone()
    funcRatio.SetName('funcRatio')
    funcRatio.SetName('funcRatio')
    funcRatio.Divide(qcdBasicBackground)

    funcRatio.GetXaxis().SetRangeUser(1000., funcRatio.GetXaxis().GetXmax())

    funcRatio.GetYaxis().SetTitle("QCD+signal fit / QCD fit")
    funcRatio.GetYaxis().SetTitleFont(43)
    funcRatio.GetYaxis().SetTitleSize(15)
    funcRatio.GetYaxis().SetLabelFont(43)
    funcRatio.GetYaxis().SetLabelSize(15)
    funcRatio.Draw()

    pad1.Update()
    
    #------------------------------------------
    #pad 2: QCD+signal
    pad2.cd()
    pad2.Clear()
    pad2.SetLogx(0)
    pad2.SetLogy(0)

    sigResidualHist.GetXaxis().SetRangeUser(1000., funcRatio.GetXaxis().GetXmax())

    sigResidualHist.GetYaxis().SetTitle("QCD+signal fit residuals")
    sigResidualHist.GetYaxis().SetTitleFont(43)
    sigResidualHist.GetYaxis().SetTitleSize(15)
    sigResidualHist.GetYaxis().SetLabelFont(43)
    sigResidualHist.GetYaxis().SetLabelSize(15)
    sigResidualHist.SetFillColor(ROOT.kRed)
    sigResidualHist.Draw()

    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()
    intervalLine.DrawLine(sigBumpLowEdge,lowy,sigBumpLowEdge,highy)
    intervalLine.DrawLine(sigBumpHighEdge,lowy,sigBumpHighEdge,highy)
    m.DrawLatex(0.75,0.10,'BH p-value = %s'%sigBumpHunterPValue)
    
    pad2.Update()

    #------------------------------------------
    #pad 3: QCD
    pad3.cd()
    pad3.Clear()
    pad3.SetLogx(0)
    pad3.SetLogy(0)

    qcdResidualHist.GetXaxis().SetTitle("m_{jj} [GeV]")
    qcdResidualHist.GetXaxis().SetTitleOffset(4)
    qcdResidualHist.GetXaxis().SetTitleFont(43)
    qcdResidualHist.GetXaxis().SetTitleSize(15)
    qcdResidualHist.GetXaxis().SetLabelFont(43)
    qcdResidualHist.GetXaxis().SetLabelSize(15)
    qcdResidualHist.GetXaxis().SetRangeUser(1000., funcRatio.GetXaxis().GetXmax())
    #qcdResidualHist.GetXaxis().SetNdivisions(50510) #TEST

    qcdResidualHist.GetYaxis().SetTitle("QCD fit residuals")
    qcdResidualHist.GetYaxis().SetTitleFont(43)
    qcdResidualHist.GetYaxis().SetTitleSize(15)
    qcdResidualHist.GetYaxis().SetLabelFont(43)
    qcdResidualHist.GetYaxis().SetLabelSize(15)
    qcdResidualHist.SetFillColor(ROOT.kRed)
    qcdResidualHist.Draw()
    
    #NOTE this is a trick to have the x axis in TeV instead of GeV
    #ROOT.TGaxis.SetMaxDigits(3) #NOTE use scientific notation
    #ROOT.TGaxis.SetExponentOffset(10.,10.,"x") #NOTE move the exponent outside of the pad

    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()
    intervalLine.DrawLine(qcdBumpLowEdge,lowy,qcdBumpLowEdge,highy)
    intervalLine.DrawLine(qcdBumpHighEdge,lowy,qcdBumpHighEdge,highy)
    m.DrawLatex(0.75,0.30,'BH p-value = %s'%qcdBumpHunterPValue)
    
    pad3.Update()
    
    #------------------------------------------
    #legend
    pad1.cd()
    lXmin = 0.75#0.70
    lYmax = 0.80#0.40

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(20)
    a.DrawLatex(lXmin,lYmax,'ATLAS')#0.34
    
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(20)
    p.DrawLatex(lXmin+0.09,lYmax,'internal') #0.09, 0.34
    #p.DrawLatex(lXmin+0.09,lYmax+0.34,'simulation') #0.1
    #p.DrawLatex(lXmin,lYmax+0.22,'internal')

    #notes
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(43)
    n.SetTextColor(1)
    n.SetTextSize(15)
    
    n.DrawLatex(lXmin,lYmax-0.10,'#sqrt{s} = 13 TeV')
    n.DrawLatex(lXmin,lYmax-0.25,'#intLdt = %.1f fb^{-1}'%float(luminosity))
    if model == 'QStar':
        n.DrawLatex(lXmin,lYmax-0.40,'m_{q*} = %.0f'%(float(mass))+' GeV')
    else:
        n.DrawLatex(lXmin,lYmax-0.40,'m_{'+model+'} = %.0f'%(float(mass))+' GeV')
        n.DrawLatex(lXmin,lYmax-0.50,'sig. width = %.2f'%float(width))
    n.DrawLatex(lXmin,lYmax-0.60,'%s par. fit func.'%nPar)

    #------------------------------------------
    c1.cd()
    c1.Update()
    #c1.WaitPrimitive()
    c1.SaveAs(stag+'.pdf')


#******************************************
def plotDoubleFuncRatio(qcdSearchFileName, sigSearchFileName1, sigSearchFileName2, model, mass, width, sigNorm1, sigNorm2, lumi, nPar, tag):

    print '\n******************************************'
    print 'plotting fit function ratios from three searches'
    
    #------------------------------------------
    #input parameters
    print '\nparameters:'
    print '  QCD file name:            %s'%qcdSearchFileName
    print '  signal file name (incl.): %s'%sigSearchFileName1
    print '  signal file name (excl.): %s'%sigSearchFileName2
    print '  model:                    %s'%model
    print '  mass:                     %s'%mass
    print '  width:                    %s'%width
    print '  signal norm (incl.):      %s'%sigNorm1
    print '  signal norm (excl.):      %s'%sigNorm2
    print '  lumi [ifb]:               %s'%lumi
    print '  n parameters:             %s'%nPar
    print '  tag:                      %s'%tag

    #------------------------------------------
    #open SearchPhase QCD results file
    if not os.path.isfile(qcdSearchFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find QCD input file')
    qcdFile = ROOT.TFile(qcdSearchFileName)
    
    #------------------------------------------
    #open SearchPhase signal results file 1
    if not os.path.isfile(sigSearchFileName1):
        raise SystemExit('\n***ERROR*** couldn\'t find signal input file 1')
    sigFile1 = ROOT.TFile(sigSearchFileName1)

    #------------------------------------------
    #open SearchPhase signal results file 2
    if not os.path.isfile(sigSearchFileName2):
        raise SystemExit('\n***ERROR*** couldn\'t find signal input file 2')
    sigFile2 = ROOT.TFile(sigSearchFileName2)

    #------------------------------------------
    #first things first
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #define necessary quantities
    luminosity = float(lumi) #fb^-1
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')

    if model == 'QStar':
        stag = 'figures/functionratio.'+model+'.'+str(mass)+'.GeV.'+slumi+'.ifb.'+str(nPar)+'.par'
    else:
        stag = 'figures/functionratio.'+model+'.'+str(mass)+'.GeV.'+str(width).replace('.','p')+'.width.'+slumi+'.ifb.'+str(nPar)+'.par'

    #------------------------------------------
    #get SearchPhase results
    #histograms
    qcdBasicBackground            = qcdFile.Get("basicBkgFrom4ParamFit")
    qcdResidualHist               = qcdFile.Get("residualHist")

    sigBasicBackground1            = sigFile1.Get("basicBkgFrom4ParamFit")
    sigResidualHist1               = sigFile1.Get("residualHist")

    sigBasicBackground2            = sigFile2.Get("basicBkgFrom4ParamFit")
    sigResidualHist2               = sigFile2.Get("residualHist")

    #signal bump ***FINAL***
    qcdBumpHunterPLowHigh = qcdFile.Get('bumpHunterPLowHigh')#FINAL
    #qcdBumpHunterPLowHigh = qcdFile.Get('bumpHunterPLowHighInitial')#initial
    #qcdBumpHunterStatValue = qcdBumpHunterPLowHigh[0]
    qcdBumpLowEdge         = qcdBumpHunterPLowHigh[1]
    qcdBumpHighEdge        = qcdBumpHunterPLowHigh[2]

    sigBumpHunterPLowHigh1  = sigFile1.Get('bumpHunterPLowHigh')#FINAL
    #sigBumpHunterPLowHigh1  = sigFile1.Get('bumpHunterPLowHighInitial')#initial
    #sigBumpHunterStatValue1 = sigBumpHunterPLowHigh1[0]
    sigBumpLowEdge1         = sigBumpHunterPLowHigh1[1]
    sigBumpHighEdge1        = sigBumpHunterPLowHigh1[2]

    sigBumpHunterPLowHigh2  = sigFile2.Get('bumpHunterPLowHigh')#FINAL
    #sigBumpHunterPLowHigh2  = sigFile2.Get('bumpHunterPLowHighInitial')#initial
    #sigBumpHunterStatValue2 = sigBumpHunterPLowHigh[0]
    sigBumpLowEdge2         = sigBumpHunterPLowHigh2[1]
    sigBumpHighEdge2        = sigBumpHunterPLowHigh2[2]

    #initial p-value
    qcdBumpHunterStatOfFitToDataInitial = qcdFile.Get("bumpHunterStatOfFitToDataInitial")
    #qcdBumpHunterStatValueInitial = qcdBumpHunterStatOfFitToDataInitial[0]
    qcdBumpHunterPValueInitial    = qcdBumpHunterStatOfFitToDataInitial[1]
    #qcdBumpHunterPValueErrInitial = qcdBumpHunterStatOfFitToDataInitial[2]

    sigBumpHunterStatOfFitToDataInitial1 = sigFile1.Get("bumpHunterStatOfFitToDataInitial")
    #sigBumpHunterStatValueInitial1 = sigBumpHunterStatOfFitToDataInitial1[0]
    sigBumpHunterPValueInitial1    = sigBumpHunterStatOfFitToDataInitial1[1]
    #sigBumpHunterPValueErrInitial1 = sigBumpHunterStatOfFitToDataInitial1[2]

    sigBumpHunterStatOfFitToDataInitial2 = sigFile2.Get("bumpHunterStatOfFitToDataInitial")
    #sigBumpHunterStatValueInitial2 = sigBumpHunterStatOfFitToDataInitial2[0]
    sigBumpHunterPValueInitial2    = sigBumpHunterStatOfFitToDataInitial2[1]
    #sigBumpHunterPValueErrInitial2 = sigBumpHunterStatOfFitToDataInitial2[2]

    #final p-value
    qcdBumpHunterStatOfFitToData = qcdFile.Get("bumpHunterStatOfFitToData")
    qcdBumpHunterPValue    = qcdBumpHunterStatOfFitToData[1]

    sigBumpHunterStatOfFitToData1 = sigFile1.Get("bumpHunterStatOfFitToData")
    sigBumpHunterPValue1    = sigBumpHunterStatOfFitToData1[1]

    sigBumpHunterStatOfFitToData2 = sigFile2.Get("bumpHunterStatOfFitToData")
    sigBumpHunterPValue2    = sigBumpHunterStatOfFitToData2[1]

    #bump found?
    qcdBumpFoundVector = qcdFile.Get("bumpFound")
    qcdBumpFound = qcdBumpFoundVector[0]

    sigBumpFoundVector1 = sigFile1.Get("bumpFound")
    sigBumpFound1 = sigBumpFoundVector1[0]

    sigBumpFoundVector2 = sigFile2.Get("bumpFound")
    sigBumpFound2 = sigBumpFoundVector2[0]

    #chi2/ndf
    qcdChi2OfFitToData = qcdFile.Get('chi2OfFitToData')
    qcdChi2OfFitToDataValue = qcdChi2OfFitToData[0]
    qcdNdf = qcdFile.Get('NDF')
    qcdNdfValue = qcdNdf[0]
    qcdChi2ndf = float(qcdChi2OfFitToDataValue/qcdNdfValue)

    sigChi2OfFitToData1 = sigFile1.Get('chi2OfFitToData')
    sigChi2OfFitToDataValue1 = sigChi2OfFitToData1[0]
    sigNdf1 = sigFile1.Get('NDF')
    sigNdfValue1 = sigNdf1[0]
    sigChi2ndf1 = float(sigChi2OfFitToDataValue1/sigNdfValue1)

    sigChi2OfFitToData2 = sigFile2.Get('chi2OfFitToData')
    sigChi2OfFitToDataValue2 = sigChi2OfFitToData2[0]
    sigNdf2 = sigFile2.Get('NDF')
    sigNdfValue2 = sigNdf2[0]
    sigChi2ndf2 = float(sigChi2OfFitToDataValue2/sigNdfValue2)
    
    '''
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas
    '''

    #------------------------------------------
    #plot

    #------------------------------------------
    #canvas and pads
    c1 = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    yd = 0.5 #bottom pad (pad2) height
    pad1 = ROOT.TPad("pad1","pad1",0, 0.7, 1, 1.0)#top
    pad2 = ROOT.TPad("pad2","pad2",0, 0.5, 1, 0.7)#central top
    pad3 = ROOT.TPad("pad3","pad3",0, 0.3, 1, 0.5)#central bottom
    pad4 = ROOT.TPad("pad4","pad4",0, 0.0, 1, 0.3)#bottom
        
    pad1.SetTopMargin(0.08)
    pad1.SetBottomMargin(0.0)
    pad1.Draw()

    pad2.SetTopMargin(0.0)
    pad2.SetBottomMargin(0.0)
    pad2.Draw()

    pad3.SetTopMargin(0.0)
    pad3.SetBottomMargin(0.0)
    pad3.Draw()

    pad4.SetTopMargin(0.0)
    pad4.SetBottomMargin(0.333333)
    pad4.Draw()

    #------------------------------------------
    #common stuff
    intervalLine = ROOT.TLine()
    intervalLine.SetLineColor(ROOT.kBlue)

    m = ROOT.TLatex()
    m.SetNDC()
    m.SetTextFont(43)
    m.SetTextColor(1)
    m.SetTextSize(15)

    textx = 0.70

    #------------------------------------------
    #pad 1: functions ratio
    pad1.cd()
    pad1.Clear()
    pad1.SetLogx(0)
    pad1.SetLogy(0)

    #model name
    if model == 'QStar':
        modelName = 'q*'
    else:
        modelName = model

    #get ratios
    #sig1/qcd
    funcRatio1 = sigBasicBackground1.Clone()
    funcRatio1.SetName('funcRatio1')
    funcRatio1.SetName('funcRatio1')
    funcRatio1.Divide(qcdBasicBackground)
    funcRatio1.SetMarkerStyle(24)
    funcRatio1.SetMarkerColor(ROOT.kBlue)
    funcRatio1.SetLineColor(ROOT.kBlue)
        
    funcRatio1.GetXaxis().SetRangeUser(1000., funcRatio1.GetXaxis().GetXmax())

    funcRatio1.GetYaxis().SetTitle('fit function ratio')
    funcRatio1.GetYaxis().SetTitleFont(43)
    funcRatio1.GetYaxis().SetTitleSize(15)
    funcRatio1.GetYaxis().SetLabelFont(43)
    funcRatio1.GetYaxis().SetLabelSize(15)
    funcRatio1.Draw('HIST ][')#P
    ymin = funcRatio1.GetMinimum()
    ymax = funcRatio1.GetMaximum()
    #print '\n  min = %s'%funcRatio1.GetMaximum()
    #print '  max = %s'%funcRatio1.GetMinimum()

    #sig2/qcd
    funcRatio2 = sigBasicBackground2.Clone()
    funcRatio2.SetName('funcRatio2')
    funcRatio2.SetName('funcRatio2')
    funcRatio2.Divide(qcdBasicBackground)
    funcRatio2.SetMarkerStyle(25)
    funcRatio2.SetMarkerColor(ROOT.kGreen+1)
    funcRatio2.SetLineColor(ROOT.kGreen+1)
    funcRatio2.Draw('same HIST ][')#P
    if funcRatio2.GetMinimum() < ymin:
        ymin = funcRatio2.GetMinimum()
    if funcRatio2.GetMaximum() > ymax:
        ymax = funcRatio2.GetMaximum()
    #print '\n  min = %s'%funcRatio2.GetMinimum()
    #print '  max = %s'%funcRatio2.GetMaximum()

    #sig1/sig2
    funcRatio3 = sigBasicBackground1.Clone()
    funcRatio3.SetName('funcRatio3')
    funcRatio3.SetName('funcRatio3')
    funcRatio3.Divide(sigBasicBackground2)
    funcRatio3.SetMarkerStyle(26)
    funcRatio3.SetMarkerColor(ROOT.kRed+1)
    funcRatio3.SetLineColor(ROOT.kRed+1)
    funcRatio3.Draw('same HIST ][')#P
    if funcRatio3.GetMinimum() < ymin:
        ymin = funcRatio3.GetMinimum()
    if funcRatio3.GetMaximum() > ymax:
        ymax = funcRatio3.GetMaximum()
    #print '\n  min = %s'%funcRatio3.GetMinimum()
    #print '  max = %s'%funcRatio3.GetMaximum()

    #print '\n  min = %s'%ymin
    #print '  max = %s'%ymax
    funcRatio1.GetYaxis().SetRangeUser(ymin*0.9, ymax*1.1)#TEST
        
    pad1.Update()
    c1.Update()
    #c1.WaitPrimitive()
        
    #------------------------------------------
    #pad 2: QCD+signal 1
    pad2.cd()
    pad2.Clear()
    pad2.SetLogx(0)
    pad2.SetLogy(0)

    sigResidualHist1.GetXaxis().SetRangeUser(1000., funcRatio1.GetXaxis().GetXmax())

    sigResidualHist1.GetYaxis().SetTitle("QCD+sig. fit res.")
    sigResidualHist1.GetYaxis().SetTitleFont(43)
    sigResidualHist1.GetYaxis().SetTitleSize(15)
    sigResidualHist1.GetYaxis().SetLabelFont(43)
    sigResidualHist1.GetYaxis().SetLabelSize(15)
    sigResidualHist1.SetFillColor(ROOT.kRed)
    sigResidualHist1.Draw()

    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()
    if sigBumpFound1:
        intervalLine.SetLineColor(ROOT.kGreen+1)
    else:
        intervalLine.SetLineColor(ROOT.kBlue)
    intervalLine.DrawLine(sigBumpLowEdge1,lowy,sigBumpLowEdge1,highy)
    intervalLine.DrawLine(sigBumpHighEdge1,lowy,sigBumpHighEdge1,highy)
    m.DrawLatex(textx,0.55,'#chi^{2}/ndf = %.3f'%float(sigChi2ndf1))
    if float(sigNorm1) > 0.:
        if model == 'QStar':
            m.DrawLatex(textx,0.40,'sig. strength = %.3f'%float(sigNorm1))
        else:
            m.DrawLatex(textx,0.40,'sig. norm. = %s'%sigNorm1)        
    m.DrawLatex(textx,0.25,'initial BH p-value = %s'%sigBumpHunterPValueInitial1)
    m.DrawLatex(textx,0.10,'final BH p-value = %s'%sigBumpHunterPValue1)
    
    pad2.Update()
    c1.Update()
    #c1.WaitPrimitive()

    #------------------------------------------
    #pad 3: QCD+signal 2
    pad3.cd()
    pad3.Clear()
    pad3.SetLogx(0)
    pad3.SetLogy(0)

    sigResidualHist2.GetXaxis().SetRangeUser(1000., funcRatio1.GetXaxis().GetXmax())

    sigResidualHist2.GetYaxis().SetTitle("QCD+sig. fit res.")
    sigResidualHist2.GetYaxis().SetTitleFont(43)
    sigResidualHist2.GetYaxis().SetTitleSize(15)
    sigResidualHist2.GetYaxis().SetLabelFont(43)
    sigResidualHist2.GetYaxis().SetLabelSize(15)
    sigResidualHist2.SetFillColor(ROOT.kRed)
    sigResidualHist2.Draw()

    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()
    if sigBumpFound2:
        intervalLine.SetLineColor(ROOT.kGreen+1)
    else:
        intervalLine.SetLineColor(ROOT.kBlue)
    intervalLine.DrawLine(sigBumpLowEdge2,lowy,sigBumpLowEdge2,highy)
    intervalLine.DrawLine(sigBumpHighEdge2,lowy,sigBumpHighEdge2,highy)
    m.DrawLatex(textx,0.55,'#chi^{2}/ndf = %.3f'%float(sigChi2ndf2))
    if float(sigNorm2) > 0.:
        if model == 'QStar':
            m.DrawLatex(textx,0.40,'sig. strength = %.3f'%float(sigNorm2))
        else:
            m.DrawLatex(textx,0.40,'sig. norm. = %s'%sigNorm2)

    m.DrawLatex(textx,0.25,'initial BH p-value = %s'%sigBumpHunterPValueInitial2)
    m.DrawLatex(textx,0.10,'final BH p-value = %s'%sigBumpHunterPValue2)
    
    pad3.Update()
    c1.Update()
    #c1.WaitPrimitive()

    #------------------------------------------
    #pad 4: QCD
    pad4.cd()
    pad4.Clear()
    pad4.SetLogx(0)
    pad4.SetLogy(0)

    qcdResidualHist.GetXaxis().SetTitle("m_{jj} [GeV]")
    qcdResidualHist.GetXaxis().SetTitleOffset(4)
    qcdResidualHist.GetXaxis().SetTitleFont(43)
    qcdResidualHist.GetXaxis().SetTitleSize(15)
    qcdResidualHist.GetXaxis().SetLabelFont(43)
    qcdResidualHist.GetXaxis().SetLabelSize(15)
    qcdResidualHist.GetXaxis().SetRangeUser(1000., funcRatio1.GetXaxis().GetXmax())
    #qcdResidualHist.GetXaxis().SetNdivisions(50510) #TEST

    qcdResidualHist.GetYaxis().SetTitle("QCD fit res.")
    qcdResidualHist.GetYaxis().SetTitleFont(43)
    qcdResidualHist.GetYaxis().SetTitleSize(15)
    qcdResidualHist.GetYaxis().SetLabelFont(43)
    qcdResidualHist.GetYaxis().SetLabelSize(15)
    qcdResidualHist.SetFillColor(ROOT.kRed)
    qcdResidualHist.Draw()
    
    #NOTE this is a trick to have the x axis in TeV instead of GeV
    #ROOT.TGaxis.SetMaxDigits(3) #NOTE use scientific notation
    #ROOT.TGaxis.SetExponentOffset(10.,10.,"x") #NOTE move the exponent outside of the pad

    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()
    if qcdBumpFound:
        intervalLine.SetLineColor(ROOT.kGreen+1)
    else:
        intervalLine.SetLineColor(ROOT.kBlue)
    intervalLine.DrawLine(qcdBumpLowEdge,lowy,qcdBumpLowEdge,highy)
    intervalLine.DrawLine(qcdBumpHighEdge,lowy,qcdBumpHighEdge,highy)
    m.DrawLatex(textx,0.60,'#chi^{2}/ndf = %.3f'%float(qcdChi2ndf))
    m.DrawLatex(textx,0.50,'initial BH p-value = %s'%qcdBumpHunterPValueInitial)
    m.DrawLatex(textx,0.40,'final BH p-value = %s'%qcdBumpHunterPValue)

    pad4.Update()
    c1.Update()
    #c1.WaitPrimitive()
    
    #------------------------------------------
    #legend
    pad1.cd()
    lXmin = textx#0.70
    lYmax = 0.80#0.40

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(20)
    a.DrawLatex(lXmin,lYmax,'ATLAS')#0.34
    
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(20)
    p.DrawLatex(lXmin+0.09,lYmax,'internal') #0.09, 0.34
    #p.DrawLatex(lXmin+0.09,lYmax+0.34,'simulation') #0.1
    #p.DrawLatex(lXmin,lYmax+0.22,'internal')

    #notes
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(43)
    n.SetTextColor(1)
    n.SetTextSize(15)
    
    n.DrawLatex(lXmin,lYmax-0.10,'#sqrt{s} = 13 TeV')
    n.DrawLatex(lXmin,lYmax-0.20,'L_{int} = %.1f fb^{-1}'%float(luminosity))
    if model == 'QStar':
        n.DrawLatex(lXmin,lYmax-0.30,'m_{q*} = %.0f'%(float(mass))+' GeV')
    else:
        n.DrawLatex(lXmin,lYmax-0.30,'sig. mass = %.0f'%(float(mass))+' GeV')
        n.DrawLatex(lXmin,lYmax-0.40,'sig. width = %.0f%%'%(float(width)*100.))
    n.DrawLatex(lXmin,lYmax-0.50,'%s par. fit func.'%nPar)

    #legend
    l = ROOT.TLegend(lXmin, lYmax-0.52, lXmin+0.20, lYmax-0.78)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(12)
    l.AddEntry(funcRatio1,'incl. / QCD','l')#pl
    l.AddEntry(funcRatio2,'excl. / QCD','l')#pl
    l.AddEntry(funcRatio3,'incl. / excl.','l')#pl
    l.Draw()
    
    #------------------------------------------
    c1.cd()
    c1.Update()
    #c1.WaitPrimitive()
    c1.SaveAs(stag+'.'+tag+'.pdf')


#******************************************
def plotDoubleFuncRatioWilks(qcdSearchFileName1,
                                qcdSearchFileName2,
                                sigInclSearchFileName1,
                                sigInclSearchFileName2,
                                sigExclSearchFileName1,
                                sigExclSearchFileName2,
                                model,
                                mass,
                                width,
                                normHigh,
                                normLow,
                                lumi,
                                nPar1,
                                nPar2,
                                tag):

    print '\n******************************************'
    print 'plotting fit function ratios comparing two fit functions'
    
    #------------------------------------------
    #input parameters
    print '\nparameters:'
    print '  QCD file name 1:            %s'%qcdSearchFileName1
    print '  QCD file name 2:            %s'%qcdSearchFileName2
    print '  signal file name (incl.) 1: %s'%sigInclSearchFileName1
    print '  signal file name (incl.) 2: %s'%sigInclSearchFileName2
    print '  signal file name (excl.) 1: %s'%sigExclSearchFileName1
    print '  signal file name (excl.) 2: %s'%sigExclSearchFileName2
    print '  model:                      %s'%model
    print '  mass:                       %s'%mass
    print '  width:                      %s'%width
    print '  signal norm (incl.):        %s'%normHigh
    print '  signal norm (excl.):        %s'%normLow
    print '  lumi [ifb]:                 %s'%lumi
    print '  n parameters 1:             %s'%nPar1
    print '  n parameters 2:             %s'%nPar2
    print '  tag:                        %s'%tag

    #------------------------------------------
    #open SearchPhase QCD results file
    if not os.path.isfile(qcdSearchFileName1):
        raise SystemExit('\n***ERROR*** couldn\'t find QCD input file 1')
    qcdFile1 = ROOT.TFile(qcdSearchFileName1)

    if not os.path.isfile(qcdSearchFileName2):
        raise SystemExit('\n***ERROR*** couldn\'t find QCD input file 2')
    qcdFile2 = ROOT.TFile(qcdSearchFileName2)
    
    #------------------------------------------
    #open SearchPhase signal included results file
    if not os.path.isfile(sigInclSearchFileName1):
        raise SystemExit('\n***ERROR*** couldn\'t find signal input file 1 (signal included)')
    sigInclFile1 = ROOT.TFile(sigInclSearchFileName1)

    if not os.path.isfile(sigInclSearchFileName2):
        raise SystemExit('\n***ERROR*** couldn\'t find signal input file 2 (signal included)')
    sigInclFile2 = ROOT.TFile(sigInclSearchFileName2)

    #------------------------------------------
    #open SearchPhase signal excluded results file
    if not os.path.isfile(sigExclSearchFileName1):
        raise SystemExit('\n***ERROR*** couldn\'t find signal input file 1 (signal excluded)')
    sigExclFile1 = ROOT.TFile(sigExclSearchFileName1)

    if not os.path.isfile(sigExclSearchFileName2):
        raise SystemExit('\n***ERROR*** couldn\'t find signal input file 2 (signal excluded)')
    sigExclFile2 = ROOT.TFile(sigExclSearchFileName2)

    #------------------------------------------
    #first things first
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #define necessary quantities
    luminosity = float(lumi) #fb^-1
    slumi = ('%.1f'%float(lumi.replace('p','.'))).replace('.','p')

    if model == 'QStar':
        stag = 'figures/functionRatioWilks.'+model+'.'+str(mass)+'.GeV.'+slumi+'.ifb.'+str(nPar1)+'.par1.'+str(nPar2)+'.par2'
    else:
        stag = 'figures/functionRatioWilks.'+model+'.'+str(mass)+'.GeV.'+str(width).replace('.','p')+'.width.'+slumi+'.ifb.'+str(nPar1)+'.par1.'+str(nPar2)+'.par2'

    #------------------------------------------
    #get SearchPhase results
    #histograms
    qcdBasicBackground1            = qcdFile1.Get("basicBkgFrom4ParamFit")
    qcdResidualHist1               = qcdFile1.Get("residualHist")

    qcdBasicBackground2            = qcdFile2.Get("basicBkgFrom4ParamFit")
    qcdResidualHist2               = qcdFile2.Get("residualHist")
    
    sigInclBasicBackground1            = sigInclFile1.Get("basicBkgFrom4ParamFit")
    sigInclResidualHist1               = sigInclFile1.Get("residualHist")

    sigInclBasicBackground2            = sigInclFile2.Get("basicBkgFrom4ParamFit")
    sigInclResidualHist2               = sigInclFile2.Get("residualHist")

    sigExclBasicBackground1            = sigExclFile1.Get("basicBkgFrom4ParamFit")
    sigExclResidualHist1               = sigExclFile1.Get("residualHist")

    sigExclBasicBackground2            = sigExclFile2.Get("basicBkgFrom4ParamFit")
    sigExclResidualHist2               = sigExclFile2.Get("residualHist")

    #signal bump ***FINAL***
    qcdBumpHunterPLowHigh1 = qcdFile1.Get('bumpHunterPLowHigh')
    qcdBumpLowEdge1        = qcdBumpHunterPLowHigh1[1]
    qcdBumpHighEdge1       = qcdBumpHunterPLowHigh1[2]

    qcdBumpHunterPLowHigh2 = qcdFile2.Get('bumpHunterPLowHigh')
    qcdBumpLowEdge2        = qcdBumpHunterPLowHigh2[1]
    qcdBumpHighEdge2       = qcdBumpHunterPLowHigh2[2]

    sigInclBumpHunterPLowHigh1  = sigInclFile1.Get('bumpHunterPLowHigh')
    sigInclBumpLowEdge1         = sigInclBumpHunterPLowHigh1[1]
    sigInclBumpHighEdge1        = sigInclBumpHunterPLowHigh1[2]

    sigInclBumpHunterPLowHigh2  = sigInclFile2.Get('bumpHunterPLowHigh')
    sigInclBumpLowEdge2         = sigInclBumpHunterPLowHigh2[1]
    sigInclBumpHighEdge2        = sigInclBumpHunterPLowHigh2[2]

    sigExclBumpHunterPLowHigh1  = sigExclFile1.Get('bumpHunterPLowHigh')
    sigExclBumpLowEdge1         = sigExclBumpHunterPLowHigh1[1]
    sigExclBumpHighEdge1        = sigExclBumpHunterPLowHigh1[2]

    sigExclBumpHunterPLowHigh2  = sigExclFile2.Get('bumpHunterPLowHigh')
    sigExclBumpLowEdge2         = sigExclBumpHunterPLowHigh2[1]
    sigExclBumpHighEdge2        = sigExclBumpHunterPLowHigh2[2]

    #initial p-value
    qcdBumpHunterStatOfFitToDataInitial1 = qcdFile1.Get("bumpHunterStatOfFitToDataInitial")
    qcdBumpHunterPValueInitial1    = qcdBumpHunterStatOfFitToDataInitial1[1]

    qcdBumpHunterStatOfFitToDataInitial2 = qcdFile2.Get("bumpHunterStatOfFitToDataInitial")
    qcdBumpHunterPValueInitial2    = qcdBumpHunterStatOfFitToDataInitial2[1]

    sigInclBumpHunterStatOfFitToDataInitial1 = sigInclFile1.Get("bumpHunterStatOfFitToDataInitial")
    sigInclBumpHunterPValueInitial1    = sigInclBumpHunterStatOfFitToDataInitial1[1]

    sigInclBumpHunterStatOfFitToDataInitial2 = sigInclFile2.Get("bumpHunterStatOfFitToDataInitial")
    sigInclBumpHunterPValueInitial2    = sigInclBumpHunterStatOfFitToDataInitial2[1]

    sigExclBumpHunterStatOfFitToDataInitial1 = sigExclFile1.Get("bumpHunterStatOfFitToDataInitial")
    sigExclBumpHunterPValueInitial1    = sigExclBumpHunterStatOfFitToDataInitial1[1]

    sigExclBumpHunterStatOfFitToDataInitial2 = sigExclFile2.Get("bumpHunterStatOfFitToDataInitial")
    sigExclBumpHunterPValueInitial2    = sigExclBumpHunterStatOfFitToDataInitial2[1]

    #final p-value
    qcdBumpHunterStatOfFitToData1 = qcdFile1.Get("bumpHunterStatOfFitToData")
    qcdBumpHunterPValue1    = qcdBumpHunterStatOfFitToData1[1]

    qcdBumpHunterStatOfFitToData2 = qcdFile2.Get("bumpHunterStatOfFitToData")
    qcdBumpHunterPValue2    = qcdBumpHunterStatOfFitToData2[1]

    sigInclBumpHunterStatOfFitToData1 = sigInclFile1.Get("bumpHunterStatOfFitToData")
    sigInclBumpHunterPValue1    = sigInclBumpHunterStatOfFitToData1[1]

    sigInclBumpHunterStatOfFitToData2 = sigInclFile2.Get("bumpHunterStatOfFitToData")
    sigInclBumpHunterPValue2    = sigInclBumpHunterStatOfFitToData2[1]

    sigExclBumpHunterStatOfFitToData1 = sigExclFile1.Get("bumpHunterStatOfFitToData")
    sigExclBumpHunterPValue1    = sigExclBumpHunterStatOfFitToData1[1]

    sigExclBumpHunterStatOfFitToData2 = sigExclFile2.Get("bumpHunterStatOfFitToData")
    sigExclBumpHunterPValue2    = sigExclBumpHunterStatOfFitToData2[1]

    #bump found?
    qcdBumpFoundVector1 = qcdFile1.Get("bumpFound")
    qcdBumpFound1 = qcdBumpFoundVector1[0]

    qcdBumpFoundVector2 = qcdFile2.Get("bumpFound")
    qcdBumpFound2 = qcdBumpFoundVector2[0]

    sigInclBumpFoundVector1 = sigInclFile1.Get("bumpFound")
    sigInclBumpFound1 = sigInclBumpFoundVector1[0]

    sigInclBumpFoundVector2 = sigInclFile2.Get("bumpFound")
    sigInclBumpFound2 = sigInclBumpFoundVector2[0]

    sigExclBumpFoundVector1 = sigExclFile1.Get("bumpFound")
    sigExclBumpFound1 = sigExclBumpFoundVector1[0]

    sigExclBumpFoundVector2 = sigExclFile2.Get("bumpFound")
    sigExclBumpFound2 = sigExclBumpFoundVector2[0]

    #chi2/ndf
    qcdChi2OfFitToData1 = qcdFile1.Get('chi2OfFitToData')
    qcdChi2OfFitToDataValue1 = qcdChi2OfFitToData1[0]
    qcdNdf1 = qcdFile1.Get('NDF')
    qcdNdfValue1 = qcdNdf1[0]
    qcdChi2ndf1 = float(qcdChi2OfFitToDataValue1/qcdNdfValue1)

    qcdChi2OfFitToData2 = qcdFile2.Get('chi2OfFitToData')
    qcdChi2OfFitToDataValue2 = qcdChi2OfFitToData2[0]
    qcdNdf2 = qcdFile2.Get('NDF')
    qcdNdfValue2 = qcdNdf2[0]
    qcdChi2ndf2 = float(qcdChi2OfFitToDataValue2/qcdNdfValue2)
    
    sigInclChi2OfFitToData1 = sigInclFile1.Get('chi2OfFitToData')
    sigInclChi2OfFitToDataValue1 = sigInclChi2OfFitToData1[0]
    sigInclNdf1 = sigInclFile1.Get('NDF')
    sigInclNdfValue1 = sigInclNdf1[0]
    sigInclChi2ndf1 = float(sigInclChi2OfFitToDataValue1/sigInclNdfValue1)

    sigInclChi2OfFitToData2 = sigInclFile2.Get('chi2OfFitToData')
    sigInclChi2OfFitToDataValue2 = sigInclChi2OfFitToData2[0]
    sigInclNdf2 = sigInclFile2.Get('NDF')
    sigInclNdfValue2 = sigInclNdf2[0]
    sigInclChi2ndf2 = float(sigInclChi2OfFitToDataValue2/sigInclNdfValue2)

    sigExclChi2OfFitToData1 = sigExclFile1.Get('chi2OfFitToData')
    sigExclChi2OfFitToDataValue1 = sigExclChi2OfFitToData1[0]
    sigExclNdf1 = sigExclFile1.Get('NDF')
    sigExclNdfValue1 = sigExclNdf1[0]
    sigExclChi2ndf1 = float(sigExclChi2OfFitToDataValue1/sigExclNdfValue1)

    sigExclChi2OfFitToData2 = sigExclFile2.Get('chi2OfFitToData')
    sigExclChi2OfFitToDataValue2 = sigExclChi2OfFitToData2[0]
    sigExclNdf2 = sigExclFile2.Get('NDF')
    sigExclNdfValue2 = sigExclNdf2[0]
    sigExclChi2ndf2 = float(sigExclChi2OfFitToDataValue2/sigExclNdfValue2)

    #log likelihood
    qcdLogL1 = qcdFile1.Get('logLOfFitToData')
    qcdLogLValue1 = qcdLogL1[0]

    qcdLogL2 = qcdFile2.Get('logLOfFitToData')
    qcdLogLValue2 = qcdLogL2[0]

    sigInclLogL1 = sigInclFile1.Get('logLOfFitToData')
    sigInclLogLValue1 = sigInclLogL1[0]

    sigInclLogL2 = sigInclFile2.Get('logLOfFitToData')
    sigInclLogLValue2 = sigInclLogL2[0]

    sigExclLogL1 = sigExclFile1.Get('logLOfFitToData')
    sigExclLogLValue1 = sigExclLogL1[0]

    sigExclLogL2 = sigExclFile2.Get('logLOfFitToData')
    sigExclLogLValue2 = sigExclLogL2[0]

    #------------------------------------------
    #check Wilks' p-values

    #QCD
    print '\nQCD'
    qcdDeltaLogL = float(qcdLogLValue1 - qcdLogLValue2)
    print '  delta log likelihood = %f'%qcdDeltaLogL
    qcdDeltaNdf = int(qcdNdfValue1 - qcdNdfValue2)
    print '  delta ndf = %s'%qcdDeltaNdf
    qcdWilksPVal = ROOT.TMath.Prob( abs(qcdDeltaLogL), abs(qcdDeltaNdf) )
    print '  Wilks\' p-value = %f'%qcdWilksPVal

    #sig included
    print '\nsignal included'
    sigInclDeltaLogL = float(sigInclLogLValue1 - sigInclLogLValue2)
    print '  delta log likelihood = %f'%sigInclDeltaLogL
    sigInclDeltaNdf = int(sigInclNdfValue1 - sigInclNdfValue2)
    print '  delta ndf = %s'%sigInclDeltaNdf
    sigInclWilksPVal = ROOT.TMath.Prob( abs(sigInclDeltaLogL), abs(sigInclDeltaNdf) )
    print '  Wilks\' p-value = %f'%sigInclWilksPVal

    #sig excluded
    print '\nsignal excluded'
    sigExclDeltaLogL = float(sigExclLogLValue1 - sigExclLogLValue2)
    print '  delta log likelihood = %f'%sigExclDeltaLogL
    sigExclDeltaNdf = int(sigExclNdfValue1 - sigExclNdfValue2)
    print '  delta ndf = %s'%sigExclDeltaNdf
    sigExclWilksPVal = ROOT.TMath.Prob( abs(sigExclDeltaLogL), abs(sigExclDeltaNdf) )
    print '  Wilks\' p-value = %f'%sigExclWilksPVal
    
    #------------------------------------------
    #plot

    #------------------------------------------
    #canvas and pads
    c1 = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    yd = 0.5 #bottom pad (pad2) height
    pad1 = ROOT.TPad("pad1","pad1",0, 0.7, 1, 1.0)#top
    pad2 = ROOT.TPad("pad2","pad2",0, 0.5, 1, 0.7)#central top
    pad3 = ROOT.TPad("pad3","pad3",0, 0.3, 1, 0.5)#central bottom
    pad4 = ROOT.TPad("pad4","pad4",0, 0.0, 1, 0.3)#bottom
        
    pad1.SetTopMargin(0.08)
    pad1.SetBottomMargin(0.0)
    pad1.Draw()

    pad2.SetTopMargin(0.0)
    pad2.SetBottomMargin(0.0)
    pad2.Draw()

    pad3.SetTopMargin(0.0)
    pad3.SetBottomMargin(0.0)
    pad3.Draw()

    pad4.SetTopMargin(0.0)
    pad4.SetBottomMargin(0.333333)
    pad4.Draw()

    #------------------------------------------
    #common stuff
    intervalLine = ROOT.TLine()
    intervalLine.SetLineColor(ROOT.kBlue)

    m = ROOT.TLatex()
    m.SetNDC()
    m.SetTextFont(43)
    m.SetTextColor(1)
    m.SetTextSize(10)

    textx = 0.70
    deltax = 0.05
    
    #------------------------------------------
    #pad 1: functions ratio
    pad1.cd()
    pad1.Clear()
    pad1.SetLogx(0)
    pad1.SetLogy(0)

    #model name
    if model == 'QStar':
        modelName = 'q*'
    else:
        modelName = model

    #sig. excl. 1 / qcd 1
    funcRatio1 = sigExclBasicBackground1.Clone()
    funcRatio1.SetName('funcRatio1')
    funcRatio1.SetName('funcRatio1')
    funcRatio1.Divide(qcdBasicBackground1)
    funcRatio1.SetMarkerStyle(25)
    funcRatio1.SetMarkerColor(ROOT.kRed)
    funcRatio1.SetLineColor(ROOT.kRed)

    funcRatio1.GetXaxis().SetRangeUser(1000., funcRatio1.GetXaxis().GetXmax())

    funcRatio1.GetYaxis().SetTitle('fit function ratio')
    funcRatio1.GetYaxis().SetTitleFont(43)
    funcRatio1.GetYaxis().SetTitleSize(15)
    funcRatio1.GetYaxis().SetLabelFont(43)
    funcRatio1.GetYaxis().SetLabelSize(15)

    funcRatio1.Draw('HIST ][')
    ymin = funcRatio1.GetMinimum()
    ymax = funcRatio1.GetMaximum()
    #print '\n  min = %s'%funcRatio1.GetMinimum()
    #print '  max = %s'%funcRatio1.GetMaximum()

    #sig. excl. 2 / qcd 2
    funcRatio2 = sigExclBasicBackground2.Clone()
    funcRatio2.SetName('funcRatio2')
    funcRatio2.SetName('funcRatio2')
    funcRatio2.Divide(qcdBasicBackground2)
    funcRatio2.SetMarkerStyle(25)
    funcRatio2.SetMarkerColor(ROOT.kOrange)
    funcRatio2.SetLineColor(ROOT.kOrange)

    funcRatio2.Draw('same HIST ][')
    if funcRatio2.GetMinimum() < ymin:
        ymin = funcRatio2.GetMinimum()
    if funcRatio2.GetMaximum() > ymax:
        ymax = funcRatio2.GetMaximum()
    #print '\n  min = %s'%funcRatio2.GetMinimum()
    #print '  max = %s'%funcRatio2.GetMaximum()

    #sig. excl. 1 / sig. excl. 2
    funcRatio = sigExclBasicBackground1.Clone()
    funcRatio.SetName('funcRatio')
    funcRatio.SetName('funcRatio')
    funcRatio.Divide(sigExclBasicBackground2)
    funcRatio.SetMarkerStyle(25)
    funcRatio.SetMarkerColor(ROOT.kCyan+1)
    funcRatio.SetLineColor(ROOT.kCyan+1)

    funcRatio.Draw('same HIST ][')
    if funcRatio.GetMinimum() < ymin:
        ymin = funcRatio.GetMinimum()
    if funcRatio.GetMaximum() > ymax:
        ymax = funcRatio.GetMaximum()
    #print '\n  min = %s'%funcRatio.GetMinimum()
    #print '  max = %s'%funcRatio.GetMaximum()
    
    #print '\n  min = %s'%ymin
    #print '  max = %s'%ymax
    funcRatio1.GetYaxis().SetRangeUser(ymin*0.9, ymax*1.1)
    
    pad1.Update()
    c1.Update()
    #c1.WaitPrimitive()
        
    #------------------------------------------
    #pad 2: QCD+signal included
    pad2.cd()
    pad2.Clear()
    pad2.SetLogx(0)
    pad2.SetLogy(0)

    sigInclResidualHist1.GetXaxis().SetRangeUser(1000., funcRatio1.GetXaxis().GetXmax())
    sigInclResidualHist1.GetYaxis().SetTitle("QCD+sig. fit res.")
    sigInclResidualHist1.GetYaxis().SetTitleFont(43)
    sigInclResidualHist1.GetYaxis().SetTitleSize(15)
    sigInclResidualHist1.GetYaxis().SetLabelFont(43)
    sigInclResidualHist1.GetYaxis().SetLabelSize(15)
    sigInclResidualHist1.SetLineColor(ROOT.kRed)
    sigInclResidualHist1.SetFillColor(ROOT.kRed)
    sigInclResidualHist1.SetFillStyle(3004)    
    sigInclResidualHist1.Draw()

    sigInclResidualHist2.SetLineColor(ROOT.kOrange)
    sigInclResidualHist2.SetFillColor(ROOT.kOrange)
    sigInclResidualHist2.SetFillStyle(3005)    
    sigInclResidualHist2.Draw('same')
    
    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()

    #default
    if sigInclBumpFound1:
        intervalLine.SetLineColor(ROOT.kGreen+3)
    else:
        intervalLine.SetLineColor(ROOT.kBlue+2)
    intervalLine.SetLineWidth(2)
    intervalLine.DrawLine(sigInclBumpLowEdge1,lowy,sigInclBumpLowEdge1,highy)
    intervalLine.DrawLine(sigInclBumpHighEdge1,lowy,sigInclBumpHighEdge1,highy)

    #alternative
    if sigInclBumpFound2:
        intervalLine.SetLineColor(ROOT.kGreen+1)
    else:
        intervalLine.SetLineColor(ROOT.kBlue)
    intervalLine.SetLineWidth(2)
    intervalLine.DrawLine(sigInclBumpLowEdge2,lowy,sigInclBumpLowEdge2,highy)
    intervalLine.DrawLine(sigInclBumpHighEdge2,lowy,sigInclBumpHighEdge2,highy)

    #signal normalization
    if float(normHigh) > 0.:
        if model == 'QStar':
            m.DrawLatex(textx,0.90,'sig. strength = %.3f'%float(normHigh))
        else:
            m.DrawLatex(textx,0.90,'sig. norm. = %.3f'%float(normHigh))

    #Wilks' p-value
    sigInclWilksPVal = ROOT.TMath.Prob( abs(float(sigInclLogLValue1) - float(sigInclLogLValue2)), abs(int(sigInclNdfValue1) - int(sigInclNdfValue2)) )
    m.DrawLatex(textx,0.80,'Wilks\' p-value = %.5f'%sigInclWilksPVal)
    
    #default
    m.DrawLatex(textx,0.64,'%s par. fit func.:'%nPar1)
    m.DrawLatex(textx+deltax,0.56,'#chi^{2}/ndf = %.3f      logL = %.1f'%(float(sigInclChi2ndf1), sigInclLogLValue1))
    m.DrawLatex(textx+deltax,0.48,'initial BH p-value = %s'%sigInclBumpHunterPValueInitial1)
    m.DrawLatex(textx+deltax,0.40,'final BH p-value = %s'%sigInclBumpHunterPValue1)

    #alternative
    m.DrawLatex(textx,0.32,'%s par. fit func.:'%nPar2)
    m.DrawLatex(textx+deltax,0.24,'#chi^{2}/ndf = %.3f      logL = %.1f'%(float(sigInclChi2ndf2), sigInclLogLValue2))
    m.DrawLatex(textx+deltax,0.16,'initial BH p-value = %s'%sigInclBumpHunterPValueInitial2)
    m.DrawLatex(textx+deltax,0.08,'final BH p-value = %s'%sigInclBumpHunterPValue2)
    
    pad2.Update()
    c1.Update()
    #c1.WaitPrimitive()

    #------------------------------------------
    #pad 3: QCD+signal excluded
    pad3.cd()
    pad3.Clear()
    pad3.SetLogx(0)
    pad3.SetLogy(0)

    sigExclResidualHist1.GetXaxis().SetRangeUser(1000., funcRatio1.GetXaxis().GetXmax())
    sigExclResidualHist1.GetYaxis().SetTitle("QCD+sig. fit res.")
    sigExclResidualHist1.GetYaxis().SetTitleFont(43)
    sigExclResidualHist1.GetYaxis().SetTitleSize(15)
    sigExclResidualHist1.GetYaxis().SetLabelFont(43)
    sigExclResidualHist1.GetYaxis().SetLabelSize(15)
    sigExclResidualHist1.SetLineColor(ROOT.kRed)
    sigExclResidualHist1.SetFillColor(ROOT.kRed)
    sigExclResidualHist1.SetFillStyle(3004)    
    sigExclResidualHist1.Draw()

    sigExclResidualHist2.SetLineColor(ROOT.kOrange)
    sigExclResidualHist2.SetFillColor(ROOT.kOrange)
    sigExclResidualHist2.SetFillStyle(3005)    
    sigExclResidualHist2.Draw('same')
    
    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()

    #default
    if sigExclBumpFound1:
        intervalLine.SetLineColor(ROOT.kGreen+3)
    else:
        intervalLine.SetLineColor(ROOT.kBlue+2)
    intervalLine.SetLineWidth(2)
    intervalLine.DrawLine(sigExclBumpLowEdge1,lowy,sigExclBumpLowEdge1,highy)
    intervalLine.DrawLine(sigExclBumpHighEdge1,lowy,sigExclBumpHighEdge1,highy)

    #alternative
    if sigExclBumpFound2:
        intervalLine.SetLineColor(ROOT.kGreen+1)
    else:
        intervalLine.SetLineColor(ROOT.kBlue)
    intervalLine.SetLineWidth(2)
    intervalLine.DrawLine(sigExclBumpLowEdge2,lowy,sigExclBumpLowEdge2,highy)
    intervalLine.DrawLine(sigExclBumpHighEdge2,lowy,sigExclBumpHighEdge2,highy)

    #signal normalization
    if float(normLow) > 0.:
        if model == 'QStar':
            m.DrawLatex(textx,0.90,'sig. strength = %.3f'%float(normLow))
        else:
            m.DrawLatex(textx,0.90,'sig. norm. = %.3f'%float(normLow))

    #Wilks' p-value
    sigExclWilksPVal = ROOT.TMath.Prob( abs(float(sigExclLogLValue1) - float(sigExclLogLValue2)), abs(int(sigExclNdfValue1) - int(sigExclNdfValue2)) )
    m.DrawLatex(textx,0.80,'Wilks\' p-value = %.5f'%sigExclWilksPVal)
    
    #default
    m.DrawLatex(textx,0.64,'%s par. fit func.:'%nPar1)
    m.DrawLatex(textx+deltax,0.56,'#chi^{2}/ndf = %.3f      logL = %.1f'%(float(sigExclChi2ndf1), sigExclLogLValue1))
    m.DrawLatex(textx+deltax,0.48,'initial BH p-value = %s'%sigExclBumpHunterPValueInitial1)
    m.DrawLatex(textx+deltax,0.40,'final BH p-value = %s'%sigExclBumpHunterPValue1)

    #alternative
    m.DrawLatex(textx,0.32,'%s par. fit func.:'%nPar2)
    m.DrawLatex(textx+deltax,0.24,'#chi^{2}/ndf = %.3f      logL = %.1f'%(float(sigExclChi2ndf2), sigExclLogLValue2))
    m.DrawLatex(textx+deltax,0.16,'initial BH p-value = %s'%sigExclBumpHunterPValueInitial2)
    m.DrawLatex(textx+deltax,0.08,'final BH p-value = %s'%sigExclBumpHunterPValue2)

    pad3.Update()
    c1.Update()
    #c1.WaitPrimitive()

    #------------------------------------------
    #pad 4: QCD
    pad4.cd()
    pad4.Clear()
    pad4.SetLogx(0)
    pad4.SetLogy(0)

    qcdResidualHist1.GetXaxis().SetTitle("m_{jj} [GeV]")
    qcdResidualHist1.GetXaxis().SetTitleOffset(4)
    qcdResidualHist1.GetXaxis().SetTitleFont(43)
    qcdResidualHist1.GetXaxis().SetTitleSize(15)
    qcdResidualHist1.GetXaxis().SetLabelFont(43)
    qcdResidualHist1.GetXaxis().SetLabelSize(15)
    qcdResidualHist1.GetXaxis().SetRangeUser(1000., funcRatio1.GetXaxis().GetXmax())

    qcdResidualHist1.GetYaxis().SetTitle("QCD fit res.")
    qcdResidualHist1.GetYaxis().SetTitleFont(43)
    qcdResidualHist1.GetYaxis().SetTitleSize(15)
    qcdResidualHist1.GetYaxis().SetLabelFont(43)
    qcdResidualHist1.GetYaxis().SetLabelSize(15)
    qcdResidualHist1.SetLineColor(ROOT.kRed)
    qcdResidualHist1.SetFillColor(ROOT.kRed)
    qcdResidualHist1.SetFillStyle(3004)    
    qcdResidualHist1.Draw()

    qcdResidualHist2.SetLineColor(ROOT.kOrange)
    qcdResidualHist2.SetFillColor(ROOT.kOrange)
    qcdResidualHist2.SetFillStyle(3005)    
    qcdResidualHist2.Draw('same')
    
    #bump range
    ROOT.gPad.Update()
    lowy = ROOT.gPad.GetFrame().GetY1()
    highy = ROOT.gPad.GetFrame().GetY2()

    #default
    if qcdBumpFound1:
        intervalLine.SetLineColor(ROOT.kGreen+3)
    else:
        intervalLine.SetLineColor(ROOT.kBlue+2)
    intervalLine.SetLineWidth(2)
    intervalLine.DrawLine(qcdBumpLowEdge1,lowy,qcdBumpLowEdge1,highy)
    intervalLine.DrawLine(qcdBumpHighEdge1,lowy,qcdBumpHighEdge1,highy)

    #alternative
    if qcdBumpFound1:
        intervalLine.SetLineColor(ROOT.kGreen+1)
    else:
        intervalLine.SetLineColor(ROOT.kBlue)
    intervalLine.SetLineWidth(2)
    intervalLine.DrawLine(qcdBumpLowEdge2,lowy,qcdBumpLowEdge2,highy)
    intervalLine.DrawLine(qcdBumpHighEdge2,lowy,qcdBumpHighEdge2,highy)

    #Wilks' p-value
    qcdWilksPVal = ROOT.TMath.Prob( abs(float(qcdLogLValue1) - float(qcdLogLValue2)), abs(int(qcdNdfValue1) - int(qcdNdfValue2)) )
    m.DrawLatex(textx,0.90,'Wilks\' p-value = %.5f'%qcdWilksPVal)
    
    #default
    m.DrawLatex(textx,0.82,'%s par. fit func.:'%nPar1)
    m.DrawLatex(textx+deltax,0.76,'#chi^{2}/ndf = %.3f      logL = %.1f'%(float(qcdChi2ndf1), qcdLogLValue1))
    m.DrawLatex(textx+deltax,0.70,'initial BH p-value = %s'%qcdBumpHunterPValueInitial1)
    m.DrawLatex(textx+deltax,0.64,'final BH p-value = %s'%qcdBumpHunterPValue1)

    #alternative
    m.DrawLatex(textx,0.58,'%s par. fit func.:'%nPar2)
    m.DrawLatex(textx+deltax,0.52,'#chi^{2}/ndf = %.3f      logL = %.1f'%(float(qcdChi2ndf2), qcdLogLValue2))
    m.DrawLatex(textx+deltax,0.46,'initial BH p-value = %s'%qcdBumpHunterPValueInitial2)
    m.DrawLatex(textx+deltax,0.40,'final BH p-value = %s'%qcdBumpHunterPValue2)

    pad4.Update()
    c1.Update()
    #c1.WaitPrimitive()
    
    #------------------------------------------
    #legend
    pad1.cd()
    lXmin = textx
    lYmax = 0.80

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(20)
    a.DrawLatex(lXmin,lYmax,'ATLAS')
    
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(20)
    p.DrawLatex(lXmin+0.09,lYmax,'internal')
    
    #notes
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(43)
    n.SetTextColor(1)
    n.SetTextSize(15)
    
    n.DrawLatex(lXmin,lYmax-0.10,'#sqrt{s} = 13 TeV')
    n.DrawLatex(lXmin,lYmax-0.20,'L_{int} = %.1f fb^{-1}'%float(luminosity))
    if model == 'QStar':
        n.DrawLatex(lXmin,lYmax-0.30,'m_{q*} = %.0f'%(float(mass))+' GeV')
    else:
        n.DrawLatex(lXmin,lYmax-0.30,'sig. mass = %.0f'%(float(mass))+' GeV')
        n.DrawLatex(lXmin,lYmax-0.40,'sig. width = %.0f%%'%(float(width)*100.))
    n.DrawLatex(lXmin,lYmax-0.50,'%s and %s par. fit func.'%(nPar1, nPar2))

    #legend
    l = ROOT.TLegend(lXmin, lYmax-0.52, lXmin+0.20, lYmax-0.78)
    l.SetFillStyle(0)
    l.SetFillColor(0)
    l.SetLineColor(0)
    l.SetTextFont(43)
    l.SetBorderSize(0)
    l.SetTextSize(12)
    l.AddEntry(funcRatio1,'%s par. fit, excl. / QCD'%nPar1,'l')
    l.AddEntry(funcRatio2,'%s par. fit, excl. / QCD'%nPar2,'l')
    l.AddEntry(funcRatio,'%s par. fit / %s par. fit'%(nPar1, nPar2),'l')
    l.Draw()
    
    #------------------------------------------
    c1.cd()
    c1.Update()
    #c1.WaitPrimitive()
    c1.SaveAs(stag+'.'+tag+'.pdf')


#******************************************
if __name__ == "__main__":

    '''
    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 8:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \nplot search phase results including the bump identified by the BumpHunter \
            \nHOW TO: python -u plotSearchResults.py qcdFileName sigFileName model mass luminosity[pb^-1] par5 jobSeed'
            %(len(sys.argv), 8))
    
    #------------------------------------------
    #input parameters
    qcdSearchFileName = sys.argv[1].strip()
    sigSearchFileName = sys.argv[2].strip()
    model = sys.argv[3].strip()
    mass = sys.argv[4].strip()
    width = sys.argv[5].strip()
    lumi = sys.argv[6].strip()
    nPar = sys.argv[7].strip()

    plotFuncRatio(qcdSearchFileName, sigSearchFileName, model, mass, width, lumi, nPar)
    '''
    '''
    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 12:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \nplot search phase results including the bump identified by the BumpHunter \
            \nHOW TO: python -u plotSearchResults.py qcdFileName sigFileName1 sigFileName2 model mass width sigNorm1 sigNorm2 luminosity[pb^-1] nPar tag'
            %(len(sys.argv), 12))
    
    #------------------------------------------
    #input parameters
    qcdSearchFileName = sys.argv[1].strip()
    sigSearchFileName1 = sys.argv[2].strip()
    sigSearchFileName2 = sys.argv[3].strip()
    model = sys.argv[4].strip()
    mass = sys.argv[5].strip()
    width = sys.argv[6].strip()
    sigNorm1 = sys.argv[7].strip()
    sigNorm2 = sys.argv[8].strip()
    lumi = sys.argv[9].strip()
    nPar = sys.argv[10].strip()
    tag = sys.argv[11].strip()

    plotDoubleFuncRatio(qcdSearchFileName, sigSearchFileName1, sigSearchFileName2, model, mass, width, sigNorm1, sigNorm2, lumi, nPar, tag)
    '''
    
    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 16:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \nplot search phase results including the bump identified by the BumpHunter \
            \nHOW TO: python -u plotFuncRatio.py qcdSearchFileName1 qcdSearchFileName2 sigInclSearchFileName1 sigInclSearchFileName2 sigExclSearchFileName1 sigExclSearchFileName2 model mass width normHigh normLow lumi nPar1 nPar2 tag'
            %(len(sys.argv), 16))
    
    #------------------------------------------
    #input parameters
    qcdSearchFileName1 = sys.argv[1].strip()
    qcdSearchFileName2 = sys.argv[2].strip()
    sigInclSearchFileName1 = sys.argv[3].strip()
    sigInclSearchFileName2 = sys.argv[4].strip()
    sigExclSearchFileName1 = sys.argv[5].strip()
    sigExclSearchFileName2 = sys.argv[6].strip()
    model = sys.argv[7].strip()
    mass = sys.argv[8].strip()
    width = sys.argv[9].strip()
    normHigh = sys.argv[10].strip()
    normLow = sys.argv[11].strip()
    lumi = sys.argv[12].strip()
    nPar1 = sys.argv[13].strip()
    nPar2 = sys.argv[14].strip()
    tag = sys.argv[15].strip()

    plotDoubleFuncRatioWilks(qcdSearchFileName1, qcdSearchFileName2, sigInclSearchFileName1, sigInclSearchFileName2, sigExclSearchFileName1, sigExclSearchFileName2, model, mass, width, normHigh, normLow, lumi, nPar1, nPar2, tag)
    
    #------------------------------------------
    print '\ndone'
