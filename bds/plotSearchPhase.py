#!/bin/python

#******************************************
#plot SearchPhase results
#EXAMPLE python plotSearchPhase.py path/to/file/searchPhaseResults.root

#******************************************
#import stuff
import ROOT
import re, sys, os, math, shutil, fileinput
import raffaello

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def plotGaussianSearchPhase(inputFileName, norm):

    print '\n******************************************\nplotting search phase results with injected gaussian'
    print '\nparameters:'
    print '  input file name:      %s'%inputFileName
    print '  signal normalization: %s'%norm

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
    
    #------------------------------------------
    #get info from input file name
    baseFileName = inputFileName.split('/')[-1]
    sBaseFileName = baseFileName.split('.')

    sigMass = 999.
    sigWidth = 999.
    luminosity = 999.
    fitFuncPar = 999
    
    for ii,s in enumerate(sBaseFileName):
        #print '%s %s'%(ii, s)
        if s == 'GeV':
            sigMass = float(sBaseFileName[ii-1])
        if s == 'width':
            sigWidth = float(sBaseFileName[ii-1].replace('p','.'))
        if s == 'ifb':
            luminosity = float(sBaseFileName[ii-1].replace('p','.'))
        if s == 'par':
            fitFuncPar = int(sBaseFileName[ii-1])

    print '\nsignal mass = %.1f'%sigMass
    print 'signal width = %.2f'%sigWidth
    print 'luminosity = %.1f/fb'%luminosity
    print 'fit func. par. = %i'%fitFuncPar
    
    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%inputFileName)
    inputFile = ROOT.TFile(inputFileName)

    #------------------------------------------
    #set weights
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get SearchPhase results
    #histograms
    basicData                  = inputFile.Get("basicData")
    basicBackground            = inputFile.Get("basicBkgFrom4ParamFit")
    residualHist               = inputFile.Get("residualHist")
    #bumpHunterStatHistNullCase = inputFile.Get("bumpHunterStatHistNullCase")
    #print 'basicBackground entries = %s'%basicBackground.GetEntries()

    #vector
    bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToData")
    #bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToDataInitial")#TEST #NO
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHigh')
    #bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHighInitial')#TEST #NO
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    bumpFoundVector = inputFile.Get("bumpFound")
    bumpFound = bumpFoundVector[0]
    
    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "bump window excluded? %s"%bumpFound
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas

    #------------------------------------------
    #fit chi2/ndf
    chi2OfFitToData = inputFile.Get('chi2OfFitToData')
    chi2OfFitToDataValue = chi2OfFitToData[0]
    ndf = inputFile.Get('NDF')
    ndfValue = ndf[0]
    chi2ndf = float(chi2OfFitToDataValue/ndfValue)
    print '\nfit chi2/ndf = %.3f'%chi2ndf

    #------------------------------------------
    #define necessary quantities
    sBHpval = ('%.3f'%bumpHunterPValue).replace('.','p')
    #name = localdir+'/figures/+'.'.join( sBaseFileName[0:len(sBaseFileName)-1])#DEFAULT
    name = localdir+'/figures/searchPhase.'+'.'.join( sBaseFileName[1:len(sBaseFileName)-1])#TEST
    notes = []
    notes.append('sig. mass = %.0f GeV'%float(sigMass))
    notes.append('sig. width = %.0f%%'%(float(sigWidth)*100.))
    notes.append('sig. norm = %s'%norm)
    
    #------------------------------------------
    #plot
    raffaello.drawSearchPhase(basicData,
                                basicBackground,
                                residualHist,
                                'm_{jj} [GeV]',
                                'events',
                                'significance',
                                name,
                                luminosity,
                                13,
                                2000.,
                                14000.,
                                chi2ndf,
                                bumpHunterPValue,
                                fitFuncPar,
                                -1,
                                -1,
                                bumpLowEdge,
                                bumpHighEdge,
                                bumpFound,
                                False,
                                notes)

#******************************************
def plotQStarSearchPhase(inputFileName, norm):

    print '\n******************************************\nplotting search phase results with injected q*'
    print '\nparameters:'
    print '  input file name:      %s'%inputFileName
    print '  signal normalization: %s'%norm

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
    
    #------------------------------------------
    #get info from input file name
    baseFileName = inputFileName.split('/')[-1]
    sBaseFileName = baseFileName.split('.')

    sigMass = 999.
    luminosity = 999.
    fitFuncPar = 999
    
    for ii,s in enumerate(sBaseFileName):
        #print '%s %s'%(ii, s)
        if s == 'GeV':
            sigMass = float(sBaseFileName[ii-1])
        if s == 'ifb':
            luminosity = float(sBaseFileName[ii-1].replace('p','.'))
        if s == 'par':
            fitFuncPar = int(sBaseFileName[ii-1])

    print '\nq* mass = %.1f'%sigMass
    print 'luminosity = %.1f/fb'%luminosity
    print 'fit func. par. = %i'%fitFuncPar
    
    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%inputFileName)
    inputFile = ROOT.TFile(inputFileName)

    #------------------------------------------
    #set weights
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get SearchPhase results
    #histograms
    basicData                  = inputFile.Get("basicData")
    basicBackground            = inputFile.Get("basicBkgFrom4ParamFit")
    residualHist               = inputFile.Get("residualHist")
    #bumpHunterStatHistNullCase = inputFile.Get("bumpHunterStatHistNullCase")
    #print 'basicBackground entries = %s'%basicBackground.GetEntries()

    #vector
    bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToData")
    #bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToDataInitial")#TEST #NO
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHigh')
    #bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHighInitial')#TEST #NO
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    bumpFoundVector = inputFile.Get("bumpFound")
    bumpFound = bumpFoundVector[0]
    
    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "bump window excluded? %s"%bumpFound
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas

    #------------------------------------------
    #fit chi2/ndf
    chi2OfFitToData = inputFile.Get('chi2OfFitToData')
    chi2OfFitToDataValue = chi2OfFitToData[0]
    ndf = inputFile.Get('NDF')
    ndfValue = ndf[0]
    chi2ndf = float(chi2OfFitToDataValue/ndfValue)
    print '\nfit chi2/ndf = %.3f'%chi2ndf

    #------------------------------------------
    #define necessary quantities
    sBHpval = ('%.3f'%bumpHunterPValue).replace('.','p')
    #name = localdir+'/figures/+'.'.join( sBaseFileName[0:len(sBaseFileName)-1])#DEFAULT
    name = localdir+'/figures/searchPhase.'+'.'.join( sBaseFileName[1:len(sBaseFileName)-1])#TEST
    notes = []
    notes.append('m_{q*} = %.0f GeV'%float(sigMass))
    notes.append('#mu_{q*} = %s'%norm)
    
    #------------------------------------------
    #plot
    raffaello.drawSearchPhase(basicData,
                                basicBackground,
                                residualHist,
                                'm_{jj} [GeV]',
                                'events',
                                'significance',
                                name,
                                luminosity,
                                13,
                                2000.,
                                14000.,
                                chi2ndf,
                                bumpHunterPValue,
                                fitFuncPar,
                                -1,
                                -1,
                                bumpLowEdge,
                                bumpHighEdge,
                                bumpFound,
                                False,
                                notes)
    
#******************************************
def plotCISearchPhase(inputFileName, seed):

    print '\n******************************************\nplotting search phase results on contact interaction'
    print '\nparameters:'
    print '  input file name: %s'%inputFileName
    print '  seed:            %s'%seed

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
    
    #------------------------------------------
    #get info from input file name
    baseFileName = inputFileName.split('/')[-1]
    sBaseFileName = baseFileName.split('.')

    luminosity = 999.
    fitFuncPar = 999
    
    for ii,s in enumerate(sBaseFileName):
        #print '%s %s'%(ii, s)
        if s == 'ifb':
            luminosity = float(sBaseFileName[ii-1].replace('p','.'))
        if s == 'par':
            fitFuncPar = int(sBaseFileName[ii-1])

    print 'luminosity = %.1f/fb'%luminosity
    print 'fit func. par. = %i'%fitFuncPar
    
    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%inputFileName)
    inputFile = ROOT.TFile(inputFileName)

    #------------------------------------------
    #set weights
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get SearchPhase results
    #histograms
    basicData                  = inputFile.Get("basicData")
    basicBackground            = inputFile.Get("basicBkgFrom4ParamFit")
    residualHist               = inputFile.Get("residualHist")
    #bumpHunterStatHistNullCase = inputFile.Get("bumpHunterStatHistNullCase")
    #print 'basicBackground entries = %s'%basicBackground.GetEntries()

    #vector
    bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToData")
    #bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToDataInitial")#TEST #NO
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHigh')
    #bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHighInitial')#TEST #NO
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    bumpFoundVector = inputFile.Get("bumpFound")
    bumpFound = bumpFoundVector[0]
    
    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "bump window excluded? %s"%bumpFound
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas

    #------------------------------------------
    #fit chi2/ndf
    chi2OfFitToData = inputFile.Get('chi2OfFitToData')
    chi2OfFitToDataValue = chi2OfFitToData[0]
    ndf = inputFile.Get('NDF')
    ndfValue = ndf[0]
    chi2ndf = float(chi2OfFitToDataValue/ndfValue)
    print '\nfit chi2/ndf = %.3f'%chi2ndf

    #------------------------------------------
    #define necessary quantities
    sBHpval = ('%.3f'%bumpHunterPValue).replace('.','p')
    #name = localdir+'/figures/+'.'.join( sBaseFileName[0:len(sBaseFileName)-1])#DEFAULT
    name = localdir+'/figures/searchPhase.'+'.'.join( sBaseFileName[1:len(sBaseFileName)-1])#TEST
    notes = []
    notes.append('seed = %s'%seed)
    
    #------------------------------------------
    #plot
    raffaello.drawSearchPhase(basicData,
                                basicBackground,
                                residualHist,
                                'm_{jj} [GeV]',
                                'events',
                                'significance',
                                name,
                                luminosity,
                                13,
                                2000.,
                                14000.,
                                chi2ndf,
                                bumpHunterPValue,
                                fitFuncPar,
                                -1,
                                -1,
                                bumpLowEdge,
                                bumpHighEdge,
                                bumpFound,
                                False,
                                notes)

#******************************************
def plotJESSearchPhase(inputFileName, variation):

    print '\n******************************************\nplotting search phase results on JES variation'
    print '\nparameters:'
    print '  input file name: %s'%inputFileName
    print '  variation:       %s'%variation

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
    
    #------------------------------------------
    #get info from input file name
    baseFileName = inputFileName.split('/')[-1]
    sBaseFileName = baseFileName.split('.')

    luminosity = 999.
    fitFuncPar = 999
    
    for ii,s in enumerate(sBaseFileName):
        #print '%s %s'%(ii, s)
        if s == 'ifb':
            luminosity = float(sBaseFileName[ii-1].replace('p','.'))
        if s == 'par':
            fitFuncPar = int(sBaseFileName[ii-1])

    print 'luminosity = %.1f/fb'%luminosity
    print 'fit func. par. = %i'%fitFuncPar
    
    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%inputFileName)
    inputFile = ROOT.TFile(inputFileName)

    #------------------------------------------
    #set weights
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get SearchPhase results
    #histograms
    basicData                  = inputFile.Get("basicData")
    basicBackground            = inputFile.Get("basicBkgFrom4ParamFit")
    residualHist               = inputFile.Get("residualHist")
    #bumpHunterStatHistNullCase = inputFile.Get("bumpHunterStatHistNullCase")
    #print 'basicBackground entries = %s'%basicBackground.GetEntries()

    #vector
    bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToData")
    #bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToDataInitial")#TEST #NO
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHigh')
    #bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHighInitial')#TEST #NO
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    bumpFoundVector = inputFile.Get("bumpFound")
    bumpFound = bumpFoundVector[0]
    
    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "bump window excluded? %s"%bumpFound
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas

    #------------------------------------------
    #fit chi2/ndf
    chi2OfFitToData = inputFile.Get('chi2OfFitToData')
    chi2OfFitToDataValue = chi2OfFitToData[0]
    ndf = inputFile.Get('NDF')
    ndfValue = ndf[0]
    chi2ndf = float(chi2OfFitToDataValue/ndfValue)
    print '\nfit chi2/ndf = %.3f'%chi2ndf

    #------------------------------------------
    #define necessary quantities
    sBHpval = ('%.3f'%bumpHunterPValue).replace('.','p')
    #name = localdir+'/figures/+'.'.join( sBaseFileName[0:len(sBaseFileName)-1])#DEFAULT
    name = localdir+'/figures/searchPhase.'+'.'.join( sBaseFileName[1:len(sBaseFileName)-1])#TEST
    notes = []
    notes.append('JES = %s'%variation)
    
    #------------------------------------------
    #plot
    raffaello.drawSearchPhase(basicData,
                                basicBackground,
                                residualHist,
                                'm_{jj} [GeV]',
                                'events',
                                'significance',
                                name,
                                luminosity,
                                13,
                                2000.,
                                14000.,
                                chi2ndf,
                                bumpHunterPValue,
                                fitFuncPar,
                                -1,
                                -1,
                                bumpLowEdge,
                                bumpHighEdge,
                                bumpFound,
                                False,
                                notes)

#******************************************
def plotSearchPhase(inputFileName, notes = []):

    print '\n******************************************\nplotting search phase results'
    print '\nparameters:'
    print '  input file name: %s'%inputFileName
    print '  notes:'
    for note in notes:
        print '\t%s'%note

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
    
    #------------------------------------------
    #get info from input file name
    baseFileName = inputFileName.split('/')[-1]
    sBaseFileName = baseFileName.split('.')

    luminosity = 999.
    fitFuncPar = 999
    
    for ii,s in enumerate(sBaseFileName):
        #print '%s %s'%(ii, s)
        if s == 'ifb':
            luminosity = float(sBaseFileName[ii-1].replace('p','.'))
        if s == 'par':
            fitFuncPar = int(sBaseFileName[ii-1])

    print 'luminosity = %.1f/fb'%luminosity
    print 'fit func. par. = %i'%fitFuncPar
    
    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%inputFileName)
    inputFile = ROOT.TFile(inputFileName)

    #------------------------------------------
    #set weights
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    ROOT.TH2.SetDefaultSumw2()
    ROOT.TH2.StatOverflows()

    #------------------------------------------
    #get SearchPhase results
    #histograms
    basicData                  = inputFile.Get("basicData")
    basicBackground            = inputFile.Get("basicBkgFrom4ParamFit")
    residualHist               = inputFile.Get("residualHist")
    #bumpHunterStatHistNullCase = inputFile.Get("bumpHunterStatHistNullCase")
    #print 'basicBackground entries = %s'%basicBackground.GetEntries()

    #vector
    bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToData")
    #bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToDataInitial")#TEST #NO
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHigh')
    #bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHighInitial')#TEST #NO
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    bumpFoundVector = inputFile.Get("bumpFound")
    bumpFound = bumpFoundVector[0]

    #fit chi2/ndf
    chi2OfFitToData = inputFile.Get('chi2OfFitToData')
    chi2OfFitToDataValue = chi2OfFitToData[0]
    ndf = inputFile.Get('NDF')
    ndfValue = ndf[0]
    chi2ndf = float(chi2OfFitToDataValue/ndfValue)

    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "bump window excluded? %s"%bumpFound
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas

    print '\nfit chi2/ndf = %.3f'%chi2ndf

    #------------------------------------------
    #define necessary quantities
    sBHpval = ('%.3f'%bumpHunterPValue).replace('.','p')
    #name = localdir+'/figures/+'.'.join( sBaseFileName[0:len(sBaseFileName)-1])#DEFAULT
    name = localdir+'/figures/searchPhase.'+'.'.join( sBaseFileName[1:len(sBaseFileName)-1])#TEST
    
    #------------------------------------------
    #plot
    raffaello.drawSearchPhase(basicData,
                                basicBackground,
                                residualHist,
                                'm_{jj} [GeV]',
                                'events',
                                'significance',
                                name,
                                luminosity,
                                13,
                                2000.,
                                14000.,
                                chi2ndf,
                                bumpHunterPValue,
                                fitFuncPar,
                                -1,
                                -1,
                                bumpLowEdge,
                                bumpHighEdge,
                                bumpFound,
                                False,
                                notes)
    
#******************************************
if __name__ == "__main__":

    #check input parameters
    if len(sys.argv) != 3:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \nplot search phase results including the bump identified by the BumpHunter \
            \nHOW TO: python -u plotSearchResults.py inputFileName notes'
            %(len(sys.argv), 3))

    #get input parameters
    inputFileName = sys.argv[1].strip()
    notes = sys.argv[2].strip()

    #plot
    plotSearchPhase(inputFileName, notes)
    print '\ndone'
