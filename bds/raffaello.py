#!/bin/python

#******************************************
#plot serach phase results from Statistical Analysis Bayesian tool
#this is a simplified version of the morisot module

#EXAMPLE python raffaello.py path/to/file/searchPhaseResults.root

#******************************************
#import stuff
import ROOT, sys, os

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def getAxisRangeFromHist(hist):
    #NOTE taken from morisot modules
    #axis range should be decided by data hist
    firstBin =0
    while (hist.GetBinContent(firstBin+1)==0 and firstBin < hist.GetNbinsX()) :
        firstBin+=1
    lastBin = hist.GetNbinsX()+1
    while (hist.GetBinContent(lastBin-1)==0 and lastBin > 0) :
        lastBin-=1
    if (firstBin > lastBin) :
        firstBin=1
        lastBin = hist.GetNbinsX()
    return firstBin,lastBin

#******************************************
def drawSearchPhase(dataHist,
                    fitHist,
                    significanceHist,
                    xLab,
                    yLab,
                    sigLab,
                    name,
                    luminosity,
                    CME,
                    fitMin,
                    fitMax,
                    chi2ndf,
                    BHpval,
                    fitFuncPar,
                    userFirstBin=-1,
                    userLastBin=-1,
                    bumpLow=0,
                    bumpHigh=0,
                    bumpFound=False,
                    logx=False,
                    notes=[]):
    
    #canvas
    c1 = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    c1.SetLogy(1)
    c1.SetLogx(logx)

    #pads
    outpad = ROOT.TPad("extpad","extpad", 0., 0.,   1., 1.)
    pad1   = ROOT.TPad("pad1",  "pad1",   0., 0.33, 1., 1.)
    pad2   = ROOT.TPad("pad2",  "pad2",   0., 0.,   1., 0.33)

    #setup drawing options
    outpad.SetFillStyle(4000)#transparent
    pad1.SetBottomMargin(0.00001)
    pad1.SetBorderMode(0)
    pad1.SetLogy(1)
    pad1.SetLogx(logx)
    pad2.SetTopMargin(0.00001)
    pad2.SetBottomMargin(0.3)
    pad2.SetBorderMode(0)
    pad2.SetLogx(logx)
    pad1.Draw()
    pad2.Draw()
    outpad.Draw()

    #------------------------------------------
    #draw data and fit histograms
    pad1.cd()

    #use bin range within which bkg plot has entries, 
    #plus one empty on either side if available
    #print 'userFirstBin, userLastBin = %s, %s'%(userFirstBin,userLastBin)
    firstBin,lastBin = getAxisRangeFromHist(dataHist)
    #print 'firstBin, lastBin = %s, %s'%(firstBin,lastBin)
    if (userFirstBin>0) :
        firstBin=userFirstBin
        #print 'first bin = %s'%firstBin
    if (userLastBin>0 and userLastBin>=firstBin) :
        lastBin = userLastBin
        #print 'last bin  = %s'%LastBin

    #draw fit histogram
    fitHist.SetLineColor(ROOT.kRed)
    fitHist.SetFillStyle(0)
    fitHist.SetLineWidth(2)
    
    fitHist.GetYaxis().SetTitle(yLab)
    fitHist.GetYaxis().SetTitleFont(43)
    fitHist.GetYaxis().SetTitleSize(20)
    #fitHist.GetYaxis().SetTitleOffset(1.0)
    fitHist.GetYaxis().SetLabelFont(43)
    fitHist.GetYaxis().SetLabelSize(20)
    #fitHist.GetYaxis().SetRangeUser(0.5,fitHist.GetMaximum()*10.)

    #fitHist.GetXaxis().SetTitle(xLab)
    #fitHist.GetXaxis().SetTitleFont(43)
    #fitHist.GetXaxis().SetTitleSize(20)
    ##fitHist.GetXaxis().SetTitleOffset(1.0)
    #fitHist.GetXaxis().SetLabelFont(43)
    #fitHist.GetXaxis().SetLabelSize(20)
    fitHist.GetXaxis().SetRange(firstBin,lastBin)
    
    fitHist.Draw('hist ][')
    c1.Update()
        
    #draw data histogram
    dataHist.SetMarkerStyle(20)
    dataHist.SetMarkerColor(ROOT.kBlack)
    dataHist.SetLineColor(ROOT.kBlack)
    dataHist.GetXaxis().SetRange(firstBin,lastBin)
    
    dataHist.Draw("E same")
    c1.Update()
        
    #set y axis range
    if dataHist.GetBinContent(lastBin) <= fitHist.GetBinContent(lastBin):
        ymin = dataHist.GetBinContent(lastBin)
        if ymin == 0.:
            ymin = 0.5
        fitHist.SetMinimum(ymin*0.5)
        c1.Update()

    #------------------------------------------
    #draw significance histogram
    pad2.cd()
    
    significanceHist.GetYaxis().SetTitle(sigLab)
    significanceHist.GetYaxis().SetTitleFont(43)
    significanceHist.GetYaxis().SetTitleSize(20)
    #significanceHist.GetYaxis().SetTitleOffset(1.0)
    significanceHist.GetYaxis().SetLabelFont(43)
    significanceHist.GetYaxis().SetLabelSize(20)
    #significanceHist.GetYaxis().SetRangeUser(-5.,fitHist.GetMaximum*1.1)

    significanceHist.GetXaxis().SetTitle(xLab)
    significanceHist.GetXaxis().SetTitleFont(43)
    significanceHist.GetXaxis().SetTitleSize(20)
    significanceHist.GetXaxis().SetTitleOffset(4.0)
    significanceHist.GetXaxis().SetLabelFont(43)
    significanceHist.GetXaxis().SetLabelSize(20)
    significanceHist.GetXaxis().SetRange(firstBin,lastBin)

    significanceHist.SetLineColor(ROOT.kBlack)
    significanceHist.SetLineWidth(2)
    significanceHist.SetFillColor(ROOT.kRed)
    significanceHist.SetFillStyle(1001)

    significanceHist.Draw("HIST")
    c1.Update()
    
    #------------------------------------------
    #labels
    pad1.cd()
    
    #position
    lXmin=0.64
    lXmax=0.94
    lYmin=0.45
    lYmax=0.85

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
    a.DrawLatex(lXmin,lYmax,'ATLAS')
        
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    p.DrawLatex(lXmin+0.13,lYmax,'internal')

    #notes
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(43)
    n.SetTextColor(1)
    n.SetTextSize(15)
    n.DrawLatex(lXmin,lYmax-0.04,'#sqrt{s} = %s TeV'%CME)
    if float(luminosity)>0.:
        n.DrawLatex(lXmin,lYmax-0.10,'#intLdt = %.1f fb^{-1}'%float(luminosity))
    n.DrawLatex(lXmin,lYmax-0.16,'#chi^{2}/ndf = %.3f'%float(chi2ndf))
    n.DrawLatex(lXmin,lYmax-0.20,'bump range = %.0f - %.0f GeV'%(float(bumpLow), float(bumpHigh)))
    n.DrawLatex(lXmin,lYmax-0.24,'BH p-value = %.4f'%float(BHpval))
    if bumpFound:
        n.DrawLatex(lXmin,lYmax-0.28,'bump range excluded')
    else:
        n.DrawLatex(lXmin,lYmax-0.28,'bump range not excluded')            
    n.DrawLatex(lXmin,lYmax-0.32,'%s par. fit func.'%fitFuncPar)
    for ii, note in enumerate(notes):
        n.DrawLatex(lXmin,lYmax-0.36-0.04*ii,note)
    c1.Update()

    #------------------------------------------
    #draw bump lines
    line = ROOT.TLine()
    if bumpFound:
        line.SetLineColor(ROOT.kGreen+1)
    else:
        line.SetLineColor(ROOT.kBlue)
    line.SetLineWidth(1)
    pad1.cd()
    line.DrawLine( bumpLow, dataHist.GetYaxis().GetXmin(),
                    bumpLow, dataHist.GetBinContent( dataHist.FindBin(bumpLow) ) )
    line.DrawLine( bumpHigh, dataHist.GetYaxis().GetXmin(),
                    bumpHigh, dataHist.GetBinContent( dataHist.FindBin(bumpHigh)-1 ) )
    pad2.cd()
    line.DrawLine( bumpLow, significanceHist.GetMinimum(),
                    bumpLow, 2*significanceHist.GetMaximum() )
    line.DrawLine( bumpHigh, significanceHist.GetMinimum(),
                    bumpHigh, 2*significanceHist.GetMaximum() )
    c1.Update()    

    #------------------------------------------
    #save
    c1.Update()    
    #c1.WaitPrimitive() #TEST
    #c1.SaveAs('figures/'+name+'.pdf')
    c1.SaveAs(name+'.pdf') #CHECK #NOTE uncomment this line

    #------------------------------------------

#******************************************
def drawTomography(tomographyHist,
                    xLab,
                    yLab,
                    sigLab,
                    name,
                    luminosity,
                    CME,
                    fitMin,
                    fitMax,
                    BHpval,
                    fitFuncPar,
                    userFirstBin=-1,
                    userLastBin=-1,
                    bumpLow=0,
                    bumpHigh=0,
                    bumpFound=False,
                    logx=False,
                    notes=[]):

    #------------------------------------------
    #canvas
    c = ROOT.TCanvas('c1', 'c1', 400, 50, 800, 600)
    c.SetLogx(0)
    c.SetLogy(1)

    #------------------------------------------
    #draw
    tomographyHist.SetLineColor(ROOT.kRed)
    tomographyHist.SetTitle("");
    tomographyHist.GetXaxis().SetTitle("m_{jj} [GeV]")
    tomographyHist.GetXaxis().SetNdivisions(805,ROOT.kTRUE)
    #tomographyHist.GetXaxis().SetMoreLogLabels()
    tomographyHist.GetYaxis().SetTitle("Poisson p-value")
    tomographyHist.GetYaxis().SetTitleOffset(1.2);
    #tomographyHist.GetYaxis().SetMoreLogLabels()
    tomographyHist.SetMarkerColor(ROOT.kRed)
    tomographyHist.SetMarkerSize(0.2)
    tomographyHist.Draw("AP");

    #------------------------------------------
    #labels
    c.cd()

    #position
    lXmin=0.55
    lXmax=0.85
    lYmin=0.45
    lYmax=0.85

    #ATLAS
    a = ROOT.TLatex()
    a.SetNDC()
    a.SetTextFont(73)
    a.SetTextColor(1)
    a.SetTextSize(30)
    a.DrawLatex(lXmin,lYmax,'ATLAS')
        
    #internal
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(43)
    p.SetTextColor(1)
    p.SetTextSize(30)
    p.DrawLatex(lXmin+0.13,lYmax,'internal')

    #notes
    n = ROOT.TLatex()
    n.SetNDC()
    n.SetTextFont(43)
    n.SetTextColor(1)
    n.SetTextSize(20)
    n.DrawLatex(lXmin,lYmax-0.04,'#sqrt{s} = %s TeV'%CME)
    if float(luminosity)>0.:
        n.DrawLatex(lXmin,lYmax-0.10,'#intLdt = %.1f fb^{-1}'%float(luminosity))
    n.DrawLatex(lXmin,lYmax-0.16,'bump range = %.0f - %.0f GeV'%(float(bumpLow), float(bumpHigh)))
    n.DrawLatex(lXmin,lYmax-0.20,'BH p-value = %.4f'%float(BHpval))
    if bumpFound:
        n.DrawLatex(lXmin,lYmax-0.24,'bump range excluded')
    else:
        n.DrawLatex(lXmin,lYmax-0.24,'bump range not excluded')            
    n.DrawLatex(lXmin,lYmax-0.28,'%s par. fit func.'%fitFuncPar)
    for ii, note in enumerate(notes):
        n.DrawLatex(lXmin,lYmax-0.32-0.04*ii,note)
    c.Update()

    #------------------------------------------    
    #save
    c.Update()    
    #c.WaitPrimitive()
    #c.SaveAs('figures/'+name+'.pdf')
    c.SaveAs(name+'.pdf')

    
#******************************************
#main
#******************************************
if __name__ == "__main__":

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 2:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters: %s/%s \
            \\nplot serach phase results from Statistical Analysis Bayesian tool \
            \nHOW TO: python -u plotSearchResults.py inputFileName'
            %(len(sys.argv), 2))
    
    #------------------------------------------
    #get input parameters
    inputFileName = sys.argv[1].strip()
    print '\n******************************************\nplotting search phase results'

    #------------------------------------------
    #open SearchPhase results file
    if not os.path.isfile(inputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find input file: %s'%inputFileName)
    inputFile = ROOT.TFile(inputFileName)

    #------------------------------------------
    #first things first
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #get SearchPhase results
    #histograms
    basicData                  = inputFile.Get("basicData")
    basicBackground            = inputFile.Get("basicBkgFrom4ParamFit")
    residualHist               = inputFile.Get("residualHist")
    #bumpHunterStatHistNullCase = inputFile.Get("bumpHunterStatHistNullCase")

    #vector
    bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToData")#DEFAULT
    #bumpHunterStatOfFitToData = inputFile.Get("bumpHunterStatOfFitToDataInitial")#TEST use initial BH results #NO
    bumpHunterStatValue = bumpHunterStatOfFitToData[0]
    bumpHunterPValue    = bumpHunterStatOfFitToData[1]
    bumpHunterPValueErr = bumpHunterStatOfFitToData[2]

    #vector
    bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHigh')#DEFAULT
    #bumpHunterPLowHigh = inputFile.Get('bumpHunterPLowHighInitial')#TEST use initial BH results #NO
    #bumpHunterStatValue = bumpHunterPLowHigh[0]
    bumpLowEdge         = bumpHunterPLowHigh[1]
    bumpHighEdge        = bumpHunterPLowHigh[2]

    bumpFoundVector = inputFile.Get("bumpFound")
    bumpFound = bumpFoundVector[0]
    
    #print
    print "\nbump range: %s GeV - %s GeV"%(bumpLowEdge,bumpHighEdge)
    print "bump window excluded? %s"%bumpFound
    print "BumpHunter stat = %s"%bumpHunterStatValue
    print "BumpHunter p-value = %s +/- %s"%(bumpHunterPValue, bumpHunterPValueErr)

    bumpHunterSigmas = ROOT.Math.normal_quantile(1.-bumpHunterPValue, 1.)
    print "BumpHunter sigmas = %s"%bumpHunterSigmas

    fittedParameters = inputFile.Get('fittedParameters')

    #------------------------------------------
    #fit chi2/ndf
    chi2OfFitToData = inputFile.Get('chi2OfFitToData')
    chi2OfFitToDataValue = chi2OfFitToData[0]
    ndf = inputFile.Get('NDF')
    ndfValue = ndf[0]
    chi2ndf = float(chi2OfFitToDataValue/ndfValue)
    print '\nfit chi2/ndf = %.3f'%chi2ndf

    #------------------------------------------
    #define necessary quantities
    luminosity = -999.
    fitFuncPar = len(fittedParameters)
    name = inputFileName.replace('.root', '')
    notes = []
        
    #------------------------------------------
    #plot
    drawSearchPhase(basicData,
                    basicBackground,
                    residualHist,
                    'm_{jj} [GeV]',
                    'events',
                    'significance',
                    name,
                    luminosity,
                    13,
                    2000.,
                    14000.,
                    chi2ndf,
                    bumpHunterPValue,
                    fitFuncPar,
                    -1,
                    -1,
                    bumpLowEdge,
                    bumpHighEdge,
                    bumpFound,
                    False,
                    notes)
