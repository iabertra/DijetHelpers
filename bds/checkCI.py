#!/usr/bin/env python

#******************************************
#import stuff
import ROOT, math, os, sys
import plotUtils
import findBarelyDetectableSignal
import plotSearchPhase

#******************************************
#set ATLAS style
ROOT.gROOT.LoadMacro('~/RootUtils/AtlasStyle.C')
ROOT.SetAtlasStyle()

#******************************************
def checkCI(CIInputFileName, QCDInputFileName, CIInputHistName, QCDInputHistName, lumi, seed, nPar, tag):
    
    #------------------------------------------
    #input parameters
    print '\n******************************************'
    print 'checking CI mjj distribution'
    print '  CI file:  %s'%CIInputFileName
    print '  CI hist:  %s'%CIInputHistName
    print '  QCD file: %s'%QCDInputFileName
    print '  QCD hist: %s'%QCDInputHistName
    print '  lumi:     %s'%lumi
    print '  seed:     %s'%seed
    print '  tag:     %s'%tag

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()

    #------------------------------------------
    #check input files
    if not os.path.isfile(CIInputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find CI input file')
    if not os.path.isfile(QCDInputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find QCD input file')

    #open input files
    fCI = ROOT.TFile.Open(CIInputFileName,'READ')
    if not fCI:
        raise SystemExit('\n***ERROR*** couldn\'t open CI input file')
    fQCD = ROOT.TFile.Open(QCDInputFileName,'READ')
    if not fQCD:
        raise SystemExit('\n***ERROR*** couldn\'t open QCD input file')

    #get input histograms
    qcdci = fCI.Get(CIInputHistName)
    if not qcdci:
        raise SystemExit('\n***ERROR*** couldn\'t find input CI histogram')
    qcdci.SetName('mjj.QCDCI')
    qcdci.SetTitle('mjj.QCDCI')
    
    qcd = fQCD.Get(QCDInputHistName)
    if not qcd:
        raise SystemExit('\n***ERROR*** couldn\'t find input QCD histogram')
    qcd.SetName('mjj.QCD')
    qcd.SetTitle('mjj.QCD')

    #------------------------------------------
    #plot CI
    inputFileName, inputHistName = plotCI(CIInputFileName, QCDInputFileName, CIInputHistName, QCDInputHistName, lumi, seed, tag)

    #------------------------------------------
    #search CI
    stag = str(lumi)+'.ifb.'+str(nPar)+'.par.'+str(seed)+'.seed'
    bumpHunterPValueInitial, bumpHunterPValueFinal, bumpFound = findBarelyDetectableSignal.runSearchPhase(int(nPar),
                                                                                                          lumi,
                                                                                                          stag,
                                                                                                          tag,
                                                                                                          inputFileName,
                                                                                                          inputHistName)
    searchPhaseOutputFile = 'results/searchPhase.'+stag+'.'+tag+'.root'
    plotSearchPhase.plotCISearchPhase(searchPhaseOutputFile, seed)

    return


#******************************************
def plotCI(CIInputFileName, QCDInputFileName, CIInputHistName, QCDInputHistName, lumi, seed, tag):
    
    #------------------------------------------
    #input parameters
    print '\n******************************************'
    print 'plotting CI mjj distribution'
    print '  CI file:  %s'%CIInputFileName
    print '  CI hist:  %s'%CIInputHistName
    print '  QCD file: %s'%QCDInputFileName
    print '  QCD hist: %s'%QCDInputHistName
    print '  lumi:     %s'%lumi
    print '  seed:     %s'%seed
    print '  tag:      %s'%tag

    #------------------------------------------
    #get directory of this script
    localdir = os.path.dirname(os.path.realpath(__file__))
	
    #------------------------------------------
    #set error sum and overflow
    ROOT.TH1.SetDefaultSumw2()
    ROOT.TH1.StatOverflows()
    
    #------------------------------------------
    #check input files
    if not os.path.isfile(CIInputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find CI input file')
    if not os.path.isfile(QCDInputFileName):
        raise SystemExit('\n***ERROR*** couldn\'t find QCD input file')

    #open input files
    fCI = ROOT.TFile.Open(CIInputFileName,'READ')
    if not fCI:
        raise SystemExit('\n***ERROR*** couldn\'t open CI input file')
    fQCD = ROOT.TFile.Open(QCDInputFileName,'READ')
    if not fQCD:
        raise SystemExit('\n***ERROR*** couldn\'t open QCD input file')

    #get input histograms
    qcdci = fCI.Get(CIInputHistName)
    if not qcdci:
        raise SystemExit('\n***ERROR*** couldn\'t find input CI histogram')
    qcdci.SetName('mjj.QCDCI')
    qcdci.SetTitle('mjj.QCDCI')
    
    qcd = fQCD.Get(QCDInputHistName)
    if not qcd:
        raise SystemExit('\n***ERROR*** couldn\'t find input QCD histogram')
    qcd.SetName('mjj.QCD')
    qcd.SetTitle('mjj.QCD')

    #------------------------------------------
    #check if output file exists already
    baseName = 'CI.mjj.'+str(lumi)+'.ifb.'+str(seed)+'.seed.'+str(tag)
    outputFileName = localdir+'/results/'+baseName+'.root'
    if os.path.isfile(outputFileName):
        print '\n***WARNING*** output file exist already'
        return outputFileName, 'mjj.QCDCI.DL'
    
    #------------------------------------------
    #scale to desired luminosity
    #NOTE input histograms are normalised to 1/fb
    scaleFactor = float(lumi) / 1.
    print 'luminosity scale factor = %s'%scaleFactor
    qcdci.Scale(scaleFactor)
    qcd.Scale(scaleFactor)

    #------------------------------------------
    #get CI mass distribution

    #OPTION 1: hand made rounded distribution
    """
    ci = ROOT.TH1D('mjj.ci', 'mjj.ci', qcd.GetXaxis().GetNbins(), qcd.GetXaxis().GetXbins().GetArray())
    for ii in xrange(qcd.GetNbinsX()):
        entries = qcdci.GetBinContent(ii) - qcd.GetBinContent(ii)
        if entries < 0.:
            entries = 0.
        #------------------------------------------
        #OPTION 1: scaled fill
        #ci.SetBinContent(ii, entries)
        #OPTION 2: data-like fill
        for jj in xrange( int( round( entries ) ) ):
            ci.Fill(ci.GetBinCenter(ii))
        #------------------------------------------
    """
    #OPTION 2: QCD+CI - QCD
    ci = qcdci - qcd
    ci.SetName('mjj.CI')
    ci.SetTitle('mjj.CI')
            
    #------------------------------------------
    #get gaussian signal (for plotting comparison)

    fgauss = ROOT.TF1('gauss','gaus(0)',qcd.GetXaxis().GetXmin(),qcd.GetXaxis().GetXmax())#gaussian
    norm = 100. * scaleFactor
    mass = 4000.
    width = 0.15
    fgauss.SetParameters(norm, float(mass), float(width)*float(mass))#NOTE parameters are: norm, mean, width
    #print '\nnorm  = %.2e'%fgauss.GetParameter(0)
    #print 'mean  = %s'%fgauss.GetParameter(1)
    #print 'sigma = %s'%fgauss.GetParameter(2)
    
    #fill histogram
    #NOTE normalization od the signal is different from the FillRandom case
    hgauss = ROOT.TH1D('mjj.gauss', 'mjj.gauss', qcd.GetXaxis().GetNbins(), qcd.GetXaxis().GetXbins().GetArray())
    hgauss.SetDefaultSumw2(ROOT.kTRUE)

    #random number generator
    rand3 = ROOT.TRandom3(1986) #1986

    for ii in xrange(hgauss.GetNbinsX()):

        #------------------------------------------
        #OPTION 1: smooth
        bincontent = int( round( fgauss.Eval( hgauss.GetBinCenter(ii) ) ) )
        for jj in xrange(bincontent):
            hgauss.Fill(hgauss.GetBinCenter(ii))

        #------------------------------------------
        #OPTION 2: Poisson noise
        """
        #pretend there are 100 times more eff. entries than necessary
        #gaussValue = int( round( fgauss.Eval( hgauss.GetBinCenter(ii) ) ) )
        #nee = int( round( 100. * gaussValue ) )
        gaussValue = fgauss.Eval( hgauss.GetBinCenter(ii) )
        nee = 100. * gaussValue

        #set seed
        #NOTE the seed for each bin must be always the same
        binSeed = int( round( hgauss.GetBinCenter(ii) ) )
        if binSeed < 0:
            binSeed = 0
        #print 'bin %s\t seed = %i'%(ii, binSeed)
        rand3.SetSeed(binSeed)

        #get data-like bin content by drawing entries
        #NOTE weights are poissonian by construction
        for jj in xrange( int( round( nee ) ) ):
            if rand3.Uniform() < (gaussValue / nee) :
                hgauss.Fill(hgauss.GetBinCenter(ii))
        """
        #------------------------------------------

    #------------------------------------------
    #print peak significance
    """
    peakBin = hgauss.FindBin( fgauss.GetParameter(1) )
    peakSignificance = hgauss.GetBinContent(peakBin) / math.sqrt(qcd.GetBinContent(peakBin))
    if debug and not batch:
        print '\ngauss hist entries = %.2e'%hgauss.GetEntries()
        print 'peak bin = %s'%peakBin
        print 'peak entries = %s'%hgauss.GetBinContent(peakBin)
        print 'peak error =   %s'%hgauss.GetBinError(peakBin)
    print '\npeak significance = %.2f'%peakSignificance
    """
    #------------------------------------------
    #remove any signal entry if there are no QCD entries (low mass region)
    """
    nbins = qcd.GetNbinsX()
    if total is not None:
        for ii in xrange(nbins):
            if qcd.GetBinContent(ii) == 0:
                total.SetBinContent(ii,0)
            else:
                break
    """

    #------------------------------------------
    #get data-like histograms
    qcdDL =   plotUtils.getDataLikeHist( plotUtils.getEffectiveEntriesHistogram(qcd,   'qcdEffEnt'),   qcd,   'mjj.QCD.DL', float(seed))
    qcdciDL = plotUtils.getDataLikeHist( plotUtils.getEffectiveEntriesHistogram(qcdci, 'qcdciEffEnt'), qcdci, 'mjj.QCDCI.DL', float(seed))
    ciDL =    plotUtils.getDataLikeHist( plotUtils.getEffectiveEntriesHistogram(ci,    'ciEffEnt'),    ci,    'mjj.CI.DL', float(seed))

    #------------------------------------------
    #save histograms
    outputFile = ROOT.TFile.Open(outputFileName, 'RECREATE')
    outputFile.cd()
    qcdci.Write()
    qcd.Write()
    ci.Write()
    qcdciDL.Write()
    qcdDL.Write()
    ciDL.Write()
    outputFile.Write()
    #outputFile.Close()
    
    #------------------------------------------
    #TEST plot scaled histograms instead of data-like
    #qcdDL = qcd
    #qcdciDL = qcdci
    #ciDL = ci

    #------------------------------------------
    #plot
    if True:
        c1 = ROOT.TCanvas('c1', 'c1', 1050, 50, 800, 600)
        c1.Clear()
        c1.SetLogy(1)
        c1.SetLogx(0)

        qcdciDL.SetMarkerColor(ROOT.kGreen+1)
        qcdciDL.SetMarkerStyle(26)
        qcdciDL.SetLineColor(ROOT.kGreen+1)

        qcdDL.SetMarkerColor(ROOT.kAzure+1)
        qcdDL.SetMarkerStyle(25)
        qcdDL.SetLineColor(ROOT.kAzure+1)

        ciDL.SetMarkerColor(ROOT.kOrange+1)
        ciDL.SetMarkerStyle(24)
        ciDL.SetLineColor(ROOT.kOrange+1)
        
        hgauss.SetMarkerColor(ROOT.kRed+1)
        hgauss.SetMarkerStyle(24)
        hgauss.SetLineColor(ROOT.kRed+1)
        
        hs = ROOT.THStack('hs','hs')
        hs.Add(qcdciDL)
        hs.Add(qcdDL)
        hs.Add(ciDL)
        hs.Add(hgauss)

        hs.Draw('nostack')
        hs.GetXaxis().SetTitle('m_{jj} [GeV]')
        hs.GetYaxis().SetTitle('events')

        #ATLAS
        aX = 0.65
        aY = 0.88
        a = ROOT.TLatex()
        a.SetNDC()
        a.SetTextFont(73)
        a.SetTextColor(1)
        a.SetTextSize(30)
        a.DrawLatex(aX,aY,'ATLAS')
		
        #internal
        p = ROOT.TLatex()
        p.SetNDC()
        p.SetTextFont(43)
        p.SetTextColor(1)
        p.SetTextSize(30)
        p.DrawLatex(aX+0.13,aY,'internal')
    
        #note
        n = ROOT.TLatex()
        n.SetNDC()
        n.SetTextFont(42)
        n.SetTextColor(1)
        n.SetTextSize(0.04)
        n.DrawLatex(aX,aY-0.08,'#intL dt = %s fb^{-1}'%lumi)
        n.DrawLatex(aX,aY-0.14,'#Lambda = %.0f TeV'%float(7.))
        n.DrawLatex(aX,aY-0.18,'constructive interf.')
        n.DrawLatex(aX,aY-0.22,'#eta_{LL} = -1')
        n.DrawLatex(aX,aY-0.26,'data-like')
        n.DrawLatex(aX,aY-0.30,'seed = %s'%seed)

        #legend
        lXmin = aX
        lXmax = aX+0.15
        lYmin = aY-0.32
        lYmax = aY-0.44
        l = ROOT.TLegend(lXmin, lYmin, lXmax, lYmax)
        l.SetFillStyle(0)
        l.SetFillColor(0)
        l.SetLineColor(0)
        l.SetTextFont(43)
        l.SetBorderSize(0)
        l.SetTextSize(15)
        l.AddEntry(qcdciDL,'QCD + CI','pl')
        l.AddEntry(qcdDL,'QCD',"pl")
        l.AddEntry(ciDL,'CI','pl')
        sgauss = 'gaussian (%.0f TeV, %.0f%%)'%(mass, float(width*100.))
        l.AddEntry(hgauss,sgauss,'pl')
        l.Draw()
        
        c1.Update()
        c1.WaitPrimitive()#TEST
        #c1.SaveAs(localdir+'/figures/'+baseName+'.pdf')
        del c1

    #------------------------------------------
    #delete
    del qcdci
    del qcd
    del ci
    
    #------------------------------------------
    return outputFileName, qcdciDL.GetName()

#******************************************
if __name__ == '__main__':

    #------------------------------------------
    #check input parameters
    if len(sys.argv) != 8:
        raise SystemExit(
            '\n***ERROR*** wrong input parameters (%s/%s) \
            \nHOW TO: time python -u checkCI.py CIInputFileName QCDInputFileName CIInputHistName QCDInputHistName lumi seed tag'\
            %(len(sys.argv),8))

    #------------------------------------------
    #get input parameters and run
    CIInputFileName = sys.argv[1].strip()
    QCDInputFileName = sys.argv[2].strip()
    CIInputHistName = sys.argv[3].strip()
    QCDInputHistName = sys.argv[4].strip()
    lumi = sys.argv[5].strip()
    seed = sys.argv[6].strip()
    tag = sys.argv[7].strip()

    output = plotCI(CIInputFileName, QCDInputFileName, CIInputHistName, QCDInputHistName, lumi, seed, tag)
    
    #------------------------------------------
    print '\ndone: %s'%list(output)
