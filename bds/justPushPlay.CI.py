#!/usr/bin/env python

#******************************************
#import stuff
import checkCI

#******************************************
if __name__ == '__main__':

    print '\n******************************************'
    print 'looping over fit function parameters and seeds'

    nParameters = ['3', '4', '5']
    seeds = [x for x in range(10)]
    luminosity = ['1', '10']
    tag = '7.TeV.lambda.const'
    
    #combine input parameters
    combinations = [(np, s, l) for l in luminosity for s in seeds for np in nParameters]
    
    #loop over combinations
    for combination in combinations:

        #print '  %s %s %s'%(combination[0], combination[1], combination[2])
        print '\n******************************************'
        print 'n parameters: %s'%combination[0]
        print 'seed:         %s'%combination[1]
        print 'luminosity:   %s'%combination[2]
        print 'tag:          %s'%tag

        checkCI.checkCI('data/CI_minus_Lambda7TeV.root',
                        'data/private_mc15_QCDforCI.root',
                        'mjj6',
                        'mjj6',
                        combination[2],
                        combination[1],
                        combination[0],
                        tag)
        print '\ndone'
        
    #------------------------------------------
    print '\n******************************************'
    print '\ndone looping over combinations'

    #checkCI.checkCI('data/CI_minus_Lambda7TeV.root', 'data/private_mc15_QCDforCI.root', 'mjj6', 'mjj6', '1', '1974', '3', 'test')
    #print '\ndone'
